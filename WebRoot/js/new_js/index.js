$(function () {
	  var $fullText = $('.admin-fullText');
      $('#admin-fullscreen').on('click', function() {
          $.AMUI.fullscreen.toggle();
      });

      $(document).on($.AMUI.fullscreen.raw.fullscreenchange, function() {
          $fullText.text($.AMUI.fullscreen.isFullscreen ? '退出全屏' : '开启全屏');
      });
      // ==========================
      // 侧边导航下拉列表
      // ==========================
  $('.tpl-left-nav-link-list').on('click', function() {
          $(this).siblings('.tpl-left-nav-sub-menu').slideToggle(80)
              .end()
              .find('.tpl-left-nav-more-ico').toggleClass('tpl-left-nav-more-ico-rotate');
      })
   // ==========================
   // 头部导航隐藏菜单
   // ==========================

   $('.tpl-header-nav-hover-ico').on('click', function() {
   $('.tpl-left-nav').toggle();
   $('.tpl-content-wrapper').toggleClass('tpl-content-wrapper-hover');
   });

 	/*openMenu("1","menuaaf1ad7f6ae14c88a2a35827f606a0d1","","实时总览","/backstage/structure/list",null);*/
 	
});

//打开菜单
function openMenu(type,id,parentId,menuName,resUrl,param){
	    if('1'!=type || "noset"==resUrl)return;
		else if('/'==resUrl.substring(0,1))TabControlAppend(id,menuName,resUrl);
		else TabControlAppend(id,menuName,resUrl);	
}
function removeTab(menuName){
	TabControlRemove(menuName);
}
function getMenu(layer,ref){
	$("#menu_li_id").empty();
	JY.Ajax.doRequest(null,'',{layer:layer,ref:ref},function(data){
		data.obj+="<script>";
		data.obj+=" $('.tpl-left-nav-link-list').on('click', function() {$(this).siblings('.tpl-left-nav-sub-menu').slideToggle(80).end().find('.tpl-left-nav-more-ico').toggleClass('tpl-left-nav-more-ico-rotate');});";
		data.obj+="</script>"
    	$("#menu_li_id").html(data.obj); 
		//菜单点击事件
		$('.tpl-left-nav-menu a:not(.nav-link)').on('click',function(){
			$('.tpl-left-nav-menu a:not(.nav-link)').removeClass("active");
			$(this).addClass('active');
		});
	});
	
}
function logout(){
	JY.Model.confirm("确认要退出吗？",function(){window.location.href=jypath+"/system_logout";});	
}

