//new_file17.jsp页面
//成果列表
$(document).ready(function() {
		name = $("#likeString").val();
		limit = 10;
		offset = 0;
		infoLikeList(name,limit,offset);
		$("#search-btn").click(function(){
			$(location).attr('href', 'newfile17?string=' + $("#likeString").val() + '');
		});
});

function infoLikeList(name,limit,offset){
	$.ajax({
		url :Server + "/fon/infolist",
		data : {
			"name":name,
			"limit":limit,
			"offset":offset,
		},
		type : "POST",
		dataType: "JSON",
		success : function(data) {
			var html = "";
			var pattern = '<li onclick="infodetail(#88)" >\
                <div class="vertical" style="padding-left: 20px;">\
                    <div class="vertical_img">\
                    	<img src="#0" />\
                    </div>\
                </div>\
                <div class="info">\
                    <div class="title">#1</div>\
                    <div class="row2">\
                        <div class="font">类型：#4<span></span>\
                        </div>\
                        <div class="font">交易方式：\
                            <span>完全转让</span>\
                        </div>\
                    </div>\
                    <div class="row3">\
                        <br >#2\
                    </div>\
                </div>\
                <div class="active">\
                    <div style="text-align: right;">\
                        <span style="color: rgb(255,138,0);font-size: 20px;">#6</span>\
                    </div>\
                    <div style="text-align: right;">\
                         成熟度：<span style="">#8</span>\
                    </div>\
                    <div style="text-align: right;">\
                         地址：<span>#3</span>\
                    </div>\
                </div>\
            </li>';
			for (var i = 0; i < data.rows.length; i++) {
				var item = "";
				var chengshudu ="";
				var leixing ="";
				if(data.rows[i].puser&&data.rows[i].img){
					if(data.rows[i].resultMaturity == 0)
						chengshudu = "正在研发";
					if(data.rows[i].resultMaturity == 1)
						chengshudu = "已有样品";
					if(data.rows[i].resultMaturity == 2)
						chengshudu = "通过小试";
					if(data.rows[i].resultMaturity == 3)
						chengshudu = "通过中试";
					if(data.rows[i].resultMaturity == 4)
						chengshudu = "可以量产";
					
					if(data.rows[i].resultType == 0)
						leixing = "专利";
					if(data.rows[i].resultType == 1)
						leixing = "非专利";
					
					item += pattern
						.replace("#88", data.rows[i].resultId)//id
						.replace("#0", data.rows[i].img.rimagePath)//图片
						.replace("#1", data.rows[i].resultName)//成果名
						.replace("#2", data.rows[i].puser.puUsername)//用户名
						.replace("#4", leixing)//类型
						.replace("#6", data.rows[i].rp.rpayRate)//价格
						.replace("#3", data.rows[i].puser.puAddress)//地址
						.replace("#8", chengshudu)//成熟度
					html += item;
				}
				if(data.rows[i].puser&&!data.rows[i].img){
					item += pattern
						.replace("#88", data.rows[i].resultId)//id
						.replace("#0", data.rows[i].rimagePath)//图片
						.replace("#1", data.rows[i].resultName)//成果名
						.replace("#2", data.rows[i].puser.puUsername)//用户名
						.replace("#4", leixing)//类型
						.replace("#6", data.rows[i].rp.rpayRate)//价格
						.replace("#3", data.rows[i].puser.puAddress)//地址
						.replace("#8", chengshudu)//成熟度
					html += item;
				}
				if(!data.rows[i].puser&&data.rows[i].img){
					item += pattern
						.replace("#88", data.rows[i].resultId)//id
						.replace("#0", data.rows[i].rimagePath)//图片
						.replace("#1", data.rows[i].resultName)//成果名
						.replace("#2", data.rows[i].puUsername)//用户名
						.replace("#4", leixing)//类型
						.replace("#6", data.rows[i].rp.rpayRate)//价格
						.replace("#3", data.rows[i].puAddress)//地址
						.replace("#8", chengshudu)//成熟度
					html += item;
				}
				if(!data.rows[i].puser&&!data.rows[i].img){
					item += pattern
						.replace("#88", data.rows[i].resultId)//id
						.replace("#0", data.rows[i].rimagePath)//图片
						.replace("#1", data.rows[i].resultName)//成果名
						.replace("#2", data.rows[i].puUsername)//用户名
						.replace("#4", leixing)//类型
						.replace("#6", data.rows[i].rpayRate)//价格
						.replace("#3", data.rows[i].puAddress)//地址
						.replace("#8", chengshudu)//成熟度
					html += item;
				}
			}
			$("#inneedlist").html(html);
		},
	});
}
//result_details18.jsp页面
//成果点击详情
function infodetail(id){
	$(location).attr('href', 'result_details18?id=' + id + '');
}