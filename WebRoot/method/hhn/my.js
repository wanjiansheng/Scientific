/*首页分类*/
$(document).ready(function() {
	$.ajax({
		url :Server + "/fon/infomaxclass",
		type : "post",
		dataType : "JSON",
		success : function(data) {
			var needhtml = "";
			for (var i = 0; i < data.length; i++) {
				needhtml += "<li onclick=\"location.href='"+indexUrl+"/newfile17?string=&tradeId="+data[i].tradeId+"'\" style='text-align:left; color:black' onmouseover='doajaxing(this," + data[i].bId + ")' ><span>" + data[i].tradeName + "</span>\
							</li>";
			}
			$("#sorts").html(needhtml);
		}
	});
});

/*首页左侧详情展示*/
$(document).ready(function() {
	Date.prototype.Format = function (fmt) { //author: meizz 
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
	        "h+": this.getHours(), //小时 
	        "m+": this.getMinutes(), //分 
	        "s+": this.getSeconds(), //秒 
	        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	        "S": this.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	    for (var k in o)
	    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	    return fmt;
	}

	var time1 = new Date().Format("yyyy-MM-dd");
	var time2 = new Date().Format("yyyy-MM-dd HH:mm:ss");

	function util(string){
		switch(string) {
			case "一月":
				return "01";
				break;
			case "二月":
				return "02";
				break;
			case "三月":
				return "03";
				break;
			case "四月":
				return "04";
				break;
			case "五月":
				return "05";
				break;
			case "六月":
				return "06";
				break;
			case "七月":
				return "07";
				break;
			case "八月":
				return "08";
				break;
			case "九月":
				return "09";
				break;
			case "十月":
				return "10";
				break;
			case "十一月":
				return "11";
				break;
			case "十二月":
				return "12";
				break;
			default:
				return "00";
		}
	}


	function datatosrt(str){
		var m = "";
		if(str.length == 10){
			m = str.substr(6,10)+"-"+util(str.substr(0,2))+"-"+"0"+str.substr(3,1);			
		}
		if(str.length == 11){
			if(str.substr(2,1)=="月"){
				/*此情况月份三位字符*/
				m = str.substr(7,11)+"-"+util(str.substr(0,3))+"-"+"0"+str.substr(4,1);
			}else{
				m = str.substr(7,11)+"-"+util(str.substr(0,2))+"-"+str.substr(3,2);
			}
		}
		if(str.length == 12){
			m = str.substr(8,12)+"-"+util(str.substr(0,3))+"-"+str.substr(4,2);
		}
		return m;
	}
	$.ajax({
		url :Server + "/fon/nowneedandresult",
		type : "post",
		dataType : "JSON",
		success : function(data) {
			var time1 = new Date().Format("yyyy-MM-dd");
			var time2 = new Date();
			
			$("#needid").val(data[0].needId);//需求id
			$("#need2").text(data[0].needExplain);//最新发布需求
			$("#need1").text(data[0].needusername);//最新发布用户名
			$("#need3").text(new Date(data[0].needCreateTime).Format("yyyy-MM-dd"));//最新发布需求时间 
			
			$("#infoid").val(data[1].resultId);//成果id
			$("#info2").text(data[1].resultDesc);//最新科研成果
			$("#info1").text(data[1].needusername);//最新科研成果用户名 */
			$("#info3").text(datatosrt(data[1].resultCreateTime));//最新科研成果时间  
		}
	});
});



/*首页数据统计*/
$(document).ready(function() {
	$.ajax({
		url :Server + "/fon/ofstatistical",
		type : "post",
		dataType : "JSON",
		success : function(data) {
			$("#numbermax0").text(data[0]==null?0:data[0]);//技术成果
			$("#numbermax1").text(data[1]==null?0:data[1]);//签约项目
			$("#numbermax2").text(data[2]==null?0:data[2]);//服务企业
			$("#numbermax3").text(data[3]==null?0:data[3]);//交易总额
		}
	});
});

/*首页9个推荐*/
$(document).ready(function() {
	$.ajax({
		url :Server + "/fon/homerecommendation",
		type : "post",
		dataType : "JSON",
		success : function(data) {
			var html1 = "";
			var html2 = "";
			var html3 = "";
			var li1= '<li class="first first_1" style="background-size:100% 100%;">\
				<a href="#">\
			<p class="title" style="border: none;color: black;">海水养殖</p>\
			<!--< style="width: 90px;height: 40px;border: 1px solid rgb(255,138,0);background-color: white; color:rgb(255,138,0) ;">更多&gt;</div>-->\
		    <p class="sun_more" >更多&gt;</p>\
		</a>\
	</li>';
			var li2= '<li class="first first_2" style="background-size:100% 100%;">\
				<a href="#">\
			<p class="title" style="border: none;color: black;">海底矿场利用</p>\
			<!--< style="width: 90px;height: 40px;border: 1px solid rgb(255,138,0);background-color: white; color:rgb(255,138,0) ;">更多&gt;</div>-->\
		    <p class="sun_more" >更多&gt;</p>\
		</a>\
	</li>';
			var li3= '<li class="first first_3" style="background-size:100% 100%;">\
				<a href="#">\
			<p class="title" style="border: none;color: black;">海洋能源利用</p>\
			<!--< style="width: 90px;height: 40px;border: 1px solid rgb(255,138,0);background-color: white; color:rgb(255,138,0) ;">更多&gt;</div>-->\
		    <p class="sun_more" >更多&gt;</p>\
		</a>\
	</li>';
			var pattern = '<li class="item" onclick="indexrecommend(#8)" >\
				<div>\
				<img src="#6"></div>\
				<a href="result_details18?id=#0" onclick="return apparatus_add_click(98)">\
					<h1 title="液相色谱质谱联用仪 （LC-MS）">#1</h1>\
					<p title="UPLC/Premier">成熟度：#2</p>\
					<p class="main_color">￥<b></b><span class="unit">#3/元</span></p>\
				</a>\
			</li>';
			for(var i = 0; i< 3; i++){
			var item1 = "";
			var order = data[i];
			var chengshudu ="";
			if(order.resultMaturity == 0)
				chengshudu = "正在研发";
			if(order.resultMaturity == 1)
				chengshudu = "已有样品";
			if(order.resultMaturity == 2)
				chengshudu = "通过小试";
			if(order.resultMaturity == 3)
				chengshudu = "通过中试";
			if(order.resultMaturity == 4)
				chengshudu = "可以量产";
			var p = 980-i*i*120;
			item1 += pattern
				.replace("#8", order.resultId)
				.replace("#6", order.img.rimagePath)
				.replace("#0", order.resultId)
				.replace("#1", order.resultName)
				.replace("#2", chengshudu)
				.replace("#3",p)
			html1 += item1;
			}
			
			for(var i = 3; i< 6; i++){
				var item2 = "";
				var order = data[i];
				var chengshudu ="";
				if(order.resultMaturity == 0)
					chengshudu = "正在研发";
				if(order.resultMaturity == 1)
					chengshudu = "已有样品";
				if(order.resultMaturity == 2)
					chengshudu = "通过小试";
				if(order.resultMaturity == 3)
					chengshudu = "通过中试";
				if(order.resultMaturity == 4)
					chengshudu = "可以量产";
				var p = 380+i*i*20;
				item2 += pattern
					.replace("#8", order.resultId)
					.replace("#6", order.img.rimagePath)
					.replace("#0", order.resultId)
					.replace("#1", order.resultName)
					.replace("#2", chengshudu)
					.replace("#3",p)
				html2 += item2;
			}
			for(var i = 6; i< 9; i++){
				var item3 = "";
				var order = data[i];
				var chengshudu ="";
				if(order.resultMaturity == 0)
					chengshudu = "正在研发";
				if(order.resultMaturity == 1)
					chengshudu = "已有样品";
				if(order.resultMaturity == 2)
					chengshudu = "通过小试";
				if(order.resultMaturity == 3)
					chengshudu = "通过中试";
				if(order.resultMaturity == 4)
					chengshudu = "可以量产";
				var p = 500+i*i*10;
				item3 += pattern
					.replace("#8", order.resultId)
					.replace("#6", order.img.rimagePath)
					.replace("#0", order.resultId)
					.replace("#1", order.resultName)
					.replace("#2", chengshudu)
					.replace("#3",p)
				html3 += item3;
			}
		$("#tuijianinfo1").html(li1+html1);
		$("#tuijianinfo2").html(li2+html2);
		$("#tuijianinfo3").html(li3+html3);
		}
	});
});

//首页成果点击详情
function indexrecommend(id){
	$(location).attr('href', 'result_details18?id=' + id + '');
}


function GetQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r != null) return unescape(r[2]);
	return null;
}




//
//专家列表
function details(id){
		$(location).attr("href","scienic_expertsshow12?euId="+id+"");
	}

$(document).ready(function() {
	name = $("#specialistString").val();
	limit = 10;
	offset = 0;
	findofspecialist(name,limit,offset);
	$("#specialist").click(function(){
		//findofspecialist($("#specialistString").val());
		$(location).attr('href', 'scienic_expertsshow121?string=' + $("#specialistString").val() + '');
	});
})


function findofspecialist(name,limit,offset){
	var m = new Object;
	m.name = name;
	m.limit = limit;
	m.offset = offset;
	$.ajax({
		url:Server + "/fon/findofspecialist",
		data:m,
		type:"POST",
		dataType:"json",
		success : function(data){
			var html = "";
				var pattern = '<li onclick="details(#6)">\
					<div class="d5_ptl">\
						<img src="#0" alt="专家头像"/>\
						<div class="ntb_zhezhao"></div>\
					</div>\
					<div class="d5_ptr">\
						<p class="d5_np1">#1 <span>#2</span></p>\
						<p class="d5_np2">#4</p>\
						<p class="d5_np3">熟悉学科: <span>#5</span></p><br>\
						<div class="active">\
						</div>\
					</div></a>\
				</li>';
				for(var i = 0; i< data.length -1; i++){
				var item = "";
				var order = data[i];
				item += pattern
					.replace("#0", order.euIcon)
					.replace("#1", order.euRealname)
					.replace("#2", order.euTitle)
					.replace("#4", order.euUnitName)
					.replace("#5", order.euDirection)
					.replace("#6", order.euId);
				html += item;
			}
			$("#expertlist").html(html);
		},
	});
}




















//new_file17.jsp页面
//成果列表
/*$(document).ready(function() {
		name = $("#likeStringofinof").val();
		limit = 10;
		offset = 0;
		infoLikeListofinfo(name,limit,offset);
		$("#searchofinfo").click(function(){
			$(location).attr('href', 'newfile17?string=' + $("#likeStringofinof").val() + '');
		});
});

function infoLikeListofinfo(name,limit,offset){
	$.ajax({
		url :Server + "/fon/infolist",
		data : {
			"name":name,
			"limit":limit,
			"offset":offset,
		},
		type : "POST",
		dataType: "JSON",
		success : function(data) {
			var html = "";
			var pattern = '<li onclick="infodetail(#88)" >\
              <div class="vertical" style="padding-left: 20px;">\
                  <div class="vertical_img">\
                  	<img src="'+Server+'/UPLOAD/image/RESULT_IMAGE#0" />\
                  </div>\
              </div>\
              <div class="info">\
                  <div class="title">#1</div>\
                  <div class="row2">\
                      <div class="font">类型：#4<span></span>\
                      </div>\
                      <div class="font">交易方式：\
                          <span>完全转让</span>\
                      </div>\
                  </div>\
                  <div class="row3">\
                      <br >#2\
                  </div>\
              </div>\
              <div class="active">\
                  <div style="text-align: right;">\
                      <span style="color: rgb(255,138,0);font-size: 20px;">#6</span>\
                  </div>\
                  <div style="text-align: right;">\
                       成熟度：<span style="">#8</span>\
                  </div>\
                  <div style="text-align: right;">\
                       地址：<span>#3</span>\
                  </div>\
              </div>\
          </li>';
			for (var i = 0; i < data.rows.length; i++) {
				var item = "";
				var chengshudu ="";
				var leixing ="";
				if(data.rows[i].puser&&data.rows[i].img){
					if(data.rows[i].resultMaturity == 0)
						chengshudu = "正在研发";
					if(data.rows[i].resultMaturity == 1)
						chengshudu = "已有样品";
					if(data.rows[i].resultMaturity == 2)
						chengshudu = "通过小试";
					if(data.rows[i].resultMaturity == 3)
						chengshudu = "通过中试";
					if(data.rows[i].resultMaturity == 4)
						chengshudu = "可以量产";
					
					if(data.rows[i].resultType == 0)
						leixing = "专利";
					if(data.rows[i].resultType == 1)
						leixing = "非专利";
					
					item += pattern
						.replace("#88", data.rows[i].resultId)//id
						.replace("#0", data.rows[i].img.rimagePath)//图片
						.replace("#1", data.rows[i].resultName)//成果名
						.replace("#2", data.rows[i].puser.puUsername)//用户名
						.replace("#4", leixing)//类型
						.replace("#6", data.rows[i].rp.rpayRate)//价格
						.replace("#3", data.rows[i].puser.puAddress)//地址
						.replace("#8", chengshudu)//成熟度
					html += item;
				}
				if(data.rows[i].puser&&!data.rows[i].img){
					item += pattern
						.replace("#88", data.rows[i].resultId)//id
						.replace("#0", data.rows[i].rimagePath)//图片
						.replace("#1", data.rows[i].resultName)//成果名
						.replace("#2", data.rows[i].puser.puUsername)//用户名
						.replace("#4", leixing)//类型
						.replace("#6", data.rows[i].rp.rpayRate)//价格
						.replace("#3", data.rows[i].puser.puAddress)//地址
						.replace("#8", chengshudu)//成熟度
					html += item;
				}
				if(!data.rows[i].puser&&data.rows[i].img){
					item += pattern
						.replace("#88", data.rows[i].resultId)//id
						.replace("#0", data.rows[i].rimagePath)//图片
						.replace("#1", data.rows[i].resultName)//成果名
						.replace("#2", data.rows[i].puUsername)//用户名
						.replace("#4", leixing)//类型
						.replace("#6", data.rows[i].rp.rpayRate)//价格
						.replace("#3", data.rows[i].puAddress)//地址
						.replace("#8", chengshudu)//成熟度
					html += item;
				}
				if(!data.rows[i].puser&&!data.rows[i].img){
					item += pattern
						.replace("#88", data.rows[i].resultId)//id
						.replace("#0", data.rows[i].rimagePath)//图片
						.replace("#1", data.rows[i].resultName)//成果名
						.replace("#2", data.rows[i].puUsername)//用户名
						.replace("#4", leixing)//类型
						.replace("#6", data.rows[i].rpayRate)//价格
						.replace("#3", data.rows[i].puAddress)//地址
						.replace("#8", chengshudu)//成熟度
					html += item;
				}
			}
			$("#inneedlist").html(html);
		},
	});
}*/
//result_details18.jsp页面
//成果点击详情
function infodetail(id){
	$(location).attr('href', 'result_details18?id=' + id + '');
}

























//scienic_needList16.jsp页面
//需求列表

/*$(document).ready(function() {
	alert("js1");
	name = $("#likeStringofneed").val();
	limit = 10;
	offset = 0;
	needlistofsecond(name,limit,offset,tradeId,ntypeId,needContent,priceTag);
	$("#searchofneed").click(function(){
		$(location).attr('href', 'scienic_needList16?string=' + $("#likeStringofneed").val() + '');
	});
});

$(document).ready(function() {
	alert("js2");
	Date.prototype.Format = function (fmt) { //author: meizz 
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
	        "h+": this.getHours(), //小时 
	        "m+": this.getMinutes(), //分 
	        "s+": this.getSeconds(), //秒 
	        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	        "S": this.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	    for (var k in o)
	    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	    return fmt;
	}
	var m = new Object;
	m.name = $("#likeStringofneed").val();
	m.limit = 10;
	m.offset = 0;
	m.needContent = 0;
	$.ajax({
		//url:Server + "/fon/listfindlike",
		url:"http://localhost/fon/listfindlike",
		data:m,
		type:"POST",
		dataType:"json",
		success : function(data){
			var html = "";
				var pattern = '<li>\
                <div class="vertical">技术需求</div>\
                <div class="info">\
                    <div class="title">#1</div>\
                    <div class="row2">\
                        <div class="font">需求简介：<span>#2</span>\
                        </div>\
                        <div class="font">地址：<span>#3</span>\
                        </div>\
                        <div class="font">发布时间：<span>#4</span>\
                        </div>\
                        <div class="font">行业类型：<span>#5</span>\
                        </div>\
                    </div>\
                    <div class="row3">预算：<span>#6万</span>\
                    </div>\
                </div>\
                <div class="active">\
                    <div>\
                        <a onclick="finddetail(#0)">查看详情</a>\
                    </div>\
                    <div>\
                        <a onclick="phone(#9)" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">联系方式</a>\
                    </div>\
                </div>\
            </li>';
				for(var i = 0; i< data.list.length; i++){
				var item = "";
				var order = data.list[i];
				var t = "";
				if(order.tTrade!=null){
					var t = order.tTrade.tradeName;
				}
				item += pattern
					.replace("#0", order.needId)//需求ID
					.replace("#1", order.needName)//需求名称
					.replace("#2", order.needExplain)//需求简介
					.replace("#3", order.address)//地址
					.replace("#4", new Date(order.needCreateTime).Format("yyyy-MM-dd"))//发布时间
					.replace("#5", t)//行业类别
					.replace("#6", order.needBudget)//预算
				    .replace("#9", order.needId);
				   
				html += item;
			}
			$("#needlist").html(html);
		},
	});
})*/
function finddetail(needId){
$(location).attr('href', 'new_file8?needId='+needId+'');
}
function phone(needId){
	$.ajax({
		"url" :Server + "/res/need/phone",
		"type" : "POST",
		"data" : {
			"needId" : needId
		},
	success : function(data){
		var v = data;
		$("#ww1").val(v[0].contacts);
		$("#ww1").attr("disabled","disabled");
		$("#ww2").val(v[0].phone);
		$("#ww2").attr("disabled","disabled");
		$("#ww3").val(v[0].address);
		$("#ww3").attr("disabled","disabled");
	}
	})
}
/*function needlistofsecond(name,limit,offset,tradeId,ntypeId,needContent,priceTag){
	alert(name,limit,offset,tradeId,ntypeId,needContent,priceTag);
	Date.prototype.Format = function (fmt) { //author: meizz 
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
	        "h+": this.getHours(), //小时 
	        "m+": this.getMinutes(), //分 
	        "s+": this.getSeconds(), //秒 
	        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	        "S": this.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	    for (var k in o)
	    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	    return fmt;
	}
	$.ajax({
		//url:Server + "/fon/listfindlike",
		url:"http://localhost/fon/listfindlike",
		data:{
			"name":name,
			"limit":limit,
			"offset":offset,
			"tradeId":tradeId,
			"ntypeId":ntypeId,
			"needContent":needContent,
			"priceTag":priceTag
		},
		type:"POST",
		dataType:"json",
		success : function(data){
			var html = "";
				var pattern = '<li>\
                <div class="vertical">技术需求</div>\
                <div class="info">\
                    <div class="title">#1</div>\
                    <div class="row2">\
                        <div class="font">需求简介：<span>#2</span>\
                        </div>\
                        <div class="font">地址：<span>#3</span>\
                        </div>\
                        <div class="font">发布时间：<span>#4</span>\
                        </div>\
                        <div class="font">行业类型：<span>#5</span>\
                        </div>\
                    </div>\
                    <div class="row3">预算：<span>#6万</span>\
                    </div>\
                </div>\
                <div class="active">\
                    <div>\
                        <a onclick="finddetail(#0)">查看详情</a>\
                    </div>\
                    <div>\
                        <a href="#this">联系方式</a>\
                    </div>\
                </div>\
            </li>';
				for(var i = 0; i< data.list.length; i++){
					
				var item = "";
				var order = data.list[i];
				var t = "";
				if(order.tTrade!=null){
					var t = order.tTrade.tradeName;
				}
				
				item += pattern
					.replace("#0", order.needId)//需求ID
					.replace("#1", order.needName)//需求名称
					.replace("#2", order.needExplain)//需求简介
					.replace("#3", order.address)//地址
					.replace("#4", new Date(order.needCreateTime).Format("yyyy-MM-dd"))//发布时间
					.replace("#5", t)//行业类别
					.replace("#6", order.needBudget);//预算
				   
				html += item;
			}
			$("#needlist").html(html);
		},
	});
}*/









//new_file35
$(document).ready(function() {
	var m = new Object;
	m.usertype = 0;
	m.userid = 123;
	$.ajax({
		url:Server + "/fon/userofinfolist",
		data:m,
		type:"POST",
		dataType:"json",
		success : function(data){
			var html = "";
				var pattern = '<li>\
					<div class="row2">\
				<div class="tab1"><img src="#0" /></div>\
				<div class="tab2">#1</div>\
				<div class="tab3">#2</div>\
				<div class="tab4" id="tab4">#3</div>\
				<div class="tab5">\
					<p>#3</p>\
				</div>\
				<div class="tab6">\
					<div class="btn">\
						<a href="#4">查看</a>\
					</div>\
					<div class="btn" style="background-color: rgb(0,174,255);">\
						<a href="#5" >删除</a>\
					</div>\
					<!--<div class="time">还剩11:23:02</div>-->\
				</div>\
			</div>\
		</li>';
				for(var i = 0; i< data.length -1; i++){
				var item = "";
				var order = data[i];
				item += pattern
					.replace("#0", 1)//图片
					.replace("#1", order.resultName)//成果名
					.replace("#2", order.resultMaturity)//成熟度
					.replace("#3", order.resultTeamchampion)//团队带头人
					.replace("#4", order.resultControlstatus)//状态
					.replace("#5", order.resultId)//查看ID
					.replace("#6", order.resultId);//删除ID
				html += item;
			}
			$("").html(html);
		},
	});
})

//跳转登录页面
function loadUser(){
	$.ajax({
		url:"http://218.5.80.6:8070/OCEAN/api/login",
		type:"GET",
		data:{"url":"'+Server+'/Scientific/"},
		success:function(data){
			alert("成功");
		},
		error:function(data){
			alert("失败");
		}
	});
}




