function load(){
	var usertype = $('#usertypeoflogin').val();
	var userid = $('#useridoflogin').val();
	$.ajax({
		url : Server +"/fon/userofneedlist" ,
		type : "POST",
		dataType : "JSON",
		data : {			
			"offset" : 0,
			"limit" : 10,
			"userid" : userid,
			"usertype" : usertype,
		},
		success : function(data){
			console.log(data)
			var dataList = data;
			var html = "";
			var pattern='<li>\
				<div class="row2">\
					<div class="tab2">#name</div>\
					<div class="tab3">#introduction</div>\
					<div class="tab4">#number</div>\
					<div class="tab5">#statu</div>\
					<div class="tab6">\
						<div class="btn">\
							<a onclick="findDetail(#0)">查看</a>\
						</div>\
						<div class="btn" style="background-color: rgb(0,174,255);">\
							<a onclick="remove(#1)" >删除</a>\
						</div>\
					</div>\
				</div>\
			</li>';
			var item = "";
			for(var i = 0 ;i <data.length ; i++){
				//管理状态 0：展示 1：锁定
				var Statue ="";
				switch (data[i].nesultControlStatus) 
				{
				case 1 : 
					Statue ="展示中";
					break;
				case 2 :
					Statue ="锁定中";
					break;
				case 3 :
					Statue ="等待买家确认";
				}
				item += pattern.replace("#name", data[i].needName)
				               .replace("#introduction", data[i].needExplain)
				               .replace("#number", data[i].stpnumber)
				               .replace("#statu", Statue)
				               .replace("#0", data[i].nTrade.needId)
				               .replace("#1", data[i].nTrade.needId)
				}
			$("#needManage").html(item);
		}
	})
		
}


function findDetail(needid){
	//needid = $('#0').val();
	//alert(needid);
	$(location).attr('href', 'new_file22?needid='+ needid +'');
}


function remove(needId){ 
	var msg = "您真的确定要删除吗？\n\n请确认！"; 
	if (confirm(msg)==true){
		detelet(needId); 
		return true;
	}else{ 
		return false; 
	} 
} 


function detelet(needId){
	$.ajax({
		url:Server +"/res/need/remove",
		data:{
			"needId":needId,
		},
		type:"POST",
		success : function(data){
			if(data.code == 1){
				alert("删除失败");
				load();
			}else if(data.code == 0){
				alert("删除成功");
				load();
			}
		}
	});
}

