function load(id){
	$.ajax({
		url:Server + "/res/dea/info",
		type: "POST",
		dataType : "JSON",
		data: {			
			"id" :id
		},
		success : function(data){
			var v =data ;
			var m =v[0].pay;
			var Statu="";
			var Statu1="";
				
				//成熟度0：正在研发1：已有样品2：通过小试3：通过中试4：可以量产
				switch(v[0].resultMaturity){
				case 0:
					Statu1 ="正在研发";
					break;
				case 1:
					Statu1 ="已有样品";
					break;
				case 2:
					Statu1 ="通过小试";
					break;
				case 3:
					Statu1 ="通过中试";
					break;
				case 4:
					Statu1 ="可以量产";
					break;
				}
				$("#in0").html(v[0].resultName);
				$("#in1").html(Statu1);
				$("#in2").html(v[0].resultTechlevel);
				$("#in3").html(v[0].awards);
				$("#in4").html(v[0].unit);
				$("#in5").html(v[0].ipNumber);
				$("#pay1").html(v[0].pay[0].rpayRate);
				$("#pay2").html(v[0].pay[1].rpayRate);
				$("#pay3").html(v[0].pay[2].rpayRate);
				
				var img = '<img src='+v[0].image[0].rimagePath+' />';
				$("#cgImg").html(img);
				//$("#in8").html(m[i].rpayRate);
			
		}
	
	})
	
}

function shangchuan(id) {
	//alert(id);
	rinfoId = $('#8').val();
	rdealId = $('#9').val();
	//alert(rinfoId);
	//alert(rdealId);
	$(location).attr('href', 'cgpingjia37_5?id=' + rinfoId + '&rdealId=' + rdealId + '');
}
function next(id){
	$.ajax({
		url:Server + "/res/dea/deal",
		type: "POST",
		dataType : "JSON",
		data: {			
			"id" :id
		},
		async : false,
		success : function(data){
			var o = data ;
			var Statu="";
			//支付方式 0:支付宝 1:微信支付 2:xxx 3:xxx
			switch(o[0].payType){
			case 0:
				Statu ="支付宝";
				break;
			case 1:
				Statu ="微信支付";
				break;
			case 2:
				Statu ="银联";
				break;
			case 3:
				Statu ="xxx";
				break;
			}
			$("#0").html(o[0].name);
			$("#1").html(o[0].phone);
			$("#2").html(o[0].address);
			$("#3").html(o[0].unit);
			$("#4").html(o[0].rdealNumber);
			$("#5").html(o[0].rdealApplyTime);
			$("#6").html(Statu);
			$("#7").html(o[0].payNumber);
			$("#8").val(o[0].rinfoId);
			$("#9").val(o[0].rdealId);
			var sdcList = o[0].sdc;
			var imgHtml = '';
			var docHtml = '';
			for (var i = 0; i < sdcList.length; i++) {
				varsdcPath = '';
				if (sdcList[i].upType == '1') {
					sdcPath = sdcList[i].path;
					imgHtml += '<img class="img-rounded zoomify" src=' + sdcPath + '>';
				} else if (sdcList[i].upType == '2') {
					sdcPath = sdcList[i].path;
					docHtml += '<a style="color: red; font-size: 20px" class="right_a" href=' + sdcPath + ' target="_blank">点击下载查看交易合同</a>';
				}
			}
			$(".pic").html(imgHtml);
			$("#doc").html(docHtml);
		}
	})
	
	
}