$(document).ready(function(){
	$("#shangchuan").click(function(){
		upload();
		return false;
	});
});
function load(id) {
	$.ajax({
		url : Server +"/res/dea/info",
		type : "POST",
		dataType : "JSON",
		data : {
			"id" : id
		},
		success : function(data) {
			var v = data;
			var m = v[0].pay;
			var Statu = "";
			var Statu1 = "";

			//成熟度0：正在研发1：已有样品2：通过小试3：通过中试4：可以量产
			switch (v[0].resultMaturity) {
			case 0:
				Statu1 = "正在研发";
				break;
			case 1:
				Statu1 = "已有样品";
				break;
			case 2:
				Statu1 = "通过小试";
				break;
			case 3:
				Statu1 = "通过中试";
				break;
			case 4:
				Statu1 = "可以量产";
				break;
			}
			$("#in0").html(v[0].resultName);
			$("#in1").html(Statu1);
			$("#in2").html(v[0].resultTechlevel);
			$("#in3").html(v[0].awards);
			$("#in4").html(v[0].unit);
			$("#in5").html(v[0].ipNumber);
			$("#in7").val(v[0].resultId);
			$("#pay1").html(v[0].pay[0].rpayRate);
			$("#pay2").html(v[0].pay[1].rpayRate);
			$("#pay3").html(v[0].pay[2].rpayRate);
			var img = '<img src='+v[0].image[0].rimagePath+' />';
			$("#cgImg").html(img);

			//$("#in8").html(m[i].rpayRate);

		}
	})

}

function next(id) {
	$.ajax({
		url : Server +"/res/dea/deal",
		type : "POST",
		dataType : "JSON",
		data : {
			"id" : id
		},
		success : function(data) {
			var o = data;
			var Statu = "";
			//支付方式 0:支付宝 1:微信支付 2:xxx 3:xxx
			//			switch(o[0].payType){
			//			case 0:
			//				Statu ="支付宝";
			//				break;
			//			case 1:
			//				Statu ="微信支付";
			//				break;
			//			case 2:
			//				Statu ="银联";
			//				break;
			//			case 3:
			//				Statu ="xxx";
			//				break;
			//			}
			$("#0").html(o[0].name);
			$("#1").html(o[0].phone);
			$("#2").html(o[0].address);
			$("#3").html(o[0].unit);
			$("#in6").val(o[0].rdealId);
			//			$("#4").html(o[0].rdealNumber);
			//			$("#5").html(o[0].rdealApplyTime);
			//			$("#6").html(Statu);
			//			$("#7").html(o[0].payNumber);
			$("#4").val(o[0].rinfoId);
			$("#7").val(o[0].rdealId);
		}
	})
}
function uploadImages(submitArr){
	var rdealId = $("#in6").val();
	$.ajax({    
        url : Server +'/res/dea/uploadImage',    
        type : 'post', 
        data : {"submitArr" : JSON.stringify(submitArr),"rdealId" : rdealId}, 
        traditional: true,
        //processData: false,   用FormData传fd时需有这两项    
        //contentType: false,    
        success : function(data){    
        	uploadFile();
      　	}
    })
}

function update(){
    rdealId= $('#7').val()
    var resultId = $("#in7").val();
   // alert(rdealId); 
		$.ajax({
		    //url:"http://127.0.0.1/res/need/updatestatus",
			url : Server +"/res/need/updatestatus",
			type : "POST",
			data:$('#signupForm').serialize(),
			success : function(data){
				$(location).attr('href', 'cgdaizhifu37_2?id='+resultId+'');
			},
			error : function(msg){
				alert("修改失败，请联系管理员！");
			}
		});
	}
function uploadFile() {
	$("#fileTypeError").html('');
	
	var fileName = $('#file_upload').val(); //获得文件名称
	var fileType = fileName.substr(fileName.lastIndexOf("."), fileName.length); //截取文件类型,如(.xls)
	if (fileType == '.xls' || fileType == '.doc' || fileType == '.docx' || fileType == '.pdf') { //验证文件类型,此处验证也可使用正则
		$.ajax({
			url : Server +"/res/dea/uploadFile", //上传地址		
			type : 'POST',
			cache : false,
			data : new FormData($('#uploadForm')[0]),
			processData : false,
			 async:false,
			contentType : false,
			success : function(data) {
				alert("上传成功");
				update();				
				return false;
			},
			error: function(data){
				alert("上传失败");
			}
		});
	} else {
		$("#fileTypeError").html('*上传文件类型错误,支持类型: .xls .doc .docx .pdf');
	}
}