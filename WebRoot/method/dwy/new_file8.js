function load(needId) {
	$.ajax({
		url : Server + "/res/need/detail",
		type : "POST",
		data : {
			"offset" : 0,
			"limit" : 10,
			"needId" : needId
		},
		success : function(data) {
			var m = data.rows;
			var Status = ""; //需求类型0：技术需求1：合作需求2：人才需求
			var Status1 = ""; //展示状态 0：未解决 1：已解决
			switch (m[0].ntypeId) {
			case 0:
				Status = "技术需求";
				break;
			case 1:
				Status = "合作需求";
				break;
			case 2:
				Status = "人才需求";
				break;
			}
			switch (m[0].ntradeId) {
			case 0:
				Status1 = "需求未解决";
				break;
			case 1:
				Status1 = "需求已解决"
				break ;
			}
			var Time = "";
			var Time1 = ""
			if (m[0].needCloseTime != null && m[0].needCloseTime != '' && m[0].needCloseTime != undefined) {
				Time = (m[0].needCloseTime).substring(0, (m[0].needCloseTime).indexOf(' '));
			} else {
				Time = "没有录入截止时间"
			}
			if (m[0].needCreateTime != null && m[0].needCreateTime != '' && m[0].needCreateTime != undefined) {
				Time1 = (m[0].needCreateTime).substring(0, (m[0].needCreateTime).indexOf(' '));
			} else {
				Time1 = "没有录入发布时间"
			}
			$("#in0").html(m[0].needName);
			$("#in1").html(Status);
			$("#in2").html(m[0].tradeName);
			$("#in3").html(m[0].needExplain);
			$("#in4").html(Time1);
			$("#in5").html(Time);
			$("#in6").html(m[0].address);
			$("#in7").html(m[0].needBudget);
			$("#in8").html(Status1);
			$("#9").html(needId);
			$("#10").val(needId);
		}
	})
}






function particulars(needId) {
	$.ajax({
		url : Server + "/res/need/tech",
		type : "POST",
		dataType : "JSON",
		data : {
			"offset" : 0,
			"limit" : 10,
			"needId" : needId
		},
		success : function(data) {
			var m = data.rows;
			var html = "";
			var item = "";
			for (var k = 0; k < m.length; k++) {
				if (m[k].demand.ntradeId == 0) {
					var pattern = '<li class="sun_li_row">\
					<div class="li_title" style=" padding-left: 10px;"><a onclick="upload(#8,#9)">专家填写评审意见</a></div>\
					<div style="padding-right: 20px;display: inline-block;">\
					 <div class="li_row1">\
					 	<div class="info_left">#0</div>\
					 	<div class="info_right">\
					 		<span>#1</span><br />\
					 		<span style="color: rgb(6,185,254);">#2</span>\
					 	</div>\
					 </div>\
					 <div class="li_row2">';
				} else {
					var pattern = '<li class="sun_li_row">\
						<div class="li_title" style=" padding-left: 10px;"><a></a></div>\
						<div style="padding-right: 20px;display: inline-block;">\
						 <div class="li_row1">\
						 	<div class="info_left">#0</div>\
						 	<div class="info_right">\
						 		<span>#1</span><br />\
						 		<span style="color: rgb(6,185,254);">#2</span>\
						 	</div>\
						 </div>\
						 <div class="li_row2">';
				}

			}

			for (var i = 0; i < m.length; i++) {
				
				item += pattern.replace("#0", m[i].techName==null ? "" : m[i].techName)
					.replace("#1", m[i].name==null ? "" : m[i].name)
					.replace("#2", m[i].phone==null ? "" : m[i].phone )
					.replace("#8", m[i].techId==null ? "" : m[i].techId)
					.replace("#9", m[i].unId==null ? "" : m[i].unId)
				var pl = "";
				var plhtml = '<div class="info_left" >#3</div>\
						 	<div class="info_right" style="float: right;">\
					 		<span>#4</span><br />\
					 	</div>';
				var plList = m[i].proofreview
				for (var j = 0; j < plList.length; j++) {
					item += plhtml.replace("#3", plList[j].reviewContent==null ? "" :  plList[j].reviewContent)
						.replace("#4",plList[j].euRealname==null ? "" :  plList[j].euRealname )
				}
				item += "</div>\
						 </div>\
						</div>\
					</li>";
			}
			$("#detailList").html(item);
		}
	})
}

function upload(techId, unId) {
	if(sessionStorage.getItem("euId")==null){
		alert("您不是专家，不能评论")
	}else{
		$(location).attr("href", "update_subject10_1_zj?techId=" + techId + "&needId=" + unId + "");
	}
}

function next(needId) {
	$.ajax({
		url : Server + "/res/need/comment",
		type : "POST",
		dataType : "JSON",
		data : {
			"unId" : needId
		},
		success : function(data) {
			var v = data;
			var html = "";
			var item = "";
			var pattern = '<li>\
				<div class="fontInfo">\
			<div class="left">#co1</div>\
			<div class="right">#co2</div>\
		</div>\
		\
		<div class="pic">\
				<!-- <img src="#co3"/>-->\
			<!-- <img src="images/productDetailPic.png"/> -->\
		</div>\
	</li>';
			for (var i = 0; i < v.length; i++) {
				item += pattern.replace("#co1", v[i].ncomentContent)
					.replace("#co2", v[i].name)
			//.replace("#co3", v[i].sdc.path)
			}
			$("#commentList").html(item);

		}
	})
}

function describe(needId) {
	$.ajax({
		url : Server + "/res/need/describe",
		type : "POST",
		data : {
			"needId" : needId
		},
		success : function(data) {
			console.log(JSON.stringify(data));
			if (data[0].needDetails != '' && data[0].needDetails != null)
				$("#detail").html(data[0].needDetails);
		}
	})
}

function phones(needId) {
	$.ajax({
		"url" : Server + "/res/need/phone",
		"type" : "POST",
		"data" : {
			"needId" : needId
		},
		success : function(data) {
			var v = data;
			$("#ww1").val(v[0].contacts);
			$("#ww1").attr("disabled", "disabled");
			$("#ww2").val(v[0].phone);
			$("#ww2").attr("disabled", "disabled");
			$("#ww3").val(v[0].address);
			$("#ww3").attr("disabled", "disabled");
		}
	})
}
/*
 * $.ajax({
			"url" : "http://127.0.0.1/res/need/judge",
			"type" : "POST",
			"data" : {
				"limit" : 10,
				"offset" : 0,
				"nbuyUtype" : 1,
				"unId" : 21312366 ,
				"id" : "9d66cc1724464f6eaad57b24ba1f09df"
			},
			success : function(data){
				var m = data.rows
				alert(m);
				if(m==''){
					alert(m);
					$(location).attr('href', 'need_details10?needId='+needId+'');
				}else if(m!=''){
					alert('你已经承接过需求,');
				}
				
			}
		})
 */

$(document).ready(function() {
	$("#need").click(function(needId) {
		if ($("#useridoflogin").val() == 'null') {
			alert("未登录，请登录！");
		} else {
			var usertype = $('#usertypeoflogin').val();
			var userid = $('#useridoflogin').val();
			needId = $('#10').val();
			$.ajax({
				"url" : Server + "/res/need/judgePerson",
				"type" : "POST",
				"data" : {
					"limit" : 10,
					"offset" : 0,
					"typeId" : usertype,
					"needId" : needId,
					"id" : userid
				},
				success : function(data) {
					var p = data.rows;
					//alert(p);
					if (p == '') {
						$.ajax({
							"url" : Server + "/res/need/judge",
							"type" : "POST",
							"data" : {
								"limit" : 10,
								"offset" : 0,
								"nbuyUtype" : usertype,
								"unId" : needId,
								"id" : userid
							},
							success : function(data) {
								var m = data.rows
								//alert(m);
								if (m == '') {
									//alert(m);
									$(location).attr('href', 'need_details10?needId=' + needId + '');
								} else if (m != '') {
									alert('你已经承接过需求,无法在次进行需求承接');
								}
							}
						})
					} else if (p != '') {
						alert('此需求是你发布，无法进行需求承接');
					}
				}
			})
		}
	})
})