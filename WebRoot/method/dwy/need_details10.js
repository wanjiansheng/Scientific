function uploadFile() {
	$("#uid").val($("#useridoflogin").val());
	$("#typeId").val($("#usertypeoflogin").val());
	$("#fileTypeError").html('');
	$("#fileTypeError1").html('');
	//var techId = $("#in7").val();
	var unId = $("#unId").val();
	var fileName1 = $('#file_upload1').val(); 
	var fileName = $('#file_upload').val();//获得文件名称
	var fileType1 = fileName1.substr(fileName1.lastIndexOf("."), fileName1.length);
	var fileType = fileName.substr(fileName.lastIndexOf("."), fileName.length);//截取文件类型,如(.xls)
	if((fileType1 != '.xls' && fileType1 != '.doc' && fileType1 != '.docx' && fileType1 != '.pdf')){
		$("#fileTypeError1").html('*上传文件类型错误,支持类型: .doc .docx .xls .pdf');
	}
	if ((fileType != '.xls' && fileType != '.doc' && fileType != '.docx' && fileType != '.pdf')) { //验证文件类型,此处验证也可使用正则
		$("#fileTypeError").html('*上传文件类型错误,支持类型: .doc .docx .xls .pdf');
	} 
	if((fileType1 == '.xls' || fileType1 == '.doc' || fileType1 == '.docx' || fileType1 == '.pdf')&&(fileType == '.xls' || fileType == '.doc' || fileType == '.docx' || fileType == '.pdf'))
	{
		$.ajax({
			url :Server + "/res/need/upload0", //上传地址		
			type : 'POST',
			cache : false,
			data : new FormData($('#uploadForm1')[0]),
			processData : false,
			 async:false,
			contentType : false,
			success : function(data) {																			 
				alert("上传成功")
				$(location).attr('href', "new_file8?needId="+unId+"");
				//$(location).attr('href', "new_file8?needId="+unId +"");
				return false;
			},
			error: function(data){
				alert("上传失败");
			}
		})
	}
}

