function load(techId){
	$.ajax({
		url : Server +"/res/need/techDetail",
		type : "POST",
		dataType : "JSON",
		data : {
			"techId" : techId
		},
		success : function(data){
			console.log(data)
			var way ="";
			switch (data[0].supportWay)
			{
			case 0 :
				way = "技术支持";
				break;
			case 1 :
				way = "资金支持";
				break;
			case 2 :
				way = "人员支持";
				break;			
			}
			$("#0").val(data[0].techName);
			$("#1").val(data[0].techTrade);
			$("#2").val(data[0].serviceArea);
			$("#3").val(data[0].teamDesc);
			$("#4").val(data[0].techCooperation);
			$("#5").val(data[0].techPatent);
			$("#6").val(data[0].techCase);
			$("#7").val(data[0].talentsName);
			$("#8").val(data[0].talentsMobile);
			$("#9").val(data[0].talentsEmails);
			//$("#10").val(data[0].techDocument);
			$("#11").val(way);
			$("#12").val(data[0].supportDetails);
			$("#13").val(data[0].supportNeed);
			$("#14").val(data[0].talentsOrganization);
			$("#15").val(data[0].talentsDesc);
			$("#16").val(data[0].talentsResume);			
			var docHtml ='';
			var doc1Html = '';	
			docHtml += '<a style="color: red; font-size: 20px" class="right_a" href=' + Server + '/' + data[0].techDocument + ' target="_blank">点击下载查看文档</a>';
			doc1Html += '<a style="color: red; font-size: 20px" class="right_a" href=' + Server + '/' + data[0].talentsResume + ' target="_blank">点击下载查看简历</a>';
			$("#doc").html(docHtml);
			$("#doc1").html(doc1Html);
		}
	})
}