function load() {
	var typeId = $('#usertypeoflogin').val();
	/*var typeId = "1";*/
	var id = $('#useridoflogin').val();
	/*var id = "53954806924e497099791a551ae18412";*/
	$.ajax({
		url : Server +"/res/deal/list",
		type : "POST",
		data : {
			"offset" : 0,
			"limit" : 10,
			"typeId" : typeId,
			"id" : id
		},
		success : function(data) {
			var m = data.rows;
			var html = "";
			var pattern = '<li>\
				<div class="row1">\
			<div>\
				<span>#0</span>\
				<span>订单编号: #1</span>\
			</div>\
			<div><img src="images/usercenter_icon2.png" onclick="delResult(#9);"/></div>\
		</div>\
		<div class="row2">\
			<div class="tab1"><img src="#2" /></div>\
			<div class="tab2">#3</div>\
			<div class="tab3">#4</div>\
			<div class="tab4">#5</div>\
			<div class="tab5">\
				<p>#6</p>\
				<p>\
					<a onclick="findOrder(#7,#12)">查看详情</a>\
				</p>\
			</div>\
			<div class="tab6">\
				<div class="btn">\
					<a onclick="Order(#10,#11)">#8</a>\
				</div>\
				<!--<div class="time">还剩11:23:02</div>-->\
			</div>\
		</div>\
	</li>';
			var item = "";
			for (var i = 0; i < m.length; i++) {
				var Status = ""; //购买状态0：等待完成1：待支付2：已支付3：已评价
				var Status1 = "";
				switch (m[i].rdealPayStatus) {
				case 0:
					Status = "等待完成";
					Status1 = "上传资料";
					break;
				case 1:
					Status = "待支付";
					Status1 = "去支付";
					break;
				case 2:
					Status = "已支付";
					Status1 = "去评价";
					break;
				case 3:
					Status = "已评价";
					Status1 = "";
					break;
				}
				item += pattern
					.replace("#0", m[i].rdealAffirmTime)
					.replace("#1", m[i].rdealNumber)
					.replace("#2", m[i].rimagePath)
					.replace("#3", m[i].resultName)
					.replace("#4", m[i].rpayRate)
					.replace("#5", m[i].resultTeamchampion)
					.replace("#6", Status)
					.replace("#7", m[i].rinfoId)
					.replace("#8", Status1)
					.replace("#9", m[i].rdealId)
					.replace("#10", m[i].rinfoId)
					.replace("#11", m[i].rdealPayStatus)
					.replace("#12", m[i].rdealPayStatus),
				html += item;
			}
			$("#dealList").html(item);
		},
	});
}

//function Order(rdealPayStatus,resultId){
//	if(rdealPayStatus == 0){
//		$(location).attr("href",'new_38_1?id='+resultId+'');
//	}else if(rdealPayStatus == 1){
//		$(location).attr("href","cgdaizhifu37_2?id="+resultId+"");
//	}else if(rdealPayStatus == 2){
//		$(location).attr("href","cgdaipingjia37_4?id="+resultId+"");
//	}
//}

function delResult(id) {
	var msg = "您真的确定要删除吗？\n\n请确认！";
	if (confirm(msg) == true) {
		delRst(id);
		return true;
	} else {
		return false;
	}
}

function delRst(id) {
	$.ajax({
		url : Server +"/res/deal/del",
		data : {
			"rdealId" : id,
		},
		type : "POST",
		dataType : "json",
		success : function(data) {
			alert("删除成功");
			load();
		},
		error : function(msg) {
			alert("删除失败");
		}
	});
}



function findOrder(id,stu) {
	$(location).attr("href","cgdaipingjia37_4?id="+id+"&stu="+stu);
}




function Order(resultId,rdealPayStatus) {
	if(rdealPayStatus==0){
	    $(location).attr('href', 'new_38_1?id=' + resultId + '&stu=10');}
	else if(rdealPayStatus==1){
		$(location).attr("href","cgdaizhifu37_2?id="+resultId+"&rdealId="+rdealPayStatus);
	}else if(rdealPayStatus==2){
		$(location).attr("href","cgdaipingjia37_4?id="+resultId+"&stu=10");
	}
}