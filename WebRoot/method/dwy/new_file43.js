function load(){
	var typeId = $('#usertypeoflogin').val();
	var id = $('#useridoflogin').val();
	$.ajax({
		url : Server +"/res/need/techmanage",
		type : "POST",
		dataType : "JSON",
	    data : {
	    	"offset" : 0,
	    	"limit" : 10,
	    	"typeId" :typeId,
	    	"id": id
	    },
	    success : function(data){
	    	
	    	var dataList = data.rows;
	    	var html = "";
	    	var pattern='<li>\
				<div class="row2">\
					<div class="tab2">#type</div>\
					<div class="tab3">#name</div>\
					<div class="tab4">#area</div>\
					<div class="tab5">#time</div>\
					<div class="tab6">\
						<div class="btn">\
							<a onclick="techDetail(#0)">查看</a>\
						</div>\
					</div>\
				</div>\
			</li>';
	    	var item = "";
	    	for(var i = 0 ;i<dataList.length; i++){
	    		var needType = "";
	    		switch (dataList[i].ntypeId)
	    		{
	    		case 0 :
	    			needType = "技术需求 ";
	    			break;
	    		case 1 :
	    			needType = "合作需求";
	    			break;
	    		case 2 :
	    			needType = "人才需求";
	    			break;
	    		}
	    		var Time = "";
	    		if(dataList[i].techCreateTime!=null &&dataList[i].techCreateTime!=""){
	    			 Time =( dataList[i].techCreateTime).substring(0,(dataList[i].techCreateTime).indexOf(' '));
	    		}else {
	    			Time = " undefined"
	    		}
	    		item += pattern.replace("#type", needType)
	    		               .replace("#name", dataList[i].techName)
	    		               .replace("#area", dataList[i].serviceArea)
	    		               .replace("#time", Time)
	    		               .replace("#0", dataList[i].techId)
	    	}
	    	$("#techManage").html(item);
	    }
	})
}
function techDetail(techId){
$(location).attr('href', 'techDetail?techId='+techId+'');
}

