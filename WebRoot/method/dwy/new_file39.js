function load(puId){
	var uid = $("#useridoflogin").val();
	var usertype = $("#usertypeoflogin").val();
	var token = $("#token").val();
	getUser39(uid,usertype,token);
}
/*获取登录用户的信息*/
function getUser39(uid,usertype,token){
	$.ajax({
		url:Server +"/thirdparty/user/getUserInfoById",
		//url:"http://localhost/thirdparty/user/getUserInfoById",
		type:"POST",
		data:{
			"uid":uid,
			"token":token,
			"type":usertype
		},
		dataType:"json",
		success:function(data){
			var headHtml = "";
			var userInfo = data["userInfo"];
			if(userInfo != "" && userInfo != null){
				$("#mobile").val(userInfo['mobile']);
				$("#puEmail").val(userInfo['email']);
				$("#puRealname").val(userInfo['name']);
				$("#puAddress").val(userInfo['address']);
			}
		},
		error:function(data){
			console.log("失败");
		}
	});
}
/*function update(){  
	$.ajax({
		url : Server +"/res/need/update",
		type : "POST",
		data:$('#signupForm').serialize(),
		success : function(data){
			alert("修改成功！");
		},
		error : function(msg){
			alert("修改失败，请联系管理员！");
		}
	});
}*/

function resert() {
	$.ajax({
		type : "POST",
		url : Server +"/res/need/adminResetPwd",
		data : $('#resertForm').serialize(),// 你的formid
		error : function(request) {
			parent.layer.msg("系统错误，联系管理员");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg(data.msg);
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.msg(data.msg);
			}

		}
	});

}

function validateRule() {
	//var icon = "<i class='fa fa-times-circle'></i> ";
	$("#resertForm").validate({
		rules : {

			password : {
				required : true,
				minlength : 6
			}
		},
		messages : {
			password : {
				required : icon + "请输入您的密码",
				minlength : icon + "密码必须6个字符以上"
			}
		}
	})
}