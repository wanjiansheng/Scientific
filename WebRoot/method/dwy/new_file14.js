function need(){
	$.ajax({
		url : Server +"/res/need/all" ,
		type : "POST",
		dataType : "JSON",
		data : {			
			"offset" : 0,
			"limit" : 10
		},
		success : function(data){
			var v = data.rows;
			var html = "";
			var item = "";
			var pattern='<li onclick="findNeed(#5)">\
          					<div class="item1">#0</div>\
						\
						<div class="item3">#1</div>\
						<div class="item4">#2</div>\
						<div class="item5">#3</div>\
						<div class="item6 change-item6 ">#4</div>\
          				</li>'
			for (var i = 0; i < v.length; i++){
				var Status = "";//购买状态0：等待完成1：待支付2：已支付3：已评价
				if(v[i].srd!=null){
				switch (v[i].srd.rdealPayStatus)
				{
				case 0:
					Status = "展示中";
					break;
				case 1:
					Status = "展示中";
					break;
				case 2:
					Status = "交易成功";
					break;
				case 3:
					Status = "交易成功";
					break;
				}}else{Status = "展示中" ;}
				
				item += pattern.replace("#0", v[i].needName)
				               .replace("#1", v[i].needExplain)
				               .replace("#2", v[i].tradeName)
				               .replace("#3", v[i].needBudget)
				               .replace("#4", Status)
				               .replace("#5", v[i].needId)
			}
			$("#needallList").html(item);
			
		}
		
	})
}


function findNeed(needId){
$(location).attr('href', 'new_file8?needId='+needId+'');
}

function deal(){
	$.ajax({
		url : Server +"/res/deal/all" ,
		type : "POST",
		dataType : "JSON",
		data : {			
			"offset" : 0,
			"limit" : 10
		},
		success : function(data){
			var m = data.rows;
			var html = "";
			var item = "";
			var pattern='<li onclick="findResult(#5)">\
          					<div class="item1">#0</div>\
						\
						<div class="item3">#1</div>\
						<div class="item4">#2</div>\
						<div class="item5">#3</div>\
						<div class="item6 change-item6">#4</div>\
          				</li>'
			for (var i = 0; i < m.length; i++){
				var Status = "";//购买状态0：等待完成1：待支付2：已支付3：已评价
				var name = "";
				if(m[i].srd!=null){
				switch (m[i].srd.rdealPayStatus)
				{
				case 0:
					Status = "展示中";
					break;
				case 1:
					Status = "展示中";
					break;
				case 2:
					Status = "交易成功";
					break;
				case 3:
					Status = "交易成功";
					break;
				}}else{Status = "展示中"; }
				if(m[i].sentype != null){
					name = m[i].sentype.tradeName
				}else{name = undefined;}
				item += pattern.replace("#0", m[i].resultName)
				               .replace("#1", m[i].resultDesc)
				               .replace("#2", name)
				               .replace("#3", m[i].resultMinprice)
				               .replace("#4", Status)
				               .replace("#5", m[i].resultId)
				}
			$("#dealallList").html(item);

            $(".item6").html(function(i,h){
                if(h == "交易成功"){
                    this.style.color="#52C600";
                }
            })
			
		}
		
	})
}


// $(function () {
//     $(".item6").html(function(i,h){
//         if(h == "交易成功"){
//             this.style.color="red";
//         }
//     })
// })






// function dsds(){
//     $(".item6").html(function(i,h){
//         if(h == "交易成功"){
//             this.style.color="#52C600";
//         }
//     })
// }
//


function findResult(resultId){
$(location).attr('href', 'result_details18?id='+resultId+'');
}
