<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <style type="">
        .cur{
            background-color: #efefef;
            color: #ff8a00;
        }
    </style>
    <script type="text/javascript">
    </script>
</head>
<body>
<ul class="ce">
    <li>
        <a >科研成果 <span><img class="more" src="images/sy_arrow_down.png"/></span></a>
        <ul class="er" >
            <li ><a href="/Scientific/updata34">发布成果</a></li>
            <li ><a href="/Scientific/new_file35">管理成果</a></li>
        </ul>
    </li>
    <li>
        <a  >订单管理 <span><img class="more" src="images/sy_arrow_down.png"/></span></a>
        <ul class="er" >
            <li><a href="/Scientific/orderManagement37">等待完成</a></li>
            <li><a href="/Scientific/orderManagement37">完成交易</a></li>
        </ul>
    </li>
    <li>
        <a  >难题发布 <span><img class="more" src="images/sy_arrow_down.png"/></span></a>
        <ul class="er">
            <li><a href="/Scientific/new_file38">发布难题</a></li>
            <li><a href="/Scientific/new_file38_1">难题发布管理</a></li>
        </ul>
    </li>
    <li>
        <a  >科研需求 <span><img class="more" src="images/sy_arrow_down.png"/></span></a>
        <ul class="er">
            <li><a href="/Scientific/publishDemand45">发布需求</a></li>
            <li><a href="/Scientific/new_file44">管理需求</a></li>
            <!-- <li><a>评审意见</a></li> -->
        </ul>
    </li>
    <li ><a href="/Scientific/new_file43">技术方案管理</a></li>
    <li><a id="centerUrl" href="/Scientific/new_file39">账号设置</a></li>
    <li><a href="/Scientific/new_file41">信用认证和交易</a></li>
    <div class="clear"></div>
</ul>

<script type="text/javascript" src="js/jquery.2.1.1.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type=”text/javascript”>
var urlstr = location.href;
//alert((urlstr + ‘/’).indexOf($(this).attr(‘href’)));
var urlstatus=false;
$("a").each(function () {
if ((urlstr + '/').indexOf($(this).attr('href') > -1&&$(this).attr('href')!="") {
$(this).addClass('cur'); urlstatus = true;
} else {
$(this).removeClass('cur');
}
});
if (!urlstatus) {$("a").eq(0).addClass('cur'); }
</script>



<script type="text/javascript">
    $(document).ready(function(){ 
  $(".er li a").each(function(){ 
    $this = $(this); 
    if($this[0].href==String(window.location)){ 
      $this.addClass("xz"); 
    } 
  }); 
  centerUrl();
  
}); 
 function centerUrl(){
   if($("#usertypeoflogin").val() == "1"){
    			$("#centerUrl").attr("href","/Scientific/new_file39");
    		}else if($("#usertypeoflogin").val() == "2"){
    			$("#centerUrl").attr("href","/Scientific/new_file40");
    		}
   }
</script>



</body>
</html>
