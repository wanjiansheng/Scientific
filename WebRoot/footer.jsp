<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<head>
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <meta charset="UTF-8">
    <title>底部</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- 我爱你 -->
    <style>
        *{
            margin: 0px;
            padding: 0px;
            border: 0px;
        }
    
    </style>
    <style>
    footer {
        clear: both;
        width: 100%;
        height: 130px;
        bottom: 0;
        line-height: 0px;
        background-color: #F9F9F9;
        border-top: 2px solid #FF8A00;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .foot ul li{
        display: inline-flex;
        align-items: center;
        margin-left: 20px;
        margin-top: 25px;
    }
    footer .foot {
        text-align: center;
        line-height: 40px;
    }
    footer .foot p {
    color: #202020;
    font-size: 14px;
}
</style>
</head>
<body>

<!--footer-->
<footer>
    <div class="foot clearfix">
        <ul class="fl" style="    margin-top: 44px;">
            <p>闽ICP备16030544号-1  热线电话：0592-5937233  服务时间：周一至周五 8：00-18:00 </p>
        </ul>
        <div class="fl" style="margin-left: 20px; margin-top: 10px;">
            <img src="images/qcode.png" width="100" height="100">
        </div>
    </div>
</footer>

<script type="text/javascript">
    $.ajax({
                    url :Server + "/visitRecord/getRecord",
                    //url :"http://localhost/visitRecord/getRecord",
                    data : {
                    },
                    type : "POST",
                    dataType: "JSON",
                    success : function(data) {
                        $("#total").text(data.allRecord);
                        $("#today").text(data.todayRecord);
                        //$(".foot").append("<p>总访问量："+data.allRecord+"  今日访问量："+data.todayRecord+"</p>");
                    },
            }); 

</script>
</body>
</html>