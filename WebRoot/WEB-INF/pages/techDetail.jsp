<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

	<head>
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<title>仪器共享</title>

		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">

		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。

">
<!-- 		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
 -->		<link rel="stylesheet" href="new_css/publishService.css">
		<link rel="stylesheet" href="css/new_style.css">
		<link rel="stylesheet" href="css/common.css">
		<script src="new_js/jquery-1.11.3.js"></script>
		<script src="method/dwy/techDetail.js"></script>
		<script >
		//load();
		//next();
		$(document).ready(function(){
			load(GetQueryString("techId"));
			 //particulars(GetQueryString("needId"));
			// next(GetQueryString("id"));
			//describe(GetQueryString("needId"));
			//$("#shangchuan").click(function(rinfoId){
			//	rinfoId = $('#4').val();
			//    $(location).attr('href', 'cgdaizhifu37_2?id='+rinfoId+'');
  			//});
			});
			function GetQueryString(name) {
				var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
				var r = window.location.search.substr(1).match(reg);
				if(r != null) return unescape(r[2]);
				return null;
			}
			
		</script>
		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$(this).addClass('selected').siblings().removeClass('selected');
				});
			});
		</script>

		<style type="">
		.mainList .content{
			    padding-top: 33px;
		}
			.mainList .content{
				    margin-top: 20px;
			}
			.mainList .bNav{
/*				height: 200px; 
*/    /* line-height: 226px; */
    font-size: 16px;
    color: #202020;
    clear: both;
    /* margin-top: 20px; */
    padding-top: 100px;
			}
			.logo {
    height: 80px;
    line-height: 200px;
}
		</style>

		<style type="">
			.text-overflow {
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
}
body {
  padding: 0;
  background: #f7f7f7;
}
.text-overflow {
  overflow: hidden;
  word-break: break-all;
  white-space: nowrap;
}
.fl {
  float: left;
}
.fr {
  float: right;
}
.clear,
.clearfix {
  clear: both;
}
.text-center {
  text-align: center;
}
.text-left {
  text-align: left;
}
.text-right {
  text-align: right;
}
.img-responsive {
  width: 100%;
}
.container {
 /* width: 1200px;*/
  margin: 0 auto;
  clear: both;
  
}
body,
* {
  padding: 0;
  margin: 0;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  font-family: "Microsoft YaHei", "Consolas", "Monaco", "Bitstream Vera Sans Mono", "Courier New", Courier, monospace;
  letter-spacing: 1px;
}
a {
  text-decoration: none;
  color: #666;
}
.flex {
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
}
ul,
li {
  list-style: none ;
}
/*头部样式*/
a:hover {
  text-decoration: none;
}
#header {
  background: #333;
  color: #afb5c1;
  height: 38px;
  line-height: 38px;
  font-size: 16px;
}
#header div.container:after {
  display: block;
  content: '';
  clear: both;
  height: 0;
}
#header div.container div.company_name span {
  font-size: 15px;
}
#header div.container div.company_name span a {
  overflow: hidden;
  max-width: 258px;
  display: inline-block;
  vertical-align: middle;
  height: 20px;
  line-height: 20px;
  position: relative;
  top: -2px;
}
#header div.container div.company_name > span.identify {
  background: #a4a5a9;
  color: #33383e;
  font-size: 14px;
  -webkit-border-radius: 8px;
  -moz-border-radius: 8px;
  border-radius: 8px;
  padding: 0 5px;
}
#header div.container div.fr > div ul a {
  display: block;
}
#header div.container div.fr > div ul a:hover {
  background: #ddd;
  color: #f90;
}
#header div.container div.fr a {
  color: #afb5c1;
}
#header div.container div.fr a:hover {
  text-decoration: none;
}
#header div.container div.fr a.message_alert:before {
  display: inline-block;
  position: relative;
  height: 30px;
  top: 3px;
  margin-right: 30px;
  content: url(../images/message_alert.jpg);
}
#header div.container div.fr .user_name {
  float: right;
  text-align: center;
}
#header div.container div.fr .user_name:after {
  display: block;
  content: '';
  clear: both;
  height: 0;
}
#header div.container div.fr .user_name:hover {
  background: #1a1a1a;
}
#header div.container div.fr .user_name:hover ul.dropdown {
  display: block;
  z-index: 10;
  list-style: none;
}
#header div.container div.fr .user_name b {
  display: inline-block;
  height: 20px;
  width: 20px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  background: #73bf65;
  font-weight: normal;
  position: relative;
  font-size: 12px;
  color: #fff;
  text-align: center;
  line-height: 20px;
}
#header div.container div.fr .user_name > div {
  float: right;
  max-width: 188px;
  overflow: hidden;
  height: 38px;
  position: relative;
  margin-left: 5px;
  cursor: pointer;
  display: inline-block;
}
#header div.container div.fr .user_name > div ul.dropdown {
  position: absolute;
  display: none;
  list-style: none;
  background: #fff;
  -webkit-box-shadow: inset 0 0 5px #ddd;
  -moz-box-shadow: inset 0 0 5px #ddd;
  box-shadow: inset 0 0 5px #ddd;
  top: 38px;
  right: 0;
  width: 106px;
  text-align: center;
}
#header div.container div.fr .user_name > div ul.dropdown a {
  display: block;
}
#header div.container div.fr .user_name > div ul.dropdown a:hover {
  background: #ddd;
  color: #f60;
}
#header div.container div.fr > div.drop {
  min-width: 90px;
  margin-left: 20px;
  text-align: center;
  display: inline-block;
  position: relative;
}
#header div.container div.fr > div.drop:hover .drop-down{
  display: block;
}
#header div.container div.fr > div.drop:hover a.news{
  color: #f60;
}
#header div.container div.fr >div.drop .drop-down{
  position: absolute;
  display: none;
  width: 300px;
  z-index: 100;
  text-align: left;
  right: -104px;
  background: #fcfcfc;
  -webkit-box-shadow: inset 0 0 5px #ddd;
  -moz-box-shadow: inset 0 0 5px #ddd;
  box-shadow: inset 0 0 5px #ddd;
}
#header div.container div.fr >div.drop .drop-down>ul.tab{
  position: relative;
  height: 40px;
  text-align: center;
  border-bottom: 1px solid #ddd;
}
#header div.container div.fr >div.drop .drop-down>ul.tab:after {
  display: block;
  content: '';
  clear: both;
  height: 0;
}
#header div.container div.fr >div.drop .drop-down>ul.tab  i.line{
  display: inline-block;
  width: 1px;
  height: 20px;
  background: #ddd;
  position: absolute;
  left: 50%;
  margin-top: 10px;
}
#header div.container div.fr >div.drop .drop-down>ul.tab li{
  float: left;
  width: 50%;
  height: 22px;
  cursor: pointer;
}
#header div.container div.fr >div.drop .drop-down>ul.tab li.words{
  background: url(../images/new-gout.png) no-repeat;
  margin-top:10px; 
  background-position: 70px 0;
}
#header div.container div.fr >div.drop .drop-down>ul.tab li.envelope{
  background: url(../images/xin.png) 64px 0 no-repeat;
  margin-top:12px;
}
#header div.container div.fr >div.drop .drop-down>ul.tab li.envelope:hover{
  background: url(../images/new-xin.png) 64px 0 no-repeat;
}
#header div.container div.fr >div.drop>.drop-down .tab-content ul{
  height: 275px;
  max-height: 275px;
  overflow-y: auto;  
  margin: 0;     
}
::-webkit-scrollbar{
  width:6px;
  background: #c3c3c3;
}
::-webkit-scrollbar-track{  
  border-radius: 10px;  
  background-color: #ddd;  
}  
::-webkit-scrollbar-thumb{
  border-radius: 3px;
}
#header div.container div.fr >div.drop>.drop-down .tab-content .no-news{
  height: 320px;
  text-align: center;
  font-size: 14px;
  color: #999;
  display: none;
  vertical-align: middle;
}
#header div.container div.fr >div.drop>.drop-down .tab-content ul li{
  border-bottom: 1px solid #ddd; 
}
#header div.container div.fr >div.drop>.drop-down .tab-content ul li:hover{
  background: #ddd;
}
#header div.container div.fr >div.drop>.drop-down .tab-content ul li a{
  color: #666;
  padding: 0 20px;
  overflow: hidden;
  text-overflow:ellipsis;
  white-space: nowrap;
  font-size: 14px;
}
#header div.container div.fr >div.drop>.drop-down .tab-content ul li p{
  color: #999;
  padding: 0 20px;
  margin-top: -6px;
  overflow: hidden;
  text-overflow:ellipsis;
  white-space: nowrap;
  font-size: 12px;
}
#header div.container div.fr >div.drop>.drop-down .tab-content .messages{
  display: none;
}
#header div.container div.fr >div.drop>.drop-down .tab-content .news-list a{
  color: #666;
  overflow: hidden;
  text-overflow:ellipsis;
  white-space: nowrap;
  font-size: 14px;
}
#header div.container div.fr >div.drop>.drop-down .tab-content .news-list span.fr{
  display: inline-block;
  height: 16px;
  width: 16px;
  color: #fff;
  background: #f60;
  border-radius: 50%;
  text-align: center;
  line-height: 16px;
  margin-top: 8px;
  font-size: 12px;
  letter-spacing: 0;
}
#header div.container div.fr >div.drop>.drop-down .tab-content .news-list a.look-all{
  display: block;
  text-align: center;
  font-size: 14px;
  color: #666;
  line-height: 40px;
}
#header div.container div.fr >div.drop>.drop-down .tab-content .messages > ul.new-information li:first-child a{
  color: #666;
}
#header div.container div.fr .messages > ul.new-information li:nth-child(2) a{
  color: #666;
}
#header div.container div.fr .messages>a.look-all{
  display: block;
  text-align: center;
  font-size: 14px;
  color: #666;
  line-height: 40px;
}
#header div.container div.fr > div ul a {
  display: block;
}
#header div.container div.fr > div ul a:hover {
  background: #ddd;
  color: #f90;
}
#header div.container div.fr > a {
  display: inline-block;
  padding-left: 15px;
}
#header div.container div.items {
  padding-left: 270px;
}
#header div.container div.items ul.flex {
  width: 616px;
  font-size: 16px;
}
#header div.container div.items ul.flex li {
  -webkit-box-flex: 1;
  -moz-box-flex: 1;
  -ms-flex: 1;
  width: 100%;
  font-size: 16px;
}
#header div.container div.items ul.flex li a {
  color: #afb5c1;
  display: block;
  text-align: center;
  font-size: 16px;
}
#header div.container div.items ul.flex li:hover {
  background: #171a1d;
}
#header div.container div.items ul.flex li.active {
  background: #171a1d;
}
#header div.container div.items ul.flex li.active a {
  color: #f5f5f3;
}
#header div.container div.items ul.flex li.get_custom {
  position: relative;
  text-align: center;
  cursor: default;
  z-index: 10000;
}
#header div.container div.items ul.flex li.get_custom:hover {
  background: #171a1d;
}
#header div.container div.items ul.flex li.get_custom:hover ul {
  display: block;
}
#header div.container div.items ul.flex li.get_custom >ul {
  position: absolute;
  background: #fff;
  width: 102px;
  display: none;
  -webkit-box-shadow: inset 0 0 5px #ddd;
  -moz-box-shadow: inset 0 0 5px #ddd;
  box-shadow: inset 0 0 5px #ddd;
  margin: 0;
}
#header div.container div.items ul.flex li.get_custom >ul li {
  line-height: 30px;
  height: 30px;
}
#header div.container div.items ul.flex li.get_custom >ul a {
  display: block;
}
#header div.container div.items ul.flex li.get_custom >ul a:hover {
  background: #ddd;
  color: #f60;
}
#header div.container div.fr > div.drop:hover .drop-down {
  display: block;
}
#header div.container div.fr > div.drop:hover a.news {
  color: #f60;
}
#front_header {
  height: 39px;
  background: #fff;
  line-height: 39px;
  font-size: 13px;
  color: #666;
  border-bottom:3px solid #f8f0ea;
  text-align: center;
}
#front_header .header_wrap {
	/*width:1200px;*/
	display:inline-block;
	height:100%;
}
#front_header .header_wrap .fl img {
	vertical-align:initial;
}
#front_header .header_wrap .right {
	height:100%;
	padding:0 5px;
	position: relative;
}
#front_header .header_wrap .right:hover {
	background:#f4f4f4;
	cursor:pointer;
}
#front_header .header_wrap .fr .main {
	display:inline-block;
	height:100%;
}
#front_header .header_wrap .fr .main .user_pic {
	height:100%;
	/*line-height:36px;*/
    margin-right:20px;

}
#front_header .header_wrap .fr .main .user_pic img {
	vertical-align: middle;
}
#front_header .header_wrap .fr .main .user_name {
	padding-left:5px;
	padding-right:5px;
}
#front_header .header_wrap .fr .out {
	position: absolute;
	top:36px;
	left:0;
	width:100%;
	height:82px;
	z-index:100;
	box-shadow: 0 2px 10px 1px #999;
	display:none;
}
#front_header .header_wrap .fr .out>div{
	height:50%;
}
#front_header .header_wrap .fr .out .user_center {
	width:100%;
	background:#ff8a00;
	
}
#front_header .header_wrap .fr .out .user_center a {
	color:#fff;
}
#front_header .header_wrap .fr .out .log_out {
	width:100%;
	background:#fff;
}
#front_header .header_wrap .fr .out .tri {
	position: absolute;
	top:-6px;
	left:0;
	width:100%;
	text-align: center;
	height:6px;
	font-size:0;
	line-height:6px;
}
#front_header .header_wrap .fr .out .tri img {
	height:100%;
}

#front_header:after {
  display: block;
  content: '';
  clear: both;
  height: 0;
}

div#nav_top {
  height: 90px;
  background:url(image/sy_bg.png) no-repeat center;
  height:175px;
}
div#nav_top:after {
  display: block;
  content: '';
  clear: both;
  height: 0;
}
/*div#nav_top > div.container > div.fl {
  width: 340px;
  padding-top: 50px;
}
div#nav_top > div.container > div.fl:after {
  display: block;
  content: '';
  clear: both;
  height: 0;
}*/
/*div#nav_top > div.container > div.fl > div.logo {
  float: left;
  border-right: 1px solid #ddd;
  padding-right: 15px;
}*/
div#nav_top > div.container > div.fl > div.web_name {
  font-size: 22px;
  color: #585858;
  margin-left: 15px;
  line-height: 22px;
  float: left;
  margin-top: 11.5px;
}
div#nav_top > div.container > div.fl > div.web_name p {
	font-size:14px;
}
div#nav_top > div.container > div.fr {
  width: 700px;
}
div#nav_top > div.container > div.fr:after {
  display: block;
  content: '';
  clear: both;
  height: 0;
}
div#nav_top > div.container > div.fr > img {
  margin-top: 22px;
}
div#nav_top > div.container > div.fr a.publish {
  float: right;
  margin-top: 38px;
  margin-right: 7.36040609%;
  height: 36px;
  width: 14.97461929%;
  border: 2px solid #f60;
  text-align: center;
  color: #f60;
  line-height: 34px;
  font-size: 14px;
}
div#nav_top > div.container > div.fr a.publish:before {
  display: inline-block;
  content: url(../images/publish.png);
  position: relative;
  top: 2px;
  left: 0;
  margin-right: 8px;
  line-height: 16px;
}
/*头部的搜索*/
#header_form {
  float: left;
  width: 100%;
  margin-top: 68px;
  position:relative;
}
#header_form  img {
	position: absolute;
	top:50%;
	left:10px;
	transform: translateY(-50%);
	-webkit-transform: translateY(-50%);
	-moz-transform: translateY(-50%);
	-ms-transform: translateY(-50%);
}
#header_form:after {
  display: block;
  content: '';
  clear: both;
  height: 0;
}
#header_form input {
  width: 84.24242424%;
  height: 36px;
  float: left;
  padding: 0 45px;
  border: 2px solid #f60;
  border-right: none;
  outline: none;
  font-size:12px;
}
#header_form button {
  float: left;
  width: 15.75757576%;
  height: 36px;
  background: #f60;
  border: 1px solid #f60;
  color: #fff;
  font-size: 16px;
  cursor: pointer;
  outline: none;
}
#header_form ul.choose li {
  float: left;
  width: 50px;
  height: 20px;
  font-size: 14px;
  color: #fe7e3f;
  text-align: center;
  cursor: pointer;
}
#header_form ul.choose li:hover {
  background-color: #ff6600;
  color: #fff;
}
#header_form ul.choose li.visited {
  background-color: #ff6600;
  color: #fff;
}
#footer {
  background: #f9f9f9;
  color: #6f6f6f;
  border-top: 2px solid #ff8a00;
  height:87px;
  line-height:85px;
  text-align: center;
}

#msg-notice {
  display: none;
  position: absolute;
  background: #f60;
  font-size: 11px;
  border-radius: 50%;
  display: block;
  width: 16px;
  height: 16px;
  line-height: 16px;
  right: -23px;
  z-index: 1;
  color: #fff;
  padding: 0;
  top: 1px;
}

#machines_wrap {
	background:url(images/sy_bgb.png) no-repeat;
	background-size:100% 100%;
}
/*幻灯片样式*/
.owl-theme .owl-controls .owl-pagination {
    position: absolute;
    bottom: 25px;
    left: 0;
    right: 0;
    text-align: center;
}
.owl-controls .owl-page{
    display: inline-block;
    cursor: pointer;
    width: 8px !important;
    height: 8px !important;
    border-radius: 50%  !important;
    background: #e6e6e6;
    margin-left: 10px;
    -webkit-transition: background 0.5s;
    transition: background 0.5s;
}

.owl-controls .owl-page.active,.owl-controls .owl-page:hover {
    background: #ed1717;
}
/*.owl-theme .owl-controls .owl-buttons div {
    position: absolute;
    top: 50%;
    margin-top: -20px;
    width: 40px;
    height: 40px;
    padding: 0;
    -webkit-transition: background 0.5s, opacity 0.5s;
    transition: background 0.5s, opacity 0.5s;
}*/
/*.owl-theme .owl-controls .owl-buttons .owl-prev{
    background:rgba(0,0,0,0.4) url("../images/icon_prev.png");
    left: 10px;
    opacity: 0.8;
}
.owl-theme .owl-controls .owl-buttons .owl-next{
    background:rgba(0,0,0,0.4) url("../images/icon_next.png");
    right: 10px;
    opacity: 0.8;
}
.owl-theme .owl-controls .owl-buttons .owl-prev:hover{
    background:rgba(0,0,0,0.8) url("../images/icon_prev2.png");
    left: 10px;
}
.owl-theme .owl-controls .owl-buttons .owl-next:hover{
    background:rgba(0,0,0,0.8) url("../images/icon_next2.png");
    right: 10px;
}*/

		</style>

	</head>



	<body style="background:#fff;">

		<%@include file="/head.jsp"%>

		<div class="" id="nav_top">
			<div class="container">
      <style type="text/css">
        .xinxin {
    width: 1200px;
    margin: 0 auto;
}
      </style>
				<div class="xinxin">
					<div class="logo ">
	<a href="#this" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>

	</div>
					<div class="web_name">
						<!--<img src="images/icon3.png" />-->
					<div>
			</div>
		</div>
		<div class="tab_top" style="display:none;">
			<div class="tab_top_content">
				<div class="item1 "><a href="#this">仪器管理与分布 <img src="images/insUser_icon.png" /></a></div>
				<div class="item2 ">
					<a href="#this">预约订单管理</a>
				</div>
				<div class="item2 selected">
					<a href="#this">实验订单管理</a>
				</div>
			</div>
		</div>
		<div class="mainList">
			<div class="bNav">您的位置：首页》用户中心》技术方案管理<span>》方案详情</span></div>
			<div class="content">
				
				<div class="right">
					<!--<div class="title"><span>仪器订单</span></div>-->
					<ul>
						<!--<li>
							<label>使用时间</label>
							<input type="text" name="" id="" value="" />
						</li>-->
						
						
						
						<!--<li class="sun_item">
						   <div class="sun_item_div1">仪器信息</div>
						   <div class="sun_item_div2">
						   	<div class="sun_item_img"> 
						   	<img src="images/productDetailPic.png" />
						   	</div>
						   	<div class="item_box">
						   		<br />
						   		<p>高内涵筛选系统</p>
						   	<br />s
						   	
						   		<p>opertter</p>
						   		<br />
						   
						   		<div>仪器综合评分：<ul class="comment">  
    <li style="margin-bottom: 0px;">☆</li>  
    <li style="margin-bottom: 0px;">☆</li>  
    <li style="margin-bottom: 0px;">☆</li>  
    <li style="margin-bottom: 0px;">☆</li>  
    <li style="margin-bottom: 0px;">☆</li>  
</ul></div>
						   	</div>
						   </div>
						</li>-->
						<!--<li class="sun_item2">
							<div>
							<div class="sun_item2_row"></div>
							<div class="sun_item2_row2">
								
							</div>
							<textarea></textarea>
							</div>
						</li>-->
						<!--<li class="item2">
							<label>添加图片</label>
							<div class="addPic">
								<div class="pic">
									<img src="images/addPIC.png"/>
									<img src="images/delete.png" class='delete'/>
								</div>
								<div class="fill">
									<label for="fillPic"><img src="images/add.png"/></label>
									<input type="file" name="" id="fillPic" value="" />
								</div>
							</div>
						</li>-->
						<li>
							<label>方案名称：</label>
							<input type="text" placeholder="" name="" id="0" value="" />
						</li>
						<li>
							<label>行业：</label>
							<input type="text" placeholder="" name="" id="1" value="" />
						</li>
						<li>
							<label>所在地区：</label>
							<input type="text" placeholder="" name="" id="2" value="" />
						</li>
						<li>
							<label>团队情况：</label>
							<input type="text" placeholder="" name="" id="3" value="" />
						</li>
						<li>
							<label>合作说明：</label>
							<input type="text" placeholder="" name="" id="4" value="" />
						</li>
						<li>
							<label>技术专利：</label>
							<input type="text" placeholder="" name="" id="5" value="" />
						</li>
						<li>
							<label>案例：</label>
							<input type="text" placeholder="" name="" id="6" value="" />
						</li>
						<li>
							<label>联系人：</label>
							<input type="text" placeholder="" name="" id="7" value="" />
						</li>
						<li>
							<label>联系电话：</label>
							<input type="text" placeholder="" name="" id="8" value="" />
						</li>
						<li>
							<label>联系邮箱：</label>
							<input type="text" placeholder="" name="" id="9" value="" />
						</li>
						<!--
                        	<li class="item8">
								<label>上传文档</label>
								<label for="commitFile"><img src="images/commitFile.png"/></label>
								<input style="width: 0px;height: 0px; opacity:0;" type="file" name="" id="commitFile" value="" />
						</li>
                        -->
                        <li>
                        	<label>文档路径：</label>
                        	<div class="row" id="doc" style="display:inline"></div>
                        	<!-- <input type="text"  name="" id="10" value="" /> -->
                        </li>
						<li>
							<label>支付方式：</label>
							<input type="text" placeholder="" name="" id="11" value="" />
						</li>
						<li>
							<label>支持详情：</label>
							<input type="text" placeholder="" name="" id="12" value="" />
						</li>
						<li>
							<label>支持要求：</label>
							<input type="text" placeholder="" name="" id="13" value="" />
						</li>
						<li>
							<label>所属机构：</label>
							<input type="text" placeholder="" name="" id="14" value="" />
						</li>
						<li>
							<label>简介：</label>
							<input type="text" placeholder="" name="" id="15" value="" />
						</li>
						<li>
							<label>简历：</label>
							<div class="row" id="doc1" style="display:inline"></div>
							<!-- <input type="text" placeholder="" name="" id="16" value="" /> -->
						</li>
						<!--
                        	<li class="item8">
								<label>上传简历</label>
								<label for="commitFile"><img src="images/commitFile.png"/></label>
								<input style="width: 0px;height: 0px; opacity:0;" type="file" name="" id="commitFile" value="" />
						</li>
                        -->
						
						<!--<li class="item11">
							<label>仪器应用项目：</label>
							<select>
								<option>请选择仪器</option>
							</select>
						</li>-->
						
						<!--<li>
							<label>测试样品</label>
							<input type="text" name="" id="" value="" />
						</li>
						<li>
							<label>联系人</label>
							<input type="text" name="" id="" value="" />
						</li>
						
						<li>
							<label>联系电话</label>
							<input type="text" name="" id="" value="" />
						</li>
						
						<li class="item8">
								<label>测试项目</label>
								<label for="commitFile"><img src="images/commitFile.png"/></label>
								<input style="width: 0px;height: 0px; opacity:0;" type="file" name="" id="commitFile" value="" />
						</li>-->
						<!--<li>
							<label>地址</label>
							<input type="text" name="" id="" value="" />
						</li>
						<li>
							<label>服务结果获取时间</label>
							<input type="text" name="" id="" value="" />
						</li>
						<li class="item10">
							<label><span>*</span>有效时间</label>
							<select>
								<option>2018-4-9</option>
							</select>
							<span>到</span>
							<select>
								<option>2018-4-9</option>
							</select>
						</li>-->
						
<!--						
						<li class="item4">
							<label>备注：</label>
							<textarea></textarea>
						</li>-->
						<!--<li class="item12">
							<label></label>
							<table>
									<tr>
										<th>序号</th>
										<th>测试内容</th>
										<th>数量</th>
										<th>预算费用</th>
										<th>样品描述与测试要求</th>
									</tr>
									<tr>
										<td>1</td>
										<td>元素测定</td>
										<td>1</td>
										<td>150</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>元素测定</td>
										<td>1</td>
										<td>150</td>
										<td></td>
									</tr>
								</table>
						</li>
					-->
					</ul>
					<!-- <button ><a href="lab_order_wait9.html" style="color: white;">确定上传</a></button>-->
				</div>
			</div>
			<div class="pageButton"></div>
			
			<!--找个分页插件插入-->
     
		</div>
</div>
		<%@include file="/footer.jsp"%>

		</div>
		<script src="js/jquery-1.11.3.js"></script>  
<script>  
    $(function () {  
        var wjx_k = "☆";  
        var wjx_s = "★";  
        //prevAll获取元素前面的兄弟节点，nextAll获取元素后面的所有兄弟节点  
        //end 方法；返回上一层  
        //siblings 其它的兄弟节点  
        //绑定事件  
        $(".comment li").on("mouseenter", function () {  
            $(this).html(wjx_s).prevAll().html(wjx_s).end().nextAll().html(wjx_k);  
        }).on("click", function () {  
            $(this).addClass("active").siblings().removeClass("active")  
        });  
        $(".comment").on("mouseleave", function () {  
            $(".comment li").html(wjx_k);  
            $(".active").text(wjx_s).prevAll().text(wjx_s);  
        })  
    });  
</script>  


	</body>

</html>