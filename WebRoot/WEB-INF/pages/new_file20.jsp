<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<title>仪器共享</title>
<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">
<meta name="description"
	content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。">
<link rel="stylesheet" href="new_css/header_footer.css?t=1">
	<link rel="stylesheet" href="new_css/marineEquipment.css">
	<link rel="stylesheet" href="css/common.css">

<style>
ul, ol {
	padding: 0;
}

.banner {
	position: relative;
	overflow: auto;
	text-align: center;
}

.banner li {
	list-style: none;
}

.banner ul li {
	float: left;
}

.banner ul li img {
	width: 100% !important;
	max-height: 330px !important;
}

#b04 {
	width: 100%;
}

#b04 .dots {
	position: absolute;
	left: 0;
	right: 0;
	bottom: 20px;
}

#b04 .dots li {
	display: inline-block;
	width: 10px;
	height: 10px;
	margin: 0 4px;
	text-indent: -999em;
	border: 2px solid #fff;
	border-radius: 6px;
	cursor: pointer;
	opacity: .4;
	-webkit-transition: background .5s, opacity .5s;
	-moz-transition: background .5s, opacity .5s;
	transition: background .5s, opacity .5s;
}

#b04 .dots li.active {
	background: #fff;
	opacity: 1;
}

#b04 .arrow {
	position: absolute;
	top: 200px;
}

#b04 #al {
	left: 15px;
	top: 45%;
}

#b04 #ar {
	right: 15px;
	top: 45%;
}
.specialist li .item{
  margin-top: 50px;
}


.mainList .content .box3 .box_content ul li>div:hover{
 border:1px solid rgb(255,138,0)!important;
}
 .result li:hover{
   border-bottom:1px solid rgb(255,138,0)!important;
} 

.mainList .content .box2 .box_content ul li>div:hover{
   border:1px solid rgb(255,138,0)!important;
       box-sizing: border-box;
}
/* .box_content .result li:hover{
   border-bttom:1px solid rgb(255,138,0);
} */


/*.mainList .content .box3 .box_content ul li>div{
	padding: 10px 10px 0px 10px;
}
*/

</style>
<link rel="stylesheet" href="css/common.css">
<style type="">
	
	
</style>
<script src="js/jquery-1.11.3.js"></script>
<script src="js/unslider.min.js"></script>

<script>
	$(function() {
		$('#front_header .header_wrap .right').hover(function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
			$('#front_header .header_wrap .right .out').show();
		}, function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
			$('#front_header .header_wrap .right .out').hide();
		});
		$('#sorts').on('mouseover', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon4.png');
		});
		$('#sorts').on('mouseout', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon5.png');
		});

		$('.mainList .content .left li').click(function() {
			$('.mainList .content .left li').removeClass('selected');
			$(this).addClass('selected');
		});
		$('.mainList .content .left .list_header').click(function() {
			if ($('.mainList .content .left .secendList').is(':hidden')) {
				$('.mainList .content .left .secendList').slideDown();
				$('.mainList .content .left .list_header img').attr("src", "images/insUser_icon.png");
			} else {
				$('.mainList .content .left .secendList').slideUp();
				$('.mainList .content .left .list_header img').attr("src", "images/icon4.png");
			}
			;

		});
		$("#row5 font label").click(function() {
			$("#row5 font label img").attr('src', 'images/icon35.png');
			$("#row5 font label").siblings('input').prop('checked', false);
			$(this).find('img').attr('src', 'images/icon36.png');
			$(this).siblings('input').prop('checked', true);
		});
		$("#row8 font label").click(function() {
			$("#row8 font label img").attr('src', 'images/icon35.png');
			$("#row8 font label").siblings('input').prop('checked', false);
			$(this).find('img').attr('src', 'images/icon36.png');
			$(this).siblings('input').prop('checked', true);
		});
		//					轮播图
		var unslider04 = $('#b04').unslider({
				dots : true
			}),
			data04 = unslider04.data('unslider');
			console.log(data04);

		$('.unslider-arrow04').click(function() {
			var fn = this.className.split(' ')[1];
			data04[fn]();
		});
	});
</script>

</head>

<body style="background:#fff;">

	<%@include file="/head.jsp"%>

	<div class="" id="nav_top">
		<div class="container">
			<div class="fl">
				<div class="logo ">


	<a href="index" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>
				</div>
				<!-- <div class="web_name">用户中心</div> -->
			</div>
		</div>
	</div>
	<div class="title">
		<a href="#this">海洋装备</a>
	</div>
	<div class="banner" id="b04">
		<ul>
			<li><img src="images/001.png" alt="" width="640" height="480"></li>
			<li><img src="images/002.png" alt="" width="640" height="480"></li>
			<li><img src="images/003.png" alt="" width="640" height="480"></li>
			<!-- <li><img src="images/04.jpg" alt="" width="640" height="480"></li>
			<li><img src="images/05.jpg" alt="" width="640" height="480"></li> -->
		</ul>
		<a href="javascript:void(0);" class="unslider-arrow04 prev"><img
			class="arrow" id="al" src="images/arrowl.png" alt="prev" width="20"
			height="35"></a> <a href="javascript:void(0);"
			class="unslider-arrow04 next"><img class="arrow" id="ar"
			src="images/arrowr.png" alt="next" width="20" height="37"></a>
	</div>
	<div class="mainList">
		<div class="content" id="height-content">
			<div class="box box1">
				<div class='box_title'>
					<div>科研成果展示</div>
					<div>
						<img src="images/marineEquipmentpic.png" />
					</div>
					<div class="more">
						<a onclick="result_list()"><img src="images/more2.png" /></a>
					</div>
				</div>
				<div class="box_content">
					<ul class="result">
						<li>
							<div class='pic'>
								<img src="images/ma.png" />
							</div>
							<div class="item">
								<div class="row1"></div>
								<div class="row2 row2-active">正渗透海水淡化系统</div>
								<div class="row3">成熟度：正在研发</div>
								<div class="row4">¥2000元</div>
							</div>
						</li>
						<li>
							<div class='pic'>
								<img src="images/ma.png" />
							</div>
							<div class="item">
								<div class="row1"></div>
								<div class="row2">正渗透海水淡化系统</div>
								<div class="row3">成熟度：正在研发</div>
								<div class="row4">¥2000元</div>
							</div>
						</li>
						<li class="selected">
							<div class='pic'>
								<img src="images/ma.png" />
							</div>
							<div class="item">
								<div class="row1"></div>
								<div class="row2">正渗透海水淡化系统</div>
								<div class="row3">成熟度：正在研发</div>
								<div class="row4">¥2000元</div>
							</div>
						</li>
						<li>
							<div class='pic'>
								<img src="images/ma.png" />
							</div>
							<div class="item">
								<div class="row1"></div>
								<div class="row2">正渗透海水淡化系统</div>
								<div class="row3">成熟度：正在研发</div>
								<div class="row4">¥2000元</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="box box2">
				<div class='box_title'>
					<div>科研需求展示</div>
					<div>
						<img src="images/marineEquipmentpic.png" />
					</div>
					<div class="more">
						<a onclick="Marine_demand_list()"><img src="images/more2.png" /></a>
					</div>
				</div>
				<div class="box_content">
					<ul class="demand">
						<li>
							<div>
								<div class="row1">一种感应发亮的鞋底的实用新型专利定制实用新型专实用一种感应发亮的鞋底的实用新型专利定制实用新型专实用</div>
								<div class="row2">
									<span>需求简介：</span>一种感应发亮的鞋底的实用新型专利定制定感应发亮的鞋应发亮的鞋
								</div>
								<div class="row3">预算：5-10万</div>
							</div>
						</li>
						<li class="selected">
							<div>
								<div class="row1">一种感应发亮的鞋底的实用新型专利定制实用新型专实用一种感应发亮的鞋底的实用新型专利定制实用新型专实用</div>
								<div class="row2">
									<span>需求简介：</span>一种感应发亮的鞋底的实用新型专利定制定感应发亮的鞋应发亮的鞋
								</div>
								<div class="row3">预算：5-10万</div>
							</div>
						</li>
						<li>
							<div>
								<div class="row1">一种感应发亮的鞋底的实用新型专利定制实用新型专实用一种感应发亮的鞋底的实用新型专利定制实用新型专实用</div>
								<div class="row2">
									<span>需求简介：</span>一种感应发亮的鞋底的实用新型专利定制定感应发亮的鞋应发亮的鞋
								</div>
								<div class="row3">预算：5-10万</div>
							</div>
						</li>
						<li>
							<div>
								<div class="row1">一种感应发亮的鞋底的实用新型专利定制实用新型专实用一种感应发亮的鞋底的实用新型专利定制实用新型专实用</div>
								<div class="row2">
									<span>需求简介：</span>一种感应发亮的鞋底的实用新型专利定制定感应发亮的鞋应发亮的鞋
								</div>
								<div class="row3">预算：5-10万</div>
							</div>
						</li>
					</ul>
				</div>
			</div>


<!-- <style type="">
	.pic img {
    width: 100% !important;
    height: 100%  !important; 
}
</style> -->

			<div class="box box3">
				<div class='box_title'>
					<div>专家展示</div>
					<div>
						<img src="images/marineEquipmentpic.png" />
					</div>
					<div class="more">
						<a onclick="specialist_list()"><img src="images/more2.png" /></a>
					</div>
				</div>
				<div class="box_content">
					<ul class="specialist">
						<li>
							<div>
								<div class='pic-change'>
									<img src="images/marineEquipmentpic2.png" />
								</div>
								<div class="item">
									<div class="row1">李斯</div>
									<div class="row2">甘肃省科学院生物研究所</div>
									<div class="row3">研究员/副所长</div>
									<!-- <div class="row4">
										<a href="#this">立即咨询</a> <a href="#this">方案评审</a>
									</div> -->
								</div>
							</div>
						</li>
						<li>
							<div>
								<div class='pic'>
									<img src="images/marineEquipmentpic2.png" />
								</div>
								<div class="item">
									<div class="row1">李斯</div>
									<div class="row2">甘肃省科学院生物研究所</div>
									<div class="row3">研究员/副所长</div>
									<!-- <div class="row4">
										<a href="#this">立即咨询</a> <a href="#this">方案评审</a>
									</div> -->
								</div>
							</div>
						</li>
						<li>
							<div>
								<div class='pic'>
									<img src="images/marineEquipmentpic2.png" />
								</div>
								<div class="item">
									<div class="row1">李斯</div>
									<div class="row2">甘肃省科学院生物研究所</div>
									<div class="row3">研究员/副所长</div>
									<!-- <div class="row4">
										<a href="#this" class="selected">立即咨询</a> <a href="#this">方案评审</a>
									</div> -->
								</div>
							</div>
						</li>
						<li>
							<div>
								<div class='pic'>
									<img src="images/marineEquipmentpic2.png" />
								</div>
								<div class="item">
									<div class="row1">李斯</div>
									<div class="row2">甘肃省科学院生物研究所</div>
									<div class="row3">研究员/副所长</div>
									<!-- <div class="row4">
										<a href="#this">立即咨询</a> <a href="#this" class="selected">方案评审</a>
									</div> -->
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="box box2">
				<div class='box_title'>
					<div>发布难题</div>
					<div>
						<img src="images/marineEquipmentpic.png" />
					</div>
					<div class="more">
						<a onclick="Marine_equipment_list()"><img
							src="images/more2.png" /></a>
					</div>
				</div>
				<div class="box_content">
					<ul class="equipment">
						<li>
							<div>
								<div class="row1">一种感应发亮的鞋底的实用新型专利定制实用新型专实用一种感应发亮的鞋底的实用新型专利定制实用新型专实用</div>
								<div class="row2">
									<span>需求简介：</span>一种感应发亮的鞋底的实用新型专利定制定感应发亮的鞋应发亮的鞋
								</div>
								<div class="row3">预算：5-10万</div>
							</div>
						</li>
						<li>
							<div>
								<div class="row1">一种感应发亮的鞋底的实用新型专利定制实用新型专实用一种感应发亮的鞋底的实用新型专利定制实用新型专实用</div>
								<div class="row2">
									<span>需求简介：</span>一种感应发亮的鞋底的实用新型专利定制定感应发亮的鞋应发亮的鞋
								</div>
								<div class="row3">预算：5-10万</div>
							</div>
						</li>
						<li>
							<div>
								<div class="row1">一种感应发亮的鞋底的实用新型专利定制实用新型专实用一种感应发亮的鞋底的实用新型专利定制实用新型专实用</div>
								<div class="row2">
									<span>需求简介：</span>一种感应发亮的鞋底的实用新型专利定制定感应发亮的鞋应发亮的鞋
								</div>
								<div class="row3">预算：5-10万</div>
							</div>
						</li>
						<li>
							<div>
								<div class="row1">一种感应发亮的鞋底的实用新型专利定制实用新型专实用一种感应发亮的鞋底的实用新型专利定制实用新型专实用</div>
								<div class="row2">
									<span>需求简介：</span>一种感应发亮的鞋底的实用新型专利定制定感应发亮的鞋应发亮的鞋
								</div>
								<div class="row3">预算：5-10万</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<%@include file="/footer.jsp"%>
	<script>
		$(document).ready(function() {
			Marine_demand()
			specialist()
			Marine_equipment()
			result()
		})
	
		function result() {
			$.ajax({
				url :Server + "/achievement/list",
				data : {},
				type : "POST",
				dataType : "json",
				success : function(data) {
					var html = "";
					var pattern = '<li onclick="result_details(#4);">\
								<div class="pic">\
									<img src="#0" />\
								</div>\
								<div class="item">\
									<div class="row1"></div>\
									<div class="row2 row2-active">#1</div>\
									<div class="row3">成熟度：#2</div>\
									<div class="row4">¥#3</div>\
								</div>\
							</li>';
					for (var i = 0; i < data.length; i++) {
						if (i < 4) {
							var item = "";
							var order = data[i];
							var Maturity = "";
							if (order.resultMaturity == 0) {
								Maturity = "正在研发";
							} else if (order.resultMaturity == 1) {
								Maturity = "已有样品";
							} else if (order.resultMaturity == 2) {
								Maturity = "通过小试";
							} else if (order.resultMaturity == 3) {
								Maturity = "通过中试";
							} else if (order.resultMaturity == 4) {
								Maturity = "可以量产";
							}
							if(order.rp.rpayRate != null){
								var rpayRate = order.rp.rpayRate;
							}else{
								var rpayRate = "";
							}
							item += pattern
								.replace("#0", order.img.rimagePath)
								.replace("#1", order.resultName)
								.replace("#2", Maturity)
								.replace("#3", rpayRate)
								.replace("#4", order.resultId),
							html += item;
						}
					}
					$(".result").html(html);
				},
			});
		}
	
		
		function specialist() {
			$.ajax({
				url :Server + "/eulist/list",
				data : {
					"limit" : "10",
					"offset" : "0",
				},
				type : "POST",
				dataType : "json",
				success : function(data) {
					var html = "";
					var pattern = '<li>\
									<div onclick="specialist_details(#4);">\
										<div class=" pic-change"><img src="#0" /></div>\
										<div class="item">\
										<div class="row1">#1</div>\
										<div class="row2">#2</div>\
										<div class="row3">#3</div>\
										<div class="row4">\
										</div>\
									</div>\
									</div>\
								</li>';
					for (var i = 0; i < data.length; i++) {
						if (i < 4) {
							var item = "";
							var order = data[i];
							item += pattern
								.replace("#0", order.euIcon)
								.replace("#1", order.euRealname)
								.replace("#2", order.euJob)
								.replace("#3", order.euUnitName)
								.replace("#4", order.euId),
							html += item;
						}
					}
						$(".specialist").html(html);
				},
			});
		}
	
		function Marine_demand() {
			$.ajax({
				url :Server + "/problem_select/list",
				data : {
					"limit" : "10",
					"offset" : "0",
					"needContent" : "2",
				},
				type : "POST",
				dataType : "json",
				success : function(data) {
					var html = "";
					var pattern = '<li onclick="demand_details(#3);">\
									<div>\
									<div class="row1">#0</div>\
									<div class="row2"><span>需求简介：</span>#1</div>\
									<div class="row3">预算：#2万</div>\
									</div>\
								</li>';
					for (var i = 0; i < data.length; i++) {
						if (i < 4) {
							var item = "";
							var order = data[i];
							item += pattern
								.replace("#0", order.needName)
								.replace("#1", order.needExplain)
								.replace("#2", order.needBudget)
								.replace("#3", order.needId),
							html += item;
						}
					}
						$(".demand").html(html);
				},
			});
		}
	
	
		function Marine_equipment() {
			$.ajax({
				url :Server + "/problem_select/list",
				data : {
					"limit" : "10",
					"offset" : "0",
					"needContent" : "1",
				},
				type : "POST",
				dataType : "json",
				success : function(data) {
					var html = "";
					var pattern = '<li>\
									<div onclick="Marine_equipment_detailst(#3)">\
									<div class="row1">#0</div>\
									<div class="row2"><span>难题简介：</span>#1</div>\
									</div>\
								</li>';
					for (var i = 0; i < data.length; i++) {
						if (i < 4) {
							var item = "";
							var order = data[i];
							item += pattern
								.replace("#0", order.needName)
								.replace("#1", order.needExplain)
								.replace("#3", order.needId),
							html += item;
						}
					}
						$(".equipment").html(html);
				},
			});
		}
	</script>
	<script>
		function Marine_demand_list() { <!-- 海洋需求 -->
			$(location).attr("href", "new_file7");
		}
		function demand_details(id){//需求详情
			$(location).attr("href", "new_file8?needId="+id+'');
		}
		function result_details(id){//成果详情
			$(location).attr("href", "result_details18?id="+id+'');
		}
		function specialist_list() { <!-- 专家列表 -->
			$(location).attr("href", "scienic_expertsshow121")
		}
		function specialist_details(id){//专家详情
			$(location).attr("href","scienic_expertsshow12?euId="+id+'');
		}
		function Marine_equipment_list() { <!-- 海洋难题 -->
			$(location).attr("href", "new_file15")
		}
		function Marine_equipment_detailst(id){//海洋难题详情
			$(location).attr("href", "new_file16?needId="+id+'');
		}
		function result_list() { <!-- 成果列表 -->
			$(location).attr("href", "newfile17")
		}
		function Marine() {//自身
			$(location).attr("href", "new_file20")
		}
	
		function indes() {//首页
			$(location).attr("href", "index");
		}
	</script>
</body>

</html>