<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

<head>


<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<title>成果共享</title>

<script type="text/javascript" src="js/public.js"></script>
<meta name="keywords" content="科研成果，生命科学成果，环境检测成果，实验常用设备，分析成果">
<script src="js/bootstrap.min.js"></script>
<meta name="description"
	content="中国领先的科研成果共享平台，让您快速找到各种类型的科学研究成果。提升闲置成果利用率，产生更大科研价值。涵盖：生命科学成果、环境检测成果、实验常用设备、分析成果、仪表、物性测试、测量/计量成果、在线及过程控制成果。
">





<%--<style type="text/css">--%>
<%--.d5_pert li{--%>
<%--margin: 28px;--%>
<%--width: 520px;--%>
<%--margin-right: 28px!important;--%>
<%--float: left;--%>
<%--border: 1px solid #dbdbdb;--%>
<%--border-radius: 14px;--%>
<%--background-color: #fff;--%>
<%--margin-bottom: 20px;--%>
<%---moz-box-shadow: 0px 2px 3px #999;--%>
<%---webkit-box-shadow: 0 2px 3px #999;--%>
<%--box-shadow: 0px 2px 3px #999;--%>
<%--position: relative;--%>
<%--cursor: pointer;--%>
<%--box-shadow: 0px 0px 15px 0px rgba(255,138,0,0.3);--%>
<%--}--%>


<%--.d5_pert li:hover{--%>
<%--margin: 28px;--%>
<%--width: 520px;--%>
<%--margin-right: 28px!important;--%>
<%--float: left;--%>
<%--border: 1px solid #dbdbdb;--%>
<%--border-radius: 14px;--%>
<%--background-color: #fff;--%>
<%--margin-bottom: 20px;--%>
<%---moz-box-shadow: 0px 2px 3px #999;--%>
<%---webkit-box-shadow: 0 2px 3px #999;--%>
<%--box-shadow: 0 2px 3px #999;--%>
<%--position: relative;--%>
<%--cursor: pointer;--%>
<%--box-shadow: 0 0 15px 0px rgba(255,138,0,0.3);--%>
<%--}--%>
<%--#front_header .header_wrap .registeredLink {--%>
<%--float: right;--%>
<%--margin-left: 20px;--%>
<%--}--%>



<%--#front_header .header_wrap .header_title {--%>
<%--float: right;--%>
<%--margin-right: 20px;--%>
<%--}--%>

<%--</style>--%>


<!-- 引入 Bootstrap 的 CSS 文件 -->


<link rel="stylesheet" href="css/l_css/main.css">
<link rel="stylesheet" href="css/l_css/experts.css">
<link rel="stylesheet" href="css/index.css?t=201711101207">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="css/sunstyle.css" />
<link rel="stylesheet" href="css/header_footer.css?t=1">
<link rel="stylesheet" href="css/jquery.pagination.css" />
<link rel="stylesheet" type="text/css" href="css/main_style.css" />
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/common.css">
<style>
.d5_pert .d5_ptl img {
	margin: 10px auto;
	display: block;
	width: 100%;
	height: 100%;
}

.d5_pert .d5_ptl {
	width: 120px;
	height: auto;
	margin-top: 11px;
	margin-left: 14px;
	float: left;
	position: relative;
}
</style>

<script src="js/jquery-1.11.3.js"></script>

<!-- 引入 Bootstrap 的 JS 文件 -->

<script src="js/bootstrap.min.js"></script>

<!--[if lt IE 9]><![endif]-->

<script src="js/html5shiv.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/page.js"></script>

<script src="js/respond.min.js"></script>

<script src="js/baidu.js"></script>

<script>
	$(function() {
		$('#front_header .header_wrap .right').hover(function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
			$('#front_header .header_wrap .right .out').show();
		}, function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
			$('#front_header .header_wrap .right .out').hide();
		});
		$('#sorts').on('mouseover', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon4.png');
		});
		$('#sorts').on('mouseout', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon5.png');
		});
	});
</script>
<link rel="stylesheet" href="css/common.css">
</head>

<body style="background-color: #fff">

	<%@include file="/head.jsp"%>
	<div class="logo-search-btn">
		<div class="logo-search">
			<!--left-->
			<div class="header-logo header-logo-change fl">
				<h1>
					<a href="index.jsp">科研成果共享平台</a>
				</h1>
			</div>
			<!--right-->
			<div class="header-search fl">
				<div class="searchOption fl">
					<p class="search-down">
						<a id="claa">科研成果</a> <span> <img class=""
							src="images/sy_arrow_down.png"></span>
					</p>
					<ul id="ulofsousuo" class="navbar-down" style="display: none">
						<li><a id="clab" onclick="myFunction()">科研成果</a></li>
						<li><a id="clab1" onclick="myFunction1()">科研需求</a></li>
						<li><a id="clab2" onclick="myFunction2()">专家展示</a></li>
					</ul>
				</div>
				<div class="searchText fl">
					<input id="likeString" type="text" placeholder="关键词"> <input
						id="findelikestringnumber" value="1" type="text"
						style="display:none"></input>
				</div>
				<div class="searchBtn fl">
					<img src="images/sy_seach.png"> <span
						onclick="searchButton()">搜索</span>
				</div>
			</div>

			<!--btn-->
			<div class="kindOfBtn fl">
				<input type="submit" id="publishNeed" value="发布需求">
			</div>
		</div>
	</div>
	<div class="clear"></div>


	<script src="js/public.js?t=3"></script>


	<div class="clear"></div>
	<div class="" id="instrument">
		<div class="navBar-nav">
			<nav>
				<ul>
					<li style="padding:0"><a href="index.jsp" id="shouye"
						class="selected">科研成果</a></li>
					<li><a href="new_file7" id="myself">科研需求展示</a></li>
					<li class="active-one"><a href="scienic_expertsshow11"
						id="zhuanjia">专家展示</a></li>
					<li><a href="new_file14" id="allNeed">交易大厅</a></li>
					<li><a href="new_file15" id="nanti">难题发布</a></li>
				</ul>
			</nav>
		</div>
		<div class="clear"></div>

		<div class="d5_nlist">
			<div class="experts_list">
				<div class="clear"></div>
				<div class="d5_ntblc d5_pert">
					<ul id="expertlist">
						<li>
							<div class="d5_ptl">
								<img src="static/picture/20170430193519110548.jpg-c100"
									alt="专家头像" />
								<!--c100-->
								<div class="ntb_zhezhao"></div>
							</div>
							<div class="d5_ptr">
								<p class="d5_np1">
									李思 <span> 研究员 <em>/</em> 副所长
									</span>
								</p>
								<p class="d5_np2">甘肃省科学院生物研究所</p>
								<p class="d5_np3">
									熟悉学科: <span>农牧废弃物资源化微生物处理</span>
								</p>
								<br>
								<div class="active">
									<div class="active_l">
										<a href="#">立即咨询</a>
									</div>
									<div class="active_r">
										<a href="#">方案评审</a>
									</div>
								</div>
							</div> </a>
						</li>
						<li>
							<div class="d5_ptl">
								<img src="static/picture/20170430193519110548.jpg-c100"
									alt="专家头像" />
								<!--c100-->
								<div class="ntb_zhezhao"></div>
							</div>
							<div class="d5_ptr">
								<p class="d5_np1">
									李思 <span> 研究员 <em>/</em> 副所长
									</span>
								</p>
								<p class="d5_np2">甘肃省科学院生物研究所</p>
								<p class="d5_np3">
									熟悉学科: <span>农牧废弃物资源化微生物处理</span>
								</p>
								<br>
								<div class="active">
									<div class="active_l">
										<a href="#">立即咨询</a>
									</div>
									<div class="active_r">
										<a href="#">方案评审</a>
									</div>
								</div>
							</div> </a>
						</li>
					</ul>
				</div>
				<div class="box">
					<div id="pagination3" class="page fl"></div>
					<!-- <div class="info fl">
				<p>当前页数：<span id="current3">1</span></p> -->
				</div>
				<div class="blank20"></div>
			</div>
		</div>
	</div>

	<!-- <div class="container">

			<div class="title_first">

				<a class="fl " onclick="index();"
					style="width: 150px;text-align: center; display: block;">

					科研成果展示 </a> <a class="fl " onclick="achievement();"
					style="width: 150px;text-align: center; display: block;">

					科研需求展示 </a> <a class="fl" onclick="list()"
					style="width: 150px;text-align: center;display: block;background-color: rgb(255,138,0);color: white;"> 专家展示 </a>
				<a class="fl " onclick="deal();"
					style="width: 150px;text-align: center;display: block;"> 交易大厅 </a>
				<a class="fl " onclick="problem();"
					style="width: 150px;text-align: center;display: block;"> 难题发布 </a>

			</div>

		</div> -->
	<!-- <div class="hengxian"
			style="background: #fff; "></div>
	</div> -->
	<script>
		$(document).ready(function() {
			limit = 10;
			offset = 0;
			findofspecialist("", limit, offset);
			yemian();
			$("#publishNeed").click(function() {
				$(location).attr('href', 'publishDemand45');
			});
		})
		function yemian() {
			$.ajax({
				url : Server + "/fon/findofspecialist",
				data : {
					"name" : "",
					"limit" : limit,
					"offset" : offset,
				},
				type : "POST",
				dataType : "JSON",
				success : function(data) {
					$("#pagination3").pagination({
						currentPage : 1,
						totalPage : Math.ceil(Number(data[data.length - 1] / 10)),
						isShow : true,
						count : 7,
						homePageText : "首页",
						endPageText : "尾页",
						prevPageText : "<上一页",
						nextPageText : "下一页>",
						callback : function(current) {
							$("#current3").text(current);
							findofspecialist("", limit, $("#pagination3").pagination("getPage").current - 1);
						}
					});
				}
			});
		}
	
		function findofspecialist(name, limit, offset) {
			var m = new Object;
			m.name = name;
			m.limit = limit;
			m.offset = offset;
			$.ajax({
				url : Server + "/fon/findofspecialist",
				data : m,
				type : "POST",
				dataType : "json",
				success : function(data) {
					var html = "";
					var pattern = '<li onclick="details(#6)">\
							<div class="d5_ptl">\
								<img src="#0" alt="专家头像"/>\
								<div class="ntb_zhezhao"></div>\
							</div>\
							<div class="d5_ptr">\
								<p class="d5_np1">#1 <span>#2</span></p>\
								<p class="d5_np2">#4</p>\
								<p class="d5_np3">熟悉学科: <span>#5</span></p><br>\
								<div class="active">\
								</div>\
							</div></a>\
						</li>';
					for (var i = 0; i < data.length - 1; i++) {
						var item = "";
						var order = data[i];
						item += pattern
							.replace("#0", order.euIcon)
							.replace("#1", order.euRealname)
							.replace("#2", order.euTitle)
							.replace("#4", order.euUnitName)
							.replace("#5", order.euDirection)
							.replace("#6", order.euId);
						html += item;
					}
					$("#expertlist").html(html);
				},
			});
		}
		/* $(document).ready(function(){
			roal()
		})
		
		function roal(){
			$.ajax({
				url:Server + "/eulist/list",
				data:{
					"limit":"10",
					"offset":"0",
					"name":$("#ss").val(),
				},
				type:"POST",
				dataType:"json",
				success : function(data){
					var html = "";
						var pattern = '<li onclick="details(#6)">\
							<div class="d5_ptl">\
								<img src="#0" alt="专家头像"/>\
								<div class="ntb_zhezhao"></div>\
							</div>\
							<div class="d5_ptr">\
								<p class="d5_np1">#1 <span> #2</span></p>\
								<p class="d5_np2">#4</p>\
								<p class="d5_np3">熟悉学科: <span>#5</span></p><br>\
								<div class="active">\
								</div>\
							</div></a>\
						</li>';
						for(var i = 0; i< data.length; i++){
						var item = "";
						var order = data[i];
						item += pattern
							.replace("#0", order.euIcon)
							.replace("#1", order.euRealname)
							.replace("#2", order.euTitle)
							.replace("#4", order.euUnitName)
							.replace("#5", order.euDirection)
							.replace("#6", order.euId);
						html += item;
					}
					$("#expertlist").html(html);
				},
			});
		} */
	
		function list() {
			$(location).attr("href", "scienic_expertsshow11");
		}
	
		function details(id) {
			$(location).attr("href", "scienic_expertsshow12?euId=" + id + "");
		}
	
		function index() {
			$(location).attr("href", "index");
		}
	
		function achievement() { //科研需求展示
			$(location).attr("href", "new_file7");
		}
	
		function deal() { //交易大厅
			$(location).attr("href", "new_file14");
		}
	
		function problem() { //难题发布
			$(location).attr("href", "new_file15");
		}
	</script>



	<div class="clear"></div>
	<%@include file="/footer.jsp"%>
	<script type="text/javascript" src="js/common.js"></script>
	<script src="js/jquery.pagination.min.js"></script>

</body>

</html>
