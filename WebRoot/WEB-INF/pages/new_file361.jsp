<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
	<head>
	<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>仪器共享</title>
		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">
		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
">

		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/uploadData.css">
		<link rel="stylesheet" href="css/common.css">
		<style type="text/css">
			.sun_box1{
				display: inline-block;
				padding: 48px 0px;
                float: left;
                width: 300px;
                height: 372px;
                margin-left: 20px;
                margin-right: 20px;
			}
			
			.sun_box1 img{
				width: 100%;
    height: 100%;
			}
			.mainList .content .box1 .box_content .colorFont {
    font-size: 16px;
    color: #ff8a00;
}
.sun_row{
	overflow: hidden;
}
.sun_row .left{
	float: left;
}
.sun_row .right{
	float: right;
	text-align: right;
}
div#statusbox {
    border: 1px solid #ccc;
    margin-bottom: 20px;
}
.haxi {
    margin-top: -25px;
}

div#selectbox {
    border-bottom: 1px solid #ccc;
}

	img#caocao_pic {
	vertical-align: middle;
	margin-right: 6px;
	}

	.left {
	margin-top: 10px;
	width: 500px;
	height: 30px;
	line-height: 50px;
	}
	.mainList .content .box2 .btn a {
    display: inline-block;
    width: 180px;
    height: 46px;
    text-align: center;
    line-height: 46px;
    color: #fff;
   
    font-size: 18px;
    border-radius: 8px;
}


.setbuy{
	background-color: #ff8a00;
}
		</style>
		<script src="js/jquery-1.11.3.js"></script>
		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$('.mainList .content .left li').removeClass('selected');
					$(this).addClass('selected');
				});
				$('.mainList .content .left .list_header').click(function() {
					if($('.mainList .content .left .secendList').is(':hidden')) {
						$('.mainList .content .left .secendList').slideDown();
						$('.mainList .content .left .list_header img').attr("src", "images/insUser_icon.png");
					} else {
						$('.mainList .content .left .secendList').slideUp();
						$('.mainList .content .left .list_header img').attr("src", "images/icon4.png");
					};

				});
				$("#row5 font label").click(function() {
					$("#row5 font label img").attr('src', 'images/icon35.png');
					$("#row5 font label").siblings('input').prop('checked', false);
					$(this).find('img').attr('src', 'images/icon36.png');
					$(this).siblings('input').prop('checked', true);
				});
				$("#row8 font label").click(function() {
					$("#row8 font label img").attr('src', 'images/icon35.png');
					$("#row8 font label").siblings('input').prop('checked', false);
					$(this).find('img').attr('src', 'images/icon36.png');
					$(this).siblings('input').prop('checked', true);
				});
			});
		</script>

	</head>

	<body style="background:#fff;">

		<%@include file="/head.jsp"%>

		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
	<a href="index" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>

	</div>
					<div class="web_name">
						用户中心
					</div>
				</div>
			</div>
		</div>
		<div class="mainList">
			<div class="bNav">您的位置：
			<a href="index">首页</a> > 
			<a href="orderManagement37">成果列表</a> > 
			<span>成果详情</span></div>
			<div class="content">
				<div class="box1">
				    <div class="box_head sun_box1">
					    <img id="imgofinfo" />
				    </div>
					
					
					<div id="infoname" class="box_title">一种新型自动化分拣装置的实用新型专利定制</div>
					<div class="box_content">
						<p>
							<font>技术成熟度：<span id="resultMaturity" >技术难题解决</span></font>
						</p>
						<p>技术水平：<span id="resultTechlevel">成熟</span></p>
						<p>获奖情况：<span id="awards">浙江 温州</span></p>
						<p>所属单位：<span id="unit" >5-10万</span></p>
						<p>知识产权编号：<span id="ipNumber" >5-10万</span></p>
						<p>
							交付方式:<font style="color: black;">完全转让<span class="colorFont" id="pay1" >50-100万</span></font>
							<font style="color: black;">许可转让<span id="pay2" class="colorFont">50-100万</span></font>
							<font style="color: black;">技术入股<span id="pay3" class="colorFont">50-100万</span></font>
						</p>
					</div>
				</div>
				<div class="box2" id="statusbox" >		
				</div>
				</div>
			<!--找个分页插件插入-->
		</div>
				<script>
	            	$(document).ready(function(){
	            		$.ajax({
	            			url:Server + "/fon/infodetails",
	            			type:"POST",
	            			dataType:"JSON",
	            			data:{
	            				id:${param.infoid}
	            			},
	            			success: function(data){
	            				var chengshudu = "0";
	            				if(data[0].resultMaturity == 0)
									chengshudu = "正在研发";
								if(data[0].resultMaturity == 1)
									chengshudu = "已有样品";
								if(data[0].resultMaturity == 2)
									chengshudu = "通过小试";
								if(data[0].resultMaturity == 3)
									chengshudu = "通过中试";
								if(data[0].resultMaturity == 4)
									chengshudu = "可以量产";
	            			
	            				if(data[1]!= null){
	            					$("#tabView1").html(data[1].resultDetails);
	            				}
	            				$("#infoid").text(data[0].resultId);
	            				/* 珍爱JavaScript，原理jQuery */
	            				document.getElementById("imgofinfo").src=data[0].image[0].rimagePath;
	            				$("#infoname").text(data[0].resultName);
	            				$("#infonamew").text(data[0].resultName);
	            				$("#resultMaturity").text(chengshudu);
	            				$("#resultTechlevel").text(data[0].resultTechlevel);
	            				$("#awards").text(data[0].awards);
	            				$("#unit").text(data[0].unit);
	            				$("#ipNumber").text(data[0].ipNumber);
	            				$("#pay1").text(data[0].pay[0].rpayRate);
	            				$("#pay2").text(data[0].pay[1].rpayRate);
	            				$("#pay3").text(data[0].pay[2].rpayRate);
	            			}
	            		});
	            	});
	            </script>
	            
	            <script>
	            	function buyorsellofinfo(){
						var typeId = $('#usertypeoflogin').val();
						var id = $('#useridoflogin').val();
	            		$.ajax({
	            			url:Server + "/fon/buyofinfo",
	            			type:"POST",
	            			dataType:"JSON",
	            			data:{
	            				usertype : typeId,
	            				userid : id,
	            				infoid : ${param.infoid}
	            			},
	            			success: function(data){
	            			//【00:买家状态，待确认，		页面中含有【接受】和【拒绝】按钮
							//【01:买家状态，不存在的情况，	因为此情况买家列表不展示】
							//【02:买家状态，确认交易，		一个页面中含有【已接受】，买家点击接受之后触发
							//【10:卖家状态，展示中，		页面显示所有买家的列表
							//【11:卖家状态，等待买家确认，	页面显示等待确认的买家信息
							//【12:卖家状态，确认交易，		页面显示【已锁定】
	            				switch(data.buytype){
	            					case "00":
	            						var item = "";
	            						var statushtml = '<div class="boxTitle"><span style="color: rgb(255,138,0);font-size: 19px;font-weight: 200;">已被服务商设置为买家</span></div>\
											<div class="item item3">\
												<div class="row">\
													<span>#0<span style="color: #CCCCCC;">（#1）</span></span>\
													<span style="color:rgb(78,201,254);">#2</span>\
												</div>\
											</div>\
											<div class="btn">\
												<a id="buyisreceive">接受</a>\
												<a id="buyisontreceive">拒绝</a>\
											</div>';
											if(data.buydate[0].rbuyUtype == 1){
												var rbuyUtype = "企事业单位";
												statushtml.replace("#1",rbuyUtype);
											}else{
												var rbuyUtype = "个人单位";
												statushtml.replace("#1",rbuyUtype);
											}
											var m = statushtml.replace("#0",data.buydate[0].unitname)
													.replace("#1",rbuyUtype)
													.replace("#2",data.buydate[0].userphone)
	            							$("#statusbox").html(m); 
	            							$("#buyisreceive").click(function(){
	            								$.ajax({
	            									url:Server + "/fon/buyofinfoofbuyishave",
							            			type:"POST",
							            			dataType:"JSON",
							            			data:{
							            				usertype : typeId,
	            										userid : id,
	            										infoid : ${param.infoid}
							            			},
							            			complete : function(data){
							            				window.location.reload();
							            			}
	            								});
	            							});
	            							$("#buyisontreceive").click(function(){
	            								$.ajax({
	            									url:Server + "/fon/buyofinfoofbuyisnot",
							            			type:"POST",
							            			dataType:"JSON",
							            			data:{
							            				usertype : typeId,
	            										userid : id,
	            										infoid : ${param.infoid}
							            			},
							            			success : function(data){
							            				var rejecturl = "new_file35";
							            				window.location.replace(rejecturl);
							            			}
	            								});	
	            							});
	            						break;
	            					case "01":
	            						break;
	            					case "02":
	            						var statushtml = '<div class="boxTitle"><span style="color: rgb(255,138,0);font-size: 19px;font-weight: 200;">已和该成果卖家达成协议</span></div>\
											<div class="btn">\
												<a href="#this" style="border-color: black;background-color:#ccc" >已接受</a>\
											</div>\
											</div>';
											$("#statusbox").html(statushtml);
	            						break;
	            					case "10":
	            					
			            				var html = "";
										var pattern = '<div id="selectbox" class="selectBox">\
												<div align="left" class="left">\
													<!--<input type="radio" name="b" id="" value="#00" />\-->\
													<label>\
														<img src="images/icon35.png" id="caocao_pic" name="b" onclick="change_pic()">\
														<span>设置为买家</span>\
														<input value="#01" id="type1" style="display:none" ></input>\
														<input value="#02" id="type2" style="display:none" ></input>\
														<input value="#4" id="id1" style="display:none" ></input>\
													</label>\
												</div>\
												<div align="right"  class="haxi">\
													<p style="font-size: 18px;">#1<font style="color: #CCCCCC;">（#2）</font></p>\
													<p style="color: rgb(110,211,254);">#3</p>\
												</div>\
										</div>';
										var str2 ="";
										if(data.buydate.length != 0){
											str2 = '<div class="btn">\
												<a id="setbuy">确认</a>\
											</div>';
										}
										for(var i = 0; i< data.buydate.length; i++){
											if(data.buydate[i].rbuyUtype == 1){
												var rbuyUtype = "企事业单位";
											}else{
												var rbuyUtype = "个人单位";
											}
											var item = "";
											var order = data.buydate[i];
											item += pattern
												.replace("#4", order.rbuyUid)//买家id
												.replace("#01", order.rbuyUtype)//买家类型
												.replace("#02", ${param.infoid})//成果ID
												.replace("#1", order.unitname)//单位名
												.replace("#2", rbuyUtype)//用户类型
												.replace("#3", order.userphone)//联系方式
											html += item;
										}
										html += str2;
										$("#statusbox").html(html);
										$("#setbuy").click(function(){
											var userid = $("label img[src='images/icon36.png']").next().next().next().next().val();
											var usertype = $("label img[src='images/icon36.png']").next().next().val();
											var infoid = $("label img[src='images/icon36.png']").next().next().next().val();
											$.ajax({
            									url:Server + "/fon/sellofinfoofisbuy",
						            			type:"POST",
						            			dataType:"JSON",
						            			data:{
						            				"usertype" : usertype,
						            				"userid" : userid,
						            				"infoid" : infoid
						            			},
						            			success : function(data){
						            				buyorsellofinfo();
						            			}
	            							});
										});
	            						break;
	            					
	            					case "11":
	            						var statushtml = '<div class="item item3">\
											<div class="row sun_row">\
												<div class="right">\
													<p style="font-size: 18px;">#0<font style="color: #CCCCCC;">（#1）</font></p>\
													<p style="color: rgb(110,211,254);">#2</p>\
												</div>\
											</div>\
											</div>\
											<div class="btn" >\
												<a style="background-color: #ccc">等待买家确认</a>\
											</div>';
											
											if(data.buydate[0].rbuyUtype == 1){
												var rbuyUtype = "企事业单位";
												statushtml.replace("#1",rbuyUtype);
											}else{
												var rbuyUtype = "个人单位";
												statushtml.replace("#1",rbuyUtype);
											}
											var m = statushtml.replace("#0",data.buydate[0].unitname)
													.replace("#1",rbuyUtype)
													.replace("#2",data.buydate[0].userphone)
	            							$("#statusbox").html(m); 
	            						break;
	            					case "12":
	            						var statushtml = '<div class="item item3">\
											<div class="row sun_row">\
												<div class="right">\
													<p style="font-size: 18px;">#0<font style="color: #CCCCCC;">（#1）</font></p>\
													<p style="color: rgb(110,211,254);">#2</p>\
												</div>\
											</div>\
											</div>\
											<div class="btn">\
												<a style="background-color:#ccc">已锁定</a>\
											</div>';
											if(data.buydate[0].rbuyUtype == 1){
												var rbuyUtype = "企事业单位";
												statushtml.replace("#1",rbuyUtype);
											}else{
												var rbuyUtype = "个人单位";
												statushtml.replace("#1",rbuyUtype);
											}
											var m = statushtml.replace("#0",data.buydate[0].unitname)
													.replace("#1",rbuyUtype)
													.replace("#2",data.buydate[0].userphone)
	            							$("#statusbox").html(m); 
	            						break;
	            					default:
	            						alert("参数错误！请联系管理员！");
	            				}
	            			}
	            		});
	            	}
	            	$(document).ready(function(){
	            		buyorsellofinfo();
	            	});
	            </script>
			<%@include file="/footer.jsp"%>


	<script>
	function change_pic(){
	var imgObj = document.getElementById("caocao_pic");
	var flag=(imgObj.getAttribute("src",2)=="images/icon35.png")
	imgObj.src=flag?"images/icon36.png":"images/icon35.png";
	}
	</script>
	</body>

</html>