<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

	<head>
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<title>仪器共享</title>

		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">

		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。

">
		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/publishService.css">
		<link rel="stylesheet" href="css/new_style.css">
		<link rel="stylesheet" href="css/common.css">
		<style type="text/css">
			.mainList .content .right button {
    width: 180px;
    height: 50px;
    text-align: center;
    line-height: 50px;
    background: #ff8a00;
    border: none;
    color: #fff;
    font-size: 20px;
    border-radius: 8px;
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    margin-left: 187px;
    margin-top: 20px;
        margin-bottom: 30px;
}
		</style>
		<script src="new_js/jquery-1.11.3.js"></script>
		<script src="method/dwy/update_subject10_1_zj.js"></script>
		<script >
		 // load();
		 // particulars();
		 // next();
		 // describe();
		   
		   
		 // $(document).ready(function(){
			//comment(GetQueryString("techId"));
			 //particulars(GetQueryString("needId"));
			 //next(GetQueryString("needId"));
			//describe(GetQueryString("needId"));
			
		//	});
			function GetQueryString(name) {
				var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
				var r = window.location.search.substr(1).match(reg);
				if(r != null) return unescape(r[2]);
				return null;
			}
		</script>
		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$(this).addClass('selected').siblings().removeClass('selected');
				});
			});
		</script>

	</head>



	<body style="background:#fff;">

	<%@include file="/head.jsp"%>

		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
	<a href="#this" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>

	</div>
					<!-- <div class="web_name">
						<img src="images/icon3.png" />
						<p>仪器管理</p>
					</div> -->
				</div>
			</div>
		</div>
		<div class="tab_top" style="display:none;">
			<div class="tab_top_content">
				<div class="item1 "><a href="#this">仪器管理与分布 <img src="images/insUser_icon.png" /></a></div>
				<div class="item2 ">
					<a href="#this">预约订单管理</a>
				</div>
				<div class="item2 selected">
					<a href="#this">实验订单管理</a>
				</div>
			</div>
		</div>
		<div class="mainList">
			<div class="bNav">您的位置：首页》全功能紫外-可见-近红外荧光光谱仪><span>评审意见</span></div>
			<div class="content">
				
				<div class="right">
					<div class="title"><span>评审意见</span></div>
					<ul>
						<!--<li>
							<label>使用时间</label>
							<input type="text" name="" id="" value="" />
						</li>-->
						
						
						
						<!--<li class="sun_item">
						   <div class="sun_item_div1">仪器信息</div>
						   <div class="sun_item_div2">
						   	<div class="sun_item_img"> 
						   	<img src="images/productDetailPic.png" />
						   	</div>
						   	<div class="item_box">
						   		<br />
						   		<p>高内涵筛选系统</p>
						   	<br />s
						   	
						   		<p>opertter</p>
						   		<br />
						   
						   		<div>仪器综合评分：<ul class="comment">  
    <li style="margin-bottom: 0px;">☆</li>  
    <li style="margin-bottom: 0px;">☆</li>  
    <li style="margin-bottom: 0px;">☆</li>  
    <li style="margin-bottom: 0px;">☆</li>  
    <li style="margin-bottom: 0px;">☆</li>  
</ul></div>
						   	</div>
						   </div>
						</li>-->
						<!--<li class="sun_item2">
							<div>
							<div class="sun_item2_row"></div>
							<div class="sun_item2_row2">
								
							</div>
							<textarea></textarea>
							</div>
						</li>-->
						<!--<li class="item2">
							<label>添加图片</label>
							<div class="addPic">
								<div class="pic">
									<img src="images/addPIC.png"/>
									<img src="images/delete.png" class='delete'/>
								</div>
								<div class="fill">
									<label for="fillPic"><img src="images/add.png"/></label>
									<input type="file" name="" id="fillPic" value="" />
								</div>
							</div>
						</li>-->
						<!--<li>
							<label>课题名称：</label>
							<input type="text" name="" id="" value="" />
						</li>
						<li class="item11">
							<label>仪器应用项目：</label>
							<select>
								<option>请选择仪器</option>
							</select>
						</li>
						
						<li>
							<label>测试样品</label>
							<input type="text" name="" id="" value="" />
						</li>
						<li>
							<label>联系人</label>
							<input type="text" name="" id="" value="" />
						</li>
						
						<li>
							<label>联系电话</label>
							<input type="text" name="" id="" value="" />
						</li>
						
						<li class="item8">
								<label>测试项目</label>
								<label for="commitFile"><img src="images/commitFile.png"/></label>
								<input style="width: 0px;height: 0px; opacity:0;" type="file" name="" id="commitFile" value="" />
						</li>-->
						<!--<li>
							<label>地址</label>
							<input type="text" name="" id="" value="" />
						</li>
						<li>
							<label>服务结果获取时间</label>
							<input type="text" name="" id="" value="" />
						</li>
						<li class="item10">
							<label><span>*</span>有效时间</label>
							<select>
								<option>2018-4-9</option>
							</select>
							<span>到</span>
							<select>
								<option>2018-4-9</option>
							</select>
						</li>-->

						<li class="item4">
							<label style="vertical-align: top;">评审意见：</label>
							<textarea placeholder="请输入评审意见" style="width: 900px;height: 300px;" id="textCom"></textarea>
						</li>
						
						
						<!--<li class="item12">
							<label></label>
							<table>
									<tr>
										<th>序号</th>
										<th>测试内容</th>
										<th>数量</th>
										<th>预算费用</th>
										<th>样品描述与测试要求</th>
									</tr>
									<tr>
										<td>1</td>
										<td>元素测定</td>
										<td>1</td>
										<td>150</td>
										<td></td>
									</tr>
									<tr>
										<td>2</td>
										<td>元素测定</td>
										<td>1</td>
										<td>150</td>
										<td></td>
									</tr>
								</table>
						</li>
					-->
					</ul>
					<button ><a  style="color: white;" onclick="comment();">确定上传</a></button>
				</div>
			</div>
			<div class="pageButton"></div>
			
			<!--找个分页插件插入-->
		</div>

		<%@include file="/footer.jsp"%>
		<script src="js/jquery-1.11.3.js"></script>  
<script>  
    $(function () {  
        var wjx_k = "☆";  
        var wjx_s = "★";  
        //prevAll获取元素前面的兄弟节点，nextAll获取元素后面的所有兄弟节点  
        //end 方法；返回上一层  
        //siblings 其它的兄弟节点  
        //绑定事件  
        $(".comment li").on("mouseenter", function () {  
            $(this).html(wjx_s).prevAll().html(wjx_s).end().nextAll().html(wjx_k);  
        }).on("click", function () {  
            $(this).addClass("active").siblings().removeClass("active")  
        });  
        $(".comment").on("mouseleave", function () {  
            $(".comment li").html(wjx_k);  
            $(".active").text(wjx_s).prevAll().text(wjx_s);  
        })  
    });  
</script>  

	</body>

</html>