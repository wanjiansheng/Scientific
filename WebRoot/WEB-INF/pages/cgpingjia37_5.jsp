<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

<head>
<!--  -->

<!--  -->

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>

<title>仪器共享</title>

<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">

<meta name="description"
	content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。

">
<link rel="stylesheet" href="new_css/comments.css">
<script src="method/dwy/cgpingjia37_5.js"></script>
<script src="js/jquery-1.11.3.js"></script>
<link rel="stylesheet" href="css/common.css">
<script>
	//load();
	//next();
	$(document).ready(function() {
		load(GetQueryString("id"));
		//particulars(GetQueryString("needId"));
		// next(GetQueryString("id"));
		//describe(GetQueryString("needId"));

		next(GetQueryString("id"));
	});
	function GetQueryString(name) {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
		var r = window.location.search.substr(1).match(reg);
		if (r != null) return unescape(r[2]);
		return null;
	}
</script>
<link rel="stylesheet" href="new_css/header_footer.css?t=1">
<link rel="stylesheet" href="new_css/uploadData.css">
<link rel="stylesheet" href="new_css/productDetail.css">
<link rel="stylesheet" href="css/new_style.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="new_css/zoomify.min.css">
<link rel="stylesheet" href="new_css/style.css">
<link rel="stylesheet" href="js/layui/css/layui.css" media="all">
	<style>
	div#nav_top {
	background: url("image/sy_bg.png") no-repeat center !important;
	margin: 0 auto !important;
	height: 175px !important;
	width: 1200px !important;
	}
	div#nav_top > div.container > div.fl{
	width: 100% !important;
	padding-top: 50px;
	margin-left: -235px;
	}

	.bNav{
	clear: both;
	width: 1200px;
	margin: 0 auto;
	font-size: 16px;
	text-align: left;
	}

	.bNav span{
	color: #FF8A00;
	}
	.wrap {
	height: 32px;
	border-bottom: 1px solid #ccc;
	margin-left: 20px;
	margin-top: 20px;
	font-size: 20px;

	}
	.wrap span {
	border-bottom: 3px solid #ff8a00;
	}
	.mainList {
	width: 1200px;
	margin: 0 auto;
	border: 1px solid #ccc;
	margin-bottom: 72px;

	margin-top: 20px;
	}
	.box {
	margin-top: 20px;
	}
	.box_title {
	margin-left: 30px;
	margin-bottom: 20px;

	}
	.boxTitle {
	margin-left: 30px;
	margin-top: 20px;
	}
	.item_title {
	display: inherit;
	background-color: #ededed;
	height: 40px;
	line-height: 40px;
	color: #202020;
	font-size: 16px;
	padding-left: 30px;
	}
	.row1 {
	margin-left: 30px;
	font-size: 18px;
	margin-top: 20px;
	}
	.row2 {
	margin-left: 30px;
	}
	.row2 span {
	font-size: 14px;
	color: #999;
	/* margin-left: 30px; */
	}
	.row2 a {
	font-size: 16px;
	}
	.row3 {
	margin-right: 20px;
	}
	.xuxian {
	border: 1px dashed #ededed;
	margin-left: 30px;
	margin-right: 20px;
	margin-bottom: 30px;
	}
	.caoni {
	margin-left: 30px;
	margin-bottom: 30px;
	font-size: 16px;
	}
	textarea#textCom {
	margin: 30px auto;
	margin-left: 1.4%;
	}
	textarea {
	width: 97% !important;
	height: 330px;
	resize: none;
	padding: 10px;
	border: 1px solid #D3D3D3 !important;

	}
	.mainList .content .right .box .box_content .item {
	float: left;
	width: 100%;
	line-height: 30px;
	/*margin-top: 50px;*/
	}
	.woqu a {
	width: 180px;
	height: 46px;
	display: inline-block;
	line-height: 46px;
	background-color: #ff8a00;
	text-align: center;
	font-size: 16px;
	border-radius: 5px;
	color: #fff;
	margin-left: 43%;
	margin-bottom: 30px;
	clear: both;
	}
	.row.row-img {
	margin-top: 20px;
	}
	.pic {
	margin-left: 110px;
	margin-top: -20px;
	}
	.mainList .content .right .box .box_content .item1 .item_content .text .row3 {
	margin-bottom: 20px;
	font-size: 18px;
	color: #ed1717;
	text-align: right;
	}
	.this-haha {
	border-bottom: 1px dashed #ccc;
	}

	a.hannan {
	/* float: left; */
	width: 150px;
	height: 40px;
	line-height: 40px;
	display: inline-block;
	background-color: #06B9FE;
	text-align: center;
	font-size: 16px;
	border-radius: 5px;
	color: #fff;
	margin-left: 70px;
	
	}
	.item.item5.hahax {
	height: 100%;
	min-height: 300px;
	}
	img.img-rounded.zoomify {
    /*margin-left: 30px;*/
    margin-right: 5px;
}
	</style>
<script src="js/zoomify.min.js"></script>
<script>
	$(function() {
		$('#front_header .header_wrap .right').hover(function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
			$('#front_header .header_wrap .right .out').show();
		}, function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
			$('#front_header .header_wrap .right .out').hide();
		});
		$('#sorts').on('mouseover', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon4.png');
		});
		$('#sorts').on('mouseout', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon5.png');
		});

		$('.mainList .content .left li').click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
		});
		//点击图片放大
		$('.pic img').zoomify();
	});
</script>

</head>

<body style="background:#fff;">

	<%@include file="/head.jsp"%>

	<div class="" id="nav_top">
		<div class="container">
			<div class="fl">
				<div class="logo ">
	<a href="#this" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>

	</div>
				
			</div>
		</div>
	</div>
	<div class="bNav">
	您的位置：
	<a href="index">首页</a> >
	<a href="new_file39">用户中心</a> >
	<span>评价</span>
	</div>
	<div class="mainList">

		<div class="content">
			<div class="right">
				<div class="title">
					<div class="wrap">
						<span>成果评价</span>
					</div>
				</div>
				<div class="box">
					<div class="box_title">对成果进行评价</div>
					<div class="box_content">
						<div class="item item1">
							<div class="item_title">服务名称</div>
							<div class="item_content">
								<div class="text">
									<div class="row1">
										<span id="ww0"></span>
									</div>
									<div class="row2">
										<span>成果简介：</span><span id="ww1"></span>
									</div>
									<div class="row3">
										<span id="ww2"></span>
									</div>
								</div>
							</div>
						</div>
						<form id="signupForm" style="display:none">
			                <input name="rdealId" type="hidden" id="7" value="${param.rdealId}" >
			                <input type="text" name="rdealPayStatus" id="rdealPayStatus" value="3"/>
			            </form>
						<div class="item item2">
							<div class="item_title"></div>
							<div class="item_content">
								<div class="boxTitle">
								<div class="item item5 hahax">
									<label style="">查看合同文档:	</label>
									<div class="row" id="doc"></div>
									<div class="row">
										<label>交易合同:</label>
										<div class="pic"></div>
										<%--<a class="hannan">交易合同</a>--%>
										<%--<div class="pic"></div> --%>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box_content">
						<div class="item item3">
							<div class="item_title">给您留下的印象</div>
							<div class="item_content">
								<textarea placeholder="请填写交流过程中的问题以及服务" id="textCom"></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="evaluation woqu">
			<a onclick="insert()" >评论</a>
		</div>
	</div>
</div>

	<%@include file="/footer.jsp"%>

</body>

</html>