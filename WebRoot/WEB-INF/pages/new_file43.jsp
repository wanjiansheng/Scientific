<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<title>仪器共享</title>
		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">
		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
">
		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/orderManagement.css">
		<link rel="stylesheet" href="css/common.css">
		<style type="text/css">

.mainList .content .right .infoList li .row2 .tab6 .btn {
    display: inline-block;
    width: 66px;
    height: 35px;
    line-height: 35px;
    text-align: center;
    background: #f7843c;
    border-radius: 5px;
    /* margin: auto; */
}
.mainList .content .right .infoList li .row2 .tab2 {
    width: 25%;
     text-align: center; 
}
.mainList .content .right .tab_header .item3 {
    width: 27%;
}
.mainList .content .right .infoList li .row2 .tab3 {
    width: 27%;
}
.mainList .content .right .infoList li .row2 .tab4 {
    width: 15%;
    border: 0px solid #e6e6e6;
}
.mainList .content .right .infoList li .row2 .tab5 {
    width: 15%;
    border-right: 0px solid #e6e6e6;
}
.mainList .content .right .infoList li {
    height: 110px;
    margin-bottom: 9px;
    border: 1px solid #e6e6e6;
}
		</style>
		
		<script src="js/jquery-1.11.3.js"></script>
		<script src="method/dwy/new_file43.js"></script>
		<script >
		$(document).ready(function(){
		  load();
		});
		 // particulars();
		 // next();
		 // describe();
		   
		   
		 
			
		</script>
		<script>
			$(function() {
				
				/*-----------*/
				$(".tab_span1").click(function(){
					$(this).addClass('tab2').siblings().removeClass('tab2');
       $("#top2_item1").css("display","block");
       $("#top2_item2").css("display","block");
       $("#top1_item1").css("display","none");
       $("#top1_item2").css("display","none");
});


$(".tab_span2").click(function(){
	$(this).addClass('tab2').siblings().removeClass('tab2');
       $("#top1_item1").css("display","block");
       $("#top1_item2").css("display","block");
       $("#top2_item1").css("display","none");
       $("#top2_item2").css("display","none");
});			
				/*-----------*/
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$('.mainList .content .left li').removeClass('selected');
					$(this).addClass('selected');
				});
				$('.mainList .content .left .list_header').click(function() {
					if($('.mainList .content .left .secendList').is(':hidden')) {
						$('.mainList .content .left .secendList').slideDown();
						$('.mainList .content .left .list_header img').attr("src", "images/insUser_icon.png");
					} else {
						$('.mainList .content .left .secendList').slideUp();
						$('.mainList .content .left .list_header img').attr("src", "images/icon4.png");
					};

				});
				$("#row5 font label").click(function() {
					$("#row5 font label img").attr('src', 'images/icon35.png');
					$("#row5 font label").siblings('input').prop('checked', false);
						$(this).find('img').attr('src', 'images/icon36.png');
						$(this).siblings('input').prop('checked', true);
				});
				$("#row8 font label").click(function() {
					$("#row8 font label img").attr('src', 'images/icon35.png');
					$("#row8 font label").siblings('input').prop('checked', false);
						$(this).find('img').attr('src', 'images/icon36.png');
						$(this).siblings('input').prop('checked', true);
				});
			});
		</script>

	</head>

	<body style="background:#fff;">

		<%@include file="/head.jsp"%>
		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
	<a href="index" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>

	</div>
					<div class="web_name">
						用户中心
					</div>
				</div>
			</div>
		</div>
		<div class="mainList">
			<div class="bNav">您的位置：
			<a href="index" style="color: #575757">首页</a> > 
		            <a href="new_file39" style="color: #575757">用户中心</a> > 


			<span>技术方案管理</span></div>
			<div class="content">
			<%@include file="/left-tab.jsp"%>
				<div class="right">
					<div class="title"><span>技术方案管理</span>
						<select style="float: right;
	width: 198px;
	height: 40px;
	line-height: 40px;
	margin-top: 3px;
	border: 1px solid #efefef;">
									<option>技术方案</option>
									<option></option>
						</select>
					</div>
					
					<div class="tab_header" id="top1_item1">
						<div class="item2">需求类型</div>
						
						<div class="item3">技术名称</div>
						<div class="item4">服务地区</div>
						<div class="item5">上传时间</div>
						<div class="item6">操作</div>
					</div>
					
					<div class="infoList" id="top1_item2">
						<ul id="techManage">
						</ul>
					</div>
                
                	
                <!----------------------------------->
				   
				<!----------------------------------->
				
				</div>
			</div>
			<div class="pageButton"></div>
			<!--找个分页插件插入-->
		</div>

		<%@include file="/footer.jsp"%>

	</body>

</html>