<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<title>仪器共享</title>
<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">
<meta name="description"
	content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
">
<style type="text/css">
.sun_box1 {
	display: inline-block;
	padding: 48px 0px;
	float: left;
	width: 300px;
	height: 372px;
	margin-left: 20px;
	margin-right: 20px;
}

.sun_box1 img {
	width: 100%;
	height: 100%;
}

.mainList .content .box1 .box_content .colorFont {
	font-size: 16px;
	color: #ff8a00;
}
div#nav_top > div.container > div.fl {
    width: 340px;
    padding-top: 50px;
    margin-left: -237px;
}

.mainList .content .box2 {
    margin-top: 22px;
    border: 1px solid #d3d3d3 !important;
    padding: 0 20px;
}
</style>
<script src="method/dwy/cgdaizhifu37_2.js"></script>
<script src="js/jquery-1.11.3.js"></script>
<script>
	$(document).ready(function() {
		load(GetQueryString("id"));
		next(GetQueryString("id"));
		//update()
	});
	function GetQueryString(name) {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
		var r = window.location.search.substr(1).match(reg);
		if (r != null) return unescape(r[2]);
		return null;
	}
</script>
<link rel="stylesheet" href="new_css/header_footer.css?t=1">
<link rel="stylesheet" href="new_css/uploadData.css">
<link rel="stylesheet" href="new_css/productDetail.css">
<link rel="stylesheet" href="css/new_style.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="new_css/zoomify.min.css">
<link rel="stylesheet" href="new_css/style.css">
<link rel="stylesheet" href="js/layui/css/layui.css" media="all">
<link rel="stylesheet" href="css/common.css">
<script src="js/zoomify.min.js"></script>
<script>
	$(function() {
		$('#front_header .header_wrap .right').hover(function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
			$('#front_header .header_wrap .right .out').show();
		}, function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
			$('#front_header .header_wrap .right .out').hide();
		});
		$('#sorts').on('mouseover', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon4.png');
		});
		$('#sorts').on('mouseout', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon5.png');
		});

		$('.mainList .content .left li').click(function() {
			$('.mainList .content .left li').removeClass('selected');
			$(this).addClass('selected');
		});
		$('.mainList .content .left .list_header').click(function() {
			if ($('.mainList .content .left .secendList').is(':hidden')) {
				$('.mainList .content .left .secendList').slideDown();
				$('.mainList .content .left .list_header img').attr("src", "images/insUser_icon.png");
			} else {
				$('.mainList .content .left .secendList').slideUp();
				$('.mainList .content .left .list_header img').attr("src", "images/icon4.png");
			}
			;

		});
		$("#row5 font label").click(function() {
			$("#row5 font label img").attr('src', 'images/icon35.png');
			$("#row5 font label").siblings('input').prop('checked', false);
			$(this).find('img').attr('src', 'images/icon36.png');
			$(this).siblings('input').prop('checked', true);
		});
		$("#row8 font label").click(function() {
			$("#row8 font label img").attr('src', 'images/icon35.png');
			$("#row8 font label").siblings('input').prop('checked', false);
			$(this).find('img').attr('src', 'images/icon36.png');
			$(this).siblings('input').prop('checked', true);
		});
		
		//点击图片放大
		$('.pic img').zoomify();
	});
</script>

</head>

<body style="background:#fff;">

	<%@include file="/head.jsp"%>
	<div class="" id="nav_top">
		<div class="container">
			<div class="fl">
				<div class="logo ">
	<a href="#this" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>
	</div>
				<div class="web_name">用户中心</div>
			</div>
		</div>
	</div>
	<div class="mainList">
		<div class="bNav">
			您的位置：
			<a href="index">首页</a> > 
			<a href="">成果列表 </a> > 
			<span>成果详情</span>
		</div>
		<div class="content">
			<div class="box1">
				<div class="box_head sun_box1" id="cgImg">
					<img src="images/04.jpg" />
				</div>
             

				<div class="box_title" id="in0">一种新型自动化分拣装置的实用新型专利定制</div>
				<div class="box_content">
					<p>
						<font>技术成熟度：<span id="in1"></span></font>
					</p>
					<p>
						技术水平：<span id="in2"></span>
					</p>
					<p>
						获奖情况：<span id="in3"></span>
					</p>
					<p>
						所属单位：<span id="in4"></span>
					</p>
					<p>
						知识产权编号：<span id="in5"></span>
					</p>
					<p>

						交付方式:<font style="color: black;">完全转让<span id="pay1"
							class="colorFont"></span></font> <font style="color: black;">许可转让<span
							id="pay2" class="colorFont"></span></font> <font style="color: black;">完全转让<span
							id="pay3" class="colorFont"></span></font>

					</p>
				</div>
			</div>
			<div class="box2">
				<!--<div class="boxTitle">评审意见信息</div>
					<div class='item item1'>
						<div class="left">仪器特别好用，服务态度也很好 仪器特别好用，服务态度也很好仪器特别好用，服务态度也很好服务态度也很好</div>
						<div class="right">一日***记忆<span>（服务商）</span></div>
					</div>
					<div class='item item2'>
						<div class="row">
							<div class="left">仪器特别好用，服务态度也很好 仪器特别好用，服务态度也很好仪器特别好用，服务态度也很好服务态度也很好</div>
							<div class="right">一日***记忆<span>（服务商）</span></div>
						</div>
						<div class="row">
							<div class="left">仪器特别好用，服务态度也很好 仪器特别好用，服务态度也很好仪器特别好用，服务态度也很好服务态度也很好</div>
							<div class="right">一日***记忆<span>（服务商）</span></div>
						</div>
						<div class="row">
							<div class="left">仪器特别好用，服务态度也很好 仪器特别好用，服务态度也很好仪器特别好用，服务态度也很好服务态度也很好</div>
							<div class="right">一日***记忆<span>（服务商）</span></div>
						</div>
						
					</div>
					-->


				<div class="boxTitle">买家信息</div>
				<div class="item item3">
					<div class="row">
						<span>用户姓名：</span><span type="text" name="" id="0" value=""></span>
						<span>联系方式：</span><span type="text" name="" id="1" value=""></span>
						<span>地址：</span><span type="text" name="" id="2" value=""></span>
					</div>
					<div class="row">
						<span>单位：</span><span type="text" name="" id="3" value=""></span>
					</div>
				</div>


				<div class="boxTitle">资料信息</div>
				<div class="item item5">

					<label>查看合同文档:</label>
					<div class="row" id="doc"></div>
					<div class="row">
						<label>交易合同</label>
						<div class="pic"></div>
					</div>
				</div>
				<div class="btn">
					<!-- <input style="display:none" name="innumber" id="4" value="${param.id}"/>  -->
					<a onclick="findpay(${param.id},${param.rdealId})">支付</a>
				</div>
			</div>
		</div>
		<div class="pageButton"></div>
		<!--找个分页插件插入-->
	</div>

	<%@include file="/footer.jsp"%>

</body>

</html>