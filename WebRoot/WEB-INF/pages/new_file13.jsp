<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<title>仪器共享</title>
		

		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">

		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。

">
		<link rel="stylesheet" href="new_css/header_footer.css">
		<link rel="stylesheet" href="new_css/bookingOrderDetail.css">
		<script src="js/jquery-1.11.3.js"></script>
		<style type="text/css">
		.box_row1>input{
			width: 968px!important;
		}
		.box_row2>div{
			width: 968px;
			height: 220px;
			overflow: auto;/*超出部分是有滚轮*/
		}
		.box_row3 .box_row3_label{
			width: auto!important;
		}
		.box_row3 .box_row3_a{
			width: 120px;
			height: 55px;
			line-height: 55px;
			text-align: center;
			color: white;
			background-color: rgb(255,138,0);
			display: inline-block;
		}
		box_row3_aa{
			width: 300px;
			height: 55px;
			background-color: #fff;
			border: 1px solid #575757;
}
		}
		.mainList .content {
    width: 100%;
    overflow: hidden;
     height: 900px; 
}
.mainList .content .right .box ul li select{
    width: 968px!important;
    height: 54px;
    padding-left: 20px;
    outline: none;
    border: 1px solid #ccc;
}

.li_row>label {
	
    display: inline;
    cursor: pointer;
        width: 800px!important;
    vertical-align: middle;
    height: 70px;
}
.li_row>label span {
    vertical-align: middle;
    padding-left: 10px;
    line-height:30px!important;
}
.li_row>label img {
    vertical-align: middle;
}
.li_box{
	border:1px solid rgb(216,216,216);
	display: inline-block;
	padding: 10px;
}

.sendBtn{
	width:120px;
	height:55px;
	
	background-color:rgb(255,138,0);
	color:#fff;
	display:block;
	text-align: center;
		line-height: 55px;
		margin-left: 112px;
		    cursor: pointer;
	
}

	li.box_row3 input {
	padding-top: 20px !important;
	}

	.mainList .content .right{
	padding-bottom: 40px;
	}

	.mainList .content .right .box:last-of-type label {
    float: left;
    margin-top: 13px;
    text-align: left;
}


		</style>
		<link rel="stylesheet" href="css/common.css">
		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$(this).addClass('selected').siblings().removeClass('selected');
				});
				
				
				
				$(".li_row label").click(function() {
//					if($(this).siblings('input').is(':checked')){
//						$(this).find('img').attr('src','images/icon35.png');
//						$(this).siblings('input').prop('checked',false);
//					}else{
//						$(this).find('img').attr('src','images/icon36.png');
//						$(this).siblings('input').prop('checked',true);
//					};
					$(".li_row label img").attr('src', 'images/icon35.png');
					$(".li_row label").siblings('input').prop('checked', false);
					$(this).find('img').attr('src', 'images/icon36.png');
					$(this).siblings('input').prop('checked', true);
				});
			});
		</script>

</head>

<body style="background:#fff;">

	<%@include file="/head.jsp"%>

	<div class="" id="nav_top">
		<div class="container">
			<div class="fl">
				<div class="logo ">
					<a class="logo-text" style="color: rgb(255,138,0);font-size: 30px;" onclick="indes();">科研成果共享平台</a>
				</div>

			</div>
		</div>
	</div>

	<div class="mainList">
		<div class="bNav">
			您的位置：
			<a href="index">首页</a> > 
			
			<a href="scienic_expertsshow11">专家列表</a> > 

			<a href="scienic_expertsshow12">专家详情</a> > 

			<span>申请专家</span>
		</div>
		<form enctype="multipart/form-data" method="post" id="formtable">
		<div class="content">
			<div class="right">
				<div class="box">
					<div class="box_title">专家信息</div>
					<ul>
						<li><label>所属单位</label> <input type="text" placeholder=""
							name="euUnitName" id="euUnit" value="" disabled="disabled"/></li>
						<li><label>专家姓名</label> <input type="text" name="euRealname" id="euName"
							value="" disabled="disabled"/></li>

					</ul>
				</div>
				<div class="box">
					<div class="box_title">需求方基本信息</div>
					<ul>
						<li><label>姓名</label> <input type="text" placeholder="请输入姓名"
							name="" id="userName" value="" disabled="disabled"/></li>
						<li><label>联系电话</label> <input type="text"
							placeholder="请输入联系电话" name="" id="phone" value="" disabled="disabled"/></li>
						<li><label>电子邮箱</label> <input type="text" placeholder="电子邮箱"
							name="" id="email" value="" disabled="disabled"/></li>
						<li><label>单位名称</label> <input type="text"
							placeholder="请输入单位名称" name="" id="unit" value="" disabled="disabled"/></li>
						<li class="box_row1">
							<label>需求名称</label>
							<select class="sun_select" id="sun_select" onchange="program();">
								<option></option>
							</select>
						</li>
						<li class="box_row2">
							<label>技术方案</label>
							<div class="li_box" id="li_box">
							</div>
						</li>
						<li class="box_row3">
							<label >上传文档</label>
							<input type="file" name="file" id="file_upload" /> <span style="color: red" id="fileTypeError"></span>
						</li>
					</ul>
				</div>
				<a onclick="s1111();" class="sendBtn btn-success">提交申请</a>
			</div>
		</div>
		</form>
		<div class="pageButton"></div>
		<!--找个分页插件插入-->
	</div>

	<%@include file="/footer.jsp"%>
	<script>
		$(document).ready(function() {
			details()
			user()
			Demand()
		})
	</script>
	<script>
			function details(){
				$.ajax({
					url:Server + "/eulist/details",
					data:{
						"euId":${param.euId},
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var details = data[0];
						$("#euUnit").val(details.euUnitName);
						$("#euName").val(details.euRealname);
					},
				});
			}
			
			function user(){
				var typeId = $('#usertypeoflogin').val();
				var id = $('#useridoflogin').val();
				$.ajax({
					url:Server + "/res/need/user",
					data:{
						"typeId":typeId,
						"id":id,
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var param = data[0];
						if(typeId == 2){//企业
							$("#userName").val(param.uuContacts);
							$("#userName").attr("name","uuContacts");
							$("#unit").val(param.uuUnitName);
							$("#unit").attr("name","uuUnitName");
							$("#phone").val(param.uuPhone);
							$("#phone").attr("name","uuPhone");
							$("#email").val(param.uuEmail);
							$("#email").attr("name","uuEmail");
						}else if(typeId == 1){//个人
							$("#userName").val(param.puRealname);
							$("#userName").attr("name","puRealname");
							$("#unit").val(param.puUnit);
							$("#unit").attr("name","puUnit");
							$("#phone").val(param.puPhone);
							$("#phone").attr("name","puPhone");
							$("#email").val(param.puEmail);
							$("#email").attr("name","puEmail");
						}
					}
				});
			}
			
			function Demand(){
				var typeId = $('#usertypeoflogin').val();
				var id = $('#useridoflogin').val();
				$.ajax({
					url:Server + "/res/need/selectDemand",
					data:{
						"typeId":typeId,
						"id":id,
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var html = "";
						var top =  '<option value="0">----请选择----</option>';
						var pattern = '<option value="#1">#0</option>';
						for(var i = 0; i < data.length; i++){
							var item = "";
							var param = data[i];
								item += pattern
									.replace("#0", param.needName)
									.replace("#1", param.needId),
								html += item;
						}
						html = top + html
						$("#sun_select").html(html);
					},
				});
			}
			
			function program(){
				var val = $("#sun_select").val();
				$.ajax({
					url:Server + "/res/need/selectTech",
					data:{
						"needId":val,
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var html = "";
						var pattern = '<div class="li_row">\
									<label><img src="images/icon35.png"/ data-appid="#1"><span>#0</span></label>\
								</div>';
						for(var i = 0; i < data.length; i++){
							var item = "";
							var param = data[i];
							item += pattern
								.replace("#0",param.techName)
								.replace("#1", param.techId),
							html += item;
						}
						$("#li_box").html(html);
						$(".li_row label").click(function() {
							var image = $(this).find('img').attr('src');
							image = (image=='images/icon35.png')?'images/icon36.png':'images/icon35.png';
							$(this).find('img').attr('src',image);
						});
					},
				});
			}
			
			function s1111(){
				var formData = new FormData($('#formtable')[0]);
				var euId = ${param.euId};
				var typeId = $('#usertypeoflogin').val();
				var id = $('#useridoflogin').val();
				var needId = $("#sun_select").val();
				var techId = []
				for(var i = 0;i < $('.li_box').find("[src='images/icon36.png']").length;i++){
					techId.push($('.li_box').find("[src='images/icon36.png']").eq(i).attr('data-appid'));
				}
				if(needId == ""){
					alert("请选择需求")
					return;
				}
				if(techId.length == 0){
					alert("请选择提交的方案")
					return;
				}
				var applyStatus = 0;//申请表状态
				var fileName = $('#file_upload').val();//获得文件名称
				var fileType = fileName.substr(fileName.lastIndexOf("."),fileName.length);//截取文件类型
				formData.append("euId",euId);
				formData.append("needUserId",id);
				formData.append("needUserType",typeId);
				formData.append("needId",needId);
				formData.append("techId",techId.join());
				if(fileType == '.xls' || fileType == '.doc' || fileType == '.pdf' || fileType == '.docx'){
					$.ajax({
						url:Server + "/problem_select/apply",
						type:"POST",
						cache : false,
						processData : false,
						async:false,
						contentType : false,
						data:formData,/* {
							"euId":euId,
							"needUserId":id,
							"needUserType":typeId,
							"needId":needId,
							"techId":techId.join(),
							"applyStatus":applyStatus,
							"file":new FormData($('#formtable')[0]),
						}, */
						//dataType:"json",
						success : function(data){
							alert("提交成功！");
							$(location).attr("href","scienic_expertsshow11");
						},
						error : function(msg){
							alert("提交失败！");
						}
					});
				}else {
					$("#fileTypeError").html('*上传文件类型错误,支持类型: .xls .doc .docx .pdf');
				}
				
			}
			
			function indes(){
				$(location).attr("href","index");
			}
		</script>

</body>

</html>