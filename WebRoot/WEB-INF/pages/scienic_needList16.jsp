<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<!-- head -->

<head>
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>成果共享</title>
    <meta name="keywords" content="科研成果，生命科学成果，环境检测成果，实验常用设备，分析成果">
    <meta name="description" content="中国领先的科研成果共享平台，让您快速找到各种类型的科学研究成果。提升闲置成果利用率，产生更大科研价值。涵盖：生命科学成果、环境检测成果、实验常用设备、分析成果、仪表、物性测试、测量/计量成果、在线及过程控制成果。

">
    <link rel="stylesheet" href="css/l_css/header.css">
    <link rel="stylesheet" href="css/l_css/main.css">
    <link rel="stylesheet" href="css/jquery.pagination.css" />
    <link rel="stylesheet" href="css/common.css">
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <style src="https://v3.bootcss.com/assets/css/ie10-viewport-bug-workaround.css"></style>
    <style>
    	#header_form .img {
	    position: absolute;
	    top: 50%;
	    left: 2px;
	    height: 32px;
	    width: 80px;
	    color: rgb(255,138,0);
	    transform: translateY(-50%);
	    -webkit-transform: translateY(-50%);
	    -moz-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
		}
		#header_form button {
		    float: left;
		    width: 15.75757576%;
		    height: 36px;
		    background: #FF8A00;
		    border: 1px solid #FF8A00;
		    color: #fff;
		    font-size: 16px;
		    cursor: pointer;
		    outline: none;
		    top: -11%;
		    padding-top: -51px;
		    margin-top: 0px;
		    margin-left: -8px;
		    /* position: relative; */
		}
		.mainList .content .list li .active>div:nth-of-type(2) a {
		    background: #00aeff;
		    padding-top: 0px;
		}
		.mainList .content .list li>div {
            display: table-cell;
            vertical-align: middle;
            font-size: 16px;
            padding: 18px;
		}








    div#nav_top {
    background: url(image/sy_bg.png) no-repeat center;
    height: 175px;
    background-size: cover;
    width: 1200px;
    margin: 0 auto;
    }
    button#searchofneed {
    padding-left: 22px;
    }
    #header_form {
    float: left;
    /* width: 100%; */
    width: 838px;
    margin-top: 68px;
    position: relative;
    margin-left: -121px;
    }

    #header_form input {
    width: 447px;
    height: 46px;
    float: left;
    padding-left: 132px;
    border: 1px solid #f60;
    border-right: none;
    outline: none;
    font-size: 12px;
    border-radius: 0px;
    }

    #header_form img {
    position: absolute;
    top: 50%;
    left: 47px;
    }

    #header_form button {
    float: left;
    /* width: 15.75757576%; */
    height: 46px !important;
    width: 161px !important;
    background: #ff8a00;
    border: 1px solid #ff8a00;
    color: #fff;
    font-size: 14px !important;
    cursor: pointer;
    outline: none;
    top: -11%;
    padding-top: -51px;
    margin-top: 0px;
    margin-left: -8px;
    position: relative;
    border-radius: 0px;
    }
    .sun_form_a {
    display: inline-block;
    width: 156px;
    height: 46px;
    float: right;
    line-height: 46px;
    background-color: rgb(6,185,254);
    color: white;
    /* margin-left: 27px; */
    text-align: center;
    font-size: 18px;
    border-radius: 5px;
    }

    #header_form .img {
    padding-left: 26px;
    width: 116px !important;
    height: 46px !important;
    border-right: 1px solid #ccc;
    line-height: 46px !important;
    }

    button#searchofinfo {
    padding-left: 33px;
    vertical-align: middle;
    text-align: center;
    justify-content: center;
    }

    .mainList .content .search .item .left {
    float: left;
    width: 8%;
    font-size: 16px;
    }

    .look{
    width: 300px;
    <%--height: 100px;--%>
    padding-top: -10px;
    margin-top: 3px;
    margin-left: -28px;
    }
    </style>
    <script src="new_js/jquery-1.11.3.js"></script>
    <script>
   		$(function () {
            addSelected();
        });
         function addSelected(){
        	$('#front_header .header_wrap .right').hover(function () {
                $('#front_header .header_wrap .top_r .main .icon img').attr('src', 'images/icon2.png');
                $('#front_header .header_wrap .top_r .out').show();
            }, function () {
                $('#front_header .header_wrap .top_r .main .icon img').attr('src', 'images/icon1.png');
                $('#front_header .header_wrap .top_r .out').hide();
            });
            $('#sorts').on('mouseover', 'li', function () {
                $(this).find('span img').attr('src', 'images/icon4.png');
            });
            $('#sorts').on('mouseout', 'li', function () {
                $(this).find('span img').attr('src', 'images/icon5.png');
            });

            $('.mainList .content .left li').click(function () {
                $(this).addClass('selected').siblings().removeClass('selected');
            });
            $('.mainList .content .search .item .right ul li').click(function () {
                $(this).addClass('selected').siblings().removeClass('selected');
            });
        }
    </script>

</head>
<!-- head end-->
<!-- body -->

<body style="background: #fff">

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title" id="myModalLabel">联系我们</h4>
		            </div>
		            <div class="modal-body">
		                <form>
		                    <div class="form-group">
		                        <label for="exampleInputName">姓名</label>
		                        <input type="text" class="form-control" id="ww1" placeholder="Email">
		                    </div>
		                    <div class="form-group">
		                        <label for="exampleInputPhone">电话号码</label>
		                        <input type="text" class="form-control" id="ww2" placeholder="Password">
		                    </div>
		                    <div class="form-group">
		                        <label for="exampleInputAddress">地址</label>
		                        <input type="text" class="form-control" id="ww3" placeholder="Password">
		                    </div>
		                    <!-- <button type="submit" class="btn btn-default">Submit</button> -->
		                </form>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
		               <!--  <button type="button" class="btn btn-primary">保存</button>  -->
		            </div>
		        </div>
		    </div>
		</div>
		<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
		<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
		<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
		<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- header-->
	<%@include file="/head.jsp"%>


    <div class="nav" id="nav_top">
        <div class="container">
            <div class="fl">
                <p class="look">
                    <a href="index">
                    <!--  -->
                    <span style="color: rgb(255,138,0);font-size: 36px;">科研成果共享平台</span>

                    </a>
                </p>
            </div>
            <div class="fr">
                <form action="#" id="header_form">
                    <input type="text" name="" id="likeStringofneed" placeholder="关键词" value="${param.string}" >
                    <input type="text" name="limit" hidden="hidden" value="10" placeholder="关键词">
                    <input type="text" name="offset" hidden="hidden" value="0" placeholder="关键词">
                    <button type="button" id="searchofneed"><span><img src="images/sy_seach.png"></span>搜索</button>
                    <a class="sun_form_a" onclick="mmm()">发布需求</a>
                    <!--<img src="images/search.png" />-->
    <p class="img">
    科研需求

    </p>
             </form>
            </div>
           
                <div>
                    
                </div>
          
        </div>
    </div>
  	<script>
		function mmm(){
			var upinfourl =  "/Scientific/publishDemand45?usertype="+ 0 +"&userid="+ 123;
			window.location.replace(upinfourl);
		}
	</script>
    <!-- header end-->
    <div class="mainList" style="background: #fff">
        <div class="bNav">您的位置：首页》
            <span>需求列表</span>
        </div>
        <div class="content">
            <div class="search">
                <div class='item'>
                    <div class="left">类别：</div>
                    <div class="right">
                        <ul id="typeUl">
                            <li class="selected" val="">不限</li>
                            <!-- <li>食品饮料</li>
                            <li>建筑建材</li>
                            <li>家居用品</li>
                            <li>轻功纺织</li>
                            <li>轻功纺织</li>
                            <li>化学化工</li>
                            <li>新能源</li>
                            <li>机械</li>
                            <li>生命科学成果</li>
                            <li>环保和资源</li>
                            <li>安全防护</li>
                            <li>交通运输</li> -->
                        </ul>
                    </div>
                </div>
                <div class='item'>
                    <div class="left">价格交易：</div>
                    <div class="right">
                        <ul id="priceUl">
                            <li class="selected" val="1">不限</li>
                            <li val="2">面议</li>
                            <li val="3">1-10万</li>
                            <li val="4">10-50万</li>
                            <li val="5">50-100万</li>
                            <li val="6">100-500万</li>
							<li val="7">500-1000万</li>
							<li val="8">1000万以上</li>
                        </ul>
                    </div>
                </div>
                <div class='item'>
                    <div class="left">需求类型：</div>
                    <div class="right">
                        <ul id="needTypeUl">
                            <li class="selected" val="">不限</li>
                            <!-- <li>专利</li>
                            <li>非专利</li>
                            <li>发明专利</li>
                            <li>实用新型专利</li>
                            <li>外观专利</li>
                            <li>软件著作权</li> -->
                            <li val="1">技术需求</li>
                            <li val="2">合作需求</li>
                            <li val="3">人才需求</li>
                        </ul>
                    </div>
                </div>
                <!-- <div class='item'>
                    <div class="left">交易方式：</div>
                    <div class="right">
                        <ul>
                            <li class="selected">不限</li>
                            <li>完全转让</li>
                            <li>许可转让</li>
                            <li>技术入股</li>
                            <li>5年独占许可</li>
                        </ul>
                    </div>
                </div>
                <div class='item'>
                    <div class="left">成熟度：</div>
                    <div class="right">
                        <ul>
                            <li class="selected">不限</li>
                            <li>正在研发</li>
                            <li>已有样品</li>
                            <li>通过小试</li>
                            <li>通过中式</li>
                            <li>可以量产</li>
                        </ul>
                    </div>
                </div> -->
            </div>

            <div class="listsearch_ad">
                <%--<div class="sousuo">--%>

                    <%--<input name="key" id="key" type="text" placeholder="请输入搜索内容" focucmsg="请输入搜索内容" onKeyUp="value=value.replace(/[^\a-\z\A-\Z0-9\u4E00-\u9FA5\ ]/g,'')"--%>
                        <%--onpaste="value=value.replace(/[^\a-\z\A-\Z0-9\u4E00-\u9FA5\ ]/g,'')" oncontextmenu="value=value.replace(/[^\a-\z\A-\Z0-9\u4E00-\u9FA5\ ]/g,'')"--%>
                    <%--/>--%>
                    <%--<a id="skey">搜索</a>--%>

                <%--</div>--%>
                <%--<div class="paixu">--%>
                    <%--<a href="javascript:void(0)" name="orderzixuncount" order="0" class="on">默认</a>--%>
                    <%--<a href="javascript:void(0)" name="orderzixuncount" order="1">人气&nbsp;&uarr;</a>--%>
                <%--</div>--%>
                <script>
                    $(".paixu a").click(function () {
                        $(this).addClass("on");
                        $(this).siblings().removeClass("on");
                    });
                </script>
                <div class="zhikan">
                    <label>
                        <input name="issmallproject" id="oceanEqueip" type="checkbox" value="2" />海洋装备
                    </label>
                    <%--<label>--%>
                        <%--<input name="ismatureproject" id="ismatureproject" type="checkbox" value="1" />成熟项目--%>
                    <%--</label>--%>
                </div>
            </div>

            <div class="list">
                <ul id="needlist" >
                    <li>
                        <div class="vertical">
                            技术需求
                        </div>
                        <div class="info">
                            <div class="title">一种感应发亮的鞋底的实用新型专利定制</div>
                            <div class="row2">
                                <div class="font">需求简介：
                                    <span>一种感应发亮的鞋底的实用新型专利定制</span>
                                </div>
                                <div class="font">地址：
                                    <span>浙江 温州</span>
                                </div>
                                <div class="font">发布时间：
                                    <span>2018-5-24</span>
                                </div>
                                <div class="font">行业类型：
                                    <span>教育领休闲</span>
                                </div>
                            </div>
                            <div class="row3">预算：
                                <span>5-10万</span>
                            </div>
                        </div>

                        <div class="active">
                            <div>
                                <a href="#">查看详情</a>
                            </div>
                            <div>
                                <a href="#this">联系方式</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="vertical">
                            技术需求
                        </div>
                        <div class="info">
                            <div class="title">一种感应发亮的鞋底的实用新型专利定制</div>
                            <div class="row2">
                                <div class="font">需求简介：
                                    <span>一种感应发亮的鞋底的实用新型专利定制</span>
                                </div>
                                <div class="font">地址：
                                    <span>浙江 温州</span>
                                </div>
                                <div class="font">发布时间：
                                    <span>2018-5-24</span>
                                </div>
                                <div class="font">行业类型：
                                    <span>教育领休闲</span>
                                </div>
                            </div>
                            <div class="row3">预算：
                                <span>5-10万</span>
                            </div>
                        </div>

                        <div class="active">
                            <div>
                                <a href="#">查看详情</a>
                            </div>
                            <div>
                                <a href="#this">联系方式</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="vertical">
                            技术需求
                        </div>
                        <div class="info">
                            <div class="title">一种感应发亮的鞋底的实用新型专利定制</div>
                            <div class="row2">
                                <div class="font">需求简介：
                                    <span>一种感应发亮的鞋底的实用新型专利定制</span>
                                </div>
                                <div class="font">地址：
                                    <span>浙江 温州</span>
                                </div>
                                <div class="font">发布时间：
                                    <span>2018-5-24</span>
                                </div>
                                <div class="font">行业类型：
                                    <span>教育领休闲</span>
                                </div>
                            </div>
                            <div class="row3">预算：
                                <span>5-10万</span>
                            </div>
                        </div>

                        <div class="active">
                            <div>
                                <a>查看详情</a>
                            </div>
                            <div>
                                <a href="#this">联系方式</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="box">
				<div id="pagination3" class="page fl"></div>
				<%--<div class="info fl">--%>
				<%--<p>当前页数：<span id="current3">1</span></p>--%>
			<%--</div>--%>
		</div>

		<script>
				 var tradeId="";
    			var ntypeId="";
    			var needContent=0;
  	  			var priceTag=1;
  	  			
			$(function() {
				name = $("#likeStringofneed").val();
				limit = 10;
				offset = 0;
				
				yemian();
			});
		
		$(document).ready(function() {
		name = $("#likeStringofneed").val();
		/* limit = 10;
		offset = 0; */
		needlistofsecond(name,limit,offset,tradeId,ntypeId,needContent,priceTag);
	$("#searchofneed").click(function(){
		$(location).attr('href', 'scienic_needList16?string=' + $("#likeStringofneed").val() + '');
	});
});

$(document).ready(function() {
	Date.prototype.Format = function (fmt) { //author: meizz 
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
	        "h+": this.getHours(), //小时 
	        "m+": this.getMinutes(), //分 
	        "s+": this.getSeconds(), //秒 
	        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	        "S": this.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	    for (var k in o)
	    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	    return fmt;
	}
	var m = new Object;
	m.name = $("#likeStringofneed").val();
	m.limit = 10;
	m.offset = 0;
	m.needContent = 0;
	//m.priceTag = 1;
	$.ajax({
		url:Server + "/fon/listfindlike",
		//url:"http://localhost/fon/listfindlike",
		data:m,
		type:"POST",
		dataType:"json",
		success : function(data){
			var html = "";
				var pattern = '<li>\
                <div class="vertical">技术需求</div>\
                <div class="info">\
                    <div class="title">#1</div>\
                    <div class="row2">\
                        <div class="font">需求简介：<span>#2</span>\
                        </div>\
                        <div class="font">地址：<span>#3</span>\
                        </div>\
                        <div class="font">发布时间：<span>#4</span>\
                        </div>\
                        <div class="font">行业类型：<span>#5</span>\
                        </div>\
                    </div>\
                    <div class="row3">预算：<span>#6万</span>\
                    </div>\
                </div>\
                <div class="active">\
                    <div>\
                        <a onclick="finddetail(#0)">查看详情</a>\
                    </div>\
                    <div>\
                        <a onclick="phone(#9)" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">联系方式</a>\
                    </div>\
                </div>\
            </li>';
				for(var i = 0; i< data.list.length; i++){
				var item = "";
				var order = data.list[i];
				var t = "";
				if(order.tTrade!=null){
					var t = order.tTrade.tradeName;
				}
				item += pattern
					.replace("#0", order.needId)//需求ID
					.replace("#1", order.needName)//需求名称
					.replace("#2", order.needExplain)//需求简介
					.replace("#3", order.address)//地址
					.replace("#4", new Date(order.needCreateTime).Format("yyyy-MM-dd"))//发布时间
					.replace("#5", t)//行业类别
					.replace("#6", order.needBudget)//预算
				    .replace("#9", order.needId);
				   
				html += item;
			}
			$("#needlist").html(html);
		},
	});
})
			function yemian(){
					$.ajax({
						url :Server + "/fon/listfindlike",
						//url :"http://localhost/fon/listfindlike",
						data : {
							"name":$("#likeStringofneed").val(),
							"limit":limit,
							"offset":offset,
							"tradeId":tradeId,
							"ntypeId":ntypeId,
							"needContent":needContent,
							"priceTag":priceTag
						},
						type : "POST",
						dataType: "JSON",
						success : function(data) {
							//alert(data.count);
							$("#pagination3").pagination({
								currentPage: 1,
								totalPage: Math.ceil(Number(data.count/10)),
								isShow: true,
								count: 7,
								homePageText: "首页",
								endPageText: "尾页",
								prevPageText: "<上一页",
								nextPageText: "下一页>",
								callback: function(current) {
									$("#current3").text(current);
									needlistofsecond($("#likeStringofneed").val(),limit,$("#pagination3").pagination("getPage").current-1,tradeId,ntypeId,needContent,priceTag);
								}
							});
						}
					});
				}
        </script>
        </div>
        <div class="pageButton">
        </div> 
        <!--找个分页插件插入-->
    </div>
    <script src="method/hhn/my.js"></script>
    <script src="js/jquery.pagination.min.js"></script>
    <%@include file="/footer.jsp"%>
    <script>
    	
    	$(document).ready(function(){
　　				$.ajax({
						url :Server + "/res/need/selectAllType",
						//url :"http://localhost/res/need/selectAllType",
						data : {
						},
						type : "POST",
						dataType: "JSON",
						success : function(data) {
							for(var i = 0; i < data.length; i++){
								$("#typeUl").append("<li val="+data[i].tradeId+">"+data[i].tradeName+"</li>")
							}
							addSelected();
							$("#typeUl li").click(function() {
								//alert($(this).attr("val"));
								tradeId=$(this).attr("val");
								//yemianCopy();
								yemian();
								$("#current3").text(1);
								needlistofsecond($("#likeStringofneed").val(),limit,offset,tradeId,ntypeId,needContent,priceTag);
							});
							$("#needTypeUl li").click(function() {
								//alert($(this).attr("val"));
								ntypeId=$(this).attr("val");
								//alert("tradeId:"+tradeId+",ntypeId:"+ntypeId);
								//yemianCopy();
								yemian();
								$("#current3").text(1);
								needlistofsecond($("#likeStringofneed").val(),limit,offset,tradeId,ntypeId,needContent,priceTag);
							});
							$("#oceanEqueip").click(function() {
								if($("#oceanEqueip").is(':checked')==true){
									needContent=$("#oceanEqueip").val();
								}else{
									needContent=0;
								}
								//yemianCopy();
								yemian();
								$("#current3").text(1);
								needlistofsecond($("#likeStringofneed").val(),limit,offset,tradeId,ntypeId,needContent,priceTag);
							});
							$("#priceUl li").click(function() {
								//alert($(this).attr("val"));
								priceTag=$(this).attr("val");
								//yemianCopy();
								yemian();
								$("#current3").text(1);
								needlistofsecond($("#likeStringofneed").val(),limit,offset,tradeId,ntypeId,needContent,priceTag);
							});
						}
					});
				
			});
function needlistofsecond(name,limit,offset,tradeId,ntypeId,needContent,priceTag){
	//alert("name:"+name+" limit:"+limit+" offset:"+offset+" tradeId:"+tradeId+" ntypeId:"+ntypeId+" needContent:"+needContent+" priceTag:"+priceTag);
	Date.prototype.Format = function (fmt) { //author: meizz 
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
	        "h+": this.getHours(), //小时 
	        "m+": this.getMinutes(), //分 
	        "s+": this.getSeconds(), //秒 
	        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	        "S": this.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	    for (var k in o)
	    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	    return fmt;
	}
	$.ajax({
		url:Server + "/fon/listfindlike",
		//url:"http://localhost/fon/listfindlike",
		data:{
			"name":name,
			"limit":limit,
			"offset":offset,
			"tradeId":tradeId,
			"ntypeId":ntypeId,
			"needContent":needContent,
			"priceTag":priceTag
		},
		type:"POST",
		dataType:"json",
		success : function(data){
			var html = "";
				var pattern = '<li>\
                <div class="vertical">技术需求</div>\
                <div class="info">\
                    <div class="title">#1</div>\
                    <div class="row2">\
                        <div class="font">需求简介：<span>#2</span>\
                        </div>\
                        <div class="font">地址：<span>#3</span>\
                        </div>\
                        <div class="font">发布时间：<span>#4</span>\
                        </div>\
                        <div class="font">行业类型：<span>#5</span>\
                        </div>\
                    </div>\
                    <div class="row3">预算：<span>#6万</span>\
                    </div>\
                </div>\
                <div class="active">\
                    <div>\
                        <a onclick="finddetail(#0)">查看详情</a>\
                    </div>\
                    <div>\
                        <a onclick="phone(#9)" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">联系方式</a>\
                    </div>\
                </div>\
            </li>';
				for(var i = 0; i< data.list.length; i++){
					
				var item = "";
				var order = data.list[i];
				var t = "";
				if(order.tTrade!=null){
					var t = order.tTrade.tradeName;
				}
				
				item += pattern
					.replace("#0", order.needId)//需求ID
					.replace("#1", order.needName)//需求名称
					.replace("#2", order.needExplain)//需求简介
					.replace("#3", order.address)//地址
					.replace("#4", new Date(order.needCreateTime).Format("yyyy-MM-dd"))//发布时间
					.replace("#5", t)//行业类别
					.replace("#6", order.needBudget)//预算
				    .replace("#9", order.needId);
				   
				html += item;
			}
			$("#needlist").html(html);
		},
	});
}
</script>
</body>
<!-- body end-->