<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>

	<head>
	<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>仪器共享</title>
		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">
		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
">
		<link rel="stylesheet" href="new_css//header_footer.css?t=1">
	<link rel="stylesheet" href="new_css/publishDemand.css">
	<link rel="stylesheet" href="css/common.css">
	<script src="css/ckeditor/ckeditor.js"></script>
		<style type="text/css">
			.li_row select{
				width: 28%!important;
			}
			.cke_chrome{
				display: block;
    border: 1px solid #b6b6b6;
    padding: 0;
    border-radius: 0px 0px 5px 5px;
    width: 86.8% !important;
    float: right;
    -moz-box-shadow: 0 0 3px rgba(0,0,0,.15);
    -webkit-box-shadow: 0 0 3px rgba(0,0,0,.15);
    box-shadow: 0 0 3px rgba(0,0,0,.15);
			}
		</style>
		<link rel="stylesheet" href="css/common.css">
		<script src="js/jquery-1.11.3.js"></script>
		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$('.mainList .content .left li').removeClass('selected');
					$(this).addClass('selected');
				});
				$('.mainList .content .left .list_header').click(function() {
					if($('.mainList .content .left .secendList').is(':hidden')) {
						$('.mainList .content .left .secendList').slideDown();
						$('.mainList .content .left .list_header img').attr("src", "images/insUser_icon.png");
					} else {
						$('.mainList .content .left .secendList').slideUp();
						$('.mainList .content .left .list_header img').attr("src", "images/icon4.png");
					};

				});
				/* $("#row5 font label").click(function() {
					$("#row5 font label img").attr('src', 'images/icon35.png');
					$("#row5 font label").siblings('input').prop('checked', false);
						$(this).find('img').attr('src', 'images/icon36.png');
						$(this).siblings('input').prop('checked', true);
				});
				$("#row8 font label").click(function() {
					$("#row8 font label img").attr('src', 'images/icon35.png');
					$("#row8 font label").siblings('input').prop('checked', false);
						$(this).find('img').attr('src', 'images/icon36.png');
						$(this).siblings('input').prop('checked', true);
				}); */
				
				
				
				//---------
				
				
				$('.rowRight .remove_btn').click(function(){
            		$(this).parent().remove();
            	});
				
				/* $('.add_btn').click(function(){
					
					var row_obj=$('<div class="item"></div>'); 
					var row_input=$('<input type="text" name="" id="" value="工艺美术" />'); 
					var row_img=$('<img src="images/publishDemandReduce.png" class="remove_btn"/>'); 
					var result=$('.sun_select').val();
					row_input.val(result);
					var row=$('.rowRight');
					$('.rowRight .remove_btn').click(function(){
            		$(this).parent().remove();
            		
            	});
					row_obj.append(row_input);
					row_obj.append(row_img);
					row.append(row_obj);
            		
            	}); */
			});
		</script>

	<style>
	.mainList .content .right li input {
	width:87%;
	height:40px;
	padding-left:15px;
	border: 1px solid #D3D3D3;
	border-radius: 5px;
	}
	.mainList .content .right li textarea {
	width:87%;
	height:330px;
	resize: none;
	padding:10px;
	border: 1px solid #D3D3D3;
	border-radius: 5px;
	}
	input,select {
	border: 1px solid #efefef;
	border-radius: 5px;
	}

	</style>

	</head>

	<body style="background:#fff;">

		<%@include file="/head.jsp"%>

		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
	<a href="index.jsp" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>
					</div>
					<div class="web_name">
						用户中心
					</div>
				</div>
			</div>
		</div>
		<div class="mainList">
			<div class="bNav">您的位置：
			<a href="index">首页</a> > 
			<a href="new_file39">用户中心</a> > 
			<span>发布难题</span></div>
			<div class="content">
			<%@include file="/left-tab.jsp"%>
				<form id="formtable">
				<div class="right">
					<div class="title"><span class="selected">发布难题</span></div>
					<div class="box">
						<ul>
							<li>
								<label>难题名称</label>
								<input type="text" name="" id="needName" value="" />
							</li>
							<li>
								<label>难题简介</label>
								<textarea style="height: 93px;" id="needExplain"></textarea>
							</li>
							
							<li>
								<label>详细描述</label>
						<!-- <textarea id="needDetails"></textarea> -->

						<textarea id="needDetails" rows="30" cols="50" name="editor01">请输入.</textarea>

 


							</li>
							<div style="clear:both"></div>
							<li class="row3">
								<label>行业类别</label>
								<select class="sun_select" id="hylb">
									<option>教育休闲</option>
									<option></option>
								</select>
								<img src="images/publishDemandAdd.png" class="add_btn"/>
							</li>
							
							<li class="row4">
								<div class='rowLeft'></div>
								<div class='rowRight'>
									
								</div>
							</li>
							
							
							<li class="row5" id="row5">
								<label>内容类型</label>
								<font>
									<input type="radio" name="b" id="hy" value="1" />
									<label onClick="chang_pic();"><img src="images/icon35.png" id="chang_pic"/><span>海洋装备</span></label>
								</font>
							</li>
							<li class="li_row">
								<label>所在地</label>
								<select class="sun_select" id="s" onchange="program();">
									<option>---请选择---</option>
									<option></option>
								</select>
								<select class="sun_select" id="s1" onchange="program1();">
									<option>---请选择---</option>
									<option></option>
								</select>
								<select class="sun_select" id="s2">
									<option>---请选择---</option>
									<option></option>
								</select>
								
							</li>
							<li class='rowBtn'>
								<label></label>
								<input type="button" name="" id="" value="提交" onclick="sub()" />
							</li>
						</ul>
					</div>
				</div>
				</form>
			</div>
			<div class="pageButton"></div>
			<!--找个分页插件插入-->
		</div>

		<%@include file="/footer.jsp"%>
		<script>
			$(document).ready(function(){
				city(0)
				tmt()
				var i = 1,valList = [];
				$(".add_btn").click(function(){
					var options=$("#hylb option:selected");
					var val = options.val();
					var txt = options.text();
					var str = "";
					if (valList.indexOf(val) >= 0) {
						alert('不能选重复的!');
						return;
					}
					if(i < 10){
						str += '<div class="item" id="inputclass">\
									<input type="text" name="tradeName" class="tradeName" value="'+ txt +'" data-appid="'+val+'"/>\
									<img onclick="clickimg(this)" id="remove" src="images/publishDemandReduce.png" />\
								</div>';
						$(".rowRight").append(str);
						valList.push(val)
						i++;
					}else{
						alert("最多添加9个");
					}
				});
					
			
				clickimg=function(m){
					var index = $(this).parent('.item').index();
					i = i - 1;
					valList.splice(index, 1);
					m.parentElement.parentElement.removeChild(m.parentElement);
				}
			});
			
			function city(pid){
				$.ajax({
					url:Server + "/problem_select/city",
					data:{
						"pid":pid,
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var html = "";
						var parent = '<option value="#1">#0</option>';
						for(var i = 0;i<data.length;i++){
							var item = "";
							var param = data[i];
							item += parent
								.replace("#0",param.cityName)
								.replace("#1",param.cityId),
							html += item;
						}
						$("#s").html(html);
						program();
					},
				});
			}
			
			function program(){
				var pid = $("#s").val();
				$.ajax({
					url:Server + "/problem_select/city",
					data:{
						"pid":pid,
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var html = "";
						var parent = '<option value="#1">#0</option>';
						for(var i = 0;i<data.length;i++){
							var item = "";
							var param = data[i];
							item += parent
								.replace("#0",param.cityName)
								.replace("#1",param.cityId),
							html += item;
						}
						$("#s1").html(html);
						program1();
					},
				});
			}
			
			function program1(){
				var pid = $("#s1").val();
				$.ajax({
					url:Server + "/problem_select/city",
					data:{
						"pid":pid,
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var html = "";
						var parent = '<option value="#1">#0</option>';
						for(var i = 0;i<data.length;i++){
							var item = "";
							var param = data[i];
							item += parent
								.replace("#0",param.cityName)
								.replace("#1",param.cityId),
							html += item;
						}
						$("#s2").html(html);
					},
				});
			}
			
			function sub(){
				var typeId = $('#usertypeoflogin').val();
				var id = $('#useridoflogin').val();
				var needName = $("#needName").val();
				var needExplain = $("#needExplain").val();
				var needContent = $("#hy").val();
				var needDetails = $("#needDetails").val();
				var needAddress = $("#s2").val();
				//var tradeName = $("#tradeName").data("appid");
				if(needName==""){
					alert("请输入难题名称");
					$("#needName").focus();
					return;
				}
				if(needExplain==""){
					alert("请输入难题简介");
					$("#needExplain").focus();
					return;
				}
				if(needDetails==""){
					alert("请输入详细描述");
					$("#needDetails").focus();
					return;
				}
				if($(".item").length==0){
					alert("请至少选择一种行业类别");
					$("#hylb").focus();
					return;
				}
				var tradeId = [];
        			for (var i = 0; i < $('.tradeName').length; i++) {
           				tradeId.push($('.tradeName').eq(i).attr('data-appid'));
        			}
				$.ajax({
					url:Server + "/problem_select/puzzle",
					//url:"http://localhost/problem_select/puzzle",
					data:{
						"typeId":typeId,
						"uid":id,
						"needName":needName,
						"needExplain":needExplain,
						"needContent":needContent,
						"needDetails":needDetails,
						"needAddress":needAddress,
						"tradeId":tradeId.join(),
					},
					type:"POST",
					dataType:"json",
					success : function(data) {
						if(data == 1){
							alert("添加成功");
							$(location).attr("href","new_file38_1");
						}else if(data == 0){
							alert("添加失败");
						}
					},
					error : function(msg) {
						alert("添加失败");
					}
				});
			}
			
			function chang_pic(){
				var imgObj = document.getElementById("chang_pic");
				var Flag=(imgObj.getAttribute("src",2)=="images/icon35.png")
				imgObj.src=Flag?"images/icon36.png":"images/icon35.png";
				var valObj=$("#hy").val();
				var flag=1;
				valObj=(valObj==flag)?"3":"1";
				$("#hy").val(valObj);
			}
			
			function tmt(){
				$.ajax({
					url:Server + "/problem_select/tmtList",
					data:{},
					type:"POST",
					dataType:"json",
					success : function(data){
						var html = "";
						var parent = '<option value="#1">#0</option>';
						for(var i = 0;i< data.length;i++){
							var item = "";
							var param = data[i];
							item += parent
								.replace("#0",param.tradeName)
								.replace("#1",param.tradeId),
							html += item;
						}
						$("#hylb").html(html);
					}
				});
			}
			
		</script>
		<script type="text/javascript">CKEDITOR.replace('editor01');</script>

	</body>

</html>