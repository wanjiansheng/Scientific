<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
	<head>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<title>仪器共享</title>

		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">

		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。

">
		<link rel="stylesheet" href="new_css/header_footer.css">
		<link rel="stylesheet" href="new_css/bookingOrderDetail.css">
		<link rel="stylesheet" href="css/common.css">
		<script src="new_js/jquery-1.11.3.js"></script>
		<style type="text/css">
		.box_row1>input{
			width: 968px!important;
		}
		.box_row2>textarea{
			width: 968px;
			height: 220px;
		}
		.box_row3 .box_row3_label{
			width: auto!important;
		}
		.box_row3 .box_row3_a{
			width: 120px;
			height: 55px;
			line-height: 55px;
			text-align: center;
			color: white;
			border-radius: 5px;
			background-color: rgb(255,138,0);
			display: inline-block;
		}
		.mainList .content {
    width: 100%;
    overflow: hidden;
    height: 1274px;
}
.box_row2>div {
    width: 968px;
    height: 220px;
    border-radius: 5px;
}
.li_box {
    border: 1px solid rgb(216,216,216);
    display: inline-block;
    padding: 10px;
}
.li_row>label {
    display: inline;
    cursor: pointer;
    width: 800px!important;
    vertical-align: middle;
    height: 70px;
    
}
.li_row>label img {
    vertical-align: middle;
}
.li_row>label span {
    vertical-align: middle;
    padding-left: 10px;
    line-height: 30px!important;
}
a.haha {
    width: 200px;
    height: 50px;
    background-color: #ff8a00;
    display: block;
    line-height: 50px;
    text-align: center;
    color: #fff;
    border-radius: 5px;
    margin-left: 150px;
    margin-bottom: 50px;
}

.mainList .content .right li textarea{
	width: 968px !important;
}

#central3{
	width: 840px;
    height: 54px;
    padding-left: 20px;
    outline: none;
    outline: none;
    border: 1px solid #ccc;
    border-radius: 5px;
    outline: none;
}

		</style>
		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$(this).addClass('selected').siblings().removeClass('selected');
				});
				//$(".li_row label").click(function() {
//					if($(this).siblings('input').is(':checked')){
//						$(this).find('img').attr('src','images/icon35.png');
//						$(this).siblings('input').prop('checked',false);
//					}else{
//						$(this).find('img').attr('src','images/icon36.png');
//						$(this).siblings('input').prop('checked',true);
//					};
					/*$(".li_row label img").attr('src', 'images/icon35.png');
					$(".li_row label").siblings('input').prop('checked', false);
					$(this).find('img').attr('src', 'images/icon36.png');
					$(this).siblings('input').prop('checked', true);
				}); */
			});
		</script>

	</head>

	<body style="background:#fff;">

		<%@include file="/head.jsp"%>

		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
						<a class="logo-text" style="color: rgb(255,138,0);font-size: 30px;" onclick="indes();">科研成果共享平台</a>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="mainList">
			<div class="bNav">您的位置：首页》用户中心<span>需求申请管理</span></div>
			<div class="content">
				<div class="right">
					<!--<div class="title"><span></span></div>-->
					<div class="box">
						<div class="box_title">需求方基本信息</div>
						<ul>
							<li>
								<label>姓名</label>
								<input type="text"  disabled="disabled" placeholder="" name="" id="top1" value="" />
							</li>
							<li>
								<label>联系电话</label>
								<input type="text" name=""  disabled="disabled" id="top2" value="" />
							</li>
							<li>
								<label>电子邮箱</label>
								<input type="text" placeholder=""  disabled="disabled"  name="" id="top3" value="" />
							</li>
							<li>
								<label>单位姓名</label>
								<input type="text" name="" disabled="disabled"  id="top4" value="" />
							</li>
							<li class="box_row1">
								<label>需求名称</label>
								<input type="text" placeholder="请输入单位名称"  disabled="disabled"  name="" id="central1" value="" />
							</li>
							<li class="box_row2">
								<label>需求简介</label>
								<textarea placeholder="请输入需求"  disabled="disabled"  id="central2"></textarea>
							</li>
							<li class="box_row3">
								<label >上传文档</label>
								<label class="box_row3_label" for="sun_file">
								<input type="text" placeholder="请选择要上传的文档"  name="" disabled="disabled"  id="central3" value="" />
								<input  type="file" id="sun_file" hidden="hidden" placeholder="请选择要上传的文档" />
								<a  class="box_row3_a">浏览</a>
								</label>
								<!--<input type="text" placeholder="请输入单位名称"  name="" id="" value="" />-->
							</li>
							<li class="box_row2">
								<label>需求方案</label>
								<div class="li_box">
									<div class="li_row">
										<label><img src="images/icon35.png"/><span id="central4" data-appid="">完全转让 完全转让完全转让完全转让完全转让完全转让完全转让完全转让完全转让完全转让完全转让完全转让</span></label>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="box">
						<div class="box_title">评审意见信息</div>
						<ul>
							<li class="box_row2">
								<label>评审意见</label>
								<textarea id="reviewContent" placeholder="请输入需求"></textarea>
							</li>
							
							
						</ul>
					</div>
					
					
					<a class="haha" onclick="comm();">上传评审意见</a>
				</div>
			</div>
			<div class="pageButton"></div>
			<!--找个分页插件插入-->
		</div>

		<%@include file="/footer.jsp"%>
		<script>
			$(location).ready(function(){
				load()
				need()
				techCC()
			})
			
			function load(){
				$.ajax({
					url:Server + "/eulist/user",
					data:{
						"typeId":${param.needUserType},
						"id":${param.needUserId},
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						if(${param.needUserType} == 1){
							$("#top1").val(data[0].puRealname);
							$("#top2").val(data[0].puPhone);
							$("#top3").val(data[0].puEmail);
							$("#top4").val(data[0].puUnit);
						}else if(${param.needUserType} == 0){
							$("#top1").val(data[0].uuContacts);
							$("#top2").val(data[0].uuPhone);
							$("#top3").val(data[0].uuEmail);
							$("#top4").val(data[0].uuUnitName);
						}
					}
				});
			}
			
			function need(){
				$.ajax({
					url:Server + "/eulist/applyDetails",
					data:{
						"applyId":${param.applyId},
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						$("#central1").val(data[0].demand.needName);
						$("#central2").val(data[0].demand.needExplain);
						var docHtml = '<a style="color: red; font-size: 20px" class="right_a" href=' + server + data[0].uploadDocument + ' target="_blank">查看文档</a>';
						$("#central3").val(data[0].uploadDocument);
						$(".box_row3_a").html(docHtml);
					}
				});
			}
			
			function techCC(){
				$.ajax({
					url:Server + "/eulist/techCC",
					data:{
						"applyId":${param.applyId},
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var html = "";
						var pattren = '<label><img src="images/icon35.png" data-appid="#1"/><span>#0</span></label>';
						for(var i = 0;i<data.length;i++){
							var item = "";
							var param = data[i];
							item += pattren
								.replace("#0", param.techName)
								.replace("#1", param.ati.techId),
							html += item;
						}
						$('.li_row').html(html);
						$(".li_row label").click(function() {
							var image = $(this).find('img').attr('src');
							image = (image=='images/icon35.png')?'images/icon36.png':'images/icon35.png';
							$(this).find('img').attr('src',image);
						});
					}
				});
			}
			
			function comm(){
				var tech = []
				for(var i = 0; i < $('.li_row').find("[src='images/icon36.png']").length;i++){
					tech.push($('.li_row').find("[src='images/icon36.png']").eq(i).attr('data-appid'));
				}
				var euId = ${param.euId};
				var reviewContent = $('#reviewContent').val();
				$.ajax({
					url:Server + "/eulist/techComm",
					data:{
						"euId":euId,
						"reviewContent":reviewContent,
						"tech": tech.join(),
					},
					type:"POST",
					success : function(data){
						alert("提交成功")
						$(location).attr("href","new_file48?euId="+euId+"");
					},
					error : function(msg){
						alert("提交失败")
					}
				});
			}
			
			function indes(){
				$(location).attr("href","index");
			}
		</script>

	</body>

</html>