<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<!-- head -->

<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<title>仪器共享</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">
<meta name="description"
	content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。

">
	
<link rel="stylesheet" href="css/l_css/header.css">
<link rel="stylesheet" href="css/l_css/main.css">
	<link rel="stylesheet" href="css/common.css">

	<!--<link rel="stylesheet" type="text/css" href="css/main_style.css" />-->
<style type="text/css">
.title {
	width: 100%;
	margin-bottom: 8px;
}

.title  .wrap {
	width: 1200px;
	margin: auto;
}

.title a {
	width: 173px;
	height: 37px;
	display: inline-block;
	text-align: center;
	line-height: 37px;
}

.title .selected {
	color: #fff;
	background: #ff8a00;
}

.vertical {
	width: 44px;
	background-color: white;
	display: block;
	widows: 10px;
	height: 166px;
	text-align: center;
	line-height: 42px;
	color: #fff;
	font-size: 22px;
}

/* --------- */
.tabBtn a {
    display: block;
}
.tabBtn {
	margin: 0 auto;
	margin-top: 10px;
	width: 1200px;
	overflow: hidden;
	height: 53px;
}

.tabBtn .tab1 {
	float: left;
	width: 10%;
	border: 1px solid #d3d3d3;
	/*border-left: none;*/
	height: 100%;
	line-height: 51px;
	text-align: center;
	cursor: pointer;
}

.tabBtn .tab2 {
	float: left;
	width: 10%;
	border-top: 2px solid #ff8a00;
	border-right: 1px solid #d3d3d3;
	border-left: 1px solid #d3d3d3;
	height: 100%;
	line-height: 51px;
	text-align: center;
	cursor: pointer;
}

.tabBtn .tab3 {
	float: left;
	width: 80%;
	border-bottom: 1px solid #d3d3d3;
	height: 100%;
	line-height: 51px;
	text-align: center;
}

.mainList .content .list li .active>div a{
	    cursor: pointer;
}
</style>
<script src="js/jquery-1.11.3.js"></script>
<script>
	$(function() {
		$('#front_header .header_wrap .right').hover(function() {
			$('#front_header .header_wrap .top_r .main .icon img').attr('src', 'images/icon2.png');
			$('#front_header .header_wrap .top_r .out').show();
		}, function() {
			$('#front_header .header_wrap .top_r .main .icon img').attr('src', 'images/icon1.png');
			$('#front_header .header_wrap .top_r .out').hide();
		});
		$('#sorts').on('mouseover', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon4.png');
		});
		$('#sorts').on('mouseout', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon5.png');
		});

		$('.mainList .content .left li').click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
		});
		$('.mainList .content .search .item .right ul li').click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
		});
	});
</script>

<script>
	$(document).ready(function() {
		$("#publishNeed").click(function() {
			$(location).attr('href', 'publishDemand45');
		});

	})
</script>

</head>
<!-- head end-->
<!-- body -->

<body style="background: #fff">
	<!-- header-->
	<%@include file="/head.jsp"%>

	<div class="logo-search-btn">
	<div class="logo-search">
	<!--left-->
	<div class="header-logo header-logo-change fl">
	<h1><a href="index.jsp">科研成果共享平台</a></h1>
	</div>
	<!--right-->
	<div class="header-search fl">
	<div class="searchOption fl">
	<p class="search-down" >
	<a id="claa">难题发布</a>
	</p>
	</div>
	<div class="searchText fl">
	<input type="text" placeholder="关键词" id="ss">
	</div>
	<div class="searchBtn fl" onclick="Mr(0)">
	<img src="images/sy_seach.png">
	<span>搜索</span>
	</div>
	</div>

	<!--btn-->
	<div class="kindOfBtn fl">
	<a href="new_file38"> <input type="submit" id="publishNeed" value="难题发布"></a>
	</div>
	</div>
	</div>
	<div class="clear"></div>
	<!---->
	<div class="clear"></div>
	<div class="navBar-nav">
	<nav>
	<ul>
	<li  style="padding:0"><a href="index.jsp" id="shouye" class="selected">科研成果</a></li>
	<li><a href="new_file7" id="myself">科研需求展示</a></li>
	<li><a href="scienic_expertsshow11" id="zhuanjia">专家展示</a></li>
	<li><a href="new_file14" id="allNeed">交易大厅</a></li>
	<li class="active-one"><a href="new_file15" id="nanti">难题发布</a></li>
	</ul>
	</nav>
	</div>
	<div class="clear"></div>
	<%--<div class="title" style="border-bottom:2px solid #ff8a00;">--%>
		<%--<div class="wrap">--%>
			<%--<a onclick="index();">科研成果展示</a><a onclick="achievement();" class="">科研需求展示</a><a--%>
				<%--onclick="expert();">专家展示</a><a onclick="deal();">交易大厅</a><a class="selected"--%>
				<%--onclick="problem();">难题发布</a>--%>
		<%--</div>--%>
	<%--</div>--%>

	<div class="tabBtn">
		<div class="tab2 tab_span1" id="xz">
			<a onclick="Mr(0)">默认所有</a>
		</div>
		<div class="tab1 tab_span2" id="xz1">
			<a onclick="Mr(1)">海洋装备</a>
		</div>
	</div>
	<!-- header end-->
	<div class="mainList" style="background: #fff">
		<div class="content">
			<div class="list">
				<ul>
				</ul>
			</div>
		</div>
		<div class="pageButton"></div>
		<!--找个分页插件插入-->
	</div>

	<%@include file="/footer.jsp"%>
	<script>
		$(document).ready(function(){
			Mr(0);
		})
		
		function Mr(nc){
			$.ajax({
				url :Server + "/problem_select/list",
				data : {
					"limit" : "10",
					"offset" : "0",
					"needContent": nc,
					"name":$("#ss").val(),
				},
				type : "POST",
				dataType : "json",
				success : function(data) {
					if(nc == 0){
					var css = '<div class="tab2 tab_span1">\
									<a onclick="Mr(0)">默认所有</a>\
								</div>\
								<div class="tab1 tab_span2">\
									<a onclick="Mr(1)">海洋装备</a>\
								</div>';
					}else if(nc == 1){
					var css = '<div class="tab1 tab_span1">\
									<a onclick="Mr(0)">默认所有</a>\
								</div>\
								<div class="tab2 tab_span2">\
									<a onclick="Mr(1)">海洋装备</a>\
								</div>';
					}
					var html = "";
					var pattern = '<li>\
						<div class="vertical"></div>\
						<div class="info">\
							<div class="title">#0</div>\
							<div class="row2">\
								<div class="font">\
									难题简介： <span>#1</span>\
								</div>\
							</div>\
							<div class="row3">\
								<div style="display:inline-block;margin-right:70px;"\
									class="font">\
									行业类型： <span>#2</span>\
								</div>\
								<div style="display:inline-block" class="font">\
									地址： <span>#3</span>\
								</div>\
							</div>\
						</div>\
						<div class="active">\
							<div>\
								<a onclick="details(#4);" style="background-color: rgb(255,138,0);">查看详情</a>\
							</div>\
						</div>\
					</li>';
					for (var i = 0; i < data.length; i++) {
						var item = "";
						var order = data[i];
						if(order.tTrade != null){
							var tradeName = order.tTrade.tradeName;
						}else if(order.tTrade == null){
							var tradeName = "未知";
						}
						if(order.address != null){
							var address = order.address;
						}else if(order.address == null){
							var address = "未知";
						}
						item += pattern
							.replace("#0", order.needName)
							.replace("#1", order.needExplain)
							.replace("#2", tradeName)
							.replace("#3", address)
							.replace("#4", order.needId),
						html += item;
					}
					$(".list ul").html(html);
					$(".tabBtn").html(css);
				},
			});
		}
	</script>
	<script>
		function Marine(){ <!-- 海洋设备 -->
			$(location).attr("href","new_file20");
		}
		
		function details(id){ <!-- 详情 -->
			$(location).attr("href","new_file16?needId="+id+'');
		}
		
		function achievement(){//科研需求展示
			$(location).attr("href","new_file7");
		}
		
		function expert(){//专家展示
			$(location).attr("href","scienic_expertsshow11");
		}
		
		function deal(){//交易大厅
			$(location).attr("href","new_file14");
		}
		
		function problem(){//难题发布
			$(location).attr("href","new_file15");
		}
		
		function index(){
			$(location).attr("href","index");
		}
		
		function demand(){
			$(location).attr("href","updata34");
		}
	</script>
	<script type="text/javascript" src="js/common.js"></script>
</body>
</html>