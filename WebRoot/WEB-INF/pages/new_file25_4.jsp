<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>


<!-- 汪程序员 -->
<!-- 我是程序员 -->
	<head>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<title>仪器共享</title>

		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">

		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。

">
<script src="js/jquery-1.11.3.js"></script>
<script src="js/zoomify.min.js"></script>
		<link rel="stylesheet" href="new_css/zoomify.min.css">
		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/comments.css">
		<link rel="stylesheet" href="css/common.css">
		<style>
	div#nav_top > div.container > div.fl{
	width: 100% !important;
	padding-top: 50px;
	margin-top: 15px;
	}

	.bNav{
	width: 1200px;
	margin: 0 auto;
	font-size: 16px;
	}
	.bNav span{
		color: #FF8A00;
	}
	.wrap {
	height: 32px;
	border-bottom: 1px solid #ccc;
	margin-left: 20px;
	margin-top: 20px;
	}
	.wrap span {
	 border-bottom: 3px solid #ff8a00;
	}
	.mainList {
	width: 1200px;
	margin: 0 auto;
	border: 1px solid #ccc;
	margin-bottom: 72px;

	margin-top: 20px;
	}
	.box {
	margin-top: 20px;
	}
	.box_title {
	margin-left: 30px;
	margin-bottom: 20px;
	}
	.item_title {
	display: inherit;
	background-color: #ededed;
	height: 40px;
	line-height: 40px;
	color: #202020;
	font-size: 16px;
	padding-left: 30px;
	}
	.row1 {
	margin-left: 30px;
	font-size: 18px;
	margin-top: 20px;
	}
	.row2 {
	margin-left: 30px;
	}
	.row2 span {
	font-size: 14px;
	color: #999;
	/* margin-left: 30px; */
	}
	.row2 a {
	font-size: 16px;
	}
	.row3 {
	margin-right: 20px;
	}
	.xuxian {
	border: 1px dashed #ededed;
	margin-left: 30px;
	margin-right: 20px;
	margin-bottom: 30px;
	}
	.caoni {
	margin-left: 30px;
	margin-bottom: 30px;
	font-size: 16px;
	}
	textarea#textCom {
	margin: 30px auto;
	margin-left: 1.4%;
	}
	textarea {
	width: 97% !important;
	height: 330px;
	resize: none;
	padding: 10px;
	border: 1px solid #D3D3D3 !important;
	
	}
	.mainList .content .right .box .box_content .item {
	float: left;
	width: 100%;
	}
	.woqu a {
	width: 180px;
	height: 46px;
	display: inline-block;
	line-height: 46px;
	background-color: #ff8a00;
	text-align: center;
	font-size: 16px;
	border-radius: 5px;
	color: #fff;
	margin-left: 43%;
	margin-bottom: 30px;
	clear: both;
	}
	.row.row-img {
	margin-top: 20px;
	}
	.pic {
	margin-left: 110px;
	margin-top: -20px;
	}
	.mainList .content .right .box .box_content .item1 .item_content .text .row3 {
	margin-bottom: 20px;
	font-size: 18px;
	color: #ed1717;
	text-align: right;
	}
	.this-haha {
	border-bottom: 1px dashed #ccc;
	}

	a.hannan {
	/* float: left; */
	width: 150px;
	height: 40px;
	line-height: 40px;
	display: inline-block;
	background-color: #06B9FE;
	text-align: center;
	font-size: 16px;
	border-radius: 5px;
	color: #fff;
	margin-left: 30px;
	}
	</style>
		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$(this).addClass('selected').siblings().removeClass('selected');
				});
			});
		</script>

	</head>

	<body style="background:#fff;">

		<%@include file="/head.jsp"%>

		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
						<a class="logo-text" onclick="indexs();">科研成果共享平台</a>
					</div>
					<div class="web_name">
						<img src="images/icon3.png" />
						<p>yq.shang.com</p>
					</div>
				</div>
			</div>
		</div>
	<div class="bNav">您的位置：
	<a href="index">首页</a> >
	<a href="new_file39">用户中心</a> >
	<span>评价</span></div>
		<div class="mainList">


	<div class="jiangui">
		<div class="content" id="height-content">
	<div class="right">
	<div class="title"><div class="wrap"><span>需求评价</span></div></div>
	<div class="box">
	<div class="box_title">对需求进行评价</div>
	<div class="box_content">
	<div class="item item1">
	<div class="item_title">服务名称</div>
	<div class="item_content">
	<div class="text">
	<div class="row1"><span id="in0">一种新型自动化分拣装置的实用新型专利定制</span></div>
	<div class="row2"><span>需求简介：</span><a id="in1">一种新型自动化分拣装置的实用新型专利定制,一种新型自动化分拣</a></div>
	<div class="row3">¥<span id="in2">260</span>万元/起</div>
	</div>
	</div>
	</div>

	</div>
	<div class="box_content">
	<div class="item item2 item_content">
	<div class="xuxian"></div>
	<div class="item_content caoni">
	<label>上传文档:</label>
	<div class="row" id="doc"></div>
	<div class="row row-img">
	<label>交易合同:</label>
	<%--点击这里面显示合同的照片--%>
	<div class="pic">
		<img src="">
	</div>
	</div>
	</div>
	</div>
	<div class="item item3">
	<div class="item_title">给您留下的印象</div>
	<div class="item_content">
	<textarea placeholder="请填写交流过程中的问题以及服务" id="textCom"></textarea>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="woqu"><a onclick="comment();">评论</a></div>

		</div>


	<%@include file="/footer.jsp"%>
		<script>
			$(document).ready(function(){
				load();
				next(${param.redalId});
		//点击图片放大
		$('.pic img').zoomify();
			});
			
			function load(){
				$.ajax({
					url:Server + "/res/need/detail",
					data:{
						"offset":0,
						"limit":10,
						"needId":${param.needId},
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var p = data.rows
						$("#in0").html(p[0].needName);
						$("#in1").html(p[0].needExplain);
						$("#in2").html(p[0].needBudget);
					},
				});
			}
			
			function comment(){
				var ncomentContent = $("#textCom").val();//评论内容
				var ncomentGrades = ${param.redalId};//订单id
				var unId = ${param.needId};//需求id
				var typeId = $('#usertypeoflogin').val();//用户类型id
				var id = $('#useridoflogin').val();//用户id
				$.ajax({
					url:Server + "/res/need/addComment",
					data:{
						"id":id,
						"typeId":typeId,
						"unId":unId,
						"ncomentGrades":ncomentGrades,
						"ncomentContent":ncomentContent,
						"rdealId":${param.redalId},
					},
					type:"POST",
					success : function(data){
						if(data == 0){
							alert("提交失败");
						}else if(data == 1){
							alert("提交成功");
							$(location).attr("href","orderManagement37");
						}
					},
				});
			}
			
			function next(redalId) {
	$.ajax({
		url :Server + "/res/dea/url",
		type : "POST",
		dataType : "JSON",
		data : {
			"id" : redalId
		},
		async : false,
		success : function(data) {
			var imgHtml = '';
			var docHtml = '';
			for (var i = 0; i < data.length; i++) {
				var o = data[i].sdc;
				for(var j = 0;j<o.length;j++){
					if (o[j].upType == 1) {
						sdcPath = o[j].path;
						imgHtml += '<img class="img-rounded zoomify" src=' + Server +'/'+ sdcPath + '>';
					}else if (o[j].upType == 2) {
						sdcPath = o[j].path;
						docHtml += '<a style="color: red; font-size: 20px" class="right_a" href=' + Server +'/'+ sdcPath + ' target="_blank">点击下载查看交易合同</a>';
					}
				}
			}
			$(".pic").html(imgHtml);
			$("#doc").html(docHtml);

		}
	})
}
			function indexs(){
				$(location).attr("href","index");
			}
			
			function Marine(){
				$(location).attr("href","new_file20");
			}
		</script>
	</body>

</html>