<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>成果共享</title>
    <meta name="keywords" content="科研成果，生命科学成果，环境检测成果，实验常用设备，分析成果">

    <meta name="description" content="中国领先的科研成果共享平台，让您快速找到各种类型的科学研究成果。提升闲置成果利用率，产生更大科研价值。涵盖：生命科学成果、环境检测成果、实验常用设备、分析成果、仪表、物性测试、测量/计量成果、在线及过程控制成果。

">


    <!-- 引入 Bootstrap 的 CSS 文件 -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/l_css/main.css">
    <link rel="stylesheet" href="css/l_css/experts.css">
    <link rel="stylesheet" href="css/index.css?t=201711101207">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="css/sunstyle.css" />
    <link rel="stylesheet" href="css/l_css/header.css">
    <link rel="stylesheet" href="css/jquery.pagination.css" />
    <link rel="stylesheet" href="css/common.css">
    <style type="text/css">
    	
div#nav_top>div.container>div.fr {
    width: 850px;
}
#header_form {
    float: left;
    width: 100%;
    margin-top: 60px;
    position: relative;
    margin-left: 20px;
}
#header_form input {
    width: 60%;
    height: 52px;
    float: left;
    padding-left: 117px;
    border: 1.5px solid rgb(255,138,0);
    border-right: none;
    outline: none;
    font-size: 12px;
    font-size: 17px;
}
#header_form .img {
    position: absolute;
    top: 50%;
    left: 16px;
    height: 50px;
    width: 110px;
        font-size: 18px;
    padding: 0 5px;
        border-left: none !important;
    color: rgb(255,138,0);
    border-left: 1px solid rgb(255,138,0);
    border-top: 1px solid rgb(255,138,0);
    border-bottom: 1px solid rgb(255,138,0);
    transform: translateY(-50%);
    -webkit-transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
}
#header_form button {
    float: left;
    width: 15.75757576%;
    height: 52px;
    background: rgb(255,138,0);
    border: 1px solid rgb(255,138,0);
    color: #fff;
    cursor: pointer;
    outline: none;
    font-size: 18px;
}
.sun_form_a {
    display: inline-block;
    width: 120px;
    height: 52px;
    line-height: 52px;
    background-color: rgb(6,185,254);
    color: white;
    margin-left: 27px;
    text-align: center;
    border-radius: 3px;
}
.d5_pert .d5_ptl img {
    margin: 10px auto;
    display: block;
    width: 100%;
    height: 100%;
}
.d5_pert .d5_ptl {
    width: 120px;
    height: auto;
    margin-top: 2px;
    margin-left: 11px;
    margin-bottom: 2px;
    float: left;
    position: relative;
}








/*搜索*/

div#nav_top {
    /* height: 90px; */
    background: url(image/sy_bg.png) no-repeat center;
    height: 175px;
    width: 1200px;
    margin: 0 auto;
    background-size: cover;
}

#header_form {
    float: left;
    width: 100%;
    margin-top: 60px;
    position: relative;
    margin-left: 0px;
}
#header_form input {
    width: 447px;
    height: 46px;
    float: left;
    padding-left: 117px;
    border: 1px solid rgb(255,138,0);
    border-right: none;
    outline: none;
    font-size: 12px;
    font-size: 12px;
    padding-left: 130px;
    border-radius: 0px;
}

#header_form img {
    position: absolute;
    top: 50%;
    left: 57%;
}

#header_form .img {
    position: absolute;
    top: 50%;
    /*left: 57%;*/
    line-height: 46px;
    height: 46px;
    width: 100px;
    border-right: 1px solid #ccc;
    font-size: 18px;
    padding: 0 5px;
}
#header_form button {
    float: left;
    width: 161px;
    height: 46px;
    background: rgb(255,138,0);
    border: 1px solid rgb(255,138,0);
    color: #fff;
    cursor: pointer;
    outline: none;
        padding-left: 16px;
    font-size: 14px;
}
    </style>

    <script src="js/jquery-1.11.3.js"></script>

    <!-- 引入 Bootstrap 的 JS 文件 -->

    <script src="js/bootstrap.min.js"></script>

    <!--[if lt IE 9]><![endif]-->

    <script src="js/html5shiv.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/page.js"></script>

    <script src="js/respond.min.js"></script>

    <script src="js/baidu.js"></script>
    <script>
        $(function () {
            $('#front_header .header_wrap .right').hover(function () {
                $('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
                $('#front_header .header_wrap .right .out').show();
            }, function () {
                $('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
                $('#front_header .header_wrap .right .out').hide();
            });
            $('#sorts').on('mouseover', 'li', function () {
                $(this).find('span img').attr('src', 'images/icon4.png');
            });
            $('#sorts').on('mouseout', 'li', function () {
                $(this).find('span img').attr('src', 'images/icon5.png');
            });
        });
    </script>
</head>



<!-- itwangyang520 -->

<body style="background-color: #fff">
    <!--<link rel="stylesheet" href="css/header_footer.css?t=1">-->
    <%@include file="/head.jsp"%>


    <div class="nav" id="nav_top">
        <div class="container">
            <div class="fl">
                <a href="index">
                    <span style="color: rgb(255,138,0);font-size: 34px;">科研成果共享平台</span>
                    <input id="needstring" type="hidden"  value="${param.string}" />
                </a>
            </div>
            <div class="fr">
                <form action="#" id="header_form">
                    <input type="text" name="name" id="specialistString" placeholder="关键词" value="${param.string}">
                    <input type="text" name="limit" hidden="hidden" value="10" placeholder="关键词">
                    <input type="text" name="offset" hidden="hidden" value="0" placeholder="关键词">
                    <button type="button" id="specialist"><img src="images/searchIcon.png" />
                    	搜索</button>
                    <!-- <a class="sun_form_a" href="#">发布成果</a> -->
                    <!--<img src="images/search.png" />-->
                    <p class="img">
                                专家展示

                    </p>
                </form>
            </div>
           
                <div>
                    
                </div>
          
        </div>
    </div>
    
    
    <script src="js/public.js?t=3"></script>

    <!--<div class="" id="instrument">

        <div class="container">

            <div class="title_first">

                <a class="fl " href="instrumentList11.html" style="width: 150px;text-align: center; display: block;">

                    科研成果展示

                </a>

                <a class="fl " href="instrumentList11.html" style="width: 150px;text-align: center; display: block;">

                    科研需求展示

                </a>
                <a class="fl " href="insKnowledge12.html" style="width: 150px;text-align: center;display: block;">

                    专家展示

                </a>
                <a class="fl " href="insKnowledge12.html" style="width: 150px;text-align: center;display: block;">

                    交易大厅

                </a>
                <a class="fl " href="insKnowledge12.html" style="width: 150px;text-align: center;display: block;">

                    难题发布

                </a>

            </div>

        </div>
        <div class="hengxian" style="background: #fff; border-top: 2px solid #ff8a00;">
        </div>
    </div>
    -->
    
    <div class="clear"></div>
    <div class="d5_nlist">
        <div class="experts_list">
            <div class="clear"></div>
            <div class="d5_ntblc d5_pert">
                <ul id="expertlist">
                    <li>
                            <div class="d5_ptl">
                                <img src="static/picture/20170430193519110548.jpg-c100" alt="专家头像" onerror=""
                                />
                                <!--c100-->
                                <div class="ntb_zhezhao"></div>
                            </div>
                            <div class="d5_ptr">
                                <p class="d5_np1">
                                    李思
                                    <span>
                                        研究员
                                        <em>/</em>
                                        副所长
                                    </span>
                                </p>
                                <p class="d5_np2">甘肃省科学院生物研究所</p>
                                <p class="d5_np3">
                                    熟悉学科:
                                    <span>农牧废弃物资源化微生物处理</span>
                                </p>
                                <br>
                                    <div class="active">
                                    <div class="active_l">
                                        <a href="#">立即咨询</a>
                                    </div>
                                    <div class="active_r">
                                        <a href="#">方案评审</a>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                            <div class="d5_ptl">
                                <img src="static/picture/20170430193519110548.jpg-c100" alt="专家头像" onerror=""
                                />
                                <!--c100-->
                                <div class="ntb_zhezhao"></div>
                            </div>
                            <div class="d5_ptr">
                                <p class="d5_np1">
                                    李思
                                    <span>
                                        研究员
                                        <em>/</em>
                                        副所长
                                    </span>
                                </p>
                                <p class="d5_np2">甘肃省科学院生物研究所</p>
                                <p class="d5_np3">
                                    熟悉学科:
                                    <span>农牧废弃物资源化微生物处理</span>
                                </p>
                                <br>
                                <div class="active">
                                    <div class="active_l">
                                        <a href="#">立即咨询</a>
                                    </div>
                                    <div class="active_r">
                                        <a href="#">方案评审</a>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="box">
				<div id="pagination3" class="page fl"></div>
				<!-- <div class="info fl">
				<p>当前页数：<span id="current3">1</span></p>
			</div> -->
            <div class="blank20"></div>
        </div>
    </div>
    <script>
			$(function() {
				name = $("#specialistString").val();
				limit = 10;
				offset = 0;
				function yemian(){
					$.ajax({
						url :Server + "/fon/findofspecialist",
						data : {
							"name":name,
							"limit":limit,
							"offset":offset,
						},
						type : "POST",
						dataType: "JSON",
						success : function(data) {
							$("#pagination3").pagination({
								currentPage: 1,
								totalPage: Math.ceil(Number(data[data.length - 1]/10)),
								isShow: true,
								count: 7,
								homePageText: "首页",
								endPageText: "尾页",
								prevPageText: "<上一页",
								nextPageText: "下一页>",
								callback: function(current) {
									$("#current3").text(current);
									findofspecialist($("#specialistString").val(),limit,$("#pagination3").pagination("getPage").current-1);
								}
							});
						}
					});
				}
				yemian();
			});
        </script>

    <div class="clear"></div>
    <%@include file="/footer.jsp"%>
   <script src="method/hhn/my.js"></script>
   <script src="js/jquery.pagination.min.js"></script>

</body>

</html>
 
