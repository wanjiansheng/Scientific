<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<title>仪器共享</title>
		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">
		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
">
		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/credit.css">
		<link rel="stylesheet" href="css/common.css">
		<script src="js/jquery-1.11.3.js"></script>
		<script src="method/dwy/new_file41.js"></script>
		
		<script type="text/javascript">
				function ReSizePic(ThisPic){
				    var RePicWidth = 200; //这里修改为您想显示的宽度值
				
				    //============以下代码请勿修改==================================
				
				    var TrueWidth = ThisPic.width;    //图片实际宽度
				    var TrueHeight = ThisPic.height;  //图片实际高度
				    var Multiple = TrueWidth / RePicWidth;  //图片缩小(放大)的倍数				
				    ThisPic.width = RePicWidth;  //图片显示的可视宽度
				    ThisPic.height = TrueHeight / Multiple;  //图片显示的可视高度
				}
		</script>

		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$('.mainList .content .left li').removeClass('selected');
					$(this).addClass('selected');
				});
				$('.mainList .content .left .list_header').click(function() {
					if($('.mainList .content .left .secendList').is(':hidden')) {
						$('.mainList .content .left .secendList').slideDown();
						$('.mainList .content .left .list_header img').attr("src", "images/insUser_icon.png");
					} else {
						$('.mainList .content .left .secendList').slideUp();
						$('.mainList .content .left .list_header img').attr("src", "images/icon4.png");
					};

				});
				$("#row5 font label").click(function() {
					$("#row5 font label img").attr('src', 'images/icon35.png');
					$("#row5 font label").siblings('input').prop('checked', false);
					$(this).find('img').attr('src', 'images/icon36.png');
					$(this).siblings('input').prop('checked', true);
				});
				$("#row8 font label").click(function() {
					$("#row8 font label img").attr('src', 'images/icon35.png');
					$("#row8 font label").siblings('input').prop('checked', false);
					$(this).find('img').attr('src', 'images/icon36.png');
					$(this).siblings('input').prop('checked', true);
				});
			});
		</script>

		<style type="text/css">
		.mainList .content .right .box a #xixi{

			width: 16%;
		
    display: inline-block;
    text-align: center;
    height: 40px;
    line-height: 40px;
    /* background: #ff8a00; */
    /* color: #fff; */
    border-radius: 5px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    display: inline-block;

		}
		</style>

	</head>
		<%@include file="/head.jsp"%>

		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
	<a href="index.jsp" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>

	</div>
					<div class="web_name">
						用户中心
					</div>
				</div>
			</div>
		</div>
		<div class="mainList">
			<div class="bNav">您的位置：
			<a href="index" style="color: #575757">首页</a> > 
		            <a href="new_file39" style="color: #575757">用户中心</a> > 


			<span>信用认证和交易</span></div>
			<div class="content">
				<%@include file="/left-tab.jsp"%>
				<div class="right">
					<div class="title"><span>信用认证和交易</span></div>
					<form id="signupForm" method="POST">
					<input style="display:none" name="puId" id="puId" value="${param.puId}"></input>
					<div class="box">
				<ul id="bd">
							<li class="row1" id="NameR">
								<label>姓名</label>
								<input type="text" name="name" id="identityName" value="" placeholder="" maxlength="10" />
								<!--<img src="images/Credit.png" />-->
							</li>
							<li class="row2">
								<label>性别</label>
								<input type="radio" name="sex" id="identitySex" value="0" placeholder="" />男
								<input type="radio" name="sex" id="identitySex" value="1" placeholder="" />女
							</li>
							<li class="">
								<label>出生年月</label>
								<input type="date" name="identityBirthdayS" id="identityBirthday" value="" placeholder="格式是：yyyy-yy-yy 00:00:00" />
							</li>
							<li class="">
								<label>身份证</label>
								<input type="text" name="card" id="identityIdCard" value="" placeholder="" />
							</li>
							 <li class="row5">
								<label class="label">上传照片</label>
								<div>
									<div class="addPicBox">
										<div class="addPic">
											<div>
												<label for="add1"> <img id="image0" src="images/Credit1.png" onload="ReSizePic(this);"/></label>
												 <input type="file" name="1111" id="add1" value="" onchange="selectImage0(this);" /><br/>
											</div>
											<div>示例</div>
											<div><img src="images/Credit3.png"/></div>
										</div>
										<div class="addPic">
											<div>											
												<label for='add2'><img id="image1" src="images/Credit2.png" onload="ReSizePic(this);"/></label>
												<input type="file" name="222" id="add2" value="" onchange="selectImage1(this);" /><br/>
											</div>
											<div>示例</div>
											<div><img src="images/Credit4.png"/></div>
										</div>
									</div>
									<div>
										<p>注：</p>
										<p>1. 需上传清晰的身份证正面、反面共2张照片</p>
										<p>2. 照片不超过5MB，支持格式jpg</p>
									</div>
								</div>
								</li> 
							<li id="sub">
							    <input style="display:none" name="check" id="" value="0"></input>
							    <!-- <input style="display:none" name="puId" id="" value=""></input> -->
								<label></label>
								<a class="xixi" onclick="save()"
							style="border: 1px solid #efefef;
    display: block;
    width: 16%;
    text-align: center;
    height: 50px;
    line-height: 50px;
    border-radius: 5px;
    font-size: 18px;
    color: #fff;
    background-color: #ff8a00;
    margin-left: 150px;">认证</a>
						</li>
						</ul>
					</div>
				</form>
				</div>
			</div>
			<div class="pageButton"></div>
			<!--找个分页插件插入-->
		</div>

		<%@include file="/footer.jsp"%>
		<script>
			$(document).ready(function(){
				load()
			});
			
			function load(){
				var id = $('#useridoflogin').val();
				$.ajax({
					url:Server + "/approve/userDetails",
					data:{
						"puId":id,
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						if(data != null){
							if(data.identityIsCheck == 0){
								$("#bd input").attr("disabled","disabled");
								var html = '<input style="display:none" name="identityIsCheck" id="" value="0"></input>\
									<label></label>\
									<a style="border: 1px solid #efefef;display: block;width: 16%;text-align: center;height: 50px;line-height: 50px;border-radius: 5px;font-size: 18px;color: RGB(255,255,255);background-color: RGB(163,163,163); margin-left: 150px">正在审核</a>';
								$("#sub").html(html);
							}else if(data.identityIsCheck == 1){
								$("#bd input").attr("disabled","disabled");
								var html = '';
								$("#sub").html(html);
								var bd = '<label>姓名</label>\
								<input type="text" name="identityName" id="identityName" value="" placeholder="" disabled="disabled"/>\
								<img src="images/Credit.png" />';
								$("#NameR").html(bd);
							}else if(data.identityIsCheck ==2){
								var html = '<input style="display:none" name="identityIsCheck" id="" value="0"></input>\
									<label></label>\
									<a onclick="update();"style="border: 1px solid #efefef;\
    display: block;\
    width: 16%;\
    text-align: center;\
    height: 50px;\
    line-height: 50px;\
    border-radius: 5px;\
    font-size: 18px;\
    color: #fff;\
    background-color: #ff8a00;\
    margin-left: 150px">重新提交</a>';
								$("#sub").html(html);
							}
							$("#identityName").val(data.identityName);
							$("input:radio[name='sex'][value="+data.identitySex+"]").prop('checked','true');
							$("#identityBirthday").val(new Date(data.identityBirthday).Format("yyyy-MM-dd"));
							$("#identityIdCard").val(data.identityIdCard);
							$("#image0").attr('src',data.identityFront);
							$("#image1").attr('src',data.identityBack);
						}
					}
				});
			}
			
			function update(){
				var id = $('#useridoflogin').val();
				var identityName = $("#identityName").val();
				var identitySex = $('input:radio:checked').val();
				var identityBirthday = $("#identityBirthday").val();
				var identityIdCard = $("#identityIdCard").val();
				$.ajax({
					url:Server + "/approve/userUpdate",
					data:{
						"puId":id,
						"identityName":identityName,
						"identitySex":identitySex,
						"identityBirthday":identityBirthday,
						"identityIdCard":identityIdCard,
						"identityIsCheck":0,
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						alert("提交成功")
						window.location.reload()
					},
					error : function(msg){
						alert("提交失败")
					},
				});
			}
			
			Date.prototype.Format = function(fmt){
				var o = {
					"M+": this.getMonth() + 1, //月份 
	       			"d+": this.getDate(), //日 
	        		"h+": this.getHours(), //小时 
	        		"m+": this.getMinutes(), //分 
	        		"s+": this.getSeconds(), //秒 
	        		"q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	        		"S": this.getMilliseconds() //毫秒 
				};
				if(/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
				for(var k in o)
				if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	    		return fmt;
			}
		</script>

	<style>
	input[type="date"]::-webkit-inner-spin-button {
	display: none;
	}
	/**/
	/*----------用来移除叉叉按钮----------*/
	input[type="date"]::-webkit-clear-button{
	display:none;
	}

	</style>

	</body>

</html>