<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

	<head>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<title>仪器共享</title>

		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">

		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
         
">
	<link href="css/bootstrap.min.css">
		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/productDetail.css">
		<link rel="stylesheet" href="css/new_style.css">
		
		<!--<link rel="stylesheet" href="js/layui/css/layui.css" media="all">-->
		<style type="text/css">
			.mainList .content .productInfo .right {
    float: left;
    height: 100%;
    padding-left: 25px;
    width: 100%;
}
.mainList .content .productInfo .right .row9 {
    overflow: hidden;
    margin-top: 50px;
    text-align: center;
}
.mainList .content .productInfo .right .row9 .btn1 {
    display: inline-block;
}

.mainList .content .productInfo .right .row9 .btn {
    float: none;
}

.mainList .content .productInfo {
    width: 100%;
    border: 1px solid #d3d3d3;
     padding: 0px;
     padding-top: 30px;
        height: 460px;
    overflow: hidden;
}
	.sun_li_row .li_row1 .info_left {
	float: left;
	width: 700px;
	margin-left: 20px;
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	}
.submit{
	    text-align: center;
    margin-top: 30px;
    height: 66px;
}
.submit a{
	height: 46px;
	line-height: 46px;
	display: inline-block;
	width: 180px;
	background-color: rgb(255,138,0);
	border-radius: 5px;
	color: white;
	font-size: 16px;
}
.li_title>label {
    display: inline;
    cursor: pointer;
    width: auto;
    vertical-align: middle;
    height: 70px;
    line-height: 70px;
}
.li_title>label img {
    vertical-align: middle;
}
.li_title>label span {
    vertical-align: middle;
    padding-left: 10px;
}
.li_title input[type=checkbox] {
    opacity: 0;
    filter: alpha(opacity=0);
    width: 0;
	<%--18650139758--%>
    height: 0;
	}


.mainList .content .userEvaluation{
    margin-top: 10px;
    border: 1px solid #d3d3d3;
    width: 100%;
    padding: 0px !important;
}

	.row.sun_row {
	height: 68px;
	margin-bottom: 10px;
	border-bottom: 1px solid #ccc;
	}
	.row.sun_row {
	height: 68px;
	margin-bottom: 10px;
	border-bottom: 1px solid #ccc;
	}
	.but-key {
	width: 180px;
	height: 46px;
	background-color: #ccc;
	display: block;
	text-align: center;
	line-height: 46px;
	font-size: 16px;
	color: #fff;
	margin: 0 auto;
	border-radius: 5px;
	margin-bottom: 20px;
	}

	div#needdowndetail {
	height: 100%;
	}
	.li_title {
	height: 70px;
	}
	.sun_li_row .li_row1{
	border-bottom: 0px;
	}
	.sun_li_row .li_row2 .info_left {
	float: right;
	text-align: right;
	white-space: nowrap;
	text-overflow: ellipsis;
	width: 100%;
	display: inline-block;
	border-bottom: 1px solid #ccc;
	margin-top: -13px;
	color: rgb(134,134,134);
	}
	a#setbuy {
	width: 180px;
	height: 46px;
	display: inline-block;
	background-color: #ff8a00;
	text-align: center;
	line-height: 46px;
	border-radius: 5px;
	font-size: 16px;
	color: #fff;
	margin: 0 auto;
	justify-content: center;
	}
	.btn-ok {
	margin-left: 44.2%;
	margin-bottom: 20px;
	}
	.btn-qr {
	width: 180px;
	height: 46px;
	line-height: 46px;
	background-color: #ff8a00;
	display: block;
	text-align: center;
	font-size: 16px;
	color: #fff;
	border-radius: 5px;
	margin: 0 auto;
	}
	.woqu{
	display: block;
	width: 500px;
	height: 50px;
	margin-top: 5px;
	margin-left: 20px;
	}
	.gansini{
	font-size: 18px;
	text-align: right;
	margin-top: -46px;
	margin-right: 20px;
	}
	.daye{
	color: rgb(110,211,254);
	text-align: right;
	margin-right: 20px;
	}
	.boxTitle{
	border-bottom: 1px solid #efefef
	}
		</style>
		<link rel="stylesheet" href="css/common.css">
		<script src="js/jquery-1.11.3.js"></script>
		<script src="method/dwy/new_file22.js"></script>
		<script >
		 // load();
		 // particulars();
		 // next();
		 // describe();
		   
		   
		  $(document).ready(function(){
			load(GetQueryString("needid"));
			particulars(GetQueryString("needId")); 
			// next(GetQueryString("needId"));
			//describe(GetQueryString("needId"));
			
			});
			function GetQueryString(name) {
				var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
				var r = window.location.search.substr(1).match(reg);
				if(r != null) return unescape(r[2]);
				return null;
			}
		</script>
		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$(this).addClass('selected').siblings().removeClass('selected');
				});
				
				
				
				//--------
				$(".tab_span1").click(function(){
					$(this).addClass('selected').siblings().removeClass('selected');
       $("#tabView1").css("display","block");
       $("#tabView2").css("display","none");
       $("#tabView3").css("display","none");
});


$(".tab_span2").click(function(){
	$(this).addClass('selected').siblings().removeClass('selected');
    $("#tabView2").css("display","block");
    $("#tabView1").css("display","none");
    $("#tabView3").css("display","none");
});

    $(".tab_span3").click(function(){
	$(this).addClass('selected').siblings().removeClass('selected');
	$("#tabView3").css("display","block");
    $("#tabView2").css("display","none");
    $("#tabView1").css("display","none");
});

$(".li_title>label").click(function() {
//					if($(this).siblings('input').is(':checked')){
//						$(this).find('img').attr('src','images/icon35.png');
//						$(this).siblings('input').prop('checked',false);
//					}else{
//						$(this).find('img').attr('src','images/icon36.png');
//						$(this).siblings('input').prop('checked',true);
//					};
					$(".li_title>label img").attr('src', 'images/icon35.png');
					$(".li_title>label").siblings('input').prop('checked', false);
					$(this).find('img').attr('src', 'images/icon36.png');
					$(this).siblings('input').prop('checked', true);
				});
    
				
			});
		</script>
	</head>
	<body style="background:#fff;">

	<%@include file="/head.jsp"%>
		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
						<a href="index.jsp" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);">科研成果共享平台</a>
					</div>
					<!--<div class="web_name">
						<img src="images/icon3.png" />
						<p>yq.shang.com</p>
					</div>-->
				</div>
			</div>
		</div>
		

		
		
		<div class="mainList">
			<div class="bNav">您的位置：
			<a href="index">首页</a> > 
			<a href="new_file44">需求列表</a> > 
			<span>需求详情</span></div>
			<div class="content">
				
				
				<div class='productInfo'> 
					<div class="content_title">
					<a>技术难题解决</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<img src="image/xqxq_yjj.png"/>
					<span style="color: rgb(255,29,145);" type="text" name="" id="in8" value=""></span>
				</div>
					
					<div class="right">
						<div class="row row1"><span type="text" name="" id="in0" value="" ></span></div>
						
						<div class="row row3">类型：<span type="text" name="" id="in1" value="" ></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行业类型：<span type="text" name="" id="in2" value="" ></span></div>
						<br />
						<div class="row">需求简介：<span type="text" name="" id="in3" value="" ></span></div>
						<br />
						<!--  <div class="row">发布时间：<span>美国HP公司</span></div>
						<br />
						<div class="row">完成时间：<span>化学分析仪器》电化学仪器》PH技、酸度计</span></div>
						<br />-->
						<div class="row">地址：<span type="text" name="" id="in6" value="" ></span></div>
						<br />
						<div class="row">预算：<span style="color: rgb(255,138,0);" type="text" name="" id="in7" value=""></span>&nbsp;&nbsp;
							
						</div>
						<!---------->
						<div id="findscienceIshave" class="submit">
						<a onclick="findOrder()">申请专家</a>
						</div>
						
						<!---------->
					</div>
					<script>
					$(function(){
						$.ajax({
							url:"http://localhost/fon/findscienceIshave",
	            			type:"POST",
	            			dataType:"text",
	            			data:{
	            				type : $('#usertypeoflogin').val(),
	            				userid : $('#useridoflogin').val(),
	            				needid : ${param.needid}
	            			},
	            			success: function(data){
	            				if(Number(data) == 0){
	            					$("#findscienceIshave").empty();
	            				}
	            			}
						})
					
					})
					</script>
					
					
					<!---->
					
					
					
				</div>
				
				<div class="userEvaluation">

					<div class="box" id="needdowndetail" >
						<div class='tabView sun_tabView'  id="tabView1">
							<ul id="detailList">
								<li class="sun_li_row">
									<div class="li_title" style=" padding-left: 10px;text-align: left;">
										<input type="checkbox" name="" id="" value="" />
										<label >
											<img src="images/icon35.png"/>
											<span>设置为买家</span>
										</label>

	<script>

	// var imgObj = document.getElementById("caocao_pic");
	// var flag=(imgObj.getAttribute("src",2)=="images/icon35.png")
	// imgObj.src=flag?"images/icon36.png":"images/icon35.png";
	// }
$(".li_title label").click(function() {

					$(".li_title label img").attr('src', 'images/icon35.png');
					$(".li_title label").siblings('input').prop('checked', false);
						$(this).find('img').attr('src', 'images/icon36.png');
						$(this).siblings('input').prop('checked', true);
				});
	</script>
										<!--<a>我是专家我要上传评审意见</a>-->
									</div>
									
									<div style="padding-right: 20px;display: inline-block;width: 100%;">
									 <div class="li_row1">
									 	<div class="info_left">啦啦啦啦啦啦啦啦啦啦啦阿里
									 	</div>
									 	<div class="info_right">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />
									 		<span style="color: rgb(6,185,254);">144444444444</span>

									 	</div>
									 </div>

									 <div class="li_row2">
									 	<div class="info_left" >啦啦啦啦啦啦啦啦啦啦啦阿里
									 	</div>
									 	<div class="info_right" style="float: right;">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />

									 	</div>
									 </div>

									 <div class="li_row2" style="float: right;">
									 	<div class="info_left">啦啦啦啦啦啦啦啦啦啦啦阿里</div>
									 	<div class="info_right" style="float: right;">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />
									 	</div>
									 </div>
									</div>
								</li>


								<li class="sun_li_row">
									<div class="li_title" style=" padding-left: 10px;text-align: left;">
										<input type="checkbox" name="" id="" value="" />
										<!-- <label>

<img src="images/icon35.png" id="caocao_pic" onclick="change_pic()"/>
<span>设置为买家</span></label> -->

									</div>
									<div style="padding-right: 20px;display: inline-block;width: 100%;">
									 <div class="li_row1">
									 	<div class="info_left">啦啦啦啦啦啦啦啦啦啦啦阿里
									 	</div>
									 	<div class="info_right">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />
									 		<span style="color: rgb(6,185,254);">144444444444</span>
									 	</div>
									 </div>

									 <div class="li_row2">
									 	<div class="info_left" >啦啦啦啦啦啦啦啦啦啦啦阿里
									 	</div>
									 	<div class="info_right" style="float: right;">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />
									 	</div>
									 </div>

									 <div class="li_row2" style="float: right;">
									 	<div class="info_left">啦啦啦啦啦啦啦啦啦啦啦阿里
									 	</div>
									 	<div class="info_right" style="float: right;">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />

									 	</div>
									 </div>
									</div>
								</li>
								<div class="submit">
									<a>确定</a>
								</div>
							</ul>
						</div>									
					</div>
				</div>
			</div>
			
				<script>
	            	$(document).ready(function(){
	            		buyorsellofinfo();
	            	});
	            </script>
				
			 	<script>
	            	function buyorsellofinfo(){
						var typeId = $('#usertypeoflogin').val();
						var id = $('#useridoflogin').val();
	            		$.ajax({
	            			url:Server + "/fon/buyofneed",
	            			type:"POST",
	            			dataType:"JSON",
	            			data:{
	            				usertype : typeId,
	            				userid : id,
	            				needid : ${param.needid}
	            			},
	            			success: function(data){
	            			//【00:买家状态，待确认，		页面中含有【接受】和【拒绝】按钮
							//【01:买家状态，不存在的情况，	因为此情况买家列表不展示】
							//【02:买家状态，确认交易，		一个页面中含有【已接受】，买家点击接受之后触发
							//【10:卖家状态，展示中，		页面显示所有买家的列表
							//【11:卖家状态，等待买家确认，	页面显示等待确认的买家信息
							//【12:卖家状态，确认交易，		页面显示【已锁定】
	            				switch(data.buytype){
	            					case "00":
	            						/* alert("买家！状态码："+JSON.stringify(data.buytype)); */
	            						var item = "";
	            						var statushtml = '<div class="boxTitle" style="margin-top: 12px;width: 400px;display:  block;margin-left: 20px;"><span style="color: rgb(255,138,0);font-size: 19px; font-weight: normal;">已被服务商设置为买家</span></div>\
											<div class="item item3">\
												<div class="row" style="text-align:  right;margin-right: 20px;margin-top: -24px;border-bottom:  1px solid #ccc;">\
													<span style="text-align:  right;display:  block;">#0<span style="color: #CCCCCC;">（#1）</span></span>\
													<span style="color:rgb(78,201,254);">#2</span>\
												</div>\
											</div>\
											<div class="btn" style=" margin-left: 35%; margin-right: 30px;margin-top: 20px;margin-bottom: 20px;">\
												<a id="buyisreceive" style="width:  180px;height:  46px;color:  #fff;background-color:  #ff8a00;display: inline-block;text-align:  center;line-height:  46px;border-radius:  5px;">接受</a>\
												<a id="buyisontreceive" style="width: 180px;height: 46px;color: #fff;background-color: #ff8a00;display: inline-block;text-align: center;line-height: 46px;border-radius: 5px;">拒绝</a>\
											</div>';
											if(data.buydate[0].nbuyUtype == 2){
												var rbuyUtype = "企事业单位";
												statushtml.replace("#1",rbuyUtype);
											}else if(data.buydate[0].nbuyUtype == 1){
												var rbuyUtype = "个人单位";
												statushtml.replace("#1",rbuyUtype);
											}
											var m = statushtml.replace("#0",data.buydate[0].unitname)
													.replace("#1",rbuyUtype)
													.replace("#2",data.buydate[0].userphone)
	            							$("#needdowndetail").html(m);
	            							$("#buyisreceive").click(function(){
	            								$.ajax({
	            									url:Server + "/fon/buyofneedofbuyishave",
							            			type:"POST",
							            			dataType:"JSON",
							            			data:{
							            				usertype : typeId,
	            										userid : id,
	            										needid : ${param.needid}
							            			},
							            			complete : function(data){
							            				window.location.reload();
							            			}
	            								});
	            							});
	            							$("#buyisontreceive").click(function(){
	            								$.ajax({
	            									url:Server + "/fon/buyofneedofbuyisnot",
							            			type:"POST",
							            			dataType:"JSON",
							            			data:{
							            				usertype : typeId,
	            										userid : id,
	            										needid : ${param.needid}
							            			},
							            			success : function(data){
							            				var rejecturl = "new_file44";
							            				window.location.replace(rejecturl);
							            			}
	            								});	
	            							});
	            						break;
	            					case "01":
	            						/* alert("买家！状态码："+JSON.stringify(data.buytype)); */
	            						break;
	            					case "02":
	            						/* alert("买家！状态码："+JSON.stringify(data.buytype)); */
	            						var statushtml = '<div class="boxTitle"><span style="color: rgb(255,138,0);font-size: 19px;font-weight: 200; display: block;margin-top: 0px;padding-top: 15px;padding-left: 20px;padding-bottom: 10px;">已和该成果卖家达成协议</span></div>\
											<div class="btn" style="justify-content: center;display: flex;margin-bottom: 30px; margin-top: 30px;">\
												<a href="#this" style="background: #ccc;color: #fff;display: flex;justify-content: center;width: 180px;height: 46px;line-height: 46px;font-size: 16px;border-radius: 5px;;" >已接受</a>\
											</div>\
											</div>';
										$("#needdowndetail").html(statushtml);
	            						break;
	            					case "10":
	            						/* alert("卖家！状态码："+JSON.stringify(data.buytype)); */
	            						var html = "";
										var pattern = '<li class="sun_li_row">\
											<div class="li_title" style=" padding-left: 10px;text-align: left;">\
												<input type="checkbox" name="checkbox" id="" value="#00" />\
												<input value="#01" style="display:none" ></input>\
												<input value="#02" style="display:none" ></input>\
												<label><img src="images/icon35.png"/><span>设置为买家</span></label>\
											</div>\
											<div style="padding-right: 20px;display: inline-block;">\
												<div class="li_row1">\
													<div class="info_left">#8</div>\
													<div class="info_right">\
														<span>#1</span><br />\
														<span style="color: rgb(6,185,254);">#2</span>\
													</div>\
												</div>\
												<div class="li_row2">\
													<div class="info_left">#3</div>\
													<div class="info_right" style="float: right;">\
														<span></span><br />\
													</div>\
												</div>\
											</div>\
										</li>';
										var str2 ="";
										if(data.buydate.length != 0){
											str2 = '<div class="btn btn-ok">\
												<a id="setbuy">确认</a>\
											</div>';
										}
										for(var i = 0; i< data.buydate.length; i++){
											if(data.buydate[i].nbuyUtype == 2){
												var rbuyUtype = "企事业单位";
											}else if(data.buydate[i].nbuyUtype == 1){
												var rbuyUtype = "个人单位";
											}
											var item = "";
											var order = data.buydate[i];
											item += pattern
												.replace("#00", order.nbuyUid)//买家id
												.replace("#01", order.nbuyUtype)//买家类型
												.replace("#02", order.unId)//需求ID
												.replace("#1", order.unitname)//单位名
												.replace("#2", rbuyUtype)//用户类型
												.replace("#3", order.userphone)//联系方式
												.replace("#8", order.techName)//技术方案名称
											html += item;
										}
										html += str2;
										$("#needdowndetail").html(html);
										$("#setbuy").click(function(){
											var userid = $("input[name='checkbox']:checked").val();
											var usertype = $("input[name='checkbox']:checked").next().val();
											var needid = $("input[name='checkbox']:checked").next().next().val();
											$.ajax({
            									url:Server + "/fon/sellofneedofisbuy",
						            			type:"POST",
						            			dataType:"JSON",
						            			data:{
						            				"usertype" : usertype,
						            				"userid" : userid,
						            				"needid" : needid
						            			},
						            			success : function(data){
						            				buyorsellofinfo();
						            			}
	            							});	
										}); 
	            						break;
	            					case "11":
	            						/* alert("卖家！状态码："+JSON.stringify(data.buytype)); */
	            						var statushtml = '<div class="item item3">\
											<div class="row sun_row">\
												<div class="right">\
												<p class="woqu" style="display: block; width: 500px; height: 50px; margin-top: 5px; margin-left: 20px;">#8</p>\
													<p class="gansini" style="font-size: 18px;text-align: right;margin-top: -46px;margin-right: 20px;">#0<font style="color: #CCCCCC;">（#1）</font></p>\
													<p class="daye" style="color: rgb(110,211,254);    text-align: right;margin-right: 20px;">#2</p>\
												</div>\
											</div>\
											</div>\
											<div class="btn" style="margin-bottom: 20px;">\
												<a clas="btn-qr" disabled="disabled"  style="width: 180px;height: 46px;line-height: 46px;background-color: #ccc;display: block;text-align: center;font-size: 16px;color: #fff;border-radius: 5px;margin: 0 auto;">等待买家确认</a>\
											</div>';
											
											if(data.buydate[0].nbuyUtype == 2){
												var rbuyUtype = "企事业单位";
												statushtml.replace("#1",rbuyUtype);
											}else if(data.buydate[0].nbuyUtype == 1){
												var rbuyUtype = "个人单位";
												statushtml.replace("#1",rbuyUtype);
											}
											var m = statushtml.replace("#0",data.buydate[0].unitname)
													.replace("#1",rbuyUtype)
													.replace("#2",data.buydate[0].userphone)
													.replace("#8",data.buydate[0].techName)
	            							$("#needdowndetail").html(m);
	            						break;
	            					case "12":
	            						/* alert("卖家！状态码："+JSON.stringify(data.buytype)); */
	            						var statushtml = '<div class="item item3">\
											<div class="row sun_row">\
												<div class="right" style="text-align: right;padding-right: 20px;">\
													<p style="font-size: 18px;">#0<font style="color: #CCCCCC;">（#1）</font></p>\
													<p style="color: rgb(110,211,254);">#2</p>\
												</div>\
											</div>\
											</div>\
											<div class="btn">\
												<a class="but-key">已锁定</a>\
											</div>';
											if(data.buydate[0].nbuyUtype == 2){
												var rbuyUtype = "企事业单位";
												statushtml.replace("#1",rbuyUtype);
											}else if(data.buydate[0].nbuyUtype == 1){
												var rbuyUtype = "个人单位";
												statushtml.replace("#1",rbuyUtype);
											}
											var m = statushtml.replace("#0",data.buydate[0].unitname)
													.replace("#1",rbuyUtype)
													.replace("#2",data.buydate[0].userphone)
	            							$("#needdowndetail").html(m); 
	            						break;
	            					default:
	            						alert("参数错误！请联系管理员！");
	            				} 
	            				$(".li_title label").click(function() {
									$(".li_title label img").attr('src', 'images/icon35.png');
									$(".li_title label").siblings('input').prop('checked', false);
									$(this).find('img').attr('src', 'images/icon36.png');
									$(this).siblings('input').prop('checked', true);
								}); 
	            			}
	            		});
	            	}        
	            </script>  
			<div class="pageButton"></div>
			<!--找个分页插件插入-->
		</div>

		<%@include file="/footer.jsp"%>
		<script src="js/layui/layui.js"></script>
		<script>
layui.use('carousel', function(){
  var carousel = layui.carousel;
  //建造实例
  carousel.render({
    elem: '#test1'
    ,width: '100%' //设置容器宽度
    ,arrow: 'always' //始终显示箭头
    //,anim: 'updown' //切换动画方式
  });
});
</script>
<script>
	
</script>

	</body>

</html>