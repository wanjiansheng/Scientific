<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>

<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>仪器共享</title>

<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">

<meta name="description"
	content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
         
">
<link rel="stylesheet" href="new_css/header_footer.css?t=1">
<link rel="stylesheet" href="new_css/productDetail.css">
<link rel="stylesheet" href="css/new_style.css">

<link rel="stylesheet" href="js/layui/css/layui.css" media="all">
<style type="text/css">
.mainList .content .userEvaluation {
	position: relative;
}

.mainList .content .productInfo .right {
	float: left;
	height: 100%;
	padding-left: 25px;
	width: 100%;
}

.mainList .content .productInfo .right .row9 {
	overflow: hidden;
	margin-top: 50px;
	text-align: center;
}

.mainList .content .productInfo .right .row9 .btn1 {
	display: inline-block;
}

.mainList .content .productInfo .right .row9 .btn {
	float: none;
}

.mainList .content .productInfo {
	width: 100%;
	border: 1px solid #d3d3d3;
	padding: 0px;
	padding-top: 30px;
	height: 240px;
	overflow: hidden;
}

.mainList .content .productInfo .right .row1 {
	margin-bottom: 29px;
}

.mainList .content .productInfo .right .row2 {
	margin-bottom: 39px;
}

.mainList .content .userEvaluation .box {
	overflow-y: scroll;
	max-height: 700px;
}

.mainList .content .userEvaluation .box .tabView li .pic img {
	height: 73px;
	border: 1px solid #d3d3d3;
	border-radius: 50%;
}

.li_input {
	width: 100%;
	height: 45px;
	margin-top: 10px;
	margin-bottom: 10px;
}

.li_input input {
	width: 100%;
	height: 45px;
	border-radius: 4px;
	padding-left: 10px;
}

.bottom_input {
	position: absolute;
	bottom: 0px;
	left: 0px;
	width: 100%;
	padding: 5px 30px;
	border: 1px solid rgb(244, 244, 244);
	background-color: white;
}

.bottom_input input {
	width: 100%;
	height: 45px;
	border-radius: 4px;
	background-color: rgb(244, 244, 244);
}

#tabView2 {
	padding-bottom: 60px;
}

.use_answer {
	margin-top: 10px;
	width: 98.5%;
	height: 60px;
	line-height: 60px;
	border-top: 1px solid #CCCCCC;
	overflow: hidden;
}

.use_answer .answer_a {
	margin-right: 34px;
	margin-top: 10px;
	float: right;
	display: inline-block;
	width: 120px;
	height: 48px;
	text-align: center;
	color: rgb(255, 138, 0);
	border: 1px solid #CCCCCC;
	line-height: 48px;
	border-radius: 5px;
}

.use_answer .answer_b {
	margin-right: 34px;
	margin-top: 10px;
	float: right;
	display: inline-block;
	width: 120px;
	height: 48px;
	text-align: center;
	color: rgb(255, 138, 0);
	border: 1px solid #CCCCCC;
	line-height: 48px;
	border-radius: 5px;
}

#front_header .header_wrap .registeredLink {
	float: right;
	margin-left: 20px;
}

#front_header .header_wrap .header_title {
	float: right;
	margin-right: 20px;
}

#heder-img img{
	height: 52px !important;
	width: 52px !important;
}
.mainList .content .userEvaluation .box .tabView li .fontInfo {
    overflow: hidden;
    font-size: 18px;
    color: #202020;
    margin-top: -20px;
}
.replyLike {
    float: right;
    margin-top: -20px;
    padding-right: 37px;
}
</style>
<link rel="stylesheet" href="css/common.css">
<script src="new_js/jquery-1.11.3.js"></script>
<script>
	$(function() {
		$('ul #cancel_btn').click(function() {
			$(this).parent().parent().parent().children('.li_input').toggle();
			$(this).parent().parent().parent().children('.replay').toggle();
			$(this).parent().parent().parent().children('.use_answer').toggle();

		});

		$('ul #right_a').click(function() {
			$(this).parent().parent().siblings('.li_input').toggle();
			$(this).parent().parent().siblings('.replay').toggle();
			$(this).parent().parent().siblings('.use_answer').toggle();

		});


		$('#front_header .header_wrap .right').hover(function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
			$('#front_header .header_wrap .right .out').show();
		}, function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
			$('#front_header .header_wrap .right .out').hide();
		});
		$('#sorts').on('mouseover', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon4.png');
		});
		$('#sorts').on('mouseout', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon5.png');
		});

		$('.mainList .content .left li').click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
		});



		//--------
		$(".tab_span1").click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
			$("#tabView1").css("display", "block");
			$("#tabView2").css("display", "none");
			$("#bottom_input").css("display", "none");
		});


		$(".tab_span2").click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
			$("#tabView2").css("display", "block");
			$("#bottom_input").css("display", "block");
			$("#tabView1").css("display", "none");
		});
	});
</script>
</head>
<body style="background:#fff;">
	<%@include file="/head.jsp"%>
	<div class="" id="nav_top">
		<div class="container">
			<div class="fl">
				<div class="logo ">
	<a class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>
				</div>
				<!--<div class="web_name">
						<img src="images/icon3.png" />
						<p>yq.shang.com</p>
					</div>-->
			</div>
		</div>
	</div>
	<div class="mainList">
		<div class="bNav">
			您的位置：
			<a href="index">首页</a> > 
			<a href="new_file38_1">用户中心</a> > 
			<span>难题发布管理</span>
		</div>
		<div class="content">
			<div class='productInfo'>
				<div class="right">
					<div class="row row1" id="in1">全功能紫外-可见-近红外荧光光谱仪</div>
					<div class="row row2">
						需求简介：<span id="in2">北京全功能紫外-可见-近红外荧光光谱仪全功能紫外-可见-近红外荧光光谱仪全功能紫外-可见-近红外荧光光谱仪</span>
					</div>
					<div class="row row3">
						地址：<span id="in3">23658</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行业类型：<span id="in4">23658</span>
					</div>
				</div>
			</div>
			<div class="userEvaluation">
				<div class='tab'>
					<span class="tab_span2 " id="count">全部评论()条</span>
				</div>
				<div class="box">
					<div class='tabView sun_tabView ' id="tabView1"
						style="padding: 25px;display: none;">
						<h2 style="color: rgb(255,138,0);">
							难题简介
							</h3>
							<br />

							<h3>
								难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介
								</h4>
					</div>
					<div class='tabView' id="tabView2">
						<ul class="hfpl">
							<li>
								<div class="pic" id="heder-img">
									<img src="images/productDetailPic.png" /><span>xxxxxxxx</span>
								</div>
								<div class="fontInfo">
									<div class="left">仪器特别好用，服务态度也很好
										仪器特别好用，服务态度也很好仪器特别好用，服务态度也很好服务态度也很好</div>
								</div>
								<div class="fontInfo">
									<div class="right">
										<span style="color: black;margin-right: 40px;"><img
											src="image/fbxq_z.png">&nbsp;&nbsp;赞</span> <span
											style="color: black;" id="right_a"><img
											src="image/fbxq_pl.png">&nbsp;&nbsp;评论</span>
									</div>
								</div>
								<div class="li_input" style="display: none;">
									<input type="text" placeholder="请您输入评论" />
								</div>
								<div class="replay" style="display: none;">
									<!--<div class='left'>回复：<span>谢谢您的支持！</span></div>-->
									<div class='right'>
										<a style="margin-right: 20px;" id="cancel_btn">取消</a> <a
											class="right_a"
											style="background-color: rgb(255,138,0);color: white;">发布评论</a>
									</div>
								</div>
							</li>
							
							<li>
								<div class="pic" id="heder-img">
									<img src="images/productDetailPic.png" /><span>xxxxxxxx</span>
								</div>
								<div class="fontInfo">
									<div class="left">仪器特别好用，服务态度也很好
										仪器特别好用，服务态度也很好仪器特别好用，服务态度也很好服务态度也很好</div>
								</div>
								<div class="fontInfo">
									<div class="right">
										<span style="color: black;margin-right: 40px;"><img
											src="image/fbxq_z.png">&nbsp;&nbsp;赞</span> <span
											style="color: black;" id="right_a"><img
											src="image/fbxq_pl.png">&nbsp;&nbsp;评论</span>
									</div>
								</div>
								<div class="li_input" style="display: none;">
									<input type="text" />
								</div>
								<div class="replay" style="display: none;">
									<!--<div class='left'>回复：<span>谢谢您的支持！</span></div>-->
									<div class='right'>
										<a style="margin-right: 20px;" id="cancel_btn">取消</a> <a
											class="right_a"
											style="background-color: rgb(255,138,0);color: white;">发布评论</a>
									</div>
								</div>
							</li>
							
							
							<li>
								<div class="pic" id="heder-img">
									<img src="images/productDetailPic.png" /><span>xxxxxxxx</span>
								</div>
								<div class="fontInfo">
									<div class="left">仪器特别好用，服务态度也很好
										仪器特别好用，服务态度也很好仪器特别好用，服务态度也很好服务态度也很好</div>
								</div>
								<div class="fontInfo">
									<div class="right">
										<span style="color: black;margin-right: 40px;"><img
											src="image/fbxq_z.png">&nbsp;&nbsp;赞</span> <span
											style="color: black;" id="right_a"><img
											src="image/fbxq_pl.png">&nbsp;&nbsp;评论</span>
									</div>
								</div>
								<div class="use_answer">
									<a class="answer_a">采纳答案</a>
								</div>
								<div class="li_input" style="display: none;">
									<input type="text" />
								</div>
								<div class="replay" style="display: none;">
									<!--<div class='left'>回复：<span>谢谢您的支持！</span></div>-->
									<div class='right'>
										<a style="margin-right: 20px;" id="cancel_btn">取消</a> <a
											class="right_a"
											style="background-color: rgb(255,138,0);color: white;">发布评论</a>
									</div>
								</div>
							</li>
							<li>
								<div class="pic" id="heder-img">
									<img src="images/productDetailPic.png" /><span>xxxxxxxx</span>
								</div>
								<div class="fontInfo">
									<div class="left">仪器特别好用，服务态度也很好
										仪器特别好用，服务态度也很好仪器特别好用，服务态度也很好服务态度也很好</div>
								</div>
								<div class="fontInfo">
									<div class="right">
										<span style="color: black;margin-right: 40px;"><img
											src="image/fbxq_z.png">&nbsp;&nbsp;赞</span> <span
											style="color: black;" id="right_a"><img
											src="image/fbxq_pl.png">&nbsp;&nbsp;评论</span>
									</div>
								</div>

								<div class="li_input" style="display: none;">
									<input type="text" />
								</div>
								<div class="replay" style="display: none;">
									<!--<div class='left'>回复：<span>谢谢您的支持！</span></div>-->
									<div class='right'>
										<a style="margin-right: 20px;" id="cancel_btn">取消</a> <a
											class="right_a"
											style="background-color: rgb(255,138,0);color: white;">发布评论</a>
									</div>
								</div>
							</li>
							<li>
								<div class="pic" id="heder-img">
									<img src="images/productDetailPic.png" /><span>xxxxxxxx</span>
								</div>
								<div class="fontInfo">
									<div class="left">仪器特别好用，服务态度也很好
										仪器特别好用，服务态度也很好仪器特别好用，服务态度也很好服务态度也很好</div>
								</div>
								<div class="fontInfo">
									<div class="right">
										<span style="color: black;margin-right: 40px;"><img
											src="image/fbxq_z.png">&nbsp;&nbsp;赞</span> <span
											style="color: black;" id="right_a"><img
											src="image/fbxq_pl.png">&nbsp;&nbsp;评论</span>
									</div>
								</div>
								<div class="use_answer">
									<a class="answer_a">采纳答案</a>
								</div>
								<div class="li_input" style="display: none;">
									<input type="text" />
								</div>
								<div class="replay" style="display: none;">
									<div class='right'>
										<a style="margin-right: 20px;" id="cancel_btn">取消</a> <a
											class="right_a"
											style="background-color: rgb(255,138,0);color: white;">发布评论</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="pageButton"></div>
		<!--找个分页插件插入-->
	</div>
	<%@include file="/footer.jsp"%>
	<script src="js/layui/layui.js"></script>
	<script>
		layui.use('carousel', function() {
			var carousel = layui.carousel;
			//建造实例
			carousel.render({
				elem : '#test1',
				width : '100%', //设置容器宽度
				arrow : 'always' //始终显示箭头
			//,anim: 'updown' //切换动画方式
			});
		});
	</script>
	<script>
		
		$(document).ready(function(){
			loac()
			comment()
			
			 $.ajax({
				url:Server + "/problem_select/getUidByNeedId",
				//url:"http://localhost/problem_select/getUidByNeedId",
				data:{
					"needId":${param.needId},
				},
				type:"POST",
				success : function(data){
					if(data!=$("#useridoflogin").val()){
						$(".answer_a").remove();
					}
				}
			}) 
		})
		
		function loac(){
			$.ajax({
				url:Server + "/problem_select/detalis",
				data:{
					"needId":${param.needId},
				},
				type:"POST",
				dataType:"json",
				success : function(data){
					$("#in1").html(data[0].needName);
					$("#in2").html(data[0].needExplain);
					if(data[0].address != null){
						var address = data[0].address;
					}else{
						var address = "未知";
					}
					$("#in3").html(address);
					$("#in4").html(data[0].tTrade.tradeName);
				}
			})
		}
		
		function comment(){
			$.ajax({
	   			url:Server + "/problem_select/problemList",
	   			data:{
	   				"needId":${param.needId},
	   			},
	   			type : "POST",
				dataType : "json",
	   			success : function(data){
	   				var html = "";
	   				var pattern = '';
					var pattern1 = '<li>\
								<div class="pic" id="heder-img">\
									<span>#0</span>回复：<span>#4</span>\
								</div>';
					var pattern2 = '<li>\
								<div class="pic" id="heder-img">\
									<span>#0</span>\
								</div>';
					var pattern3 = '<div class="fontInfo">\
									<div class="left">#2</div>\
								</div>\
								<div class="fontInfo">\
									<div class="right">\
										<span class="like" val="#6" style="color: black;margin-right: 40px;">\
											<img src="image/fbxq_z.png">&nbsp;&nbsp;赞(#3)\
										</span>\
										<span style="color: black;" id="right_a" val="#8">\
											<img src="image/fbxq_pl.png">&nbsp;&nbsp;评论<font>#9</font>\
										</span>\
									</div>\
								</div>\
								<div class="use_answer">\
									<a class="answer_a" val="#7">采纳答案</a>\
								</div>\
								<div style="display: none;"><ul style="background-color: #f4f4f4;padding-left: 15px;margin-right: 15px;"></ul></div>\
								<div class="li_input" style="display: none;">\
									<input type="text" />\
								</div>\
								<div class="replay" style="display: none;">\
									<div class="right">\
										<a style="margin-right: 20px;" id="cancel_btn">取消</a>\
											<a class="right_a" ref="#5" style="background-color: rgb(255,138,0);color: white;">发布评论</a>\
									</div>\
								</div>\
							</li>';
					var pattern4 = '<div class="fontInfo">\
									<div class="left">#2</div>\
								</div>\
								<div class="fontInfo">\
									<div class="right">\
										<span class="like" val="#6" style="color: black;margin-right: 40px;">\
											<img src="image/fbxq_z.png">&nbsp;&nbsp;赞(#3)\
										</span>\
										<span style="color: black;" id="right_a" val="#8">\
											<img src="image/fbxq_pl.png">&nbsp;&nbsp;评论<font>#9</font>\
										</span>\
									</div>\
								</div>\
								<div style="display: none;"><ul style="background-color: #f4f4f4;padding-left: 15px;margin-right: 15px;"></ul></div>\
								<div class="use_answer">\
									<a class="answer_b" val="#7" >已采纳</a>\
								</div>\
								<div class="li_input" style="display: none;">\
									<input type="text" />\
								</div>\
							</li>';
					var pattern5 = '<div class="fontInfo">\
									<div class="left">#2</div>\
								</div>\
								<div class="fontInfo">\
									<div class="right">\
										<span class="like" val="#6" style="color: black;margin-right: 40px;">\
											<img src="image/fbxq_z.png">&nbsp;&nbsp;赞(#3)\
										</span>\
										<span style="color: black;" id="right_a" val="#8">\
											<img src="image/fbxq_pl.png">&nbsp;&nbsp;评论<font>#9</font>\
										</span>\
									</div>\
								</div>\
								<div style="display: none;"><ul style="background-color: #f4f4f4;padding-left: 15px;margin-right: 15px;"></ul></div>\
								<div class="li_input" style="display: none;">\
									<input type="text" />\
								</div>\
							</li>';
					var isExist;
					var count=0;
					for(var i = 0; i < data.length; i++){
						var list = data[i];
						if(list.spc.pcPid==0){
							count=count+1;
						}
						//alert(list.spc.problemStatus);
						if(list.spc.problemStatus==1){
							isExist=1;
						}
					} 
					$("#count").html("全部评论("+count+"条)");
					for(var i = 0; i < data.length; i++){
						var item = "";
						var list = data[i];
						var replyCount=0;
						var replyCountStr="";
						for(var j = 0; j < data.length; j++){
							if(list.spc.pcId==data[j].spc.pcPid){
								replyCount=replyCount+1;
							}
							if(replyCount>0){
								replyCountStr="("+replyCount+")";
							}
						}
						if(isExist==1){
							if(list.spc.pcPid == 0&&list.spc.problemStatus!=1){
								pattern = pattern2 + pattern5;
								item = item + pattern
									.replace("#0", list.unit)
									.replace("#1", list.head)
									.replace("#2", list.spc.pcContent)
									.replace("#3", list.spc.pcLike)
									.replace("#5", list.spc.pcId)
									.replace("#6", list.spc.pcId)
									.replace("#7", list.spc.pcId)
									.replace("#8", list.spc.pcId)
									.replace("#9", replyCountStr),
								html += item;
							}else if(list.spc.pcPid == 0&&list.spc.problemStatus==1){
								pattern = pattern2 + pattern4;
								item = item + pattern
									.replace("#0", list.unit)
									.replace("#1", list.head)
									.replace("#2", list.spc.pcContent)
									.replace("#3", list.spc.pcLike)
									.replace("#5", list.spc.pcId)
									.replace("#6", list.spc.pcId)
									.replace("#7", list.spc.pcId)
									.replace("#8", list.spc.pcId)
									.replace("#9", replyCountStr),
								html = item+html;
							}
							/* else if(list.spc.pcPid != 0&&list.spc.problemStatus!=1){
								pattern = pattern1 + pattern5
								item = item + pattern
									.replace("#0", list.unit)
									.replace("#1", list.head)
									.replace("#2", list.spc.pcContent)
									.replace("#3", list.spc.pcLike)
									.replace("#4", list.spc.pcPid)
									.replace("#5", list.spc.pcId)
									.replace("#6", list.spc.pcId)
									.replace("#7", list.spc.pcId),
								html += item;
							}
							else if(list.spc.pcPid != 0&&list.spc.problemStatus==1){
								pattern = pattern1 + pattern4
								item = item + pattern
									.replace("#0", list.unit)
									.replace("#1", list.head)
									.replace("#2", list.spc.pcContent)
									.replace("#3", list.spc.pcLike)
									.replace("#4", list.spc.pcPid)
									.replace("#5", list.spc.pcId)
									.replace("#6", list.spc.pcId)
									.replace("#7", list.spc.pcId),
								html += item;
							} */
						}else{
							if(list.spc.pcPid == 0){
								pattern = pattern2 + pattern3;
								item = item + pattern
									.replace("#0", list.unit)
									.replace("#1", list.head)
									.replace("#2", list.spc.pcContent)
									.replace("#3", list.spc.pcLike)
									.replace("#5", list.spc.pcId)
									.replace("#6", list.spc.pcId)
									.replace("#7", list.spc.pcId)
									.replace("#8", list.spc.pcId)
									.replace("#9", replyCountStr),
								html += item;
							}
						/* 	else if(list.spc.pcPid != 0){
								pattern = pattern1 + pattern3
								item = item + pattern
									.replace("#0", list.unit)
									.replace("#1", list.head)
									.replace("#2", list.spc.pcContent)
									.replace("#3", list.spc.pcLike)
									.replace("#4", list.spc.pcPid)
									.replace("#5", list.spc.pcId)
									.replace("#6", list.spc.pcId)
									.replace("#7", list.spc.pcId),
								html += item;
							} */
						}
						
					}
					$(".hfpl").html(html);
					$('ul #right_a').click(function() {
						if(isExist==1){
							$(this).parent().parent().siblings('.li_input').toggle();
							$(this).parent().parent().siblings('.replay').toggle();
							//$(this).parent().parent().siblings('.use_answer').toggle();
							$(this).parent().parent().next().toggle();
						}else{
							//暂时没错
							$(this).parent().parent().siblings('.li_input').toggle();
							$(this).parent().parent().siblings('.replay').toggle();
							$(this).parent().parent().siblings('.use_answer').toggle();
							$(this).parent().parent().next().next().toggle();
						}
						//alert($(this).attr("val"));
						var html="";
						for(var i = 0; i < data.length; i++){
							var likeDiv = '<div class="replyLike" val='+data[i].spc.pcId+'>\
										<span>\
											<img src="image/fbxq_z.png">&nbsp;&nbsp;赞('+data[i].spc.pcLike+')\
										</span>\
									</div>';
							if($(this).attr("val")==data[i].spc.pcPid){
								html=html+"<li><div>"+data[i].unit+":</div><div>"+data[i].spc.pcContent+"</div>"+likeDiv+"</li>";
								
							}
						}
						//$(this).parent().parent().next().children('ul').html(html);
						if(isExist==1){
							$(this).parent().parent().next().children('ul').html(html);
						}else{
							//暂时没错
							$(this).parent().parent().next().next().children('ul').html(html);
						}
						
						$(".replyLike").click(function(){
						likeComment($(this).attr("val"));
						//alert($(this).attr("val"));
						var likeVal=$(this).text().trim();
						likeVal=likeVal.substring(0, likeVal.length-1);
						likeVal=parseInt(likeVal.substr(2))+1;
						$(this).html("<img src='image/fbxq_z.png'>&nbsp;&nbsp;赞("+likeVal+")");
						});
						if(isExist==1){
							$('div .li_input').remove();
						}
					});
					$('ul #cancel_btn').click(function() {
						$(this).parent().parent().parent().children('.li_input').toggle();
						$(this).parent().parent().parent().children('.replay').toggle();
						$(this).parent().parent().parent().children('.use_answer').toggle();
						$(this).parent().parent().prev().prev().toggle();
					});
					 $(".right_a").click(function(){
						//alert($(this).attr("ref"));
						//alert($(this).parent().parent().prev().children('input').val());
						addComment($(this).attr("ref"),$(this).parent().parent().prev().children('input').val());
							
					});
					$(".like").click(function(){
						likeComment($(this).attr("val"));
						var likeVal=$(this).text().trim();
						likeVal=likeVal.substring(0, likeVal.length-1);
						likeVal=parseInt(likeVal.substr(2))+1;
						$(this).html("<img src='image/fbxq_z.png'>&nbsp;&nbsp;赞("+likeVal+")");
					});
					$(".answer_a").click(function(){
						adoptComment($(this).attr("val"));
					});
				}
			});
		}
		
		function addComment(pcId,content){
						var type=$("#usertypeoflogin").val();
						var token=$("#token").val();
						var uid=$("#useridoflogin").val();
						if(content==""){
							alert("评论内容不能为空");
							return;
						}
					  $.ajax({
							url:Server + "/problem_select/addComment",
	   						//url:"http://localhost/problem_select/addComment",
							data:{
								"pcId":pcId,
								"type":type,
								"token":token,
								"uid":uid,
								"content":content,
								"needId":${param.needId}
							},
							type : "POST",
							//dataType : "json",
							success : function(data){
	  							alert(data);
	  							window.location.reload();
	 						}
	   					}); 
					}
		function likeComment(pcId){
					  $.ajax({
							url:Server + "/problem_select/likeComment",
							data:{
								"pcId":pcId
							},
							type : "POST",
							//dataType : "json",
							success : function(data){
	  							alert(data);
	  							//comment();
	 						}
	   					}); 
					}
					
		function adoptComment(pcId){
					//alert(pcId);
					if(confirm("确定采纳该评论吗？")){
						$.ajax({
							url:Server + "/problem_select/adoptComment",
							//url:"http://localhost/problem_select/adoptComment",
							data:{
								"needId":${param.needId},
								"pcId":pcId,
							},
							type : "POST",
							//dataType : "json",
							success : function(data){
	  							alert(data);
	  							comment();
	 						}
	   					});
					}else{
						return false;
					}
					    
					}
		/* function replyName(pcPid){
			var name ="";
			$.ajax({
				url:Server + "/problem_select/replyName",
				data:{
					"pcPid":pcPid,
				},
				type:"POST",
				dataType:"json",
				success : function(data){
					var list = data[0];
					var name = list.unit;
					alert(name)
					return name;
				}
			});
		} */
		
		function indes(){
			$(location).attr("href","index");
		}
	</script>

</body>

</html>