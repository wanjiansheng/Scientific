<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<title>成果共享</title>
<meta name="keywords" content="科研成果，生命科学成果，环境检测成果，实验常用设备，分析成果">
<meta name="description"
	content="中国领先的科研成果共享平台，让您快速找到各种类型的科学研究成果。提升闲置成果利用率，产生更大科研价值。涵盖：生命科学成果、环境检测成果、实验常用设备、分析成果、仪表、物性测试、测量/计量成果、在线及过程控制成果。
">
<link rel="stylesheet" href="new_css/header_footer.css?t=1">
<link rel="stylesheet" href="new_css/marineIndex.css">
<link
	href="css/bootstrap.min.css"
	rel="stylesheet">
<style
	src="https://v3.bootcss.com/assets/css/ie10-viewport-bug-workaround.css"></style>
<link href="css/common.css" />
<style>
div#navTab a {
	color: #555;
	color: #555;
	cursor: pointer;
	display: block;
}

.item_left p {
	color: #fff;
}
.mainList .content .right .item .item_right .activBtn a{
	    cursor: pointer;
}
/*img {
    vertical-align: baseline !important;
}*/

.hahaxinxin {
    background-color: #ff8a00;
    width: 100%;
    display: block;
    text-align: center;
    height: 40px;
    line-height: 40px;
}
.hahaxinxin:hover{
	color: #fff;
}
.modal-footer {
  
    /* text-align: right; */
    border-top: 1px solid #e5e5e5;
    background-color: #ff8a00;
}
</style>
<script src="js/jquery-1.11.3.js"></script>
<script src="method/dwy/new_file7.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script>
	$(document).ready(function() {

		typeList();
		typeDetail();
		$("#tenneed").click(function() {
			typeDetail();
		});

		$("#cooneed").click(function() {
			typeDetail();
		});

		$("#perneed").click(function() {
			typeDetail();
		}); 
		$("#shouye").click(function() {
			$(location).attr('href', 'index');
		});
		$("#myself").click(function() {
			$(location).attr('href', 'new_file7');
		});
		$("#zhuanjia").click(function() {
			$(location).attr('href', 'scienic_expertsshow11');
		});
		$("#allNeed").click(function() {
			$(location).attr('href', 'new_file14');
		});
		$("#nanti").click(function() {
			$(location).attr('href', 'new_file15');
		});
		$("#seaEquipment").click(function() {
			$(location).attr('href', 'new_file20');
		});
		$("#publishNeed").click(function() {
			$(location).attr('href', 'publishDemand45');
		});

	})
</script>

<script>
	$(function() {
		$('#front_header .header_wrap .right').hover(function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
			$('#front_header .header_wrap .right .out').show();
		}, function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
			$('#front_header .header_wrap .right .out').hide();
		});
		$('#sorts').on('mouseover', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon4.png');
		});
		$('#sorts').on('mouseout', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon5.png');
		});

		$('.mainList .content .left li').click(function() {
			$('.mainList .content .left li').removeClass('selected');
			$(this).addClass('selected');
			typeDetail();
		});
		$('.mainList .content .left .list_header').click(function() {
			if ($('.mainList .content .left .secendList').is(':hidden')) {
				$('.mainList .content .left .secendList').slideDown();
				$('.mainList .content .left .list_header img').attr("src", "images/insUser_icon.png");
			} else {
				$('.mainList .content .left .secendList').slideUp();
				$('.mainList .content .left .list_header img').attr("src", "images/icon4.png");
			}
			;

		});
		$("#row5 font label").click(function() {
			$("#row5 font label img").attr('src', 'images/icon35.png');
			$("#row5 font label").siblings('input').prop('checked', false);
			$(this).find('img').attr('src', 'images/icon36.png');
			$(this).siblings('input').prop('checked', true);
		});
		$("#row8 font label").click(function() {
			$("#row8 font label img").attr('src', 'images/icon35.png');
			$("#row8 font label").siblings('input').prop('checked', false);
			$(this).find('img').attr('src', 'images/icon36.png');
			$(this).siblings('input').prop('checked', true);
		});
	});
</script>

</head>

<body style="background:#fff;">
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">联系我们</h4>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="exampleInputName">姓名</label> <input type="text"
								class="form-control" id="ww1" placeholder="Email">
						</div>
						<div class="form-group">
							<label for="exampleInputPhone">电话号码</label> <input type="text"
								class="form-control" id="ww2" placeholder="Password">
						</div>
						<div class="form-group">
							<label for="exampleInputAddress">地址</label> <input type="text"
								class="form-control" id="ww3" placeholder="Password">
						</div>
						<!-- <button type="submit" class="btn btn-default">Submit</button> -->
					</form>
				</div>
				<div class="modal-footer">
					<a type="button" class="hahaxinxin" data-dismiss="modal">关闭</a>
					<!--  <button type="button" class="btn btn-primary">保存</button>  -->
				</div>
			</div>
		</div>
	</div>
	<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
	<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
	<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
	<script
		src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<%@include file="/head.jsp"%>
	<div class="logo-search-btn">
		<div class="logo-search">
			<!--left-->
			<div class="header-logo header-logo-change fl">
				<h1>
					<a href="index.jsp">科研成果共享平台</a>
				</h1>
			</div>
			<!--right-->
			<div class="header-search fl">
				<div class="searchOption fl">
					<p class="search-down">
						<a id="claa">科研成果</a> <span> <img class=""
							src="images/sy_arrow_down.png"></span>
					</p>
					<ul id="ulofsousuo" class="navbar-down" style="display: none">
						<li><a id="clab" onclick="myFunction()">科研成果</a></li>
						<li><a id="clab1" onclick="myFunction1()">科研需求</a></li>
						<li><a id="clab2" onclick="myFunction2()">专家展示</a></li>
					</ul>
				</div>
				<div class="searchText fl">
					<input id="likeString" type="text" placeholder="关键词"> <input
						id="findelikestringnumber" value="1" type="text"
						style="display:none"></input>
				</div>
				<div class="searchBtn fl">
					<img src="images/sy_seach.png"> <span
						onclick="searchButton()">搜索</span>
				</div>
			</div>
			<!--btn-->
			<div class="kindOfBtn fl">
				<input type="submit" id="publishNeed" value="发布需求">
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="clear"></div>
	<div class="navBar-nav">
		<nav>
			<ul>
				<li style="padding:0"><a href="index.jsp" id="shouye">科研成果</a></li>
				<li class="active-one"><a href="new_file7" id="myself">科研需求展示</a></li>
				<li><a href="scienic_expertsshow11" id="zhuanjia">专家展示</a></li>
				<li><a href="new_file14" id="allNeed">交易大厅</a></li>
				<li><a href="new_file15" id="nanti">难题发布</a></li>
			</ul>
		</nav>
	</div>
	<div class="clear"></div>
	<div class="mainList">
		<div class="content">
			<div id="sorts" class="left">
			</div>
			<script type="text/javascript">
				//导航栏单击变换内容
				function tabSwitch(_this, num) {
					var tag = document.getElementById("navTab");
					var number = tag.getElementsByTagName("a"); //获取导航栏元素个数(getElementsByTagName是返回元素素组)
					var divNum = document.getElementsByClassName("item"); //获取导航元素对应的div个数
					for (var i = 0; i < number.length; i++) { //number是一个数组，这里应该用number.length显示它的长度5
						number[i].className = " "; //清除所有导航栏元素的特殊样式
						if(divNum[i]){
							divNum[i].style.display = "none"; //其他所有div都隐藏
						}
					}
					_this.className = "tab2"; //给当前导航栏元素添加样式
					var content = document.getElementById("l_no2_" + num); //当前导航栏元素对应的div
					if(content){
					content.style.display = "block"; //显示当前导航栏元素对应的div部分
					}
				}
			</script>
			<div class="right">
				<div class="tabBtn l_nav1" id="navTab">
					<div class="tab1">
						<a id="tenneed" onclick="tabSwitch(this,1)" class="tab2">技术需求</a>
					</div>
					<div class="tab1">
						<a id="cooneed" onclick="tabSwitch(this,2)">合作需求</a>
					</div>
					<div class="tab1">
						<a id="perneed" onclick="tabSwitch(this,3)">人才需求</a>
					</div>

				</div>




				<div id="needList" class="l_no2">
					<div id="l_no2_1" class="item" style="display: block">
						<div class="item_left">
							<p>技术需求</p>
						</div>
						<div class="item_right">
							<div class="textInfo">
								<div class="row1">一种感应发亮的鞋底的实用新型专利定制</div>
								<!--  <div class="row2"><span>需求简介：</span>一种感应发亮的鞋底的实用新型专利定制</div>-->
								<div class="row3">
									<font><span>地址：</span>浙江 温州</font><font><span>发布时间：</span>2018-5-24</font><font><span>行业类别：</span>教育领休闲</font>
								</div>
								<div class="row4">预算：5-10万</div>
							</div>
							<div class="activBtn">
								<a href="#this">查看详情</a> <a href="#this">联系方式</a>
							</div>
						</div>
					</div>
					<div id="l_no2_2" class="item" style="display: none">
						<div class="item_left">
							<p>技术需求</p>
						</div>
						<div class="item_right">
							<div class="textInfo">
								<div class="row1">一种感应发亮的鞋底的实用新型专利定制</div>
								<!--  <div class="row2"><span>需求简介：</span>一种感应发亮的鞋底的实用新型专利定制</div>-->
								<div class="row3">
									<font><span>地址：</span>浙江 温州</font><font><span>发布时间：</span>2018-5-24</font><font><span>行业类别：</span>教育领休闲</font>
								</div>
								<div class="row4">预算：5-10万</div>
							</div>
							<div class="activBtn">
								<a href="#this">查看详情</a> <a href="#this">联系方式</a>
							</div>
						</div>
					</div>
					<div id="l_no2_3" class="item" style="display: none">
						<div class="item_left">
							<p>技术需求</p>
						</div>
						<div class="item_right">
							<div class="textInfo">
								<div class="row1">一种感应发亮的鞋底的实用新型专利定制</div>
								<!--  <div class="row2"><span>需求简介：</span>一种感应发亮的鞋底的实用新型专利定制</div>-->
								<div class="row3">
									<font><span>地址：</span>浙江 温州</font><font><span>发布时间：</span>2018-5-24</font><font><span>行业类别：</span>教育领休闲</font>
								</div>
								<div class="row4">预算：5-10万</div>
							</div>
							<div class="activBtn">
								<a href="#this">查看详情</a> <a href="#this">联系方式</a>
							</div>
						</div>
					</div>
				</div>
				<script>
					function findOrder(needId) {
						$(location).attr('href', 'new_file8?needId=' + needId + '');
					}
				</script>



			</div>
		</div>
		<div class="pageButton"></div>
		<!--找个分页插件插入-->
	</div>

	<%@include file="/footer.jsp"%>


	<script src="js/easyResponsiveTabs.js"></script>
	<script src="js/common.js"></script>
</body>

</html>