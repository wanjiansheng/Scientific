<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

	<head>
	<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>仪器共享</title>
		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">
		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
">
		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/orderManagement.css">
		<style type="text/css">
			.mainList .content .right .infoList li .row2 .tab6 .btn {
    display: inline-block;
    width: 66px;
    height: 35px;
    line-height: 35px;
    text-align: center;
    background: #f7843c;
    border-radius: 5px;
    /* margin: auto; */
}
.mainList .content .right .infoList li .row2 .tab4 {
    width: 15%;
    border-right: 1px solid #e6e6e6;
    border-left: 1px solid #e6e6e6;
    
    
   
}
.mainList .content .right .infoList li .row2 #tab4 {
    width: 15%;
    border-right: 1px solid #e6e6e6;
    border-left: 1px solid #e6e6e6;
    border-top: 0px solid #e6e6e6;
    border-bottom:0px solid #e6e6e6;    
   
}

.mainList .content .right .infoList li .row2{
     height:100%;
}

.mainList .content .right .infoList li .row2 .tab1 img {
    /* width: 15%; */
    width: 100px;
    height: 100px;
}
		</style>
		<link rel="stylesheet" href="css/common.css">
		<script src="js/jquery-1.11.3.js"></script>
		<script>
			$(function() {
				
				/*-----------*/
				$(".tab_span1").click(function(){
					$(this).addClass('tab2').siblings().removeClass('tab2');
       $("#top2_item1").css("display","block");
       $("#top2_item2").css("display","block");
       $("#top1_item1").css("display","none");
       $("#top1_item2").css("display","none");
});


$(".tab_span2").click(function(){
	$(this).addClass('tab2').siblings().removeClass('tab2');
       $("#top1_item1").css("display","block");
       $("#top1_item2").css("display","block");
       $("#top2_item1").css("display","none");
       $("#top2_item2").css("display","none");
});			
				/*-----------*/
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$('.mainList .content .left li').removeClass('selected');
					$(this).addClass('selected');
				});
				$('.mainList .content .left .list_header').click(function() {
					if($('.mainList .content .left .secendList').is(':hidden')) {
						$('.mainList .content .left .secendList').slideDown();
						$('.mainList .content .left .list_header img').attr("src", "images/insUser_icon.png");
					} else {
						$('.mainList .content .left .secendList').slideUp();
						$('.mainList .content .left .list_header img').attr("src", "images/icon4.png");
					};

				});
				$("#row5 font label").click(function() {
					$("#row5 font label img").attr('src', 'images/icon35.png');
					$("#row5 font label").siblings('input').prop('checked', false);
						$(this).find('img').attr('src', 'images/icon36.png');
						$(this).siblings('input').prop('checked', true);
				});
				$("#row8 font label").click(function() {
					$("#row8 font label img").attr('src', 'images/icon35.png');
					$("#row8 font label").siblings('input').prop('checked', false);
						$(this).find('img').attr('src', 'images/icon36.png');
						$(this).siblings('input').prop('checked', true);
				});
			});
		</script>

	</head>

	<body style="background:#fff;">

	<%@include file="/head.jsp"%>

		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
	<a href="index" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>

	</div>
					<div class="web_name">
						用户中心
					</div>
				</div>
			</div>
		</div>
		<div class="mainList">
			<div class="bNav">您的位置：

			<a href="index">首页</a> > 
			<a href="new_file39">用户中心</a> > 
			<span>管理成果</span></div>
			<div class="content">
			<%@include file="/left-tab.jsp"%>
				<script>
					$(document).ready(function(){
						$("#upnewinfo").click(function(){
							var upinfourl =  "/Scientific/updata34";
							window.location.replace(upinfourl);
						});
					});
				</script>
				<div class="right">
					<div class="title"><span>管理成果</span></div>
					
					<div class="tab_header" id="top1_item1">
						<div class="item1">成果名称</div>
						<div class="item2"></div>
						<div class="item3">成熟度</div>
						<div class="item4">团队带头人</div>
						<div class="item5">状态</div>
						<div class="item6">操作</div>
					</div>
					
					<div class="infoList" id="top1_item2">
						<ul id="infoofuserlist" >
							<!-- <li>
								<div class="row2">
									<div class="tab1"><img src="images/usercenter_pic.png" /></div>
									<div class="tab2">高内涵筛选系统</div>
									<div class="tab3">10-20万</div>
									<div class="tab4" id="tab4">张三</div>
									<div class="tab5">
										<p>待确认</p>
										<p>
											<a href="#this">查看详情</a>
										</p>
									</div>
									<div class="tab6">
										<div class="btn">
											<a href="#this">查看</a>
										</div>
										<div class="btn" style="background-color: rgb(0,174,255);">
											<a href="#this" >删除</a>
										</div>
										<div class="time">还剩11:23:02</div>
									</div>
								</div>
							</li>-->
						</ul>
					</div> 
					<script>
					function delRst(resultId){
						var msg = "您真的确定要删除吗？\n\n请确认！";
						if(confirm(msg)==true){
							del(resultId);
							return true;
						}else{
							return false;
						}
					}
					function del(resultId){
						$.ajax({
							url :Server + "/fon/infodetailsofdelect",
							data : {
								usertype:$('#usertypeoflogin').val(),
								userid:$('#useridoflogin').val(),
								resultId:resultId
							},
							type: "POST",
							dataType : "json",
							success : function(data) {
								if(data == 0){
									alert("删除成功");
									loadData();
								}else{
									alert("删除成功");
								}
							},
							error : function(msg){
								alert("删除失败");
								loadData();
							}
						});
					}
					</script>
            
                	<script>
                		$(document).ready(function(){
                			loadData();
                		});	
                		function loadData(){
							var usertype = $('#usertypeoflogin').val();
							var userid = $('#useridoflogin').val();
                			$.ajax({
                				url:Server + "/fon/userofinfolist",
                				data:{
                					limit:10,
                					offset:0,
                					usertype : usertype,
                					userid : userid,
                				},
                				dataType:"JSON",
                				success: function(data){
                					console.log(data)
                					var html = "";
									var pattern = '<li>\
										<div class="row2">\
											<div class="tab1"><img src="#0" /></div>\
											<div class="tab2">#1</div>\
											<div class="tab3">#2</div>\
											<div class="tab4" id="tab4">#3</div>\
											<div class="tab5">\
												<p>#4</p>\
											</div>\
											<div class="tab6">\
												<div class="btn">\
													<a href="new_file361?infoid=#10">查看</a>\
												</div>\
												<div class="btn" style="background-color: rgb(0,174,255);">\
													<a onclick="delRst(#57)">删除</a>\
												</div>\
											</div>\
										</div>\
									</li>';
									for(var i = 0; i< data.length; i++){
										var item = "";
										var order = data[i];
										var Maturity = null;
										var Controlstatus = null;
										switch(order.resultMaturity){
											case 0:
												Maturity = "正在研发";
												break;
											case 1:
												Maturity = "已有样品";
												break;
											case 2:
												Maturity = "通过小试";
												break;
											case 3:
												Maturity = "通过中试";
												break;
											case 4:
												Maturity = "可以量产";
												break;
											default:
										}
										switch(order.resultControlstatus){
											case 0:
												Controlstatus = "未确定";
												break;
											case 1:
												Controlstatus = "展示中";
												break;
											case 2:
												Controlstatus = "锁定中";
												break;
											case 3:
												Controlstatus = "等待买家确认";
											default:
										}	
										item += pattern
											.replace("#0", order.img.rimagePath)//成果图片 
											.replace("#1", order.resultName)//成果名称
											.replace("#2", Maturity)//成熟度
											.replace("#3", order.resultTeamchampion)//团队带头人
											.replace("#4", Controlstatus)//状态
											.replace("#10", order.resultId)//成果ID
											.replace("#12", 123)//用户ID
											.replace("#57", order.resultId)//成果ID
											/* .replace("#11", usertype)//用户类别
											.replace("#12", userid)//用户ID */
										html += item;
									}
									$("#infoofuserlist").html(html);
                				}
                			});
                		}
                	</script>
                <!----------------------------------->
				   
				<!----------------------------------->
				
				</div>
			</div>
			<div class="pageButton"></div>
			<!--找个分页插件插入-->
		</div>

		<%@include file="/footer.jsp"%>
		<script src="js/layui/layui.js"></script>
	</body>

</html>