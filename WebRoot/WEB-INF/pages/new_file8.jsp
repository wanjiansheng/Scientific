<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

	<head>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<title>成果共享</title>

		<meta name="keywords" content="科研成果，生命科学成果，环境检测成果，实验常用设备，分析成果">

		<meta name="description" content="中国领先的科研成果共享平台，让您快速找到各种类型的科学研究成果。提升闲置成果利用率，产生更大科研价值。涵盖：生命科学成果、环境检测成果、实验常用设备、分析成果、仪表、物性测试、测量/计量成果、在线及过程控制成果。
         
">
		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/productDetail.css">
		<link rel="stylesheet" href="css/new_style.css">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<style src="https://v3.bootcss.com/assets/css/ie10-viewport-bug-workaround.css"></style>
		<link rel="stylesheet" href="js/layui/css/layui.css" media="all">
		<style type="text/css">
			.mainList .content .productInfo .right {
    float: left;
    height: 100%;
    padding-left: 25px;
    width: 100%;
}
.mainList .content .productInfo .right .row9 {
    overflow: hidden;
    margin-top: 50px;
    text-align: center;
}
.mainList .content .productInfo .right .row9 .btn1 {
    display: inline-block;
}
.mainList .content .productInfo .right .row9 .btn {
    float: none;
}

.mainList .content .productInfo {
    width: 100%;
    border: 1px solid #d3d3d3;
     padding: 0px;
     padding-top: 30px;
    height: 446px;
    overflow: hidden;
}

	.mainList .content .productInfo .right .row9 .btn a {
	display: block;
    width: 158px;
    height: 44px;
    line-height: 44px;
    text-align: center;
    background: #ff8a00;
    color: #fff;
    border-radius: 6px;
    font-size: 18px;
    outline: none;
    border: none;
    border-radius: 0px;
	}
	.mainList .content .productInfo {
	width: 100%;
	border: 1px solid #d3d3d3;
	padding: 0px;
	padding-top: 30px;
	height: 469px;
	overflow: hidden;
	}
	.btn.btn-lg {
    width: 158px;
    height: 44px;
    background: rgb(6,185,254);
    color: #fff;
    text-align: center;
    vertical-align: middle;
    top: 50%;
    line-height: 22px;
    border-radius: 0px;
    border: none;
    outline: none;
}

div#nav_top > div.container > div.fl {
    width: 340px;
    padding-top: 50px;
    /*margin-left: -26px;*/
}


.hahaxinxin {
    background-color: #ff8a00;
    width: 100%;
    display: block;
    text-align: center;
    height: 40px;
    line-height: 40px;
    color: #fff;
}
.hahaxinxin:hover{
	color: #fff;
}
.modal-footer {
  
    /* text-align: right; */
    border-top: 1px solid #e5e5e5;
    background-color: #ff8a00;
	height: 57px;
	padding: 8px !important;
}

	.modal-content {
		width: 500px;
	}

	.sun_li_row .li_row2 {
		padding-top: 10px;
		padding-bottom: 10px;
		padding-left: 35px;
		margin-top: 25px;
	}
		</style>
		<link rel="stylesheet" href="css/common.css">
		<script src="js/jquery-1.11.3.js"></script>
		<script src="method/dwy/new_file8.js"></script>
		<script >
		 // load();
		 // particulars();
		 // next();
		 // describe();
		   
		   
		  $(document).ready(function(){
			load(GetQueryString("needId"));
			particulars(GetQueryString("needId"));
			next(GetQueryString("needId"));
			describe(GetQueryString("needId"));
			phones(GetQueryString("needId"));
			/* $("#need").click(function(needId){
			 needId = $('#10').val();
			 $(location).attr('href', 'need_details10?needId='+needId+'');
				              });*/
			
			});
			function GetQueryString(name) {
				var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
				var r = window.location.search.substr(1).match(reg);
				if(r != null) return unescape(r[2]);
				return null;
			}
		</script>
		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$(this).addClass('selected').siblings().removeClass('selected');
				});
				
				
				
				//--------
				$(".tab_span1").click(function(){
					$(this).addClass('selected').siblings().removeClass('selected');
       $("#tabView1").css("display","block");
       $("#tabView2").css("display","none");
       $("#tabView3").css("display","none");
});


$(".tab_span2").click(function(){
	$(this).addClass('selected').siblings().removeClass('selected');
    $("#tabView2").css("display","block");
    $("#tabView1").css("display","none");
    $("#tabView3").css("display","none");
});

    $(".tab_span3").click(function(){
	$(this).addClass('selected').siblings().removeClass('selected');
	$("#tabView3").css("display","block");
    $("#tabView2").css("display","none");
    $("#tabView1").css("display","none");
});
    
				
				
				
				
				
				
			});
		</script>
	</head>
	<body style="background:#fff;">
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title" id="myModalLabel">联系我们</h4>
		            </div>
		            <div class="modal-body">
		                <form>
		                    <div class="form-group">
		                        <label for="exampleInputName">姓名</label>
		                        <input type="text" class="form-control" id="ww1" placeholder="Email">
		                    </div>
		                    <div class="form-group">
		                        <label for="exampleInputPhone">电话号码</label>
		                        <input type="text" class="form-control" id="ww2" placeholder="Password">
		                    </div>
		                    <div class="form-group">
		                        <label for="exampleInputAddress">地址</label>
		                        <input type="text" class="form-control" id="ww3" placeholder="Password">
		                    </div>
		                    <!-- <button type="submit" class="btn btn-default">Submit</button> -->
		                </form>
		            </div>
		            <div class="modal-footer">
		               <a type="button" class="hahaxinxin" data-dismiss="modal">关闭</a>
		               <!--  <button type="button" class="btn btn-primary">保存</button>  -->
		            </div>
		        </div>
		    </div>
		</div>
		<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
		<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
		<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
		<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		<%@include file="/head.jsp"%>
		
		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
						<a href="index.jsp" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);">科研成果共享平台</a>
					</div>
					<!--<div class="web_name">
						<img src="images/icon3.png" />
						<p>yq.shang.com</p>
					</div>-->
				</div>
			</div>
		</div>
		

		
		
		<div class="mainList">
			<div class="bNav">您的位置：<a href="index">首页</a> > <a href="new_file7">需求列表</a> > <span>需求详情</span></div>
			<div class="content">
				
				<!------------------------->
				
				
				<!------------------------->
				
				<div class='productInfo'> 
					<div class="content_title">
					<a>技术难题解决</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<img src="image/xqxq_yjj.png"/>
					<span style="color: rgb(255,29,145);" type="text" name="" id="in8" value=""></span>
				</div>
					
					<div class="right">
						<div class="row row1"><span type="text" name="" id="in0" value="" ></span></div>
						
						<div class="row row3">类型：<span type="text" name="" id="in1" value="" ></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行业类型：<span type="text" name="" id="in2" value="" ></span></div>
						<div class="row">需求简介：<span type="text" name="" id="in3" value="" ></span></div>
						<div class="row">发布时间：<span type="text" name="" id="in4" value="" ></span></div>
						<div class="row">截止时间：<span type="text" name="" id="in5" value="" ></span></div>
						<div class="row">地址：<span type="text" name="" id="in6" value="" ></span></div>
						<div class="row">预算：<span style="color: rgb(255,138,0);" type="text" name="" id="in7" value=""></span>&nbsp;&nbsp;
							
						</div>
						<div class="row row9">
							 <div onclick="phone(#9)" href="#this" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" >联系方式</div>
							<div class="btn1 btn"   ><a id="need">承接需求</a></div>
							<input style="display:none" name="needId" id="10" value=""></input>
						<!--	<div class="price">价格：<span>100元/样品</span></div>-->
						</div>
					</div>
				</div>
				
				
				<div class="userEvaluation">
					<div class='tab'><span class="tab_span3 selected">详细描述</span><span class="tab_span1 ">方案与评审</span><span class="tab_span2 ">用户评价</span></div>
					<div class="box">
						<div class='tabView'  id="tabView1" style="display: none;">
				<!------------------------>
				<ul id=detailList>
								<li class="sun_li_row">
									<div class="li_title" style=" padding-left: 10px;"><a>专家填写评审意见</a></div>
									<div style="padding-right: 20px;display: inline-block;">
									 <div class="li_row1">
									 	<div class="info_left">啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里</div>
									 	<div class="info_right">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />
									 		<span style="color: rgb(6,185,254);">144444444444</span>
									 	</div>
									 </div>
									 
									 <div class="li_row2">
									 	<div class="info_left" >啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里</div>
									 	<div class="info_right" style="float: right;">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />
									 	</div>
									 </div>
									 
									 <div class="li_row2" style="float: right;">
									 	<div class="info_left">啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里</div>
									 	<div class="info_right" style="float: right;">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />
									 		
									 	</div>
									 </div>
									</div>

								</li>
								
								<li class="sun_li_row">
									<div class="li_title" style=" padding-left: 10px;"><a>专家填写评审意见</a></div>
									<div style="padding-right: 20px; display: inline-block;">
									 <div class="li_row1">
									 	<div class="info_left">啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里</div>
									 	<div class="info_right">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />
									 		<span style="color: rgb(6,185,254);">144444444444</span>
									 	</div>
									 </div>
									 
									 <div class="li_row2">
									 	<div class="info_left" >啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里</div>
									 	<div class="info_right" style="float: right;">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />
									 	</div>
									 </div>
									 
									 <div class="li_row2" style="float: right;">
									 	<div class="info_left">啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里</div>
									 	<div class="info_right" style="float: right;">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />
									 		
									 	</div>
									 </div>
									</div>

								</li>
								<li class="sun_li_row">
									<div class="li_title" style=" padding-left: 10px;"><a>专家填写评审意见</a></div>
									<div style="padding-right: 20px;display: inline-block;">
									 <div class="li_row1">
									 	<div class="info_left">啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里</div>
									 	<div class="info_right">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />
									 		<span style="color: rgb(6,185,254);">144444444444</span>
									 	</div>
									 </div>
									 
									 <div class="li_row2">
									 	<div class="info_left" >啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里</div>
									 	<div class="info_right" style="float: right;">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />
									 	</div>
									 </div>
									 
									 <div class="li_row2" style="float: right;">
									 	<div class="info_left">啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里
									 	啦啦啦啦啦啦啦啦啦啦啦阿里</div>
									 	<div class="info_right" style="float: right;">
									 		<span>啦啦啦<span>（个人服务器）</span></span><br />
									 		
									 	</div>
									 </div>
									</div>

								</li>
								
								
								
							</ul>
							
				
				
	            
	                
				
				<!------------------------>
						</div>
						<div class='tabView' id="tabView2" style="display: none;" >
							<ul id=commentList>
								<li>
									<div class="fontInfo">
										<div class="left">成果特别好用，服务态度也很好 成果特别好用，服务态度也很好成果特别好用，服务态度也很好服务态度也很好</div>
										<div class="right">一日***记忆<span>（匿名）</span></div>
									</div>
									
									<div class="pic">
										<img src="images/productDetailPic.png"/>
										<!-- <img src="images/productDetailPic.png"/> -->
									</div>
									<div class="replay">
										<!--<div class='left'>回复：<span>谢谢您的支持！</span></div>-->
										<div class='right'><a class="right_a">查看合同文档</a></div>
									</div>
								</li>
								<li>
									<div class="fontInfo">
										<div class="left">成果特别好用，服务态度也很好 成果特别好用，服务态度也很好成果特别好用，服务态度也很好服务态度也很好</div>
										<div class="right">一日***记忆<span>（匿名）</span></div>
									</div>
									
									<div class="pic">
										<img src="images/productDetailPic.png"/>
										<!-- <img src="images/productDetailPic.png"/> -->
									</div>
									<div class="replay">
										<!--<div class='left'>回复：<span>谢谢您的支持！</span></div>-->
										<div class='right'><a class="right_a">查看合同文档</a></div>
									</div>
								</li>
								<li>
									<div class="fontInfo">
										<div class="left">成果特别好用，服务态度也很好 成果特别好用，服务态度也很好成果特别好用，服务态度也很好服务态度也很好</div>
										<div class="right">一日***记忆<span>（匿名）</span></div>
									</div>
									
									<div class="pic">
										<img src="images/productDetailPic.png"/>
										<!-- <img src="images/productDetailPic.png"/> -->
									</div>
									<div class="replay">
										<!--<div class='left'>回复：<span>谢谢您的支持！</span></div>-->
										<div class='right'><a class="right_a">查看合同文档</a></div>
									</div>
								</li>
							
							</ul>

						</div>
					<div class='tabView  sun_tabView' id="tabView3"  >
							<ul id="DetailsList">
								<li style="min-height: 225px;">
								   <p style="color: rgb(255,138,0); font-size: 18px;">项目简介:</p>
								   <p style="font-size:18px;line-height: 20px;line-height: 41px;"><span type="text" name="" id="detail" value="" ></span></p>
								</li>
								
							</ul>

						</div>
					
					
					</div>
				</div>
			</div>
			<div class="pageButton"></div>
			<!--找个分页插件插入-->
		</div>

		<%@include file="/footer.jsp"%>
		<script src="js/layui/layui.js"></script>
		<script>
layui.use('carousel', function(){
  var carousel = layui.carousel;
  //建造实例
  carousel.render({
    elem: '#test1'
    ,width: '100%' //设置容器宽度
    ,arrow: 'always' //始终显示箭头
    //,anim: 'updown' //切换动画方式
  });
});
</script>
<script>
	
</script>

	</body>

</html>