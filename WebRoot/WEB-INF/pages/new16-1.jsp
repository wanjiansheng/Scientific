<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

	<head>
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<title>成果共享</title>

		<meta name="keywords" content="科研成果，生命科学成果，环境检测成果，实验常用设备，分析成果">

		<meta name="description" content="中国领先的科研成果共享平台，让您快速找到各种类型的科学研究成果。提升闲置成果利用率，产生更大科研价值。涵盖：生命科学成果、环境检测成果、实验常用设备、分析成果、仪表、物性测试、测量/计量成果、在线及过程控制成果。
         
">
		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/productDetail.css">
		<link rel="stylesheet" href="css/new_style.css">
		
		<link rel="stylesheet" href="js/layui/css/layui.css" media="all">
		<link rel="stylesheet" href="css/common.css">
		<style type="text/css">
			.mainList .content .productInfo .right {
    float: left;
    height: 100%;
    padding-left: 25px;
    width: 100%;
}
.mainList .content .productInfo .right .row9 {
    overflow: hidden;
    margin-top: 50px;
    text-align: center;
}
.mainList .content .productInfo .right .row9 .btn1 {
    display: inline-block;
}
.mainList .content .productInfo .right .row9 .btn {
    float: none;
}
.mainList .content .productInfo {
    width: 100%;
    border: 1px solid #d3d3d3;
    padding: 0px;
     padding-top:30px;
        height: 240px;
    overflow: hidden;
}
.mainList .content .productInfo .right .row1{
	margin-bottom: 29px;
}
.mainList .content .productInfo .right .row2{
	margin-bottom: 39px;
}
.mainList .content .userEvaluation .box .tabView li .pic img {
    height: 73px;
    border: 1px solid #d3d3d3;
    border-radius: 50%;
}
.li_input{
	width: 100%;
	height: 45px;
	margin-top: 10px;
	margin-bottom: 10px;
}
.li_input input{
	width: 100%;
	height: 45px;
	border-radius: 4px;
}
		</style>
		<script src="new_js/jquery-1.11.3.js"></script>
		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$(this).addClass('selected').siblings().removeClass('selected');
				});
				
				
				
				//--------
				$(".tab_span1").click(function(){
					$(this).addClass('selected').siblings().removeClass('selected');
       $("#tabView1").css("display","block");
       $("#tabView2").css("display","none");
});


$(".tab_span2").click(function(){
	$(this).addClass('selected').siblings().removeClass('selected');
    $("#tabView2").css("display","block");
    $("#tabView1").css("display","none");
});
				
				
				
				
				
				
			});
		</script>
	</head>
	<body style="background:#fff;">

		<div id="front_header">
			<div class="header_wrap">
				<div class="fl left">
					<img src="images/home.png" />
					<span>欢迎访问成果共享平台</span>
				</div>
		


				<div class="fr right">
					<div class="main">
						<div class="user_pic fl"><img src="images/user.png" /></div>
						<div class='user_name fl'>福建省水产研究所</div>
						<div class='icon fl'><img src="images/icon1.png" /></div>
					</div>
					
					<div class="out">
						<div class="user_center">
							<a href="javascript:void(0)">用户中心</a>
						</div>
						<div class="log_out">
							<a href="#this">退出登录</a>
						</div>
						<div class="tri"><img src="images/icon9.png" /></div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
	<a href="#this" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>
					</div>
					<!--<div class="web_name">
						<img src="images/icon3.png" />
						<p>yq.shang.com</p>
					</div>-->
				</div>
			</div>
		</div>
		

		
		
		<div class="mainList">
			<div class="bNav">您的位置：需求发布》<span>难题详情</span></div>
			<div class="content">
				
				<!------------------------->
				
				
				<!------------------------->
			    
				<div class='productInfo'> 
					
					<div class="right">
						<div class="row row1" id ="o" >全功能紫外-可见-近红外荧光光谱仪</div>
						<div class="row row2">难题简介：<span id="oo" >北京全功能紫外-可见-近红外荧光光谱仪全功能紫外-可见-近红外荧光光谱仪全功能紫外-可见-近红外荧光光谱仪</span></div>
						<div class="row row3">地址：<span id="ooo" >23658</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行业类型：<span id="oooo" >23658</span></div>
					</div>
				</div>
				
				<div class="userEvaluation">
					<div class='tab'><span class="tab_span1 selected">详情描述</span><span class="tab_span2 ">相关评论()条</span></div>
					<div class="box">
						<div class='tabView sun_tabView '  id="tabView1" style="padding: 25px;">
				            <h2 style="color: rgb(255,138,0);">难题简介</h3>
				            	<br />
				            	
				            <h3>难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介</h4>
						</div>
						<div class='tabView' id="tabView2" style="display: none;" >
							<ul>
								<li>
									
									
									<div class="pic">
										<img src="images/productDetailPic.png"/><span>xxxxxxxx</span>
									</div>
									<div class="fontInfo">
										<div class="left">成果特别好用，服务态度也很好 成果特别好用，服务态度也很好成果特别好用，服务态度也很好服务态度也很好</div>
										
									</div>
									<div class="fontInfo">
										
										<div class="right">
											<span style="color: black;margin-right: 40px;"><img src="image/fbxq_z.png">&nbsp;&nbsp;赞</span>
											
											<span style="color: black;"><img src="image/fbxq_pl.png">&nbsp;&nbsp;评论</span>
										</div>
									</div>
									<div class="li_input">
										<input type="text" />
									</div>
									<div class="replay">
										<!--<div class='left'>回复：<span>谢谢您的支持！</span></div>-->
										<div class='right'>
											<a style="margin-right: 20px;">取消</a>
											<a class="right_a" style="background-color: rgb(255,138,0);color: white;">发布评论</a></div>
									</div>
								</li>
								
								
								<li>
									<div class="fontInfo">
										<div class="left">成果特别好用，服务态度也很好 成果特别好用，服务态度也很好成果特别好用，服务态度也很好服务态度也很好</div>
										<div class="right">一日***记忆<span>（匿名）</span></div>
									</div>
									
									<div class="pic">
										<img src="images/productDetailPic.png"/>
										<img src="images/productDetailPic.png"/>
									</div>
									<div class="replay">
										<!--<div class='left'>回复：<span>谢谢您的支持！</span></div>-->
										<div class='right'><a class="right_a">查看合同文档</a></div>
									</div>
								</li>
								<li>
									<div class="fontInfo">
										<div class="left">成果特别好用，服务态度也很好 成果特别好用，服务态度也很好成果特别好用，服务态度也很好服务态度也很好</div>
										<div class="right">一日***记忆<span>（匿名）</span></div>
									</div>
									
									<div class="pic">
										<img src="images/productDetailPic.png"/>
										<img src="images/productDetailPic.png"/>
									</div>
									<div class="replay">
										<!--<div class='left'>回复：<span>谢谢您的支持！</span></div>-->
										<div class='right'><a class="right_a">查看合同文档</a></div>
									</div>
								</li>
							
							</ul>

						</div>
					</div>
				</div>
			</div>
			
			<div class="pageButton"></div>
			<!--找个分页插件插入-->
		</div>



		<%@include file="/footer.jsp"%>
		<script src="js/layui/layui.js"></script>
		
		
		<script>
layui.use('carousel', function(){
  var carousel = layui.carousel;
  //建造实例
  carousel.render({
    elem: '#test1'
    ,width: '100%' //设置容器宽度
    ,arrow: 'always' //始终显示箭头
    //,anim: 'updown' //切换动画方式
  });
});
</script>
<script>
$(document).ready(function(){
   	$.ajax({
		url:Server + "/problem_select/detalis",
		data:{
			"needId":${param.needId},
		},
		type : "POST",
		dataType : "json",
		success : function(data){
  			$("#o").html(data[0].needName);
  			$("#oo").text(data[0].needExplain);
  			$("#ooo").html(data[0].address);
  			$("#oooo").text(data[0].tTrade.tradeName);
 			}
   		});
   	});
</script>

	</body>

</html>