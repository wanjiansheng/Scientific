<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<!-- head -->

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>成果共享</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <meta name="keywords" content="科研成果，生命科学成果，环境检测成果，实验常用设备，分析成果">
    <meta name="description" content="中国领先的科研成果共享平台，让您快速找到各种类型的科学研究成果。提升闲置成果利用率，产生更大科研价值。涵盖：生命科学成果、环境检测成果、实验常用设备、分析成果、仪表、物性测试、测量/计量成果、在线及过程控制成果。

">
    
    <link rel="stylesheet" href="css/l_css/header.css">
    <link rel="stylesheet" href="css/l_css/main.css">
    <link rel="stylesheet" href="css/new_file14.css">

   <!--  <link rel="stylesheet" type="text/css" href="css/common.css"> -->
    <!--<link rel="stylesheet" type="text/css" href="css/main_style.css" />-->
<style type="text/css">
  .scroll_div .scroll_ul {
    width: 100%;
    cursor: pointer;
}
.scroll_div{
	width: 1198px;
	border: 1px solid #ccc;
}
.table_img img {
    height: 100%;
    width: 1200px;
}




/*原本的样式*/
<%--.scroll_ul>li .item6 {--%>
    <%--color: black !important;--%>
<%--}--%>



<%--/*交易成功的样式*/--%>
<%--.scroll_ul>li .change-item6{--%>
	<%--color: #52C600 ;--%>
	<%--font-size: 16px;--%>
<%--}--%>
</style>
    
	<script src="js/jquery-1.11.3.js"></script>
    <script src="method/dwy/new_file14.js"></script>
	<link rel="stylesheet" href="css/common.css">
		<script >
		    deal();
		    need();
		     $(document).ready(function(){
						     $("#shouye").click(function(){
				                $(location).attr('href', 'index');
				              });
				              $("#myself").click(function(){
				                $(location).attr('href', 'new_file7');
				              });
				              $("#zhuanjia").click(function(){
				                $(location).attr('href', 'scienic_expertsshow11');
				              });
				              $("#allNeed").click(function(){
				                $(location).attr('href', 'new_file14');
				              });
				              $("#nanti").click(function(){
				                $(location).attr('href', 'new_file15');
				              });
				               $("#seaEquipment").click(function(){
				                $(location).attr('href', 'new_file20');
				              });
				              })
		</script>
    <script>
        $(function () {
            $('#front_header .header_wrap .right').hover(function () {
                $('#front_header .header_wrap .top_r .main .icon img').attr('src', 'images/icon2.png');
                $('#front_header .header_wrap .top_r .out').show();
            }, function () {
                $('#front_header .header_wrap .top_r .main .icon img').attr('src', 'images/icon1.png');
                $('#front_header .header_wrap .top_r .out').hide();
            });
            $('#sorts').on('mouseover', 'li', function () {
                $(this).find('span img').attr('src', 'images/icon4.png');
            });
            $('#sorts').on('mouseout', 'li', function () {
                $(this).find('span img').attr('src', 'images/icon5.png');
            });

            $('.mainList .content .left li').click(function () {
                $(this).addClass('selected').siblings().removeClass('selected');
            });
            $('.mainList .content .search .item .right ul li').click(function () {
                $(this).addClass('selected').siblings().removeClass('selected');
            });
        });
        
        
        
    </script>
    
   
    
</head>
<!-- head end-->
<!-- body -->

<body style="background: #fff">
		<%@include file="/head.jsp"%>


        <!-- <div class="container">
            <div class="fl">
                <a href="index.jsp">
                    <span style="color: rgb(255,138,0);font-size: 34px;">科研成果共享平台</span>
                </a>
            </div>
            <div class="fr">
                <form action="#" id="header_form">
                    <input type="text" name="name" placeholder="关键词">
                    <input type="text" name="limit" hidden="hidden" value="10" placeholder="关键词">
                    <input type="text" name="offset" hidden="hidden" value="0" placeholder="关键词">
                    <button type="button" id="search-btn"><img src="images/searchIcon.png" />
                    	搜索</button>
                    <!<a class="sun_form_a" href="#">发布成果</a>-->
                    <!--<img src="images/search.png" />-->
                    <!-- <select class="img">
								<option>科研成果</option>

</select> -->
               <!--  </form>
            </div>
           
                <div>
                    
                </div>
          
        </div> -->

    <div class="logo-search-btn">
    <div class="logo-search">
    <!--left--> 
    <div class="header-logo header-logo-change fl">
    <h1><a href="index.jsp">科研成果共享平台</a></h1>
    </div>
    <!--right-->


    <!--btn-->

    </div>
    </div>
    <div class="clear"></div>
    
    	
    	
    <!---->
	<div class="clear"></div>
	<div class="navBar-nav">
	<nav>
	<ul>
	<li  style="padding:0"><a href="index.jsp" id="shouye" class="selected">科研成果</a></li>
	<li><a href="new_file7" id="myself">科研需求展示</a></li>
	<li><a href="scienic_expertsshow11" id="zhuanjia">专家展示</a></li>
	<li class="active-one"><a href="new_file14" id="allNeed">交易大厅</a></li>
	<li><a href="new_file15" id="nanti">难题发布</a></li>
	</ul>
	</nav>
	</div>
	<div class="clear"></div>

    <!-- header end-->
    <div class="mainList" style="background: #fff">
        
        <div class="content">
          <ul class="sun_main">
          	<li class="main_table">
          		<div class="table_img">
          			<img src="image/sy_jydt.png" />
          			<div class="table_title">
          				|&nbsp;&nbsp;需求交易大厅
          			</div>
          		</div>
          		<div class="main_title" style="background: url(image/sy_jydt_k.png) center; text-align: center; background-size: cover;">
          			    <div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6 change-item6">状态</div>
          		</div>
          		<div class="scroll_div">
          			<ul class="scroll_ul" id="needallList">
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6 change-item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6 change-item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6 change-item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6 change-item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6 change-item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6 change-item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6 change-item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6 change-item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6 change-item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6 change-item6">状态</div>
          				</li>
          				
          			</ul>
          		</div>
          	</li>
          	<li></li>
          </ul>   
        
        
        </div>
         <div class="content">
          <ul class="sun_main">
          	<li class="main_table">
          		<div class="table_img">
          			<img src="image/sy_jydt.png" />
          			<div class="table_title">
          				|&nbsp;&nbsp;成果交易大厅
          			</div>
          		</div>
          		<div class="main_title" style="background: url(image/sy_jydt_k.png) center; text-align: center; background-size: cover;">
          			    <div class="item1">名称</div>
						
						<div class="item3">成果简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6">状态</div>
          		</div>
          		<div class="scroll_div">
          			<ul class="scroll_ul" id="dealallList">
          				<li id="hahaxixi">
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6">状态</div>
          				</li>
          				<li>
          					<div class="item1">名称</div>
						
						<div class="item3">需求简介</div>
						<div class="item4">行业类别</div>
						<div class="item5">预算</div>
						<div class="item6">状态</div>
          				</li>
          				
          			</ul>
          		</div>
          	</li>
          	<li></li>
          </ul>   
        
        
        </div>
        <div class="pageButton"></div>
        <!--找个分页插件插入-->
	</div>

    <%@include file="/footer.jsp"%>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/scroll.js"></script>
	<style>
	.scroll_div {
	height: 400px;
	overflow: hidden
	}

	


	.scroll_ul :hover{
	background:#ff8a00;
	display: block;
	color:#fff
	}

	.changColor{
	background:#ff8a00;
	color:#fff !important;
	}



	</style>
	<script type="text/javascript">
	$(document).ready(function(){
	$('.scroll_div li:even').addClass('lieven');
	})
	$(function(){
	$("div.scroll_div").myScroll({
	speed:40, //
	rowHeight:68 //
	});
	});


	$(function(){
	$('#hahaxixi').hover(function() {
	$('#hahaxixi').addClass('changColor');
	}, function() {
	$('#hahaxixi').removeClass('over').addClass('out');
	});
	});


	</script>
    
</body>


<!-- body end-->