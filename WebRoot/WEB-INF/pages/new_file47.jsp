
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>

<head>

<meta http-equiv="X-U
A-Compatible" content="IE=edge,chrome=1">
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>仪器共享</title>

<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">

<meta name="description"
	content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
         
">
<link rel="stylesheet" href="new_css/header_footer.css?t=1">
<link rel="stylesheet" href="new_css/productDetail.css">
<link rel="stylesheet" href="css/new_style.css">
<link rel="stylesheet" href="css/common.css">

<!--<link rel="stylesheet" href="js/layui/css/layui.css" media="all">-->
<style type="text/css">
.mainList .content .productInfo .right {
	float: left;
	height: 100%;
	padding-left: 25px;
	width: 100%;
}

.mainList .content .productInfo .right .row9 {
	overflow: hidden;
	margin-top: 50px;
	text-align: center;
}

.mainList .content .productInfo .right .row9 .btn1 {
	display: inline-block;
}

.mainList .content .productInfo .right .row9 .btn {
	float: none;
}

.mainList .content .productInfo {
	width: 100%;
	border: 1px solid #d3d3d3;
	padding: 0px;
	padding-top: 30px;
	height: 446px;
	overflow: hidden;
}
</style>
	<style>

	*{
	margin: 0px;
	padding: 0px;
	border: 0px;

	}

	.clear{
	clear: both;
	}
	.review{
	width:1198px;
	height:500px;
	border: 1px solid #cccccc;
	margin: 20px auto;
	}
	.review-title{
	width:1198px;
	height:54px;
	background:rgba(244,244,244,1);
	}

	.review-title h4{

	font-size:18px;
	font-family:MicrosoftYaHei;
	color:rgba(32,32,32,1);
	line-height:54px;
	padding-left: 20px;
	}
	.review .review-dl{
	width: 96%;
	margin: 0px auto;
	}
	.review .review-dl dt{
	font-size: 18px;
	color: #202020;
	height: 57px;

	line-height: 57px;
	border-bottom: 1px solid #cccccc;
	}
	.review .review-dl dt span{
	width: 250px;
	text-align: right;
	float: right;
	<%--position: absolute;--%>
	}
	.review .review-dl dd{
	color: #707070;
	height: 40px;
	line-height: 40px;
	font-size: 18px;
	padding: 5px 0px 5px 10px;
	}

	.review .review-dl dd span{
	width:400px;
	text-align: right;
	float: right;
	}

	</style>
<script src="new_js/jquery-1.11.3.js"></script>
<script>
	$(function() {
		$('#front_header .header_wrap .right').hover(function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
			$('#front_header .header_wrap .right .out').show();
		}, function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
			$('#front_header .header_wrap .right .out').hide();
		});
		$('#sorts').on('mouseover', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon4.png');
		});
		$('#sorts').on('mouseout', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon5.png');
		});

		$('.mainList .content .left li').click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
		});



		//--------
		$(".tab_span1").click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
			$("#tabView1").css("display", "block");
			$("#tabView2").css("display", "none");
			$("#tabView3").css("display", "none");
		});


		$(".tab_span2").click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
			$("#tabView2").css("display", "block");
			$("#tabView1").css("display", "none");
			$("#tabView3").css("display", "none");
		});

		$(".tab_span3").click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
			$("#tabView3").css("display", "block");
			$("#tabView2").css("display", "none");
			$("#tabView1").css("display", "none");
		});
	});
</script>
</head>
<body style="background:#fff;">
	<%@include file="/head.jsp"%>
	<div class="" id="nav_top">
		<div class="container">
			<div class="fl">
				<div class="logo ">
					<a class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>
				</div>
				<!--<div class="web_name">
						<img src="images/icon3.png" />
						<p>yq.shang.com</p>
					</div>-->
			</div>
		</div>
	</div>
	<div class="mainList">
		<div class="bNav">
			您的位置：首页》需求列表》<span>需求详情</span>
		</div>
		<div class="content">
			<div class='productInfo'>
				<div class="content_title">
					<a>技术难题解决</a>&nbsp;&nbsp;&nbsp;&nbsp; <img src="image/xqxq_yjj.png" />
					<span style="color: rgb(255,29,145);" id="in0">需求已解决</span>
				</div>
				<div class="right">
					<div class="row row1" id="in1">全功能紫外-可见-近红外荧光光谱仪</div>
					<div class="row row3">
						类型：<span id="in2">23658</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行业类型：<span
							id="in3">23658</span>
					</div>
					<br />
					<div class="row">
						需求简介：<span id="in4">北京</span>
					</div>
					<br />
					<div class="row">
						地址：<span id="in5">综合</span>
					</div>
					<br />
					<div class="row">
						预算：<span style="color: rgb(255,138,0);" id="in6">50-100</span>&nbsp;&nbsp;
					</div>
				</div>
			</div>

	<%--评审--%>
			<div class="review">
				<!--评审标题-->
				<div class="review-title">
					<h4>评审意见</h4>
				</div>
				<dl class="review-dl">
					<dt>
						仪器特别好用，服务态度也很好 仪器特别好用，服务态度也很好仪器特别好用 <span>一日***记忆（个人服务商）</span>
					</dt>
					<div class="clear"></div>
					<dd>
						仪器特别好用，服务态度也很好 服务态度也很好1 <span>李思思（专家）</span>
					</dd>
					<div class="clear"></div>
					<dd>
						仪器特别好用，服务态度也很好 服务态度也很好2 <span>李思思（专家）</span>
					</dd>
					<div class="clear"></div>
				</dl>
			</div>



			<%--评审--%>

		</div>
		<div class="pageButton"></div>
		<!--找个分页插件插入-->
	</div>

	<%@include file="/footer.jsp"%>
	<script src="js/layui/layui.js"></script>
	<script>
		layui.use('carousel', function() {
			var carousel = layui.carousel;
			//建造实例
			carousel.render({
				elem : '#test1',
				width : '100%', //设置容器宽度
				arrow : 'always' //始终显示箭头
			//,anim: 'updown' //切换动画方式
			});
		});
	</script>
	<script>
			$(document).ready(function(){
				load()
				techT()
			});
			
			function load(){
				$.ajax({
					url:Server + "/res/need/detail",
					data:{
						"offset": 0,
						"limit": 10,
						"needId":${param.needId},
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var m = data.rows;
						var Time = ( m[0].needCloseTime).substring(0,(m[0].needCloseTime).indexOf(' '));
						var Timel =( m[0].needUpTime).substring(0,( m[0].needUpTime).indexOf(' '));
						var state = "";
						var state1 = "";
						if(m[0].ntypeId = 0){
							state = "技术需求";
						}else if(m[0].ntypeId = 1){
							state = "合作需求";
						}else if(m[0].ntypeId = 2){
							state = "人才需求";
						}
						if(m[0].ntradeId = 0){
							state1 = "需求未解决";
						}else if(m[0].ntradeId = 1){
							state1 = "需求已解决";
						}
						
						$("#in0").text(state1);
						$("#in1").text(m[0].needName);
						$("#in2").text(state);
						$("#in3").text(m[0].tTrade.tradeName);
						$("#in4").text(m[0].needExplain);
						$("#in5").text(m[0].address);
						$("#in6").text(m[0].needBudget);
					},
				});
			}
			
			function techT(){
				$.ajax({
					url:Server + "/res/need/tech",
					type:"POST",
					dataType:"JSON",
					data:{
						"offset": 0,
						"limit" : 10,
						"needId" : ${param.needId},
						"euId": ${param.euId},
					},
					success : function(data){
						var m = data.rows;
						var html = "";
						var item = "";
						var pattern = '<dt onclick="techS(#2);">#0 <span>#1（个人服务商）</span>	</dt>';
						var pattern1 = '<dd>#3 <span>#4（专家）</span></dd>';
						for(var i = 0; i < m.length; i++){
							item += pattern
								.replace("#0",m[i].techName)
								.replace("#1",m[i].name)
								.replace("#2",m[i].techId)
							var c = m[i].proofreview;
							for(var j = 0;j< c.length;j++){
								item += pattern1
									.replace("#3",c[j].reviewContent)
									.replace("#4",c[j].euId)
							}
							html += item;
						}
						$(".review-dl").html(html);
					},
				});
			}
			
			function indes(){
				$(location).attr("href","index");
			}
			
			function techS(techId){
				$(location).attr("href","techDetail?techId="+techId+"");
			}
		</script>

</body>

</html>