<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<title>成果共享</title>

<meta name="keywords" content="科研成果，生命科学成果，环境检测成果，实验常用设备，分析成果">

<meta name="description"
	content="中国领先的科研成果共享平台，让您快速找到各种类型的科学研究成果。提升闲置成果利用率，产生更大科研价值。涵盖：生命科学成果、环境检测成果、实验常用设备、分析成果、仪表、物性测试、测量/计量成果、在线及过程控制成果。">
<link rel="stylesheet" href="new_css/header_footer.css?t=1">
<link rel="stylesheet" href="new_css/productDetail.css">
<link rel="stylesheet" href="css/new_style.css">

<link rel="stylesheet" href="js/layui/css/layui.css" media="all">
<style type="text/css">
.mainList .content .userEvaluation {
	position: relative;
}

.mainList .content .productInfo .right {
	float: left;
	height: 100%;
	padding-left: 25px;
	width: 100%;
}

.mainList .content .productInfo .right .row9 {
	overflow: hidden;
	margin-top: 50px;
	text-align: center;
}

.mainList .content .productInfo .right .row9 .btn1 {
	display: inline-block;
}

.mainList .content .productInfo .right .row9 .btn {
	float: none;
}

.mainList .content .productInfo {
	width: 100%;
	border: 1px solid #d3d3d3;
	padding: 0px;
	padding-top: 30px;
	height: 240px;
	overflow: hidden;
}

.mainList .content .productInfo .right .row1 {
	margin-bottom: 29px;
}

.mainList .content .productInfo .right .row2 {
	margin-bottom: 39px;
}

.mainList .content .userEvaluation .box {
	overflow-y: scroll;
	max-height: 700px;
}

.mainList .content .userEvaluation .box .tabView li .pic img {
	height: 73px;
	border: 1px solid #d3d3d3;
	border-radius: 50%;
}

.li_input {
	width: 100%;
	height: 45px;
	margin-top: 10px;
	margin-bottom: 10px;
}

.li_input input {
	width: 100%;
    height: 45px;
    border-radius: 4px;
    border: 1px solid #efefef;
}

.bottom_input {
	position: absolute;
	bottom: 0px;
	left: 0px;
	width: 100%;
	padding: 10px 30px;
	/*border: 1px solid rgb(244, 244, 244);*/
	border-top: 3px solid #ccc;
	background-color: white;
}

.bottom_input input {
	width: 85%;
	height: 45px;
	border-radius: 4px;
	padding-left: 10px;
	background-color: rgb(244, 244, 244);
}

#tabView2 {
	padding-bottom: 60px;
}
.use_answer .answer_b {
	margin-right: 34px;
	margin-top: 10px;
	float: none;
	display: inline-block;
	width: 120px;
	height: 48px;
	text-align: center;
	color: rgb(255, 138, 0);
	border: 1px solid #CCCCCC;
	line-height: 48px;
	border-radius: 5px;
}
.replyLike {
    float: right;
    margin-top: -20px;
    padding-right: 40px;
}
</style>
<link rel="stylesheet" href="css/common.css">

<script src="js/jquery-1.11.3.js"></script>
<script>
	$(function() {
		$('ul #cancel_btn').click(function() {
			$(this).parent().parent().parent().children('.li_input').toggle();
			$(this).parent().parent().parent().children('.replay').toggle();

		});

		$('ul #right_a').click(function() {
			$(this).parent().parent().siblings('.li_input').toggle();
			$(this).parent().parent().siblings('.replay').toggle();
		});
		$('#front_header .header_wrap .right').hover(function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
			$('#front_header .header_wrap .right .out').show();
		}, function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
			$('#front_header .header_wrap .right .out').hide();
		});
		$('#sorts').on('mouseover', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon4.png');
		});
		$('#sorts').on('mouseout', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon5.png');
		});

		$('.mainList .content .left li').click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
		});



		//--------
		$(".tab_span1").click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
			$("#tabView1").css("display", "block");
			$("#tabView2").css("display", "none");
			$("#bottom_input").css("display", "none");
		});


		$(".tab_span2").click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
			$("#tabView2").css("display", "block");
			$("#bottom_input").css("display", "block");
			$("#tabView1").css("display", "none");

		});






	});
</script>
</head>
<body style="background:#fff;">

	<%@include file="/head.jsp"%>

	<div class="" id="nav_top">
		<div class="container">
			<div class="fl">
				<div class="logo ">
					<a href="index" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);">科研成果共享平台</a>
				</div>
				<!--<div class="web_name">
						<img src="images/icon3.png" />
						<p>yq.shang.com</p>
					</div>-->
			</div>
		</div>
	</div>




	<div class="mainList">
		<div class="bNav">
			您的位置：
			<a href="new_file15" style="color: #575757">
			需求发布</a> > 
			<span>难题详情</span>
		</div>
		<div class="content">

			<!------------------------->


			<!------------------------->

			<div class='productInfo'>

				<div class="right">
					<div class="row row1" id="o"></div>
					<div class="row row2">
						难题简介：<span id="oo"></span>
					</div>
					<div class="row row3">
						地址：<span id="ooo"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行业类型：<span
							id="oooo"></span>
					</div>
				</div>
			</div>

			<div class="userEvaluation">
				<div class="bottom_input" id="bottom_input" style="display: none;">
					<input type="text" placeholder="写下你的评论" />
					<a class="right_a"  id="btn-a" style="background-color: rgb(255,138,0);color: white;" >发布评论</a>
					<style>
						#btn-a{
    width: 120px;
    height: 40px;
    display: inline-block;
    text-align: center;
    line-height: 40px;
    background-color: rgb(6,185,254) !important;
    color: white;
    font-size: 14px;
    border-radius: 5px;
    margin-left:20px;
}
					</style>
				</div>
				<div class='tab'>
					<span class="tab_span1 selected">详情描述</span><span
						class="tab_span2 " id="count">相关评论()条</span>
				</div>
				<div class="box">
					<div class='tabView sun_tabView ' id="tabView1"
						style="padding: 25px;">
						<h2 style="color: rgb(255,138,0);">
							难题简介
							</h3>
							<br />

							<h3 id="ooooo">
								难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介难题简介
								</h4>
					</div>
					<div class='tabView' id="tabView2" style="display: none;">
						<ul class="hfxx">
							<li>
								<%--<div class="pic">--%>
									<%--<img src="images/productDetailPic.png" /><span>xxxxxxxx</span>&nbsp&nbsp回复&nbsp&nbsp<span>xxxxxxxx</span>--%>
								<%--</div>--%>
								<div class="fontInfo">
									<div class="left">成果特别好用，服务态度也很好
										成果特别好用，服务态度也很好成果特别好用，服务态度也很好服务态度也很好</div>
								</div>
								<div class="fontInfo">
									<div class="right">
										<span style="color: black;margin-right: 40px;"><img
											src="image/fbxq_z.png">&nbsp;&nbsp;赞</span> <span
											style="color: black;" id="right_a"><img
											src="image/fbxq_pl.png">&nbsp;&nbsp;评论</span>
									</div>
								</div>
								<div class="li_input" style="display: none;">
									<input type="text" />
								</div>
								<div class="replay" style="display: none;">
									<!--<div class='left'>回复：<span>谢谢您的支持！</span></div>-->
									<div class='right'>
										<a style="margin-right: 20px;" id="cancel_btn">取消</a> <a
											class="right_a" 
											style="background-color: rgb(255,138,0);color: white;">发布评论</a>
									</div>
								</div>
							</li>


							<!-- <li>
									<div class="fontInfo">
										<div class="left">成果特别好用，服务态度也很好 成果特别好用，服务态度也很好成果特别好用，服务态度也很好服务态度也很好</div>
										<div class="right">一日***记忆<span>（匿名）</span></div>
									</div>
									
									
									<div class="pic">
										<img src="images/productDetailPic.png"/>
										<img src="images/productDetailPic.png"/>
									</div>
									<div class="replay">
										<div class='left'>回复：<span>谢谢您的支持！</span></div>
										<div class='right'><a class="right_a">查看合同文档</a></div>
									</div>
								</li>
								<li>
									<div class="fontInfo">
										<div class="left">成果特别好用，服务态度也很好 成果特别好用，服务态度也很好成果特别好用，服务态度也很好服务态度也很好</div>
										<div class="right">一日***记忆<span>（匿名）</span></div>
									</div>
									
									<div class="pic">
										<img src="images/productDetailPic.png"/>
										<img src="images/productDetailPic.png"/>
									</div>
									<div class="replay">
										<div class='left'>回复：<span>谢谢您的支持！</span></div>
										<div class='right'><a class="right_a">查看合同文档</a></div>
									</div>
								</li>
							 -->
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="pageButton"></div>
		<!--找个分页插件插入-->
	</div>



	<%@include file="/footer.jsp"%>
	<script src="js/layui/layui.js"></script>


	<script>
		layui.use('carousel', function() {
			var carousel = layui.carousel;
			//建造实例
			carousel.render({
				elem : '#test1',
				width : '100%', //设置容器宽度
				arrow : 'always' //始终显示箭头
			//,anim: 'updown' //切换动画方式
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			details()
			comment()
		});
	</script>
	<script>
	   	function details(){
	   		$.ajax({
			url:Server + "/problem_select/detalis",
			data:{
				"needId":${param.needId}
			},
			type : "POST",
			dataType : "json",
			success : function(data){
	  			$("#o").html(data[0].needName);
	  			$("#oo").text(data[0].needExplain);
	  			if(data[0].address == null){
	  				var address = "未知";
	  			}else{
	  				var address = data[0].address;
	  			}
	  			$("#ooo").html(address);
	  			$("#oooo").text(data[0].tTrade.tradeName);
	  			$("#ooooo").text(data[0].needDetails);
	 			}
	   		});
	   	}
	   	
	   	function comment(){
	   		$.ajax({
	   			url:Server + "/problem_select/problemList",
	   			data:{
	   				"needId":${param.needId}
	   			},
	   			type : "POST",
				dataType : "json",
	   			success : function(data){
	   				var html = "";
	   				var pattern = '';
	   				var pattern1 = '<li>\
									<div class="pic">\
										 <span>#0</span>&nbsp&nbsp回复&nbsp&nbsp<span>#4</span>\
									</div>';
					var pattern2 = '<li>\
									<div class="pic">\
										<span>#0</span>\
									</div>';
					var pattern3 = '<div class="fontInfo">\
										<div class="left">#2</div>\
									</div>\
									<div class="fontInfo">\
										<div class="right">\
											<span class="like" val="#6" style="color: black;margin-right: 40px;"><img src="image/fbxq_z.png">&nbsp;&nbsp;赞(#3)</span>\
											<span style="color: black;" id="right_a" val="#7"><img src="image/fbxq_pl.png">&nbsp;&nbsp;评论<font>#8</font></span>\
										</div>\
									</div>\
									<div style="display: none;"><ul style="background-color: #f4f4f4;padding-left: 15px;"></ul></div>\
									<div class="li_input" style="display: none;">\
										<input type="text" />\
									</div>\
									<div class="replay" style="display: none;">\
										<div class="right">\
											<a style="margin-right: 20px;" id="cancel_btn">取消</a>\
											<a class="right_a" ref="#5" style="background-color: rgb(255,138,0);color: white;">发布评论</a></div>\
									</div>\
								</li>';
					var pattern4 = '<div class="fontInfo">\
									<div class="left">#2</div>\
								</div>\
								<div class="fontInfo">\
									<div class="right">\
										<span class="like" val="#6" style="color: black;margin-right: 40px;">\
											<img src="image/fbxq_z.png">&nbsp;&nbsp;赞(#3)\
										</span>\
										<span style="color: black;" id="right_a" val="#7"><img src="image/fbxq_pl.png">&nbsp;&nbsp;评论<font>#8</font></span>\
									</div>\
								</div>\
								<div style="display: none;"><ul style="background-color: #f4f4f4;padding-left: 15px;"></ul></div>\
								<div class="use_answer">\
									<a class="answer_b" val="#7" >已采纳</a>\
								</div>\
								<div class="li_input" style="display: none;">\
									<input type="text" />\
								</div>\
							</li>';
					var pattern5 = '<div class="fontInfo">\
									<div class="left">#2</div>\
								</div>\
								<div class="fontInfo">\
									<div class="right">\
										<span class="like" val="#6" style="color: black;margin-right: 40px;">\
											<img src="image/fbxq_z.png">&nbsp;&nbsp;赞(#3)\
										</span>\
										<span style="color: black;" id="right_a" val="#7"><img src="image/fbxq_pl.png">&nbsp;&nbsp;评论<font>#8</font></span>\
									</div>\
								</div>\
								<div style="display: none;"><ul style="background-color: #f4f4f4;padding-left: 15px;"></ul></div>\
								<div class="li_input" style="display: none;">\
									<input type="text" />\
								</div>\
							</li>';
					var isExist;
					var count=0;
					for(var i = 0; i < data.length; i++){
						var list = data[i];
						if(list.spc.pcPid==0){
							count=count+1;
						}
						//alert(list.spc.problemStatus);
						if(list.spc.problemStatus==1){
							isExist=1;
							$("#bottom_input").remove();
						}
					}
					$("#count").html("全部评论("+count+"条)");
					for(var i = 0; i < data.length; i++){
						var item = "";
						var list = data[i];
						var replyCount=0;
						var replyCountStr="";
						for(var j = 0; j < data.length; j++){
							if(list.spc.pcId==data[j].spc.pcPid){
								replyCount=replyCount+1;
							}
							if(replyCount>0){
								replyCountStr="("+replyCount+")";
							}
						}
						
						if(isExist==1){
							if(list.spc.pcPid == 0&&list.spc.problemStatus!=1){
								pattern = pattern2 + pattern5;
								item = item + pattern
								.replace("#5", list.spc.pcId)
								.replace("#6", list.spc.pcId)
								.replace("#7", list.spc.pcId)
								.replace("#0", list.unit)
								.replace("#1", list.head)
								.replace("#2", list.spc.pcContent)
								.replace("#3", list.spc.pcLike)
								.replace("#8", replyCountStr),
							html += item;
							}else if(list.spc.pcPid == 0&&list.spc.problemStatus==1){
								pattern = pattern2 + pattern4;
								item = item + pattern
								.replace("#5", list.spc.pcId)
								.replace("#6", list.spc.pcId)
								.replace("#7", list.spc.pcId)
								.replace("#0", list.unit)
								.replace("#1", list.head)
								.replace("#2", list.spc.pcContent)
								.replace("#3", list.spc.pcLike)
								.replace("#8", replyCountStr),
							html= item+html;
							}
							/* else if(list.spc.pcPid != 0&&list.spc.problemStatus==1){
								pattern = pattern1 + pattern4
								item = item + pattern
								.replace("#0", list.unit)
								.replace("#1", list.head)
								.replace("#2", list.spc.pcContent)
								.replace("#3", list.spc.pcLike)
								.replace("#4", list.spc.pcPid)
								.replace("#5", list.spc.pcId)
								.replace("#6", list.spc.pcId),
							html += item; 
							}*/
							/* else if(list.spc.pcPid != 0&&list.spc.problemStatus!=1){
								pattern = pattern1 + pattern5
								item = item + pattern
								.replace("#0", list.unit)
								.replace("#1", list.head)
								.replace("#2", list.spc.pcContent)
								.replace("#3", list.spc.pcLike)
								.replace("#4", list.spc.pcPid)
								.replace("#5", list.spc.pcId)
								.replace("#6", list.spc.pcId),
							html += item;
							} */
						}else{
							if(list.spc.pcPid == 0){
							pattern = pattern2 + pattern3;
							item = item + pattern
								.replace("#5", list.spc.pcId)
								.replace("#6", list.spc.pcId)
								.replace("#7", list.spc.pcId)
								.replace("#0", list.unit)
								.replace("#1", list.head)
								.replace("#2", list.spc.pcContent)
								.replace("#3", list.spc.pcLike)
								.replace("#8", replyCountStr),
							html += item;
						}
						/* else if(list.spc.pcPid != 0){
							pattern = pattern1 + pattern3
							item = item + pattern
								.replace("#0", list.unit)
								.replace("#1", list.head)
								.replace("#2", list.spc.pcContent)
								.replace("#3", list.spc.pcLike)
								.replace("#4", list.spc.pcPid)
								.replace("#5", list.spc.pcId)
								.replace("#6", list.spc.pcId),
							html += item;
						} */
						}
						
					}
					$(".hfxx").html(html);
					$('ul #right_a').click(function() {
						$(this).parent().parent().siblings('.li_input').toggle();
						$(this).parent().parent().siblings('.replay').toggle();
						$(this).parent().parent().next().toggle();
						//$(this).attr("val") 主回复id
						var html="";
						for(var i = 0; i < data.length; i++){
							var likeDiv = '<div class="replyLike" val='+data[i].spc.pcId+'>\
										<span>\
											<img src="image/fbxq_z.png">&nbsp;&nbsp;赞('+data[i].spc.pcLike+')\
										</span>\
									</div>';
							if($(this).attr("val")==data[i].spc.pcPid){
								html=html+"<li><div>"+data[i].unit+":</div><div>"+data[i].spc.pcContent+"</div>"+likeDiv+"</li>";
								
							}
						}
						$(this).parent().parent().next().children('ul').html(html);
						$(".replyLike").click(function(){
						likeComment($(this).attr("val"));
						//alert($(this).attr("val"));
						var likeVal=$(this).text().trim();
						likeVal=likeVal.substring(0, likeVal.length-1);
						likeVal=parseInt(likeVal.substr(2))+1;
						$(this).html("<img src='image/fbxq_z.png'>&nbsp;&nbsp;赞("+likeVal+")");
						});
						if(isExist==1){
							$('div .li_input').remove();
						} 
					});
					$('ul #cancel_btn').click(function() {
						$(this).parent().parent().parent().children('.li_input').toggle();
						$(this).parent().parent().parent().children('.replay').toggle();
						$(this).parent().parent().prev().prev().toggle();
					}); 
					 $(".right_a").click(function(){
						var pcId=$(this).attr("ref");
						if(pcId>=0){
							addComment($(this).attr("ref"),$(this).parent().parent().prev().children('input').val());
						}else if(pcId===undefined){
							addComment(0,$(this).prev().val());
						}	
					});
					$(".like").click(function(){
						likeComment($(this).attr("val"));
						var likeVal=$(this).text().trim();
						likeVal=likeVal.substring(0, likeVal.length-1);
						likeVal=parseInt(likeVal.substr(2))+1;
						$(this).html("<img src='image/fbxq_z.png'>&nbsp;&nbsp;赞("+likeVal+")");
					});
					
					function addComment(pcId,content){
						var type=$("#usertypeoflogin").val();
						var token=$("#token").val();
						var uid=$("#useridoflogin").val();
						if(content==""){
							alert("评论内容不能为空");
							return;
						}
					  $.ajax({
							url:Server + "/problem_select/addComment",
							//url:"http://localhost/problem_select/addComment",
							data:{
								"pcId":pcId,
								"type":type,
								"token":token,
								"uid":uid,
								"content":content,
								"needId":${param.needId}
							},
							type : "POST",
							//dataType : "json",
							success : function(data){
	  							alert(data);
	  							if(pcId==0){
	  								window.location.reload();
	  								$("#count").addClass('selected').siblings().removeClass('selected');
	  							}else{
	  								comment();
	  							}
	 						}
	   					}); 
					}
					
					function likeComment(pcId){
					  $.ajax({
							url:Server + "/problem_select/likeComment",
							data:{
								"pcId":pcId
							},
							type : "POST",
							//dataType : "json",
							success : function(data){
	  							alert(data);
	  							//comment();
	 						}
	   					}); 
					}
					
				},
	   		});
	   	};
	   	
	   	
	   	
	   	
</script>
	<style>
	.mainList .content .userEvaluation .box .tabView li {
		width: 99%;
	}
	</style>

</body>

</html>