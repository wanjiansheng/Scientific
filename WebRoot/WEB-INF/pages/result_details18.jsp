<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

	<head>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<title>成果共享</title>

		<meta name="keywords" content="科研成果，生命科学成果，环境检测成果，实验常用设备，分析成果">

		<meta name="description" content="中国领先的科研成果共享平台，让您快速找到各种类型的科学研究成果。提升闲置成果利用率，产生更大科研价值。涵盖：生命科学成果、环境检测成果、实验常用设备、分析成果、仪表、物性测试、测量/计量成果、在线及过程控制成果。
         
">
		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/productDetail.css">
		<link rel="stylesheet" href="css/new_style.css">
		<link rel="stylesheet" href="css/bootstrap-grid.min.css">
		<link rel="stylesheet" href="new_css/zoomify.min.css">
		<link rel="stylesheet" href="new_css/style.css">
		
		<link rel="stylesheet" href="js/layui/css/layui.css" media="all">
		<link rel="stylesheet" href="css/common.css">
		<style type="text/css">
			
			div#nav_top > div.container > div.fl{
				width: 340px;
    padding-top: 50px;
    margin-left: -232px;
			}
		</style>
		<script src="js/jquery-1.11.3.js"></script>
		<script src="js/zoomify.min.js"></script>
		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$(this).addClass('selected').siblings().removeClass('selected');
				});
				
				
				
				//--------
				$(".tab_span1").click(function(){
					$(this).addClass('selected').siblings().removeClass('selected');
		       $("#tabView1").css("display","block");
		       $("#tabView2").css("display","none");
				});
				
				
				$(".tab_span2").click(function(){
					$(this).addClass('selected').siblings().removeClass('selected');
				    $("#tabView2").css("display","block");
				    $("#tabView1").css("display","none");
				});
				
				//点击图片放大
				
				
				
			});
		</script>
	</head>
	<body style="background:#fff;">
	
				<div class="wj_ma modal2" id="D" style="display: none;">
		        	<div class="pos">
		        	   <div class="pos_title">联系方式</div>
		        	   <div class="middle_content">
		        	   	   <div class="row">
		        	   	   	<label>姓名</label><input type="text" value="" id="in10" disabled="disabled"/>
		        	   	   </div>
		        	   	   <div class="row">
		        	   	   	<label>电话</label><input type="text" value="" id="in11" disabled="disabled"/>
		        	   	   </div>
		        	   	   <div class="row">
		        	   	   	<label>地址</label><input type="text" value="" id="in12" disabled="disabled"/>
		        	   	   </div>
		        	   </div>
		        	   <div class="pos_buttom" id="pos_buttom" >确定</div>
		        	</div>
		        </div>
				<div class="wj_ma modal3" id="G" style="display: none;">
		        	<div class="pos" style="position:relative; top:36% ">
		        	<div style="text-align: center;
				    top: 50%;
				    justify-content: center;
				    height: 260px;
				    line-height: 260px;
				    font-size: 21px;">
		        	<p id ="goumaipbiaoqian" style="font-siz:25px !important" ></p>
		        	</div>
		        	   <div class="pos_buttom" id="pos_buttomg"  style="position:absolute;bottom:0;">确定</div>
		        	</div>
		        </div>
		        
		        <script>
		        	$(document).ready(function(){
		        		$.ajax({
		        			url:Server + "/fon/infodetailsofuserphone",
	            			type:"POST",
	            			dataType:"JSON",
	            			data:{
	            				id:${param.id}
	            			},
	            			success: function(data){
	            				$("#in10").val(data[0].infousername);
	            				$("#in11").val(data[0].infophone);
	            				$("#in12").val(data[0].infoaddress);
	            			}
		        		});
		        	})
		        </script>
	<%@include file="/head.jsp"%>
		
		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
	<a href="index.jsp" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>

	</div>
					<!--<div class="web_name">
						<img src="images/icon3.png" />
						<p>yq.shang.com</p>
					</div>-->
				</div>
			</div>
		</div>
		

		
		
		<div class="mainList">
			<div class="bNav">您的位置：<a href="index">首页</a> > <span id="infonamew" >全功能紫外-可见-近红外荧光光谱仪</span></div>
			<div class="content">
				<div class='productInfo'> 
					<div class="left">
						<div class="layui-carousel" id="test1">
						  <div carousel-item>
						    <img id="imgofinfo" width="99%"></img>
						  </div>
						</div>
						<!--轮播图插件-->
					</div>


	<%--<style type="text/css">--%>
	<%--#checkboxDiv {margin: 20px auto;}--%>
	<%--#checkboxDiv .input_check {position: absolute;width: 20px;height: 20px;visibility: hidden;}--%>
	<%--#checkboxDiv span {position: relative;}--%>
	<%--#checkboxDiv .input_check+label {display: inline-block;width: 20px;height: 20px;background: url('images/icon36.png') no-repeat;background-position: -31px -3px;border: 1px solid skyblue;}--%>
	<%--#checkboxDiv .input_check:checked+label {background-position: 0px 0px;}--%>
	<%--</style>--%>
					<div class="right">
					<form method="post" enctype="multipart/form-data" id="byResult">
						<input type="hidden" name="resultId" id="infoid"></input>
						<input type="hidden" name="unitname" id="rbuyUid"></input>
						<input type="hidden" name="rbuyUtype" id="rbuyUtype"></input>
						<div class="row row1" id="infoname" >全功能紫外-可见-近红外荧光光谱仪</div>
						
						<div class="row row3"  >技术成熟度：<span id="resultMaturity" >23658</span></div>
						<div class="row">技术水平：<span id=resultTechlevel >北京</span></div>
						<div class="row">获奖情况：<span id=awards >暂无</span></div><!--美国HP公司-->
						<div class="row">所属单位：<span id="unit" >暂无</span></div><!--化学分析成果》电化学成果》PH技、酸度计-->
						<div class="row">知识产权编号：<span id="ipNumber" >综合</span></div>
						<div class="row" id="checkboxDiv">支付方式：<span>完全转让：</span>  <input type="checkbox" class="input_check" name="rbuyPayway" value="0" /> <span id="pay1" style="color: rgb(255,138,0);">50-100万</span>&nbsp;&nbsp;
							<span>许可转让：</span>   <input type="checkbox" class="input_check" name="rbuyPayway" value="1" />  <span id="pay2" style="color: rgb(255,138,0);">50-100万</span>&nbsp;&nbsp;
							<span>技术入股：</span>   <input type="checkbox" class="input_check" name="rbuyPayway" value="2" />    <span id="pay3" style="color: rgb(255,138,0);">50-100万</span>&nbsp;&nbsp;
						</div>
					</form>
	<script type="text/javascript">
	$(document).ready(function(){
	$(function(){
	$("#checkboxDiv").find(":checkbox").each(function(){
	$(this).click(function(){
	if($(this).is(':checked')){
	$(this).attr('checked',true).siblings().attr('checked',false);
	}else{
	$(this).attr('checked',false).siblings().attr('checked',false);
	}
	});
	});
	});
	});

	</script>
						<div class="row row9">
							<div class="btn"><a id='sun_btn' style="background-color: rgb(6,185,254);margin-right: 20px;">联系方式</a></div>
							<div class="btn"><a id="goumai">购买成果</a></div>
						<!--	<div class="price">价格：<span>100元/样品</span></div>-->
						</div>
					</div>
				</div>
				
				
				<div class="userEvaluation">
					<div class='tab'><span class="tab_span1 selected">详情简介</span><span onclick="infodetail()" class="tab_span2 ">用户评价</span></div>
					<div class="box" id="infodetailsofdiv" >
						<div class='tabView sun_tabView '  id="tabView1">
				<!------------------------>
				
				
				<ul>
								<li>
									<div style="border-left: 2px solid rgb(255,138,0); padding-left: 10px;"><p>成果介绍:</p></div>
									<div style="padding-left: 10px;"><p id="resultDesc" ></p></div>

								</li>
								 
							</ul>
							
				<!------------------------>
				
	            <script>
	            	$(document).ready(function(){
	            		$.ajax({
	            			url:Server + "/fon/infodetails",
	            			type:"POST",
	            			dataType:"JSON",
	            			data:{
	            				id:${param.id}
	            			},
	            			success: function(data){
	            			console.log(JSON.stringify(data[0].awards))
	            				var chengshudu = "0";
	            				if(data[0].resultMaturity == 0)
									chengshudu = "正在研发";
								if(data[0].resultMaturity == 1)
									chengshudu = "已有样品";
								if(data[0].resultMaturity == 2)
									chengshudu = "通过小试";
								if(data[0].resultMaturity == 3)
									chengshudu = "通过中试";
								if(data[0].resultMaturity == 4)
									chengshudu = "可以量产";
	            			
	            				if(data[0]!= null){
	            					$("#tabView1").html(data[0].resultDetails);
	            				}
	            				$("#infoid").val(data[0].resultId);
	            				/* 珍爱JavaScript，原理jQuery */
	            				document.getElementById("imgofinfo").src=data[0].image[0].rimagePath;
	            				$("#infoname").text(data[0].resultName);
	            				$("#infonamew").text(data[0].resultName);
	            				$("#resultMaturity").text(chengshudu);
	            				$("#resultTechlevel").text(data[0].resultTechlevel);
	            				$("#awards").text(data[0].awards);
	            				$("#unit").text(data[0].unit);
	            				$("#ipNumber").text(data[0].ipNumber);
	            				$("#resultDesc").text(data[0].resultDesc);
	            				$("#pay1").text(data[0].pay[0].rpayRate);
	            				$("#pay2").text(data[0].pay[1].rpayRate);
	            				$("#pay3").text(data[0].pay[2].rpayRate);
	            			}
	            		});
	            	});
	            </script>
	            
	                
				
				<!------------------------>
						</div>
						<div class='tabView' id="tabView2" style="display: none;" >
							<ul>
								<li>
									 <div class="fontInfo">
										<div class="left" id="rpayContetn" >暂无用户评价</div>
										<div class="right" id="rpayUsername" ></span></div>
									</div> 
									<div id="picc" class="pic">
										<!-- <img id="path" class="img-rounded" src="/UPLOAD/image/img_contract/hetong1.jpg" />
										<img class="img-rounded" src="/UPLOAD/image/img_contract/hetong2.jpg" />
										<img class="img-rounded" src="/UPLOAD/image/img_contract/hetong3.jpg" /> -->
									</div>
									<div class="replay">
									<!--<div class='left'>回复：<span>谢谢您的支持！</span></div>-->
									<div id='download ' class='right'>
										
									</div>
								</div>
									
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="pageButton"></div>
			<!--找个分页插件插入-->
		</div>
		<script>
	            	function infodetail(){
	            		$.ajax({
	            			url:Server + "/fon/infodetailsofcomment",
	            			type:"POST",
	            			dataType:"JSON",
	            			async:false,
	            			data:{
	            				id:2,/* $("#infoid").val(), */
	            			},
	            			success: function(data){
	            				console.log(data)
	            				$("#rpayContetn").text(data[0].rpayContetn);
	            				$("#rpayUsername").text(data[0].rpayUsername);
	            				for(var i=0 ; i<data.length;i++){
	            					$("#picc").append("<img id='path' class='img-rounded' src='"+data[i].path+"'/>");
	            				}
	            				
	            				$.ajax({
	    	            			url:Server + "/fon/infodetailDoc",
	    	            			type:"POST",
	    	            			dataType:"JSON",
	    	            			async:false,
	    	            			data:{
	    	            				id:2/* $("#infoid").val(), */
	    	            			},
	    	            			success: function(data){
	    	            				$("#download").append("<a class='right_a' href='"+data[0].path+"' target='_blank'>查看合同文档</a>")
	    	            			}
	    	            		});
	            			}
	            		});
	            		
	            		$('.pic img').zoomify();
	            	}
	            </script>
		<script>
			$('#sun_btn').click(function(event){
					$('.modal2').show();
				});
			$('div[id="D"]').click(function(e){
				var target = e.target;
				$('div[id="D"]').each(function(e){
					if(target.id == 'D') {
						$(this).hide();
					}
				});
			})
			$("#pos_buttom").click(function(){
				$('.modal2').hide();
			})
			
			$('#goumai').click(function(event){
				if ($("#useridoflogin").val() == 'null') {
					alert("操作需要登录，请登录！");
					$(location).attr("href","http://xmsoc.com:20507/OCEAN/toLogin?url="+Server+"/")
				} else {
			
				$.ajax({
					url:Server + "/res/dea/verify",
					data:{
						typeId:$("#usertypeoflogin").val(),
						id:$("#useridoflogin").val(),
						resultId:$("#infoid").val(),
					},
					type:"POST",
					success : function(data){
						if(data == 0){//不是自己发布的成果
							//$('.modal3').show();
							$("#rbuyUid").val($("#useridoflogin").val());
							$("#rbuyUtype").val($("#usertypeoflogin").val());
							if($(":checkbox:checked").size() == 0){
								alert("请选择支付方式")
								return false;
							}else{
								$.ajax({
		        					url:Server + "/fon/byInfodetailsofbuyinfo",//Server + "/fon/byInfodetailsofbuyinfo"
	            					type:"POST",
	            					data:{
	            						uid:$("#useridoflogin").val(),
	            						userType:$("#usertypeoflogin").val(),
	            						resultId:$("#infoid").val()
	            					},
	            					async: false,
	            					success: function(data){
	            						if("验证通过" == data){
	            							buyResult();
	            						}else{
	            							alert(data)
	            						}
	            					},
	            					error:function(data){
	            						alert("失败")
	            					}
		        				});
		        			}
						}else if(data >= 1){
							alert("不能购买自己发布的成果！")
						}
					}
				});
				}
			});
			
			$('div[id="G"]').click(function(e){
				var target = e.target;
				$('div[id="G"]').each(function(e){
					if(target.id == 'D') {
						$(this).hide();
					}
				});
			})
			$("#pos_buttomg").click(function(){
				$('.modal3').hide();
			})
			function buyResult(){
				$.ajax({
					url:Server + "/fon/infodetailsofbuyinfo",//Server + "/fon/byInfodetailsofbuyinfo"
	            			type:"POST",
	            			data:$("#byResult").serialize(),
	            			async: false,
	            			success: function(data){
	            			alert("购买成功")
	            			},
	            			error:function(data){
	            			alert("购买失败")
	            			}
				});
			}
		</script>

		<%@include file="/footer.jsp"%>
		<script src="js/layui/layui.js"></script>
		<script>
layui.use('carousel', function(){
  var carousel = layui.carousel;
  //建造实例
  carousel.render({
    elem: '#test1'
    ,width: '100%' //设置容器宽度
    ,arrow: 'always' //始终显示箭头
    //,anim: 'updown' //切换动画方式
  });
});
</script>	        
	</body>
</html>
