<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

	<head>
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<title>仪器共享</title>
		

		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">

		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。

">
		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/appointmentPay.css">
		<link rel="stylesheet" href="css/common.css">
		<script src="js/jquery-1.11.3.js"></script>
		<script src="method/dwy/zhifu37_3.js"></script>
		<script >
		//load();
		//next();
		$(document).ready(function(){
			load(GetQueryString("id"));
			 //particulars(GetQueryString("needId"));
			 next(GetQueryString("id"));
			//describe(GetQueryString("needId"));
			
			});
			function GetQueryString(name) {
				var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
				var r = window.location.search.substr(1).match(reg);
				if(r != null) return unescape(r[2]);
				return null;
			}
			</script>
		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});
				
				$('.mainList .content .left li').click(function(){
					$(this).addClass('selected').siblings().removeClass('selected');
				});
			});
		</script>

	</head>

	<body style="background:#fff;">

		<%@include file="/head.jsp"%>

		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
	<a href="#this" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>
					</div>
					
				</div>
			</div>
		</div>
		<div class="mainList">
			<!--<div class="bNav">您的位置：首页><span>用户中心</span></div>-->
			<div class="content">
				<div class="box box1">
					
					<div class="font">
						<div class="row">订单编号：<span id="py0" ></span></div>
						<div class="row">
							<div class="left">订单名称：<span id="py1" ></span></div>
						</div>
					</div>
				</div>
			   <form id="signupForm" style="display:none">
                <input name="rdealId" type="hidden" id="7" value="" >
                <input type="text" name="rdealPayStatus" id="rdealPayStatus" value="2"/>
               </form>
				<div class="box box2">
					<div class="inner">
						<div class="title">
							<div class="left">微信支付</div>
							<div class='right'>实付金额：<span id="py2" ></span></div>
						</div>
						<div class="Qrcode">
							<div class="wrap">
							<input style="display:none" name="innumber" id="4"></input>
								<div class="pic"><img onclick="shangchuan()" src="images/erweima.png"/></div>
								<div class="text">请使用微信扫描二维码<br/>以完成支付</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="pageButton"></div><!--找个分页插件插入-->
		</div>

		<%@include file="/footer.jsp"%>

	</body>

</html>