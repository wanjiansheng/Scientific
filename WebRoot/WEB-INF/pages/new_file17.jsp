<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<!-- head -->

<head>
    <link rel="stylesheet" href="css/l_css/header.css">
    <link rel="stylesheet" href="css/l_css/main.css">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="css/jquery.pagination.css" />
    <link rel="stylesheet" href="css/common.css">
	<style>
			* {
				margin: 0;
				padding: 0;
			}
			
			body {
				font-family: "微软雅黑";
				background: #eee;
			}
			
			button {
				display: inline-block;
				padding: 6px 12px;
				font-weight: 400;
				line-height: 1.42857143;
				text-align: center;
				vertical-align: middle;
				cursor: pointer;
				border: 1px solid transparent;
				border-radius: 4px;
				border-color: #28a4c9;
				color: #fff;
				background-color: #5bc0de;
				margin: 20px 20px 0 0;
			}
			
			.box {
				width: 900px;
				/*margin: 100px auto 0;*/
				height: 34px;
			}
			
			.page {
				width: 700px;
			}
			
			.info {
				width: 200px;
				height: 34px;
				line-height: 34px;
			}
			
			.fl {
				float: left;
			}

            input {
    border: 1px solid #efefef;
    border-radius: 5px 0px 0px 5px;
}


    div#nav_top {
    background: url(image/sy_bg.png) no-repeat center;
    height: 175px;
    background-size: cover;
    width: 1200px;
    margin: 0 auto;
    }

    #header_form {
    float: left;
    /* width: 100%; */
    width: 838px;
    margin-top: 68px;
    position: relative;
    margin-left: -151px;
    }



    #header_form input {
    width: 447px;
    height: 46px;
    float: left;
    padding-left: 133px;
    border: 1px solid #f60;
    border-right: none;
    outline: none;
    font-size: 12px;
    border-radius: 0px;
    }

    #header_form img {
    position: absolute;
    top: 50%;
    left: 47px;
    }

    #header_form button {
    float: left;
    /* width: 15.75757576%; */
    height: 46px !important;
    width: 161px !important;
    background: #ff8a00;
    border: 1px solid #ff8a00;
    color: #fff;
    font-size: 14px !important;
    cursor: pointer;
    outline: none;
    top: -11%;
    padding-top: -51px;
    margin-top: 0px;
    margin-left: -8px;
    position: relative;
    border-radius: 0px;
    }
    .sun_form_a {
    display: inline-block;
    width: 156px;
    height: 46px;
    float: right;
    line-height: 46px;
    background-color: rgb(6,185,254);
    color: white;
    /* margin-left: 27px; */
    text-align: center;
    font-size: 18px;
    border-radius: 5px;
    }

    #header_form .img {
    padding-left: 26px;
    width: 116px !important;
    height: 46px !important;
    border-right: 1px solid #ccc;
    line-height: 46px !important;
    }

    button#searchofinfo {
    padding-left: 33px;
    vertical-align: middle;
    text-align: center;
    justify-content: center;
    }

    .mainList .content .search .item .left {
    float: left;
    width: 8%;
    font-size: 16px;
    }

    .look{
    width: 300px;
    height: 100px;
    padding-top: -10px;
    margin-top: -6px;
    }

		</style>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>成果共享</title>
    <meta name="keywords" content="科研成果，生命科学成果，环境检测成果，实验常用设备，分析成果">
    <meta name="description" content="中国领先的科研成果共享平台，让您快速找到各种类型的科学研究成果。提升闲置成果利用率，产生更大科研价值。涵盖：生命科学成果、环境检测成果、实验常用设备、分析成果、仪表、物性测试、测量/计量成果、在线及过程控制成果。

">



    <style type="text/css">
    	.vertical {
    width: 217px;
    background-color: white;
    display: block;
    widows: 10px;
    height: 217px;
    text-align: center;
    line-height: 42px;
    color: #fff;
    font-size: 22px;
}
.vertical_img{
	<%--border: 1px solid rgb(218,218,218);--%>
}
.vertical .vertical_img img{
	width: 180px;
    height: 180px;
}
.mainList .content .list li .active {
    width: 20%;
    border-left: 1px solid #d6d6d6;
    padding-left: 20px;
    padding-right: 20px;
}

button {
    display: inline-block;
    padding: 6px 12px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    vertical-align: middle;
    cursor: pointer;
    border: 1px solid transparent;
    border-radius: 4px;
    border-color: #28a4c9;
    color: #fff;
    background-color: #5bc0de;
    margin: 0px 20px 0 0;
    margin-top: 0px;
    margin-right: 20px;
    margin-bottom: 0px;
    margin-left: 0px;
}
#header_form button {
    float: left;
    width: 15.75757576%;
    height: 36px;
    background: #ff8a00;
    border: 1px solid #ff8a00;
    color: #fff;
    font-size: 16px;
    cursor: pointer;
    outline: none;
    top: -11%;
    padding-top: -51px;
    margin-top: 0px;
    margin-left: -8px;
    position: relative;
}
    </style>
    <script src="js/jquery-1.11.3.js"></script>
    <script>
        $(function () {
            addSelected();
        });
        function addSelected(){
        	$('#front_header .header_wrap .right').hover(function () {
                $('#front_header .header_wrap .top_r .main .icon img').attr('src', 'images/icon2.png');
                $('#front_header .header_wrap .top_r .out').show();
            }, function () {
                $('#front_header .header_wrap .top_r .main .icon img').attr('src', 'images/icon1.png');
                $('#front_header .header_wrap .top_r .out').hide();
            });
            $('#sorts').on('mouseover', 'li', function () {
                $(this).find('span img').attr('src', 'images/icon4.png');
            });
            $('#sorts').on('mouseout', 'li', function () {
                $(this).find('span img').attr('src', 'images/icon5.png');
            });

            $('.mainList .content .left li').click(function () {
                $(this).addClass('selected').siblings().removeClass('selected');
            });
            $('.mainList .content .search .item .right ul li').click(function () {
                $(this).addClass('selected').siblings().removeClass('selected');
            });
        }
    </script>
<style>
    input#key {
    border: 1px solid #efefef;
    border-radius: 5px 0px 0px 5px;
    }
    </style>
</head>
<!-- head end-->
<!-- body -->

<body style="background: #fff">
    <!-- header-->
	<%@include file="/head.jsp"%>
    <div class="nav" id="nav_top">
        <div class="container">
            <div class="fl">
    <p class="look">
                <a href="index">
               	 <!--  -->
                    <span style="color: rgb(255,138,0);font-size: 36px;">科研成果共享平台</span>
                   <%--  <input id="needstring" type="hidden"  value="${param.string}" /> --%>
                </a>
    </p>
            </div>
            <div class="fr">
                <form action="#" id="header_form">
                    <input type="text" name="name" id="likeStringofinof" value="${param.string}" placeholder="关键词" >
                    <input type="text" name="limit" hidden="hidden" value="10" placeholder="关键词">
                    <input type="text" name="offset" hidden="hidden" value="0" placeholder="关键词">
                    <button type="button" id="searchofinfo"><span><img src="images/sy_seach.png"></span>搜索</button>
                    <a class="sun_form_a" onclick="sss()" >发布成果</a>
                    
                    <!--<img src="images/search.png" />-->
                    <p class="img">
								科研成果

					</p>
                </form>
            </div>
                <div>
                    
                </div>
          
        </div>
    </div>
    
    			<script>
					function sss(){
						var upinfourl =  "/Scientific/updata34?usertype="+ 0 +"&userid="+ 123;
						window.location.replace(upinfourl);
					}
				</script>
    
    <!-- header end-->
    <div class="mainList" style="background: #fff">
        <div class="bNav">您的位置：
<a href="index">首页</a> > 
            <span>成果列表</span>
        </div>
        <div class="content">
            <div class="search">
                <div class='item'>
                    <div class="left">类别：</div>
                    <div class="right">
                        <ul id="lb">
                            <li class="selected" val="">不限</li>
                            <!-- <li>食品饮料</li>
                            <li>建筑建材</li>
                            <li>家居用品</li>
                            <li>轻功纺织</li>
                            <li>轻功纺织</li>
                            <li>化学化工</li>
                            <li>新能源</li>
                            <li>机械</li>
                            <li>生命科学成果</li>
                            <li>环保和资源</li>
                            <li>安全防护</li>
                            <li>交通运输</li> -->
                        </ul>
                    </div>
                </div>
                <div class='item'>
                    <div class="left">价格交易：</div>
                    <div class="right">
                        <ul id="priceUl">
                            <li class="selected" val="1">不限</li>
                            <li val="2">面议</li>
                            <li val="3">1-10万</li>
                            <li val="4">10-50万</li>
                            <li val="5">50-100万</li>
                            <li val="6">100-500万</li>
							<li val="7">500-1000万</li>
							<li val="8">1000万以上</li>
                        </ul>
                    </div>
                </div>
                <div class='item'>
                    <div class="left">技术类型：</div>
                    <div class="right">
                        <ul id="tecTypeUl">
                            <li val="" class="selected">不限</li>
                            <li val="1">专利</li>
                            <li val="2">非专利</li>
                        </ul>
                    </div>
                </div>
                <div class='item'>
                    <div class="left">交易方式：</div>
                    <div class="right">
                        <ul id="payModeUl">
                            <li class="selected" val="">不限</li>
                            <li val="1">完全转让</li>
                            <li val="2">许可转让</li>
                            <li val="3">技术入股</li>
                        </ul>
                    </div>
                </div>
                <div class='item'>
                    <div class="left">成熟度：</div>
                    <div class="right">
                        <ul id="maturityUl">
                            <li class="selected" val="">不限</li>
                            <li val="1">正在研发</li>
                            <li val="2">已有样品</li>
                            <li val="3">通过小试</li>
                            <li val="4">通过中试</li>
                            <li val="5">可以量产</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="listsearch_ad">
                <%--<div class="sousuo">--%>

                    <%--<input name="key" id="key" type="text" placeholder="请输入搜索内容" focucmsg="请输入搜索内容" onKeyUp="value=value.replace(/[^\a-\z\A-\Z0-9\u4E00-\u9FA5\ ]/g,'')"--%>
                        <%--onpaste="value=value.replace(/[^\a-\z\A-\Z0-9\u4E00-\u9FA5\ ]/g,'')" oncontextmenu="value=value.replace(/[^\a-\z\A-\Z0-9\u4E00-\u9FA5\ ]/g,'')"--%>
                    <%--/>--%>
                    <%--<a id="skey">搜索</a>--%>

                <%--</div>--%>
                <%--<div class="paixu">--%>
                    <%--<a href="javascript:void(0)" name="orderzixuncount" order="0" class="on">默认</a>--%>
                    <%--<a href="javascript:void(0)" name="orderzixuncount" order="1">人气&nbsp;&uarr;</a>--%>
                <%--</div>--%>
                <script>
                    $(".paixu a").click(function () {
                        $(this).addClass("on");
                        $(this).siblings().removeClass("on");
                    });
                </script>
                <div class="zhikan">
                    <label>
                        <input name="issmallproject" id="issmallproject" type="checkbox" value="1" />海洋装备
                    </label>
                    <%--<label>--%>
                        <%--<input name="ismatureproject" id="ismatureproject" type="checkbox" value="1" />成熟项目--%>
                    <%--</label>--%>
                </div>
            </div>

            <div class="list">
                <ul id="inneedlist" >
                    <li>
                        <div class="vertical" style="padding-left: 20px;">
                            <div class="vertical_img">
                            	<img src="image/dl_bg.png" />
                            </div>
                        </div>
                        <div class="info">
                            <div class="title">一种感应发亮的鞋底的实用新型专利定制</div>
                            <div class="row2">
                                <div class="font">需求简介：
                                    <span>一种感应发亮的鞋底的实用新型专利定制</span>
                                </div>
                                <div class="font">地址：
                                    <span>浙江 温州</span>
                                </div>
                                <!--<div class="font">发布时间：
                                    <span>2018-5-24</span>
                                </div>
                                -->
                            </div>
                            <div class="row3">预算：
                                
                                <span style="color: black;margin-left: 200px;">
                                	等级：
                                </span>
                                
                            </div>
                        </div>

                        <div class="active">
                            <div style="text-align: right;">
                                <span style="color: rgb(255,138,0);font-size: 20px;">面议</span>
                            </div>
                            <div style="text-align: right;">
                                                                                    成熟度：<span style="">正在研发</span>
                            </div>
                            <div style="text-align: right;">
                                                                                     成熟度：<span>正在研发</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="vertical" style="padding-left: 20px;">
                            <div class="vertical_img">
                            	<img src="image/dl_bg.png" />
                            </div>
                        </div>
                        <div class="info">
                            <div class="title">一种感应发亮的鞋底的实用新型专利定制</div>
                            <div class="row2">
                                <div class="font">需求简介：
                                    <span>一种感应发亮的鞋底的实用新型专利定制</span>
                                </div>
                                <div class="font">地址：
                                    <span>浙江 温州</span>
                                </div>
                                <!--<div class="font">发布时间：
                                    <span>2018-5-24</span>
                                </div>
                                -->
                            </div>
                            <div class="row3">预算：
                                
                                <span style="color: black;margin-left: 200px;">
                                	等级：
                                </span>
                                
                            </div>
                        </div>

                        <div class="active">
                            <div style="text-align: right;">
                                <span style="color: rgb(255,138,0);font-size: 20px;">面议</span>
                            </div>
                            <div style="text-align: right;">
                                                                                    成熟度：<span style="">正在研发</span>
                            </div>
                            <div style="text-align: right;">
                                                                                     成熟度：<span>正在研发</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="vertical" style="padding-left: 20px;">
                            <div class="vertical_img">
                            	<img src="image/dl_bg.png" />
                            </div>
                        </div>
                        <div class="info">
                            <div class="title">一种感应发亮的鞋底的实用新型专利定制</div>
                            <div class="row2">
                                <div class="font">需求简介：
                                    <span>一种感应发亮的鞋底的实用新型专利定制</span>
                                </div>
                                <div class="font">地址：
                                    <span>浙江 温州</span>
                                </div>
                                <!--<div class="font">发布时间：
                                    <span>2018-5-24</span>
                                </div>
                                -->
                            </div>
                            <div class="row3">预算：
                                
                                <span style="color: black;margin-left: 200px;">
                                	等级：
                                </span>
                                
                            </div>
                        </div>

                        <div class="active">
                            <div style="text-align: right;">
                                <span style="color: rgb(255,138,0);font-size: 20px;">面议</span>
                            </div>
                            <div style="text-align: right;">
                                                                                    成熟度：<span style="">正在研发</span>
                            </div>
                            <div style="text-align: right;">
                                                                                     成熟度：<span>正在研发</span>
                            </div>
                        </div>
                    </li>
                    
                
                </ul>
            </div>
        </div>
		<div class="box">
			<div id="pagination3" class="page fl"></div>
			<!-- <div class="info fl">
				<p>当前页数：<span id="current3">1</span></p>
			</div> -->
			<input type="text" id="allinfonumber" style="display:none"/>
		</div>
		<script>
			var tradeId="";
    		var rtypeId="";
    		var payMode="";
    		var maturity="";
    		var resultContent=0;
  	  		var priceTag=1;
			
			function getUrlParam(name) {
       				 var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
   				     var r = window.location.search.substr(1).match(reg);  //匹配目标参数
 			    	 if (r != null) return unescape(r[2]); return null; //返回参数值
  					 }
			$(function() {
				name = $("#likeStringofinof").val();
				limit = 10;
				offset = 0;
				if(getUrlParam('tradeId')!=null&&getUrlParam('tradeId')!=""){
				tradeId=getUrlParam('tradeId');
				
			}
				function yemian(){
					//alert("yemian");
					$.ajax({
						url :Server + "/fon/infolist",
						//url :"http://localhost/fon/infolist",
						data : {
							"name":$("#likeStringofinof").val(),
							"limit":limit,
							"offset":offset,
							"tradeId":tradeId,
							"rtypeId":rtypeId,
							"payMode":payMode,
							"maturity":maturity,
							"resultContent":resultContent,
							"priceTag":priceTag
						},
						type : "POST",
						dataType: "JSON",
						success : function(data) {
							$("#pagination3").pagination({
								currentPage: 1,
								totalPage: Math.ceil(Number(data.total/10)),
								isShow: true,
								count: 7,
								homePageText: "首页",
								endPageText: "尾页",
								prevPageText: "<上一页",
								nextPageText: "下一页>",
								callback: function(current) {
									$("#current3").text(current);
									infoLikeListofinfo($("#likeStringofinof").val(),limit,$("#pagination3").pagination("getPage").current-1,tradeId,rtypeId,payMode,maturity,resultContent,priceTag);
								}
							});
						}
					});
				}
				yemian();
				$.ajax({
						url :Server + "/res/need/selectAllType",
						//url :"http://localhost/res/need/selectAllType",
						type : "POST",
						dataType: "JSON",
						success : function(data) {
							for(var i = 0; i < data.length; i++){
								$("#lb").append("<li val="+data[i].tradeId+">"+data[i].tradeName+"</li>")
							}
							addSelected();
							$("#lb li").click(function() {
								//alert($(this).attr("val"));
								tradeId=$(this).attr("val");
								yemian();
								$("#current3").text(1);
								//needlistofsecond($("#likeStringofneed").val(),limit,offset,tradeId,ntypeId,needContent,priceTag);
								infoLikeListofinfo($("#likeStringofneed").val(),limit,offset,tradeId,rtypeId,payMode,maturity,resultContent,priceTag);
							});
							$("#priceUl li").click(function() {
								//alert($(this).attr("val"));
								priceTag=$(this).attr("val");
								yemian();
								$("#current3").text(1);
								//needlistofsecond($("#likeStringofneed").val(),limit,offset,tradeId,ntypeId,needContent,priceTag);
								infoLikeListofinfo($("#likeStringofneed").val(),limit,offset,tradeId,rtypeId,payMode,maturity,resultContent,priceTag);
							});
							$("#tecTypeUl li").click(function() {
								//alert($(this).attr("val"));
								rtypeId=$(this).attr("val");
								yemian();
								$("#current3").text(1);
								//needlistofsecond($("#likeStringofneed").val(),limit,offset,tradeId,ntypeId,needContent,priceTag);
								infoLikeListofinfo($("#likeStringofneed").val(),limit,offset,tradeId,rtypeId,payMode,maturity,resultContent,priceTag);
							});
							$("#payModeUl li").click(function() {
								//alert($(this).attr("val"));
								payMode=$(this).attr("val");
								yemian();
								$("#current3").text(1);
								//needlistofsecond($("#likeStringofneed").val(),limit,offset,tradeId,ntypeId,needContent,priceTag);
								infoLikeListofinfo($("#likeStringofneed").val(),limit,offset,tradeId,rtypeId,payMode,maturity,resultContent,priceTag);
							});
							$("#maturityUl li").click(function() {
								maturity=$(this).attr("val");
								yemian();
								$("#current3").text(1);
								//needlistofsecond($("#likeStringofneed").val(),limit,offset,tradeId,ntypeId,needContent,priceTag);
								infoLikeListofinfo($("#likeStringofneed").val(),limit,offset,tradeId,rtypeId,payMode,maturity,resultContent,priceTag);
							});
							$("#issmallproject").click(function() {
								if($("#issmallproject").is(':checked')==true){
									resultContent=$("#issmallproject").val();
									//alert("tradeId:"+tradeId+" priceTag:"+priceTag+" rtypeId:"+rtypeId+" payMode:"+payMode+" maturity:"+maturity+" resultContent:"+resultContent);
								}else{
									resultContent=0;
								}
								yemian();
								$("#current3").text(1);
								//needlistofsecond($("#likeStringofneed").val(),limit,offset,tradeId,ntypeId,needContent,priceTag);
								infoLikeListofinfo($("#likeStringofneed").val(),limit,offset,tradeId,rtypeId,payMode,maturity,resultContent,priceTag);
							});
						}
					});
			});
			
			$(document).ready(function() {
					
					name = $("#likeStringofinof").val();
					limit = 10;
					offset = 0;
					infoLikeListofinfo(name,limit,offset,tradeId,rtypeId,payMode,maturity,resultContent,priceTag);
					$("#searchofinfo").click(function(){
						$(location).attr('href', 'newfile17?string=' + $("#likeStringofinof").val() + '');
					});
					
			});
			
			function infoLikeListofinfo(name,limit,offset,tradeId,rtypeId,payMode,maturity,resultContent,priceTag){
				//alert("js1");
				$.ajax({
					url :Server + "/fon/infolist",
					//url :"http://localhost/fon/infolist",
					data : {
						"name":name,
						"limit":limit,
						"offset":offset,
						"tradeId":tradeId,
						"rtypeId":rtypeId,
						"payMode":payMode,
						"maturity":maturity,
						"resultContent":resultContent,
						"priceTag":priceTag
					},
					type : "POST",
					dataType: "JSON",
					success : function(data) {
						var html = "";
						var pattern = '<li onclick="infodetail(#88)" >\
			              <div class="vertical" style="padding-left: 20px;">\
			                  <div class="vertical_img">\
			                  	<img src="#0" />\
			                  </div>\
			              </div>\
			              <div class="info">\
			                  <div class="title">#1</div>\
			                  <div class="row2">\
			                      <div class="font">类型：#4<span></span>\
			                      </div>\
			                      <div class="font">交易方式：\
			                          <span>完全转让</span>\
			                      </div>\
			                  </div>\
			                  <div class="row3">\
			                      <!-- <br >#2 -->\
			                  </div>\
			              </div>\
			              <div class="active">\
			                  <div style="text-align: right;">\
			                      <span style="color: rgb(255,138,0);font-size: 20px;">#6</span>\
			                  </div>\
			                  <div style="text-align: right;">\
			                       成熟度：<span style="">#8</span>\
			                  </div>\
			                  <div style="text-align: right;">\
			                       地址：<span>#3</span>\
			                  </div>\
			              </div>\
			          </li>';
						for (var i = 0; i < data.rows.length; i++) {
							var item = "";
							var chengshudu ="";
							var leixing ="";
							if(data.rows[i].puser&&data.rows[i].img){
								if(data.rows[i].resultMaturity == 0)
									chengshudu = "正在研发";
								if(data.rows[i].resultMaturity == 1)
									chengshudu = "已有样品";
								if(data.rows[i].resultMaturity == 2)
									chengshudu = "通过小试";
								if(data.rows[i].resultMaturity == 3)
									chengshudu = "通过中试";
								if(data.rows[i].resultMaturity == 4)
									chengshudu = "可以量产";
								
								if(data.rows[i].resultType == 0)
									leixing = "专利";
								if(data.rows[i].resultType == 1)
									leixing = "非专利";
								item += pattern
									.replace("#88", data.rows[i].resultId)//id
									.replace("#0", data.rows[i].img.rimagePath)//图片
									.replace("#1", data.rows[i].resultName)//成果名
									//.replace("#2", data.rows[i].puser.puUsername)//用户名
									.replace("#4", leixing)//类型
									.replace("#6", data.rows[i].rp.rpayRate)//价格
									.replace("#3", data.rows[i].puser.puAddress)//地址
									.replace("#8", chengshudu)//成熟度
								html += item;
							}
							if(data.rows[i].puser&&!data.rows[i].img){
								item += pattern
									.replace("#88", data.rows[i].resultId)//id
									.replace("#0", data.rows[i].rimagePath)//图片
									.replace("#1", data.rows[i].resultName)//成果名
									//.replace("#2", data.rows[i].puser.puUsername)//用户名
									.replace("#4", leixing)//类型
									.replace("#6", data.rows[i].rp.rpayRate)//价格
									.replace("#3", data.rows[i].puser.puAddress)//地址
									.replace("#8", chengshudu)//成熟度
								html += item;
							}
							if(!data.rows[i].puser&&data.rows[i].img){
								item += pattern
									.replace("#88", data.rows[i].resultId)//id
									.replace("#0", data.rows[i].rimagePath)//图片
									.replace("#1", data.rows[i].resultName)//成果名
									//.replace("#2", data.rows[i].puUsername)//用户名
									.replace("#4", leixing)//类型
									.replace("#6", data.rows[i].rp.rpayRate)//价格
									.replace("#3", data.rows[i].puAddress)//地址
									.replace("#8", chengshudu)//成熟度
								html += item;
							}
							if(!data.rows[i].puser&&!data.rows[i].img){
								item += pattern
									.replace("#88", data.rows[i].resultId)//id
									.replace("#0", data.rows[i].rimagePath)//图片
									.replace("#1", data.rows[i].resultName)//成果名
									//.replace("#2", data.rows[i].puUsername)//用户名
									.replace("#4", leixing)//类型
									.replace("#6", data.rows[i].rpayRate)//价格
									.replace("#3", data.rows[i].puAddress)//地址
									.replace("#8", chengshudu)//成熟度
								html += item;
							}
						}
						
						$("#inneedlist").html(html);
						$("#lb li").each(function(i){
  				 			if(tradeId==$(this).attr("val")){
  				 				$("#lb li").eq(0).removeClass('selected');
  						 		$(this).addClass('selected');
  					 			}
						});
						
					},
				});
			}

			$(document).ready(function(){
　　				
				
			});
			
			
		</script>
    <div class="pageButton"></div>
       
    </div>
    <%@include file="/footer.jsp"%>
    <script src="method/hhn/my.js"></script>
    <script src="js/jquery.pagination.min.js"></script>
</body>