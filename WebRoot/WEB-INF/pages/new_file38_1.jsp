<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>仪器共享</title>
		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">
		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
">
		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/orderManagement.css">
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<style type="text/css">
			.mainList .content .right .infoList li .row2 .tab6 .btn {
    display: inline-block;
    width: 66px;
    height: 35px;
    line-height: 35px;
    text-align: center;
    background-color: rgb(255,138,0);
    border-radius: 5px;
    /* margin: auto; */
}
.mainList .content .right .infoList li {
    height: 109px;
    margin-bottom: 9px;
    border: 1px solid #e6e6e6;
}

	<%--.mainList .content .right .infoList{--%>
	<%--width: 998px;--%>
	<%--height: 100%;--%>
	<%--}--%>

	<%--.mainList .content .right .infoList li .row2 .tab6 {--%>
	<%--width: 18%;--%>
	<%--width: 277px !important;--%>
	<%--text-align: left;--%>
	<%--padding-left: 36px;--%>
	<%--overflow: hidden;--%>
	<%--text-overflow: ellipsis;--%>
	<%--white-space: nowrap;--%>
	<%--border-right: 1px solid #CCCCCC;--%>
	<%--}--%>

	<%--.mainList .content .right .infoList li .row2>div{--%>
	<%--display: inline-block;--%>
	<%--}--%>
	.mainList .content .right .infoList li .row2>div {
	display: inline-block;
	vertical-align: middle;
	text-align: center;
	font-size: 14px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	color: #202020;
	}

	.mainList .content .right .infoList li .row2{
	padding-top: 38px;
	}

	.mainList .content .box2 .item3 .row span, .mainList .content .box2 .item4 .row span {
	margin-right: 20px !important;
	}
		</style>
		<link rel="stylesheet" href="css/common.css">
		<script src="js/jquery-1.11.3.js"></script>
		<script>
			$(function() {
				
				/*-----------*/
				$(".tab_span1").click(function(){
					$(this).addClass('tab2').siblings().removeClass('tab2');
       $("#top2_item1").css("display","block");
       $("#top2_item2").css("display","block");
       $("#top1_item1").css("display","none");
       $("#top1_item2").css("display","none");
});


$(".tab_span2").click(function(){
	$(this).addClass('tab2').siblings().removeClass('tab2');
       $("#top1_item1").css("display","block");
       $("#top1_item2").css("display","block");
       $("#top2_item1").css("display","none");
       $("#top2_item2").css("display","none");
});			
				/*-----------*/
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$('.mainList .content .left li').removeClass('selected');
					$(this).addClass('selected');
				});
				$('.mainList .content .left .list_header').click(function() {
					if($('.mainList .content .left .secendList').is(':hidden')) {
						$('.mainList .content .left .secendList').slideDown();
						$('.mainList .content .left .list_header img').attr("src", "images/insUser_icon.png");
					} else {
						$('.mainList .content .left .secendList').slideUp();
						$('.mainList .content .left .list_header img').attr("src", "images/icon4.png");
					};

				});
				$("#row5 font label").click(function() {
					$("#row5 font label img").attr('src', 'images/icon35.png');
					$("#row5 font label").siblings('input').prop('checked', false);
						$(this).find('img').attr('src', 'images/icon36.png');
						$(this).siblings('input').prop('checked', true);
				});
				$("#row8 font label").click(function() {
					$("#row8 font label img").attr('src', 'images/icon35.png');
					$("#row8 font label").siblings('input').prop('checked', false);
						$(this).find('img').attr('src', 'images/icon36.png');
						$(this).siblings('input').prop('checked', true);
				});
			});
		</script>

	</head>
<body>
	<div style="background:#fff;">

		<%@include file="/head.jsp"%>

		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
	<a href="index.jsp" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>
					</div>
					<div class="web_name">
						用户中心
					</div>
				</div>
			</div>
		</div>
	<div class="mainList">
		<div class="bNav">您的位置：
		<a href="index">首页</a> > 
		<a href="new_file39">用户中心</a> > 
		<span>难题发布管理</span></div>
		<div class="content">
			<%@include file="/left-tab.jsp"%>
			<div class="right">
			<div class="title"><span>难题发布管理</span></div>

			<div class="tab_header" id="top1_item1">
			<div class="item1">难题名称</div>
			<div class="item2"></div>
			<div class="item3">难题简介</div>
			<div class="item4"></div>
			<div class="item5">状态</div>
			<div class="item6">操作</div>
			</div>


			<div class="infoList" id="top1_item2">
			<ul id="Mlist">
			<!-- 
			<li>
			<div class="row2">
			<div class="tab6" style="width: 40%;text-align: left;padding-left: 36px;border-right: 1px solid #CCCCCC;">1111111111高内涵筛选系统111111111111111111111111</div>
			<div class="tab7" style="width: 27%;text-align: left;padding-left: 27px;border-right: 1px solid #CCCCCC;">10-20万</div>
			<div class="tab5">
			<p>待确认</p>
			</div>
			<div class="tab6">
			<div class="btn">
			<a>查看</a>
			</div>
			<div class="btn" style="background-color: rgb(0,174,255);">
			<a >删除</a>
			</div>
			</div>
			</div>
			</li>
			 -->
			</ul>
			</div>


			<!----------------------------------->

			<!----------------------------------->

			</div>
		</div>
		<div class="pageButton"></div>
		<!--找个分页插件插入-->
	<%--找不到问题呀--%>
	</div>
	</div>


	<%@include file="/footer.jsp"%>


		<script>
			$(document).ready(function(){
				loat();
			})
		</script>
		<script>
			function loat(){
				var typeId = $('#usertypeoflogin').val();
				var id = $('#useridoflogin').val();
				$.ajax({
					url:Server + "/problem_select/manageList",
					data:{
						"limit":10,
						"offset":0,
						"typeId":typeId,
						"id":id,
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var html = "";
						var list = data.rows;
						var pattern =
							'<li>\
								<div class="row2">\
									<div class="tab6" style="width: 40%;text-align: left;padding-left: 36px;border-right: 1px solid #CCCCCC;">#0</div>\
									<div class="tab7" style="width: 27%;text-align: left;padding-left: 27px;border-right: 1px solid #CCCCCC;">#1</div>\
									<div class="tab5">\
										<p>#2</p>\
									</div>\
									<div class="tab6">\
										<div class="btn">\
											<a onclick="details(#3);">查看</a>\
										</div>\
										<div class="btn" style="background-color: rgb(0,174,255);">\
											<a onclick="del(#4);">删除</a>\
										</div>\
									</div>\
								</div>\
							</li>';
						for(var i = 0; i < list.length;i++){
							var item = "";
							var state = "";
							if(list[i].needStatus == 0){
								state = "发布中";
							}else if(list[i].needStatus == 1){
								state = "已采纳";
							}
							item += pattern
								.replace("#0",list[i].needName)
								.replace("#1",list[i].needExplain)
								.replace("#2",state)
								.replace("#3",list[i].needId)
								.replace("#4",list[i].needId),
							html += item;
						}
						$("#Mlist").html(html);
					},
				});
			}
			
			function del(needId){ 
				var msg = "您真的确定要删除吗？\n\n请确认！"; 
				if (confirm(msg)==true){
					detelet(needId); 
					return true;
				}else{ 
					return false; 
				} 
			} 

			
			function detelet(needId){
				$.ajax({
					url:Server + "/problem_select/manageDelete",
					data:{
						"needId":needId,
					},
					type:"POST",
					success : function(data){
						alert("删除成功");
						loat()
					},
					error : function(msg){
						alert("删除失败");
					}
				});
			}
			
			function details(needId){
				$(location).attr("href","new_file38_2?needId="+needId+"");
			}
			function indexs(){
				$(location).attr("href","index");
			}
		</script>


</body>
</html>
