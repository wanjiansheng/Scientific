<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<title>仪器共享</title>
<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">
<meta name="description"
	content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
">
<script src="js/jquery-1.11.3.js"></script>
<script src="js/zoomify.min.js"></script>
<link rel="stylesheet" href="new_css/zoomify.min.css">
<link rel="stylesheet" href="new_css/header_footer.css?t=1">
<link rel="stylesheet" href="new_css/uploadData.css">
<link rel="stylesheet" href="css/common.css">
<script>
	$(function() {
		$('#front_header .header_wrap .right').hover(function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
			$('#front_header .header_wrap .right .out').show();
		}, function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
			$('#front_header .header_wrap .right .out').hide();
		});
		$('#sorts').on('mouseover', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon4.png');
		});
		$('#sorts').on('mouseout', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon5.png');
		});

		$('.mainList .content .left li').click(function() {
			$('.mainList .content .left li').removeClass('selected');
			$(this).addClass('selected');
		});
		$('.mainList .content .left .list_header').click(function() {
			if ($('.mainList .content .left .secendList').is(':hidden')) {
				$('.mainList .content .left .secendList').slideDown();
				$('.mainList .content .left .list_header img').attr("src", "images/insUser_icon.png");
			} else {
				$('.mainList .content .left .secendList').slideUp();
				$('.mainList .content .left .list_header img').attr("src", "images/icon4.png");
			}
			;

		});
		$("#row5 font label").click(function() {
			$("#row5 font label img").attr('src', 'images/icon35.png');
			$("#row5 font label").siblings('input').prop('checked', false);
			$(this).find('img').attr('src', 'images/icon36.png');
			$(this).siblings('input').prop('checked', true);
		});
		$("#row8 font label").click(function() {
			$("#row8 font label img").attr('src', 'images/icon35.png');
			$("#row8 font label").siblings('input').prop('checked', false);
			$(this).find('img').attr('src', 'images/icon36.png');
			$(this).siblings('input').prop('checked', true);
		});
	});
</script>

</head>

<body style="background:#fff;">

	<%@include file="/head.jsp"%>

	<div class="" id="nav_top">
		<div class="container">
			<div class="fl">
				<div class="logo ">
					<a class="logo-text" onclick="indexs();">科研成果共享平台</a>
				</div>
				<div class="web_name">用户中心</div>
			</div>
		</div>
	</div>
	<div class="mainList">
		<div class="bNav">
			您的位置：首页>用户中心><span>上传资料</span>
		</div>
		<div class="content" id="height-content">
			<div class="box1">
				<div class="box_head">
	<a>技术难题解决</a><span><img src="image/xqxq_yjj.png" style="margin-right:10px"></span><span id="in8" style="color: rgb(255,29,145); font-size:12px">需求未解决</span>
				</div>
				<div class="box_title" id="in0">一种新型自动化分拣装置的实用新型专利定制</div>
				<div class="box_content">
					<p>
						<font>类型：<span id="in1">技术难题解决</span></font> <font>行业类别：<span
							id="in2">教育休闲-工艺美术</span></font>
					</p>
					<p>
						需求简介：<span id="in3">一种新型自动化分拣装置的实用新型专利定制,一种新型自动化分拣装置的实用新型专利定制</span>
					</p>
					<p>
						地址：<span id="in6">浙江 温州</span>
					</p>
					<p>
						预算：<span class="colorFont" id="in7">5-10万</span>
					</p>
				</div>
			</div>
			<div class="box2">
				<div class="boxTitle">评审意见信息</div>
				<div class='item item1' id="tech" data-appid="" onclick="techT();">
					<div class="left" id="p1">仪器特别好用，服务态度也很好
						仪器特别好用，服务态度也很好仪器特别好用，服务态度也很好服务态度也很好</div>
					<div class="right" id="p2">
						一日***记忆<span>（服务商）</span>
					</div>
				</div>
				<div class='item item2' id="com2">
					<div class="row">
						<div class="left">仪器特别好用，服务态度也很好
							仪器特别好用，服务态度也很好仪器特别好用，服务态度也很好服务态度也很好</div>
						<div class="right">
							一日***记忆<span>（服务商）</span>
						</div>
					</div>
					<div class="row">
						<div class="left">仪器特别好用，服务态度也很好
							仪器特别好用，服务态度也很好仪器特别好用，服务态度也很好服务态度也很好</div>
						<div class="right">
							一日***记忆<span>（服务商）</span>
						</div>
					</div>
					<div class="row">
						<div class="left">仪器特别好用，服务态度也很好
							仪器特别好用，服务态度也很好仪器特别好用，服务态度也很好服务态度也很好</div>
						<div class="right">
							一日***记忆<span>（服务商）</span>
						</div>
					</div>

				</div>
				<div class="boxTitle">买家信息</div>
				<div class="item item3">
					<div class="row">
						<span id="userName">用户姓名：詹森</span> <span id="userIphone">联系方式：18536589741</span>
						<span id="userAddress">地址：云南省昆明市盘龙区</span>
					</div>
					<div class="row">
						<span id="userUnit">单位：昆明市一二一街道</span>
					</div>
				</div>
				<div class="boxTitle">订单信息</div>
				<div class="item item4">
					<div class="row">
						<span id="orderId">订单编号：1236589745</span> 
						<span id="orderDate">付款时间：2018-6-1 23:45:23</span>
					</div>
					<div class="row">
						<span id="orderUser">支付宝交易号：123659875698547</span>
					</div>
				</div>
				<div class="boxTitle">资料信息</div>
				<div class="item item5">

					<label>查看合同文档:</label>
					<div class="row" id="doc"></div>
					<div class="row">
						<label>交易合同</label>
						<div class="pic"></div>
					</div>
				</div>
				<div class="btn">
					<a
						onclick="upload(${param.needId},${param.redalId});">评价</a>
				</div>
			</div>
		</div>
		<div class="pageButton"></div>
		<!--找个分页插件插入-->
	</div>

	<%@include file="/footer.jsp"%>
	<script>
			$(document).ready(function(){
				load()
				particulars()
				user()
				orderDetails();
				next(${param.redalId});
				
		//点击图片放大
		$('.pic img').zoomify();
			});
			
			function load(){
				$.ajax({
					url:Server + "/res/need/detail",
					data:{
						"offset":0,
						"limit":10,
						"needId":${param.needId},
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var d = data.rows;
						var status = "";//需求类型0：技术需求1：合作需求2：人才需求
						var status1 = "";//展示状态 0：未解决 1：已解决
						if(d[0].ntypeId == 0){
							status = "技术需求";
						}else if(d[0].ntypeId == 1){
							status = "合作需求";
						}else if(d[0].ntypeId == 2){
							status = "人才需求";
						}
						if(d[0].ntradeId == 0){
							status1 = "需求未解决";
						}else if(d[0].ntradeId == 1){
							status1 = "需求已解决";
						}
						$("#in0").html(d[0].needName);
						$("#in1").html(status);
						$("#in2").html(d[0].tTrade.tradeName);
						$("#in3").html(d[0].needExplain);
						$("#in6").html(d[0].address);
						$("#in7").html(d[0].needBudget);
						$("#in8").html(status1);
					},
				});
			}
			
			function particulars(){
				$.ajax({
					url:Server + "/res/need/tech",
					data:{
						"offset":0,
						"limit":10,
						"needId": ${param.needId},
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var p = data.rows;
						var html = "";
						var row = '<div class="row">\
							<div class="left">#0</div>\
							<div class="right">#1<span>（专家）</span></div>\
						</div>';
						var list = p[0].proofreview
						for(var i = 0;i < list.length;i++){
							var item = "";
							var list2 = list[i];
							item += row
								.replace("#0", list2.reviewContent)
								.replace("#1", list2.euRealname),
							html += item;
						}
						$("#com2").html(html);
						$("#p2").html(p[0].name);
						$("#p1").html(p[0].techName);
						$("#tech").attr("data-appid",p[0].techId);
					}
				});
			}
			
			function user(){
				var typeId = $('#usertypeoflogin').val();
				var id = $('#useridoflogin').val();
				$.ajax({
					url: Server + "/res/need/user",
					data:{
						"typeId":typeId,
						"id":id,
					},
					type:"POST",
					dataType:"json",
					success : function(data){
					var scz = data[0];
						if(typeId == 1){//个人
							$("#userName").html("用户姓名："+scz.puRealname);
							$("#userIphone").html("联系方式："+scz.puPhone);
							$("#userAddress").html("地址："+scz.puAddress);
							$("#userUnit").html("单位："+scz.puUnit);
						}else if(typeId == 2){//企业
							$("#userName").html("用户姓名："+scz.uuUnitName);
							$("#userIphone").html("联系方式："+scz.uuPhone);
							$("#userAddress").html("地址："+scz.uuUnitAddress);
							$("#userUnit").html("单位："+scz.uuContacts);
						}
					},
				});
			}
			
			function orderDetails(){
				$.ajax({
					url: Server + "/res/need/orderDetails",
					data:{
						"needId":${param.needId},
						"rdealId":${param.redalId},
					},
					type:"POST",
					dataType:"json",
					success : function(data){
					if(data.srd != null){
						if(data.srd.rdealEndTime != null){
							var rdealEndTime = data.srd.rdealEndTime;
						}else{
							var rdealEndTime = "";
						}
						if(data.srd.payNumber != null){
							var payNumber = data.srd.payNumber;
						}else{
							var payNumber = "";
						}
					}else{
						var rdealEndTime = "";
						var payNumber = "";
					}
						$("#orderId").html("订单编号:"+data.srd.rdealNumber);
						$("#orderDate").html("付款时间:"+rdealEndTime);
						$("#orderUser").html("支付宝交易号:"+payNumber);
					}
				});
			}
			function next(redalId) {
	$.ajax({
		url : Server + "/res/dea/url",
		type : "POST",
		dataType : "JSON",
		data : {
			"id" : redalId
		},
		async : false,
		success : function(data) {
			var imgHtml = '';
			var docHtml = '';
			for (var i = 0; i < data.length; i++) {
				var o = data[i].sdc;
				for(var j = 0;j<o.length;j++){
					if (o[j].upType == 1) {
						sdcPath = o[j].path;
						imgHtml += '<img class="img-rounded zoomify" src=' + Server +'/'+ sdcPath + '>';
					}else if (o[j].upType == 2) {
						sdcPath = o[j].path;
						docHtml += '<a style="color: red; font-size: 20px" class="right_a" href=' + Server +'/'+ sdcPath + ' target="_blank">点击下载查看交易合同</a>';
					}
				}
			}
			$(".pic").html(imgHtml);
			$("#doc").html(docHtml);

		}
	})
}
			function upload(needId,redalId){
				$(location).attr("href","new_file25_4?needId="+needId+"&redalId="+redalId+"");
			}
			
			function indexs(){
				$(location).attr("href","index");
			}
			
			function Marine(){
				$(location).attr("href","new_file20");
			}
			
			function techT(){
				var techId = $("#tech").attr("data-appid");
				$(location).attr("href","techDetail?techId="+techId+"");
			}
		</script>
</body>

</html>