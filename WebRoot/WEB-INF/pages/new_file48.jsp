<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>

	<head>
	<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>仪器共享</title>
		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">
		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
">
		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/technologyPlanReview.css">
		<link rel="stylesheet" href="css/common.css">
		<script src="js/jquery-1.11.3.js"></script>
		<script>
			$(function() {
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$('.mainList .content .left li').removeClass('selected');
					$(this).addClass('selected');
				});
				$('.mainList .content .left .list_header').click(function() {
					if($('.mainList .content .left .secendList').is(':hidden')) {
						$('.mainList .content .left .secendList').slideDown();
						$('.mainList .content .left .list_header img').attr("src", "images/insUser_icon.png");
					} else {
						$('.mainList .content .left .secendList').slideUp();
						$('.mainList .content .left .list_header img').attr("src", "images/icon4.png");
					};

				});
				$("#row5 font label").click(function() {
					$("#row5 font label img").attr('src', 'images/icon35.png');
					$("#row5 font label").siblings('input').prop('checked', false);
					$(this).find('img').attr('src', 'images/icon36.png');
					$(this).siblings('input').prop('checked', true);
				});
				$("#row8 font label").click(function() {
					$("#row8 font label img").attr('src', 'images/icon35.png');
					$("#row8 font label").siblings('input').prop('checked', false);
					$(this).find('img').attr('src', 'images/icon36.png');
					$(this).siblings('input').prop('checked', true);
				});
			});
		</script>

	</head>

	<body style="background:#fff;">
		<%@include file="/head.jsp"%>

		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
						<a class="logo-text" onclick="indes();">
							科研成果共享平台 
						</a>
					</div>
					<div class="web_name">
						用户中心
					</div>
				</div>
			</div>
		</div>
		<div class="mainList">
			<div class="bNav">您的位置：首页>用户中心><span>需求申请管理</span></div>
			<div class="content">
				<div class="left">
					<ul>
						<li class="list"><a onclick="fagl();">技术方案评审</a></li>
						<li class="list selected"><a onclick="xqsq();">需求申请管理</a></li>

					</ul>
					<!--<ul class="secendList">
						<li class="selected">等待完成</li>
						<li>完成交易 </li>
						
					</ul>-->
					<!--<ul>
						<li class="list">海洋装备<img src="../images/icon4.png" /></li>
						<li class="list">科研需求<img src="../images/icon4.png" /></li>
						<li class="list">技术方案管理</li>
						<li class="list selected" >账号设置</li>
						<li class="list">信用认证和交易</li>
					</ul>-->
				</div>
				<div class="right">
					<div class="title"><span>需求申请管理</span></div>
					<div class="box">
						<div class="header">
							<div class="header1">需求名称</div>
							<div class="header2">需求简介</div>
							<div class="header3">申请时间</div>
							<div class="header4">操作</div>
						</div>
						<ul id="applyList">
						</ul>
					</div>
				</div>
			</div>
			
			<div class="pageButton"></div>
			<!--找个分页插件插入-->
		</div>

		<%@include file="/footer.jsp"%>
		<script>
			$(document).ready(function(){
				applyList()
			});
			
			function applyList(){
				$.ajax({
					url:Server + "/eulist/applyList",
					data:{
						"limit":10,
						"offset":0,
						"euId":${param.euId},
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var html = "";
						var date = data.rows;
						var pattern = '<li class="">\
								<div class="header1">#0</div>\
								<div class="header2">#1</div>\
								<div class="header3">#2</div>\
								<div class="header4"><div class="wrap">\
								<a onclick="applyDetails(#3,#4,${param.euId},#6);">查看</a>\
								<a style="background-color: rgb(0,178,255);" onclick="del(#5);">删除</a>\
								</div>\
								</div>\
							</li>';
						for(var i = 0; i < date.length;i++){
							var item = "";
							var param = date[i];
							var needName = "";
							var needExplain = "";
							if(param.demand != null){
								needName = param.demand.needName;
								needExplain = param.demand.needExplain;
							}
							var time = toTime(param.applyTime);
							item += pattern
								.replace("#0",needName)
								.replace("#1",needExplain)
								.replace("#2",time)
								.replace("#3",param.needUserType)
								.replace("#4",param.needUserId)
								.replace("#5",param.applyId)
								.replace("#6",param.applyId),
							html += item;
						}
						$("#applyList").html(html);
					},
				});
			}
			
			function toTime(reviewCreatTime) {
				var date = new Date(reviewCreatTime);
				var time=date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
				return time
			}
			
			function del(applyId){
				var msg = "您真的确定要删除吗？\n\n请确认！";
				if(confirm(msg)==true){
					Dele(applyId);
					return true;
				}else{
					return false;
				}
			}
			
			function Dele(applyId){
				$.ajax({
					url:Server + "/eulist/applyDelete",
					data:{
						"applyId":applyId,
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						alert("删除成功");
						window.location.reload()
					},
					error : function(msg){
						alert("删除失败");
						window.location.reload()
					}
				});
			}
			
			function applyDetails(needUserType,needUserId,euId,applyId){
				$(location).attr("href","new_file49?needUserType="+needUserType+"&needUserId="+needUserId+"&euId="+euId+"&applyId="+applyId+"");
			}
			
			function fagl(){
				$(location).attr("href","new_file46?euId="+${param.euId}+"");
			}
			
			function xqsq(){
				$(location).attr("href","new_file48?euId="+${param.euId}+"");
			}
			
			function hy(){
				$(location).attr("href","new_file20");
			}
			
			function indes(){
				$(location).attr("href","index");
			}
		</script>

	</body>

</html>