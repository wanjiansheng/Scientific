<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>成果共享</title>

<meta name="keywords" content="科研成果，生命科学成果，环境检测成果，实验常用设备，分析成果">

<meta name="description"
	content="中国领先的科研成果共享平台，让您快速找到各种类型的科学研究成果。提升闲置成果利用率，产生更大科研价值。涵盖：生命科学成果、环境检测成果、实验常用设备、分析成果、仪表、物性测试、测量/计量成果、在线及过程控制成果。
         
">
<link rel="stylesheet" href="new_css/header_footer.css?t=1">
<link rel="stylesheet" href="new_css/productDetail.css">
<link rel="stylesheet" href="css/new_style.css">

<link rel="stylesheet" href="js/layui/css/layui.css" media="all">
<link rel="stylesheet" href="css/common.css">
<style type="text/css">
.mainList .content .productInfo {
	width: 100%;
	border: 1px solid #d3d3d3;
	padding: 15px;
	height: 300px;
	overflow: hidden;
}

.mainList .content .productInfo .left {
	float: left;
	height: 100%;
	border: 1px solid #f4f4f4;
	width: 274px;
	text-align: center;
}

.mainList .content .userEvaluation {
	margin-top: 10px;
	border: 1px solid #d3d3d3;
	width: 100%;
	padding: 0px;
}

.mainList .content .userEvaluation .box .tabView li {
	padding: 18px 0;
	width: 100%;
	border-bottom: none;
}
.productInfo .right{
   width:710px;
}
.left .left_img {
   width: 100%;
    height: auto;
    height: 100%;
    margin-left: 25px;
}
.mainList .content .productInfo .left {
   border: 0px solid #f4f4f4;
}
.mainList .content .productInfo {
    width: 100%;
    border: 1px solid #d3d3d3;
    border-top-color: rgb(211, 211, 211);
    border-top-style: solid;
    border-top-width: 1px;
    border-right-color: rgb(211, 211, 211);
    border-right-style: solid;
    border-right-width: 1px;
    border-bottom-color: rgb(211, 211, 211);
    border-bottom-style: solid;
    border-bottom-width: 1px;
    border-left-color: rgb(211, 211, 211);
    border-left-style: solid;
    border-left-width: 1px;
    border-image-source: initial;
    border-image-slice: initial;
    border-image-width: initial;
    border-image-outset: initial;
    border-image-repeat: initial;
    padding: 17px;
    padding-left: 0px;
    height: 300px;
    overflow: hidden;
}
.mainList .content .productInfo .left img {
    float: left;
    height: 100%;
    border: 0px solid #f4f4f4;
    width: 236px;
    text-align: center;
        margin-top: 0px;
}
.mainList .content .productInfo .right {
   float: left;
    height: 100%;
    padding-left: 0px;
    padding-bottom: 18px;
    padding-top: 18px;
    margin-left: 30px;
}
.mainList .content .productInfo .right .row9{
	    cursor: pointer;
}
</style>
<script src="js/jquery-1.11.3.js"></script>
<script>
	$(function() {
		$('#front_header .header_wrap .right').hover(function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
			$('#front_header .header_wrap .right .out').show();
		}, function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
			$('#front_header .header_wrap .right .out').hide();
		});
		$('#sorts').on('mouseover', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon4.png');
		});
		$('#sorts').on('mouseout', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon5.png');
		});

		$('.mainList .content .left li').click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
		});



		//--------
		$(".tab_span1").click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
			$("#tabView1").css("display", "block");
			$("#tabView2").css("display", "none");
		});


		$(".tab_span2").click(function() {
			$(this).addClass('selected').siblings().removeClass('selected');
			$("#tabView2").css("display", "block");
			$("#tabView1").css("display", "none");
		});
		
	});
</script>
</head>
<body style="background:#fff;">



		<div class="wj_ma modal2" id="D" style="display: none;">
        	<div class="pos">
        	   <div class="pos_title">联系方式</div>
        	   <div class="middle_content">
        	   	   <div class="row">
        	   	   	<label>姓名</label><input type="text" value="" id="in10" disabled="disabled"/>
        	   	   </div>
        	   	   <div class="row">
        	   	   	<label>电话</label><input type="text" value="" id="in11" disabled="disabled"/>
        	   	   </div>
        	   	   <div class="row">
        	   	   	<label>地址</label><input type="text" value="" id="in12" disabled="disabled"/>
        	   	   </div>
        	   </div>
        	   <div class="pos_buttom">确定</div>
        	</div>
        </div>
        
	<%@include file="/head.jsp"%>

	<div class="" id="nav_top">
		<div class="container">
			<div class="fl">
				<div class="logo ">
					<a onclick="index();" style="font-size: 30px;color: rgb(255,138,0);">科研成果共享平台</a>
				</div>
				<!--<div class="web_name">
						<img src="images/icon3.png" />
						<p>yq.shang.com</p>
					</div>-->
			</div>
		</div>
	</div>



	<!--<div class="announcement">
			<p>公告：光谱仪目前在更换耗材不接受预约，明天恢复预约。2018-4-27</p>
		</div>-->
	<div class="mainList">
		<div class="bNav">
			您的位置：

<a href="index" style="color: #575757">首页</a> > 
			<a href="scienic_expertsshow11">专家列表</a> > 
			<span>专家详情</span>
		</div>
		<div class="content">
			<div class="productInfo">
				<div class="left">
					<div class="left_img">
						<img id="in0" src="/UPLOAD/image/expert#0" />
					</div>
					<!--轮播图插件-->
				</div>
				<div class="right">
					<div class="row row1">
						<span id="in1"></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="in2"></span>
					</div>
					<div class="row row3">
						<span id="in4"></span>
					</div>
					<div class="row">
						熟悉学科：<span id="in5"></span>
					</div>
					<div class="row row9">
						<div class="btn">
							<a style="background-color: rgb(6,185,254);margin-right: 20px;" id="sun_btn">联系方式</a>
						</div>
						<div class="btn">
							<a onclick="apply(${param.euId});">立即申请</a>
						</div>
					</div>
				</div>
			</div>
			<div class="userEvaluation">
				<div class="box">
					<div class="tabView sun_tabView" id="tabView1">
						<ul>
							<li>
								<div style="border-left: 2px solid rgb(255,138,0);">
									<a class="str_a">研究方向或专长:</a>
								</div>
								<div style="padding-left: 10px;    padding-top: 20px;">
									<p id="in6"></p>
								</div>
							</li>
							<li>
								<div style="border-left: 2px solid rgb(255,138,0);">
									<a class="str_a">主要研究成果:</a>
								</div>
								<div style="padding-left: 10px;    padding-top: 20px;">
									<p id="in7"></p>
								</div>
							</li>
							<li>
								<div style="border-left: 2px solid rgb(255,138,0);">
									<a class="str_a">获得奖项:</a>
								</div>
								<div style="padding-left: 10px;    padding-top: 20px;">
									<p id="in8"></p>
								</div>
							</li>
							<li>
								<div style="border-left: 2px solid rgb(255,138,0);">
									<a class="str_a">主要社会兼职:</a>
								</div>
								<div style="padding-left: 10px;    padding-top: 20px;">
									<p id="in9"></p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>

						<!------------------------>




						<!------------------------>
					</div>
					<div class='tabView' id="tabView2" style="display: none;">
						<ul>
							<li>
								<div class="fontInfo">
									<div class="left">成果特别好用，服务态度也很好
										成果特别好用，服务态度也很好成果特别好用，服务态度也很好服务态度也很好</div>
									<div class="right">
										一日***记忆<span>（匿名）</span>
									</div>
								</div>
								<div class="pic">
									<img src="images/productDetailPic.png" /> <img
										src="images/productDetailPic.png" />
								</div>
								<div class="replay">
									<div class='left'>
										回复：<span>谢谢您的支持！</span>
									</div>
									<div class='right'>负责人：张三</div>
								</div>
							</li>

							<li>
								<div class="fontInfo">
									<div class="left">成果特别好用，服务态度也很好
										成果特别好用，服务态度也很好成果特别好用，服务态度也很好服务态度也很好</div>
									<div class="right">
										一日***记忆<span>（匿名）</span>
									</div>
								</div>
								<div class="pic">
									<img src="images/productDetailPic.png" /> <img
										src="images/productDetailPic.png" />
								</div>
								<div class="replay">
									<div class='left'>
										回复：<span>谢谢您的支持！</span>
									</div>
									<div class='right'>负责人：张三</div>
								</div>
							</li>
							<li>
								<div class="fontInfo">
									<div class="left">成果特别好用，服务态度也很好
										成果特别好用，服务态度也很好成果特别好用，服务态度也很好服务态度也很好</div>
									<div class="right">
										一日***记忆<span>（匿名）</span>
									</div>
								</div>
								<div class="pic">
									<img src="images/productDetailPic.png" /> <img
										src="images/productDetailPic.png" />
								</div>
								<div class="replay">
									<div class='left'>
										回复：<span>谢谢您的支持！</span>
									</div>
									<div class='right'>负责人：张三</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="pageButton"></div>
		<!--找个分页插件插入-->
	</div>

	<%@include file="/footer.jsp"%>
	<script src="js/layui/layui.js"></script>
	<script>
		layui.use('carousel', function() {
			var carousel = layui.carousel;
			//建造实例
			carousel.render({
				elem : '#test1',
				width : '100%', //设置容器宽度
				arrow : 'always' //始终显示箭头
			//,anim: 'updown' //切换动画方式
			});
		});
	</script>
	<script>
	$(document).ready(function(){
		$.ajax({
			url:Server + "/eulist/details",
			data:{
				"euId":${param.euId},
			},
			type:"POST",
			dataType:"json",
			success : function(data){
				$("#in0").attr("src",data[0].euIcon);
				$("#in1").text(data[0].euRealname);
				$("#in2").text(data[0].euJob);
				$("#in4").text(data[0].euUnitName);
				$("#in5").text(data[0].euSubject);
				$("#in6").text(data[0].euDirection);
				$("#in7").text(data[0].euResult);
				$("#in8").text(data[0].euAwards);
				$("#in9").text(data[0].euPartTime);
				$("#in10").val(data[0].euRealname);
				$("#in11").val(data[0].euMobile);
				$("#in12").val(data[0].euAddress);
				
				$('#sun_btn').click(function(event){
					$('.modal2').show();
				});
				$('.pos_buttom').click(function(){
					$('.modal2').hide();
				})
				$('div[id="D"]').click(function(e){
					var target = e.target;
					$('div[id="D"]').each(function(e){
						if(target.id == 'D') {
							$(this).hide();
						}
					});
				});
			},
		});
	})
	
	function index(){
		$(location).attr("href","index");
	}
	
	
</script>
<script>
	function apply(euId){
		if ($("#useridoflogin").val() == 'null') {
			alert("未登录，请登录！");
		} else {
			$(location).attr("href","new_file13?euId="+euId+"");
		}
	}
	
</script>
	
</body>

</html>