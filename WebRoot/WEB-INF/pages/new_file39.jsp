<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>

	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<title>仪器共享</title>
		<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">
		<meta name="description" content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
">
		<link rel="stylesheet" href="new_css/header_footer.css?t=1">
		<link rel="stylesheet" href="new_css/accountSet.css">
		<link rel="stylesheet" href="css/new_style.css">
		<link rel="stylesheet" href="css/common.css">
	<style>
	#puIdType{
	width: 65%;
	height: 40px;
	border: 1px solid #ccc;

	}
	</style>
		<script src="js/jquery-1.11.3.js"></script>
		<script src="method/dwy/new_file39.js"></script>
		<script src="method/hhn/loadUser.js"></script>
		<script>
				$(document).ready(function(){
				 			load();
				})


				
				
		</script>
		<script>
			$(function() {
				/*------*/
				$('#sun_btn').click(function(event){
		$('.modal2').show();

	});
	$('#sun_btn2').click(function(event){
		$("#phone").val($("#mobile").val());
		$('.modal1').show();

	});
			$('div[id="D"]').click(function(e){
		var target = e.target;
		$('div[id="D"]').each(function(e){

			if(target.id == 'D') {
				$(this).hide();
			}
		});
	})	
				
				
				/*------*/
				
				
				$('#front_header .header_wrap .right').hover(function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
					$('#front_header .header_wrap .right .out').show();
				}, function() {
					$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
					$('#front_header .header_wrap .right .out').hide();
				});
				$('#sorts').on('mouseover', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon4.png');
				});
				$('#sorts').on('mouseout', 'li', function() {
					$(this).find('span img').attr('src', 'images/icon5.png');
				});

				$('.mainList .content .left li').click(function() {
					$('.mainList .content .left li').removeClass('selected');
					$(this).addClass('selected');
				});
				$('.mainList .content .left .list_header').click(function() {
					if($('.mainList .content .left .secendList').is(':hidden')) {
						$('.mainList .content .left .secendList').slideDown();
						$('.mainList .content .left .list_header img').attr("src", "images/insUser_icon.png");
					} else {
						$('.mainList .content .left .secendList').slideUp();
						$('.mainList .content .left .list_header img').attr("src", "images/icon4.png");
					};

				});
				$("#row5 font label").click(function() {
					$("#row5 font label img").attr('src', 'images/icon35.png');
					$("#row5 font label").siblings('input').prop('checked', false);
						$(this).find('img').attr('src', 'images/icon36.png');
						$(this).siblings('input').prop('checked', true);
				});
				$("#row8 font label").click(function() {
					$("#row8 font label img").attr('src', 'images/icon35.png');
					$("#row8 font label").siblings('input').prop('checked', false);
						$(this).find('img').attr('src', 'images/icon36.png');
						$(this).siblings('input').prop('checked', true);
				});
			});
		</script>

	</head>

	<body style="background:#fff;">

<!-------------------------->
        
        <div class="wj_ma modal1" id="D" style="display: none;">
        	<div class="pos">
        	   <div class="pos_title">联系方式</div>
        	   <div class="middle_content">
        	   	   <div class="row">
        	   	   	<label>手机号</label><input id="phone" type="text" />
        	   	   </div>
        	   	   <div class="row">
        	   	   	<label>验证码</label><input type="text" />
        	   	   </div>
        	   	  
        	   	
        	   </div>
        	   <div class="pos_buttom">确定</div>
        	</div>
        	
        </div>

<!-------------------------->
<!-------------------------->
        
        <div class="wj_ma modal2" id="D" style="display: none;">
        <form id="resertForm">
        <input name="puId" type="hidden" id="8">
        	<div class="pos">
        	   <div class="pos_title">联系方式</div>
        	   <div class="middle_content">
        	   	   <div class="row">
        	   	   	<label>旧密码</label><input name="pwdOld" type="password" value="" />
        	   	   </div>
        	   	   <div class="row">
        	   	   	<label>新密码</label><input name="pwdNew" type="password" value="" />
        	   	   </div>
        	   	   <div class="row">
        	   	   	<label>确认密码</label><input name="pwdNew" type="password" value="" />
        	   	   </div>
        	   	
        	   </div>
        	   <div class="pos_buttom" id="resert()">确定</div>
        	</div>
        	
       </form>
        </div>

<!-------------------------->
		<%@include file="/head.jsp"%>
		<div class="" id="nav_top">
			<div class="container">
				<div class="fl">
					<div class="logo ">
	<a href="index" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>

	</div>
					<div class="web_name">
						用户中心
					</div>
				</div>
			</div>
		</div>
		<div class="mainList">
			<div class="bNav">您的位置：

			<a href="index">首页</a> > 
			<a href="new_file39">用户中心</a> > 
			<span>账号设置</span></div>
			<div class="content">
			<%@include file="/left-tab.jsp"%>
				<div class="right">
					<div class="title"><span>账号设置</span></div>
					<form id="signupForm" >
					<input name="puId" type="hidden" id="7">
					<div class="box">
						<ul>
							<li><!-- class="row1" readonly="readonly" -->
								<label>登录手机号</label>
								<input type="text" id="mobile" placeholder="请输入您的手机号" disabled  />
								<!-- <a id="sun_btn2">更换手机号</a>
								<a id="sun_btn">修改密码</a> -->
							</li>
							<li>
								<label>电子邮箱</label>
								<input type="text" name="puEmail" id="puEmail" value="" placeholder="请输入电子邮箱"/>
							</li>
							<li>
								<label>用户名称</label>
								<input type="text" name="puRealname" id="puRealname" value="" placeholder="请输入用户名称"/>
							</li>
							<li>
								<label>证件类型</label>
								<select type="text" name="puIdType" id="puIdType" value="">
										<option value="1" >身份证</option>
										<option value="2" >军官证</option>
										<option value="3" >护照</option>
										<option value="4" >其它</option>
									<select>
								<!-- <input type="text" name="puIdType" id="puIdType" value="" placeholder="请输入机构代码"/> -->
							</li>
							<li>
								<label>证件号码</label>
								<input type="text" name="puIdCard" id="puIdCard" value="" placeholder="请输入单位类型"/>
							</li>
							<li>
								<label>联系地址</label>
								<input type="text" name="puAddress" id="puAddress" value="" placeholder="请输入单位注册地"/>
							</li>
							
							<li>
								<label>单位</label>
								<input type="text" name="puUnit" id="puUnit" value="" placeholder="请输入单位性质"/>
							</li>
							
							<li>
								<label></label>
								<a onclick="update()">保存</a>
							</li>
						</ul>
					</div>
				   </form>
				</div>
			</div>
			<div class="pageButton"></div>
			<!--找个分页插件插入-->
		</div>

		<%@include file="/footer.jsp"%>
	
	</body>
<script>
			
			$(function(){
   				$.ajax({
						url:Server + "/per/spu/getPuserByUid",
						//url:"http://localhost/per/spu/getPuserByUid",
						type:"POST",
						data:{
							"uid":$("#useridoflogin").val()
							//"uid":"5cbc8e5610cc42fdaa3d03519caa999b"
						},
						dataType:"json",
						success:function(data){
							//alert(data.puIdType);
							$("#puIdType").val(data.puIdType);
							$("#puIdCard").val(data.puIdCard);
							$("#puUnit").val(data.puUnit);
						},
						error:function(data){
							//alert("获取本地用户信息失败");
						}
					}); 
			});
			
			function update(){
					var uid = $("#useridoflogin").val();
					var usertype = $("#usertypeoflogin").val();
					var token = $("#token").val();
					var email = $("#puEmail").val();
					var realName=$("#puRealname").val();
					var idType=$("#puIdType").val();
					var idCard=$("#puIdCard").val();
					var address=$("#puAddress").val();
					var unit=$("#puUnit").val();
					if(email==""){
						alert("请输入邮箱");
						return;
					}
					if(realName==""){
						alert("请输入用户名称");
						return;
					}
					if(idType==""){
						alert("请输入证件类型");
						return;	
					}else{
						var regu = /^[0-9]\d*$/;
						if(!regu.test(idType)){
							alert("证件类型需为整数");
							return;
						}
					}
					if(idCard==""){
						alert("请输入证件号码");
						return;
					}
					if(address==""){
						alert("请输入联系地址");
						return;
					}
					if(unit==""){
						alert("请输入单位");
						return;
					}
					$.ajax({
						url:Server + "/thirdparty/user/updateUserInfo",
						//url:"http://localhost/thirdparty/user/updateUserInfo",
						type:"POST",
						data:{
							"uid":uid,
							"token":token,
							"type":usertype,
							"email":email,
							"realName":realName,
							"idType":idType,
							"idCard":idCard,
							"address":address,
							"unit":unit,
							"phone":$("#mobile").val()
						},
						//dataType:"json",
						success:function(data){
							alert("修改成功");
						},
						error:function(data){
							alert("修改失败");
						}
					});
				}
				
				
	</script>
</html>