<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<title>仪器共享</title>
<meta name="keywords" content="科研仪器，生命科学仪器，环境检测仪器，实验常用设备，分析仪器">
<meta name="description"
	content="中国领先的科研仪器共享平台，让您快速找到各种类型的科学研究仪器。提升闲置仪器利用率，产生更大科研价值。涵盖：生命科学仪器、环境检测仪器、实验常用设备、分析仪器、仪表、物性测试、测量/计量仪器、在线及过程控制仪器。
">
<link rel="stylesheet" href="new_css/header_footer.css?t=1">
<link rel="stylesheet" href="new_css/accountSet.css">
<link rel="stylesheet" href="css/new_style.css">
<link rel="stylesheet" href="css/common.css">
<script src="js/jquery-1.11.3.js"></script>
<script>
	$(function() {
		$('#sun_btn').click(function(event){
			$('.modal2').show();
		});
		$('.pos_buttom').click(function(){
			$('.modal2').hide();
		});
		$('#sun_btn2').click(function(event){
			$('.modal1').show();
		});
		$('.pos_buttom').click(function(){
			$('.modal1').hide();
		});
		$('div[id="D"]').click(function(e){
			var target = e.target;
			$('div[id="D"]').each(function(e){
				if(target.id == 'D') {
					$(this).hide();
				}
			});
		})
		
		$('#front_header .header_wrap .right').hover(function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
			$('#front_header .header_wrap .right .out').show();
		}, function() {
			$('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
			$('#front_header .header_wrap .right .out').hide();
		});
		$('#sorts').on('mouseover', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon4.png');
		});
		$('#sorts').on('mouseout', 'li', function() {
			$(this).find('span img').attr('src', 'images/icon5.png');
		});

		$('.mainList .content .left li').click(function() {
			$('.mainList .content .left li').removeClass('selected');
			$(this).addClass('selected');
		});
		$('.mainList .content .left .list_header').click(function() {
			if ($('.mainList .content .left .secendList').is(':hidden')) {
				$('.mainList .content .left .secendList').slideDown();
				$('.mainList .content .left .list_header img').attr("src", "images/insUser_icon.png");
			} else {
				$('.mainList .content .left .secendList').slideUp();
				$('.mainList .content .left .list_header img').attr("src", "images/icon4.png");
			}
			;

		});
		$("#row5 font label").click(function() {
			$("#row5 font label img").attr('src', 'images/icon35.png');
			$("#row5 font label").siblings('input').prop('checked', false);
			$(this).find('img').attr('src', 'images/icon36.png');
			$(this).siblings('input').prop('checked', true);
		});
		$("#row8 font label").click(function() {
			$("#row8 font label img").attr('src', 'images/icon35.png');
			$("#row8 font label").siblings('input').prop('checked', false);
			$(this).find('img').attr('src', 'images/icon36.png');
			$(this).siblings('input').prop('checked', true);
		});
	});
</script>

</head>

<body style="background:#fff;">

<div class="wj_ma modal1" id="D" style="display: none;">
        	<div class="pos">
        	   <div class="pos_title">联系方式</div>
        	   <div class="middle_content">
        	   	   <div class="row">
        	   	   	<label>手机号</label><input type="text" value=""/>
        	   	   </div>
        	   	   <div class="row">
        	   	   	<label>验证码</label><input type="text" value=""/>
        	   	   </div>
        	   </div>
        	   <div class="pos_buttom">确定</div>
        	</div>
        </div>
        <div class="wj_ma modal2" id="D" style="display: none;">
        	<div class="pos">
        	   <div class="pos_title">联系方式</div>
        	   <div class="middle_content">
        	   	   <div class="row">
        	   	   	<label>旧密码</label><input type="text" value=""/>
        	   	   </div>
        	   	   <div class="row">
        	   	   	<label>新密码</label><input type="text" value=""/>
        	   	   </div>
        	   	   <div class="row">
        	   	   	<label>确认密码</label><input type="text" value=""/>
        	   	   </div>
        	   </div>
        	   <div class="pos_buttom">确定</div>
        	</div>
        </div>
<!-------------------------->

	<%@include file="/head.jsp"%>

	<div class="" id="nav_top">
		<div class="container">
			<div class="fl">
				<div class="logo ">
	<a class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>

	</div>
				<div class="web_name">用户中心</div>
			</div>
		</div>
	</div>
	<div class="mainList">
		<div class="bNav">
			您的位置：首页>用户中心><span>账号设置</span>
		</div>
		<div class="content">
			<%@include file="/left-tab.jsp"%>
			<div class="right">
				<div class="title">
					<span>账号设置</span>
				</div>
				<div class="box">
					<ul>
						<li class="row1"><label>登录账号</label> <input type="text"
							name="" id="in0" value="" placeholder="" disabled="disabled" />
							<!-- <a id="sun_btn2">更换手机号</a>
							<a id="sun_btn">修改密码</a> -->
						<li><label>电子邮箱</label> <input type="text" name="" id="in1"
							value="" placeholder="" disabled="disabled" /></li>
						<li><label>单位名称</label> <input type="text" name="" id="in2"
							value="" placeholder="请输入单位地址" /></li>
						<li><label>统一社会信用代码或组织机构代码</label> <input type="text" name=""
							id="in3" value="" placeholder="请输入机构代码" /></li>
						<li><label>单位类型</label> <input type="text" name="" id="in4"
							value="" placeholder="请输入单位类型" /></li>
						<li><label>单位注册地</label> <input type="text" name="" id="in5"
							value="" placeholder="请输入单位注册地" /></li>
						<li><label>单位性质</label> <input type="text" name="" id="in6"
							value="" placeholder="请输入单位性质" /></li>
						<li><label>企业注册资金</label> <input type="text" name="" id="in7"
							value="" placeholder="请输入企业资金" /></li>
						<li><label>企业注册时间</label> <input type="date" name="" id="in8"
							value="" placeholder="请输入企业注册时间" disabled="disabled"/></li>
						<li><label>企业资产负载率</label> <input type="text" name=""
							id="in9" value="" placeholder="请输入企业资产负载率" /></li>
						<li><label>单位地址</label> <input type="text" name="" id="in10"
							value="" placeholder="请输入单位地址" disabled="disabled"/></li>
						<li><label>用户姓名</label> <input type="text" name="" id="in11"
							value="" placeholder="请输入用户姓名" /></li>
						<li><label></label> <a id="sub" onclick="update()">保存</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="pageButton"></div>
		<!--找个分页插件插入-->
	</div>

	<%@include file="/footer.jsp"%>
	<script>
			$(document).ready(function(){
				//login()
				/* $("#sub").click(function(){
					var unitName = $("#in2").val();
					var unitCode = $("#in3").val();
					var utId = $("#in4").val();
					var domicile = $("#in5").val();
					var unId = $("#in6").val();
					var funds = $("#in7").val();
					var registerTime = $("#in8").val();
					var debtRatio = $("#in9").val();
					var unitAddress = $("#in10").val();
					var contacts = $("#in11").val();
					var id = $('#useridoflogin').val();
					$.ajax({
						url:Server + "/user/update",
						data:{
							"uuId":id,
							"uuUnitName":unitName,
							"uuUnitCode":unitCode,
							"utId":utId,
							"uuDomicile":domicile,
							"unId":unId,
							"uuFunds":funds,
							"uuRegisterTime":registerTime,
							"uuDebtRatio":debtRatio,
							"uuUnitAddress":unitAddress,
							"uuContacts":contacts,
						},
						type:"POST",
						dataType:"json",
						success : function(data){
							alert("保存成功")
						},
						error : function(msg) {
							alert("保存失败");
					}
					});
				}); */
				var uid = $("#useridoflogin").val();
				var usertype = $("#usertypeoflogin").val();
				var token = $("#token").val();
				getUser40(uid,usertype,token);
				getUuserInfo();
				function getUser40(uid,usertype,token){
					$.ajax({
						url:Server +"/thirdparty/user/getUserInfoById",
						type:"POST",
						data:{
							"uid":uid,
							"token":token,
							"type":usertype
						},
						dataType:"json",
						success:function(data){
							var headHtml = "";
							var userInfo = data["userInfo"];
							if(userInfo != "" && userInfo != null){
								$("#in0").val(userInfo['mobile']);
								$("#in1").val(userInfo['email']);
								$("#in2").val(userInfo['enterName']);
								$("#in3").val(userInfo['organCode']);
								$("#in8").val(new Date(userInfo['registTime']).Format("yyyy-MM-dd"));
								$("#in10").val(userInfo['address']);
								$("#in11").val(userInfo['name']);
							}
						},
						error:function(data){
							console.log("失败");
						}
					});
				}
				
				function getUuserInfo(){
					$.ajax({
						url:Server + "/per/suu/getUuserByUid",
						//url:"http://localhost/per/suu/getUuserByUid",
						type:"POST",
						data:{
							"uid":$("#useridoflogin").val()
							//"uid":"5cbc8e5610cc42fdaa3d03519caa999b"
						},
						dataType:"json",
						success:function(data){
							$("#in4").val(data.utId);
							$("#in5").val(data.uuDomicile);
							$("#in6").val(data.unId);
							$("#in7").val(data.uuFunds);
							$("#in9").val(data.uuDebtRatio);
						},
						error:function(data){
							//alert("获取本地用户信息失败");
						}
					}); 
				}
			});
			
			function update(){
					var uid = $("#useridoflogin").val();
					var usertype = $("#usertypeoflogin").val();
					var token = $("#token").val();
					var email = $("#in1").val();
					var unitName=$("#in2").val();
					var code=$("#in3").val();
					var unitType=$("#in4").val();
					var regPosition=$("#in5").val();
					var unitNature=$("#in6").val();
					var regCapital=$("#in7").val();
					var regTime=$("#in8").val();
					var loadRate=$("#in9").val();
					var unitAddress=$("#in10").val();
					var userName=$("#in11").val();
					if(email==""){
						alert("请输入邮箱");
						return;
					}
					if(unitName==""){
						alert("请输入单位名称");
						return;
					}
					if(code==""){
						alert("请输入统一社会信用代码或组织机构代码");
						return;
					}
					if(unitType==""){
						alert("请输入单位类型");
						return;
					}
					if(regPosition==""){
						alert("请输入单位注册地");
						return;
					}
					if(unitNature==""){
						alert("请输入单位性质");
						return;
					}
					if(regCapital==""){
						alert("请输入企业注册资金");
						return;
					}
					/* if(regTime==""){
						alert("请选择企业注册时间");
						return;
					} */
					if(loadRate==""){
						alert("请输入企业资产负载率");
						return;
					}
					/* if(unitAddress==""){
						alert("请输入单位地址");
						return;
					} */
					if(userName==""){
						alert("请输入用户姓名");
						return;
					}
					$.ajax({
						url:Server + "/thirdparty/user/updateUserInfo",
						//url:"http://localhost/thirdparty/user/updateUserInfo",
						type:"POST",
						data:{
							"uid":uid,
							"token":token,
							"type":usertype,
							"email":email,
							"unitName":unitName,
							"code":code,
							"unitType":unitType,
							"regPosition":regPosition,
							"unitNature":unitNature,
							"regCapital":regCapital,
							"regTime":regTime,
							"loadRate":loadRate,
							"unitAddress":unitAddress,
							"userName":userName,
						},
						//dataType:"json",
						success:function(data){
							alert(data);
						},
						error:function(data){
							alert("失败");
						}
					});
				}
			
			
			/* function login(){
				var id = $('#useridoflogin').val();
				$.ajax({
					url:Server + "/user/details",
					data:{
						"uuId":id,
					},
					type:"POST",
					dataType:"json",
					success : function(data){
						var phone = (data[0].uuPhone == null)?"":data[0].uuPhone;
						var email = (data[0].uuEmail == null)?"":data[0].uuEmail;
						var unitName = (data[0].uuUnitName == null)?"":data[0].uuUnitName;
						var unitCode = (data[0].uuUnitCode == null)?"":data[0].uuUnitCode;
						var utId = (data[0].utId == null)?"":data[0].utId;
						var domicile = (data[0].uuDomicile == null)?"":data[0].uuDomicile;
						var unId = (data[0].unId == null)?"":data[0].unId;
						var funds = (data[0].uuFunds == null)?"":data[0].uuFunds;
						var registerTime = (data[0].uuRegisterTime == null)?"":data[0].uuRegisterTime;
						var debtRatio = (data[0].uuDebtRatio == null)?"":data[0].uuDebtRatio;
						var unitAddress = (data[0].uuUnitAddress == null)?"":data[0].uuUnitAddress;
						var contacts = (data[0].uuContacts == null)?"":data[0].uuContacts;
						$("#in0").val(phone);
						$("#in1").val(email);
						$("#in2").val(unitName);
						$("#in3").val(unitCode);
						$("#in4").val(utId);
						$("#in5").val(domicile);
						$("#in6").val(unId);
						$("#in7").val(funds);
						$("#in8").val(new Date(registerTime).Format("yyyy-MM-dd"));
						$("#in9").val(debtRatio);
						$("#in10").val(unitAddress);
						$("#in11").val(contacts);
					}
				});
			} */
			Date.prototype.Format = function(fmt){
				var o = {
					"M+": this.getMonth() + 1, //月份 
	       			"d+": this.getDate(), //日 
	        		"h+": this.getHours(), //小时 
	        		"m+": this.getMinutes(), //分 
	        		"s+": this.getSeconds(), //秒 
	        		"q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	        		"S": this.getMilliseconds() //毫秒 
				};
				if(/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
				for(var k in o)
				if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	    		return fmt;
			}
			
			function Marine(){
				$(location).attr("href","new_file20");
			}
			
			function indes(){
				$(location).attr("href","index");
			}
		</script>
</body>

</html>