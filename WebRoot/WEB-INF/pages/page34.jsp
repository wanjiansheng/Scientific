    <%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
        <!DOCTYPE html>
        <html>
        <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" href="favicon.ico" type="image/x-icon"/>
        <title>成果共享</title>
        <meta name="keywords" content="科研成果，生命科学成果，环境检测成果，实验常用设备，分析成果">
        <meta name="description"
        content="中国领先的科研成果共享平台，让您快速找到各种类型的科学研究成果。提升闲置成果利用率，产生更大科研价值。涵盖：生命科学成果、环境检测成果、实验常用设备、分析成果、仪表、物性测试、测量/计量成果、在线及过程控制成果。">
        <link rel="stylesheet" href="../css/header_footer.css?t=1">
        <link rel="stylesheet" href="css/publishResults.css">
        <link rel="stylesheet" href="css/common.css">
        <style>
        input {
        border: 1px solid #efefef;
        border-radius: 5px;
        }</style>
            <style>
            /*.item.item1 p {
            font-size: 16px !important;
            }
            input {
            font-size: 16px !important;
            }
            textarea {
            font-size: 16px !important;
            }
            select {
            font-size: 16px !important;
            }*/

            </style>
        <script src="../js/jquery-1.11.3.js"></script>
        <script>
        $(function() {
        $('#front_header .header_wrap .right').hover(function() {
        $('#front_header .header_wrap .right .main .icon img').attr('src', '../images/icon2.png');
        $('#front_header .header_wrap .right .out').show();
        }, function() {
        $('#front_header .header_wrap .right .main .icon img').attr('src', '../images/icon1.png');
        $('#front_header .header_wrap .right .out').hide();
        });
        $('#sorts').on('mouseover', 'li', function() {
        $(this).find('span img').attr('src', '../images/icon4.png');
        });
        $('#sorts').on('mouseout', 'li', function() {
        $(this).find('span img').attr('src', '../images/icon5.png');
        });

        $('.mainList .content .left li').click(function() {
        $('.mainList .content .left li').removeClass('selected');
        $(this).addClass('selected');
        });
        $('.mainList .content .left .list_header').click(function() {
        if($('.mainList .content .left .secendList').is(':hidden')) {
        $('.mainList .content .left .secendList').slideDown();
        $('.mainList .content .left .list_header img').attr("src", "../images/insUser_icon.png");
        } else {
        $('.mainList .content .left .secendList').slideUp();
        $('.mainList .content .left .list_header img').attr("src", "../images/icon4.png");
        };

        });
        $("#row5 .selectBox .item1 label").click(function() {
        $("#row5 .selectBox .item1 label img").attr('src', '../images/icon35.png');
        $("#row5 .selectBox .item1 label").siblings('input').prop('checked', false);
        $(this).find('img').attr('src', '../images/icon36.png');
        $(this).siblings('input').prop('checked', true);
        });
        $("#row5 .selectBox .item2 label").click(function() {
        if($(this).siblings('input').is(':checked')){
        $(this).find('img').attr('src','../images/icon35.png');
        $(this).siblings('input').prop('checked',false);
        }else{
        $(this).find('img').attr('src','../images/icon36.png');
        $(this).siblings('input').prop('checked',true);
        };
        });

        });
        </script>

        </head>

        <body style="background:#fff;">

        <div id="front_header">
        <div class="header_wrap">
        <div class="fl left">
        <img src="../images/home.png" />
        <span>欢迎访问成果共享平台</span>
        </div>
        <div class="registeredLink">
        <a href="#this">注册</a>
        </div>

        <div class="fr right">
        <div class="main">
        <div class="user_pic fl"><img src="../images/user.png" /></div>
        <div class='user_name fl'>福建省水产研究所</div>
        <div class='icon fl'><img src="../images/icon1.png" /></div>
        </div>
        <div class="out">
        <div class="user_center">
        <a href="javascript:void(0)">用户中心</a>
        </div>
        <div class="log_out">
        <a href="#this">退出登录</a>
        </div>
        <div class="tri"><img src="../images/icon9.png" /></div>
        </div>
        </div>
        <div class="header_title">海洋装备</div>

        </div>
        </div>

        <div class="" id="nav_top">
        <div class="container">
        <div class="fl">
        <div class="logo ">
        <a href="#this" class="logo-text" style="font-size: 30px;color: rgb(255,138,0);" onclick="indes();">科研成果共享平台</a>

        </div>
        <div class="web_name">
        用户中心
        </div>
        </div>
        </div>
        </div>
        <div class="mainList">
        <div class="bNav">您的位置：首页>用户中心><span>发布需求</span></div>
        <div class="content">
        <div class="left">
        <ul>
        <li class="list_header">科研成果<img src="../images/insUser_icon.png" /></li>

        </ul>
        <ul class="secendList">
        <li class="selected">发布成果</li>
        <li>管理成果 </li>

        </ul>
        <ul>
        <li class="list">订单管理<img src="../images/icon4.png" /></li>
        <li class="list">海洋装备<img src="../images/icon4.png" /></li>
        <li class="list">科研需求<img src="../images/icon4.png" /></li>
        <li class="list">技术方案管理</li>
        <li class="list">账号设置</li>
        <li class="list">信用认证和交易</li>
        </ul>
        </div>
        <div class="right">
        <div class="title"><span class="selected">发布成果</span></div>
        <div class="box">
        <div class="box_title">产权信息</div>
        <div class="box_content">
        <div class="odd">
        <label>权属类型</label>
        <input type="text" name="" id="" value="" />
        <label>所属单位</label>
        <input type="text" name="" id="" value="" placeholder="请输入所属单位" />
        </div>
        <div class="odd">
        <label>知识产权编号</label>
        <input type="text" name="" id="" value="" />
        <label>获奖信息</label>
        <input type="text" name="" id="" value="" />
        </div>
        </div>
        <div class="box_border">
        <div></div>
        </div>
        </div>
        <div class="box">
        <div class="box_title">成果基本信息</div>
        <div class="box_content">
        <div class="even">
        <label>基本简介</label>
        <input type="text" name="" id="" value="" placeholder="请输入基本简介" />
        </div>
        <div class="odd">
        <label>成熟度</label>
        <input type="text" name="" id="" value="" />
        <label>成果名称</label>
        <input type="text" name="" id="" value="" placeholder="请输入成果名称" />
        </div>
        <div class="odd">
        <label>获奖情况</label>
        <input type="text" name="" id="" value="" />
        <label>技术水平</label>
        <input type="text" name="" id="" value="" />
        </div>
        <div class="row3">
        <label>行业类别</label>
        <select>
        <option>教育休闲</option>
        <option></option>
        </select>
        <img src="../images/publishDemandAdd.png" />
        </div>
        <div class="row4">
        <div class='rowLeft'></div>
        <div class='rowRight'>
        <div class="item">
        <input type="text" name="" id="" value="工艺美术" />
        <img src="../images/publishDemandReduce.png" />
        </div>
        <div class="item">
        <input type="text" name="" id="" value="工艺美术" />
        <img src="../images/publishDemandReduce.png" />
        </div>
        </div>

        </div>
        <div class="addPic">
        <label>图片</label>
        <input type="file" name="" id="add1" value="" />
        <label for="add1"><img src="../images/publishResultspic.png"/></label>
        </div>
        </div>
        <div class="box_border">
        <div></div>
        </div>
        </div>
        <div class="box">
        <div class="box_title">成果交付信息</div>
        <div class="box_content">
        <div class="row5" id="row5">
        <label>交付方式</label>
        <div class="selectBox">
        <div>
        <div class="item item1">
        <input type="radio" name="b" id="" value="" />
        <label><img src="../images/icon35.png"/><span>完全转让</span></label>
        <input type="text" name="" id="" value="" />
        <p>万元</p>
        </div>
        <div class="item item2">
        <input type="checkbox" name="" id="" value="" />
        <label><img src="../images/icon35.png"/><span>面议</span></label>
        </div>
        </div>
        <div>
        <div class="item item1">
        <input type="radio" name="b" id="" value="" />
        <label><img src="../images/icon35.png"/><span>许可转让</span></label>
        <input type="text" name="" id="" value="" />
        <p>万元</p>
        </div>
        <div class="item item2">
        <input type="checkbox" name="" id="" value="" />
        <label><img src="../images/icon35.png"/><span>面议</span></label>
        </div>
        </div>
        <div>
        <div class="item item1">
        <input type="radio" name="b" id="" value="" />
        <label><img src="../images/icon35.png"/><span>技术入股</span></label>
        <input type="text" name="" id="" value="" />
        <p>万元</p>
        </div>
        <div class="item item2">
        <input type="checkbox" name="" id="" value="" />
        <label><img src="../images/icon35.png"/><span>面议</span></label>
        </div>
        </div>
        </div>
        </div>
        <div class="odd odd2">
        <div>
        <label>最低价格</label>
        <input type="text" name="" id="" value="" />
        <span>万元</span>
        </div>
        <div>
        <label>最高价格</label>
        <input type="text" name="" id="" value="" />
        <span>万元</span>
        </div>
        </div>
        <div class="even">
        <label>交易注意事项</label>
        <input type="text" name="" id="" value="" placeholder="请输入基本简介" />
        </div>

        </div>
        <div class="box_border">
        <div></div>
        </div>
        </div>
        <div class="box">
        <div class="box_title">成果联系人详细信息</div>
        <div class="box_content">
        <div class="odd">
        <label>联系姓名</label>
        <input type="text" name="" id="" value="" />
        <label>联系电话</label>
        <input type="text" name="" id="" value="" placeholder="请输入所属单位" />
        </div>
        <div class="odd">
        <label>联系手机</label>
        <input type="text" name="" id="" value="" />
        <label>传真</label>
        <input type="text" name="" id="" value="" />
        </div>
        <div class="odd">
        <label>Email</label>
        <input type="text" name="" id="" value="" />
        <label>地址</label>
        <input type="text" name="" id="" value="" />
        </div>
        </div>
        <div class="box_border">
        <div></div>
        </div>
        </div>
        <div class="box">
        <div class="box_title">团队详细信息</div>
        <div class="box_content">
        <div class="even">
        <label>团队信息</label>
        <input type="text" name="" id="" value="" placeholder="请输入团队信息" />
        </div>
        <div class="odd">
        <label>团队带头人</label>
        <input type="text" name="" id="" value="" />
        </div>
        <div class="addPic">
        <label>团队图片</label>
        <input type="file" name="" id="add1" value="" />
        <label for="add1"><img src="../images/publishResultspic.png"/></label>
        </div>
        </div>
        </div>
        <div class="box">
        <div class="box_content">
        <div class="even btn">
        <label></label>
        <a href="#this">提交</a>
        <a href="#this">保存草稿</a>
        </div>
        </div>
        </div>
        </div>
        </div>
        <div class="pageButton"></div>
        <!--找个分页插件插入-->
        </div>

        <%@include file="/footer.jsp"%>



        </body>

        </html>