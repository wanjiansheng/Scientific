
$(function (){
    $("#search-btn").click(function(){
      var act = $("#header_form").attr('action');
      if(act.indexOf('apparatus') != -1){
        var url = act + '-' + $("input[name='ky']").val();
        location.href=url;
      }else{
        $("#header_form").submit();
      }
      
    })
  // 头部Tab方法的调用
    headTab();
    $('#front_header').on( 'mouseover','.drop-down >.tab>li.words',function(){ 
        $('.tab-content .news-list').show(); 
        $('.tab-content .messages').hide();
        $('.drop-down >.tab>li.words').css({
            'background':'url(/images/new-gout.png)  no-repeat',
            'background-position':'70px 0',
            'background-repeat':'no-repeat'
        })
        $('.drop-down >.tab>li.envelope').css({
            'background':'url(/images/xin.png)  no-repeat',
            'background-position':'64px 0',
            'background-repeat':'no-repeat'
        })
    })

    $('#front_header').on('mouseover','.drop-down >.tab>li.envelope', function () {
        $('.tab-content .messages').show();
        $('.tab-content .news-list').hide();
        $('.drop-down >.tab>li.envelope').css({
            'background':'url(/images/new-xin.png)  no-repeat',
            'background-position':'64px 0',
            'background-repeat':'no-repeat'
        })
        $('.drop-down >.tab>li.words').css({
            'background':'url(/images/gouT.png)  no-repeat',
            'background-position':'70px 0',
            'background-repeat':'no-repeat'
        })
    })


    $.ajax({
        type:'get',
        url:'/account/is_login',
        dataType:'json',
        success:function(json){
            $("#front_header").html(json.data);
            uid = json.status;
            if(json.status){
                get_unread_im_message_nums();
            }
            get_area()
        },
        error:function(){

        }
    })

    // 城市切换

    $("#front_header").on("mouseenter",".container .site",function(){
        $(".nav_city").addClass("hover");
        $(".city").css("display","block");
    })
    $("#front_header").on("mouseleave",".container .site",function(){
        $(".nav_city").removeClass("hover");
        $(".city").css("display","none");
    });
    $("#front_header").on("click", ".city_list li" , function(){
        $.ajax({
            type:'post',
            url:'/ajax/change_area',
            data:{name:$(this).text()},
            success:function(){
                console.log('切换成功');
                location.reload();
            }
        })
    })

    $("#front_header").on("click", ".clear_area", function(){
        $.ajax({
            type:'get',
            url:'/ajax/clear_area',
            data:{name:$(this).text()},
            success:function(){
                console.log('切换成功');
                location.reload();
            }
        })
    })

    window._pt_lt = new Date().getTime();
    window._pt_sp_2 = [];
    _pt_sp_2.push('setAccount,74aa9a1a');
    var _protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    (function() {
        var atag = document.createElement('script'); atag.type = 'text/javascript'; atag.async = true;
        atag.src = _protocol + 'js.ptengine.cn/74aa9a1a.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(atag, s);
    })();
});

function get_unread_im_message_nums() {
    $.get(api_url+'/publicApi/get_message',{uid:uid},function(json){
        var message_total=0;
        var system_message_nums = json.data.system_message_nums;
            var system_message_html = '';
        if(system_message_nums[0]){
            for(var i in system_message_nums){
                system_message_html += '<li><a target="_blank" href="'+task_url+'/person/message">'+system_message_nums[i].title+'</a></li>';
                message_total++;
            }
        }else{
            system_message_html += '<li class="no-news">暂无消息</li>';
        }

        $(".new-information").html(system_message_html);

        var unread_im_message_nums = json.data.unread_im_message_nums;
        var unread_im_message_html = '';
        if(unread_im_message_nums[0]){
            for(var j in unread_im_message_nums){
                unread_im_message_html += '<li>';
                unread_im_message_html += '<a target="_blank" href="'+task_url+'/IM/index?gid='+unread_im_message_nums[j].group_id+'">'+unread_im_message_nums[j].group_name;
                if(unread_im_message_nums[j].un_read_nums > 0){
                    unread_im_message_html += '<span class="fr">'+unread_im_message_nums[j].un_read_nums+'</span>';
                }
                unread_im_message_html += '</a>';
                if(unread_im_message_nums[j].last_message != null){
                    unread_im_message_html += '<p>'+unread_im_message_nums[j].last_message.content+'</p> ';
                }
                
                unread_im_message_html += '</li>';
                message_total += unread_im_message_nums[j].un_read_nums;
                var im_href = ''+task_url+'/IM/index?gid='+unread_im_message_nums[j].group_id;
            }
        }else{
            unread_im_message_html += '<li class="no-news">暂无消息</li>';
        }
        
        $(".header_im_message").html(unread_im_message_html);
        if(im_href){
            $(".im_look_all").attr('href', im_href);
        }

        if(message_total){
            $("#msg-notice").show();
            $("#msg-notice").text(message_total);
        }

        // 定时获取未读信息
        setTimeout(function(){
            get_unread_im_message_nums();
        },60000)
    },'json'); 
}

// 头部Tab
function headTab() {
  $( '.choose li' ).each( function ( index ) {
    $( this ).on( {
//      'mouseover': function () {
//        $(this).addClass('visited').siblings().removeClass('visited');
//      },
      'click': function () {
        $(this).addClass('visited').siblings().removeClass('visited');
        if( index == 0 ) {
          $("#header_form").attr('action','/apparatus/lists');
          $(this).parent().next().prop( 'placeholder', '请输入想要的仪器名称或单位名称');
        } else if ( index == 1 ) {
          $("#header_form").attr('action',task_url+'/service');
          $(this).parent().next().prop( 'placeholder', '请输入想要的服务名称或单位名称');
        } else {
          $("#header_form").attr('action',task_url+'/demand/lists');
          $(this).parent().next().prop( 'placeholder', '请输入检索关键词');
        }
      }
    } );
  } );
}

function get_area () {
    $.ajax({
        type:'get',
        url:'/ajax/get_area',
        dataType:'json',
        success:function(res){
            if(res.status == 1){
                $(".nav_city .select").text(res.data);
                $(".local-city-current div.fl").html('<span>当前选择：<span class="before">'+res.data+'</span></span>');
                var html = '<script src="http://www.yikexue.com/account/sync_area_state?area_state='+res.data+'"></script>';
                html += '<script src="http://task.yikexue.com/account/sync_area_state?area_state='+res.data+'"></script>';
                $("body").append(html);
            }else if(res.status == 2){
                $(".local-city-current div.fl .before").html(res.data);
                $(".local-city-current .fr").hide();
            }
        }
    })
}
function show_info(msg, class_name,url){
    if(class_name==""){
        class_name='error'
    }
    if(url==""){
        url=''
    }
    $(".alert").css('display','block');
    $("#alert_content").addClass(class_name);
    $("#alert_content .err_content").html(msg);
    clearInterval(intervalObj);
    intervalObj = setTimeout(function(){
        $(".alert").hide();
        $("#alert_content").removeClass(class_name);
        $("#alert_content .err_content").html('');
        jump_url = url;
        if(url){
            location.href = url;
        }
    },2000)
    return false;
}

function action_jiankong(action) {

    $.ajax({
        type:'post',
        url:api_url+'/publicApi/user_action',
        data:{uid:uid,ip:ip,action:action},
        success:function(){},
        error:function(){}
    })
}