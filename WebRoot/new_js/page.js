/**
 * Created by win7 on 2016/1/26.
 */
function page(){
    this.menu=function(){
        var ishas = true;
        /*展现菜单*/
        $("#mobileMenuBtn").on("click",function(){
            if(ishas){
                $(".head .nav_box").slideDown();
                $("#mobileMenuBtn").css({
                    "background":"url('images/btn_side-close.png') no-repeat",
                    "background-size":"100% 100%"
                });
                $(".head .shade").fadeIn();
                $(".shade").on("click", function() {
                    $(this).fadeOut();
                    $(".head .nav_box").slideUp();
                    $("#mobileMenuBtn").css({
                        "background":"url('images/nav-icon.png') no-repeat",
                        "background-size":"100% 100%"
                    });
                });
                ishas=false;
            }
            else{
                ishas=true;
                $(".head .nav_box").slideUp();
                $("#mobileMenuBtn").css({
                    "background":"url('images/nav-icon.png') no-repeat",
                    "background-size":"100% 100%"
                });
                $(".head .shade").fadeOut();
            }
        });
    };

    this.bannerHeigth=function(){
        var win_h=407;
        var head_h=$(".service_head").height();
        var win_w=$(window).width();
        if(win_w>1024){
            $(".banner .owl-carousel").css("height",407);
            $(".banner .owl-carousel .item").css("height",407);
            $(".banner .owl-carousel .item>a").css("height",407);
        }
        else{
            $(".banner .owl-carousel").css("height",win_h*0.6);
            $(".banner .owl-carousel .item").css("height",win_h*0.6);
            $(".banner .owl-carousel .item>a").css("height",win_h*0.6);
        }

    };
    this.owl=function(){
        $('#owl-demo').owlCarousel({
            items: 1,
            singleItem: true,
            autoPlay: true,
            pagination: true,
            navigation: true,
            navigationText: ["",""]
        });
        $('#owl-event').owlCarousel({
            items: 3,
            autoPlay: false,
            itemsDesktop:[1199,3],
            itemsDesktopSmall:[960,1],
            itemsTablet:[768,1],
            lazyLoad: true,
            pagination: false,
            navigation: true,
            navigationText: ["",""]
        });
    };
    this.scrollEnetr=function(){
        if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))){
            var wow = new WOW({
                boxClass: 'wow',
                animateClass: 'animated',
                offset: 150,
                mobile: true,
                live: true
            });
        };
        wow.init();
    };
    this.video=function(){
        html5media.flowplayerSwf = "other/flowplayer-3.2.18.swf";
        html5media.flowplayerControlsSwf = "other/flowplayer.controls-3.2.16.swf";
    };
    this.showNav=function(){
        var isshow=true;
        $(".catg_hide p").on("click",function(){
            if(isshow){
                $(".catg_hide").css("background-image","url(images/down2.png)");
                $(".TopNav").slideDown();
                isshow=false;
            }
            else{
                $(".catg_hide").css("background-image","url(images/down.png)");
                $(".TopNav").slideUp();
                isshow=true;
            }
        })

    }
}