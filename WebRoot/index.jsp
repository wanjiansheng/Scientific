<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!-- 我爱你 -->
<html>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


        <title>成果共享</title>

        <meta name="keywords" content="科研成果，生命科学成果，环境检测成果，实验常用设备，分析成果">


<meta name="description"
	content="中国领先的科研成果共享平台，让您快速找到各种类型的科学研究成果。提升闲置成果利用率，产生更大科研价值。涵盖：生命科学成果、环境检测成果、实验常用设备、分析成果、仪表、物性测试、测量/计量成果、在线及过程控制成果。
        ">


<!-- 引入 Bootstrap 的 CSS 文件 -->


<link rel="stylesheet" href="css/header_footer.css?t=1">
<link rel="stylesheet" href="css/index.css?t=201711101207">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="css/sunstyle.css" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/main_style.css" />
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="css/common.css" />
	<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.2.1.1.min.js"></script>

<style type="text/css">
/*#banner_container{
        border-top: 2px solid rgb(255,138,0);
        }*/
.title {
	width: 100%;
	border-bottom: 2px solid #ff8a00;
	/*margin-bottom: 8px;*/
	margin-bottom: 0px !important;
}

.title .wrap {
	width: 1200px;
	margin: auto;
}

.title .selected {
	color: #fff;
	background: #ff8a00;
}

.title a {
	width: 173px;
	height: 38px;
	display: inline-block;
	text-align: center;
	line-height: 38px;
	font-size: 18px;
}

.title_first {
	border-bottom: 2px solid rgb(255, 138, 0);
}
</style>


<script src="js/jquery-1.11.3.js"></script>

<!-- 引入 Bootstrap 的 JS 文件 -->

<script src="js/bootstrap.min.js"></script>

<!--[if lt IE 9]><![endif]-->

<script src="js/html5shiv.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/page.js"></script>

<script src="js/respond.min.js"></script>

<script src="js/baidu.js"></script>
<script>
        $(function() {
        $(".search1").click(function(){
        $(".sun_menu").toggle();
        });
        $(".sun_menu>li").click(function(){
        var sun =$(this).text();
        console.log(sun);
        $("#chose_name").text(sun);
        /*
        $("#chose_name").text();
        */
        });


        /*--------- */
        $(".tab_span1").click(function(){
        $(this).addClass('selected').siblings().removeClass('selected');
        $("#tabView1").css("display","block");
        $("#tabView2").css("display","none");
        });


        $(".tab_span2").click(function(){
        $(this).addClass('selected').siblings().removeClass('selected');
        $("#tabView2").css("display","block");
        $("#tabView1").css("display","none");
        });
        $('#front_header .header_wrap .right').hover(function() {
        $('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon2.png');
        $('#front_header .header_wrap .right .out').show();
        }, function() {
        $('#front_header .header_wrap .right .main .icon img').attr('src', 'images/icon1.png');
        $('#front_header .header_wrap .right .out').hide();
        });
        $('#sorts').on('mouseover', 'li', function() {
        $(this).find('span img').attr('src', 'images/icon4.png');
        });


        $('#sorts').on('mouseout', 'li', function() {
        $(this).find('span img').attr('src', 'images/icon5.png');
        });
        });
        </script>
</head>
<body>
	<%		
		String uid = request.getParameter("uid");
		if(uid != null){
			request.getSession().setAttribute("uid", request.getParameter("uid"));
			request.getSession().setAttribute("token", request.getParameter("token"));
			request.getSession().setAttribute("usertype", request.getParameter("usertype"));
		}
	%>


	<%@include file="/head.jsp"%>

	<div class="logo-search-btn">
		<div class="logo-search">
			<!--left-->
			<div class="header-logo header-logo-change fl">
				<h1>
					<a href="index.jsp">科研成果共享平台</a>
				</h1>
			</div>
			<!--right-->
			<div class="header-search fl">
				<div class="searchOption fl">
	<p class="search-down" >
	<a id="claa">科研成果</a>

	<span> <img class="" src="images/sy_arrow_down.png"></span>
	</p>
	<ul id="ulofsousuo" class="navbar-down" style="display: none">
	<li ><a id="clab" onclick="myFunction()" >科研成果</a> </li>
	<li><a id="clab1" onclick="myFunction1()" >科研需求</a> </li>
	<li><a id="clab2" onclick="myFunction2()" >专家展示</a> </li>
	</ul>
				</div>
				<div class="searchText fl">
					<input id="likeString" type="text" placeholder="关键词">
					<input id="findelikestringnumber" value="1" type="text" style="display:none"></input>
				</div>
				<div class="searchBtn fl" onclick="searchButton()">
					<img src="images/sy_seach.png"> <span>搜索</span>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>

	<script>
	$(document).ready(function(){
		if($("#findelikestringnumber").val() == 2){
			$("#claa").html("科研需求");
		}else if($("#findelikestringnumber").val() == 3){
			$("#claa").html("专家展示");
		}else{
			$("#claa").html("科研成果");
		}
	})
	</script>
	
	
	
	
	<script>
        var api_url = "http://115.28.133.212:8089";

        var img_url = 'http://www.ceshifan.com/task';

        var task_url = 'http://task.yikexue.com';

        var uid = 0;

        var ip = "222.212.199.65";
        </script>


	<script src="js/public.js?t=3"></script>
	<div class="" id="instrument">
	    <div class="clear"></div>
		<div class="navBar-nav">
	<nav>
	<ul>
	<li class="active-one" style="padding:0"><a href="index.jsp" id="shouye" class="selected">科研成果</a></li>
	<li><a href="new_file7" id="myself">科研需求展示</a></li>
	<li><a href="scienic_expertsshow11" id="zhuanjia">专家展示</a></li>
	<li><a href="new_file14" id="allNeed">交易大厅</a></li>
	<li><a href="new_file15" id="nanti">难题发布</a></li>
	</ul>
	</nav>
	</div>
	    <div class="clear"></div>
		<div id="banner_container"
			style=" width: 1200px;margin: 0 auto;margin-bottom:20px">


			<div class="container">

				<ul id="sorts"></ul>


				<div class="banner rowFluid"
					style="position:
        absolute;top:0;left:0;z-index:0;width:790px;height:350px;margin-left:182px">
					<div id="owl-demo" class="owl-carousel">
						<div class="item"
							style="background: url(/Ocean/Scientific/images/advertising/201903141850314551150.png) center; text-align: center; background-size:cover;">
							<a href="#" style="width: 100%; display: block;"></a>
						</div>
						<div class="item"
							style="background: url(images/banner3.jpg) center; text-align: center; background-size:cover;">
							<a href="#" style="width: 100%; display: block;"></a>
						</div>
						<div class="item"
							style="background: url(images/banner2.jpg) center; text-align: center; background-size:cover;">
							<a href="#" style="width: 100%; display: block;"></a>
						</div>
					</div>
				</div>


				<div id="current_publish" class="fr">
					<div style="height: 50px;line-height: 50px;">
						<div class="tab_span1 selected"
							style="line-height: 30px;padding:3px 8px;font-size:14px;display: inline-block;">
							最新发布需求</div>
						<div class="tab_span2"
							style="line-height: 30px;padding:3px 8px;font-size:14px;display: inline-block;">
							最新科研成果</div>

					</div>

					<div class="content" id="tabView1">
						<div id="new_demand" class="machine_demand">
							<div class="demands" id="needofcomm">
								<input id="needid" style="display:none"></input>
								<h5 id="need1" style="text-align:left"></h5>
								<div id="need2" style="width: 210px;word-wrap: break-word">
								</div>
								<h5 id="need3" style="text-align:right"></h5>
							</div>

						</div>
					</div>
					<div class="content" id="tabView2" style="display:none;">
						<div id="new_demand" class="machine_demand">
							<div class="demands" id="inofofcomm">
								<input id="infoid" style="display:none"></input>
								<h5 id="info1" style="text-align:left"></h5>
								<div id="info2" style="width: 210px;word-wrap: break-word;overflow: hidden;
	height: 14em; display: -webkit-box;
	-webkit-box-orient: vertical;
	-webkit-line-clamp: 10;">
	
								</div>
								<h5 id="info3" style="text-align:right"></h5>
							</div>
						</div>
					</div>
				</div>
				<script>
        $("#needofcomm").click(function(){
        $(location).attr('href', 'new_file8?needId=' + $("#needid").val() + '');
        })
        $("#inofofcomm").click(function(){
        $(location).attr('href', 'result_details18?id=' + $("#infoid").val() + '');
        })

        </script>
			</div>
		</div>
	</div>

	<div class="four_img">
		<div class="four_img_box">
			<div class="img_box">
				<img src="image/sy_img1.png" />
				<div class="img_box_content">
					<div class="content_left">
						<img src="image/sy_jscg.png" />
					</div>
					<div class="content_right">
						<p class="word1">技术成果</p>
						<p class="word2">
							<span id="numbermax0" style="color: rgb(221,239,66) !important;">66666</span>项
						</p>
					</div>
				</div>
			</div>
			<div class="img_box">
				<img src="image/sy_img2.png" />
				<div class="img_box_content">
					<div class="content_left">
						<img src="image/sy_qyxm.png" />
					</div>
					<div class="content_right">
						<p class="word1">签约项目</p>
						<p class="word2">
							<span id="numbermax1" style="color: rgb(221,239,66) !important;">66666</span>项
						</p>
					</div>
				</div>
			</div>
			<div class="img_box">
				<img src="image/sy_img3.png" />
				<div class="img_box_content">
					<div class="content_left">
						<img src="image/sy_fwqy.png" />
					</div>
					<div class="content_right">
						<p class="word1">服务企业</p>
						<p class="word2">
							<span id="numbermax2" style="color: rgb(221,239,66) !important;">66666</span>项
						</p>
					</div>
				</div>
			</div>
			<div class="img_box">
				<img src="image/sy_img4.png" />
				<div class="img_box_content">
					<div class="content_left">
						<img src="image/sy_jyze.png" />
					</div>
					<div class="content_right">
						<p class="word1">交易总额</p>
						<p class="word2">
							<span id="numbermax3" style="color: rgb(221,239,66);">666</span>万元
						</p>
					</div>
				</div>
			</div>

		</div>
	</div>


	<!-- 成果专区-->

	<div id="machines_wrap">
		<div class="container" id="machines">
			<ul class="item line" id="tuijianinfo1">

			</ul>
			<ul class="item line" id="tuijianinfo2">

			</ul>
			<ul class="item line" id="tuijianinfo3">

			</ul>


		</div>
	</div>
	   <style>
        /**{
            margin: 0px;
            padding: 0px;
            border: 0px;
        }*/
	<style>
	footer {
	clear: both;
	width: 100%;
	height: 130px;
	bottom: 0;
	line-height: 0px;
	background-color: #F9F9F9;
	border-top: 2px solid #FF8A00;
	display: flex;
	justify-content: center;
	align-items: center;
	}
	.foot ul li{
	display: inline-flex;
	align-items: center;
	margin-left: 20px;
	margin-top: 25px;
	}
	footer .foot {
	text-align: center;
	line-height: 40px;
	}
	footer .foot p {
	color: #202020;
	font-size: 14px;
	}

    </style>

	
 
	<!--footer-->
	<footer style="clear: both;
	width: 100%;
	height: 130px;
	bottom: 0;
	line-height: 0px;
	background-color: #F9F9F9;
	border-top: 2px solid #FF8A00;
	display: flex;
	justify-content: center;
	align-items: center">
	<div class="foot clearfix">
	<ul class="fl">
	<li>今日访问量：<span id="today">5643264785960</span></li>
	<li>总访问量：<span id="total">5643264785960</span></li>
	<p>闽ICP备16030544号-1  热线电话：0592-5937233  服务时间：周一至周五 8：00-18:00 </p>
	</ul>
	<div class="fl" style="margin-left: 20px; margin-top: 10px;">
	<img src="images/qcode.png" width="100" height="100">
	</div>
	</div>
	</footer>

	<style>
/*侧边栏的设计*/
#newBridge .icon-right-center {
	right: 0 !important;
	bottom: 285px !important;
	height: 105px !important;
}

#newBridge .nb-icon-base .nb-icon-inner-wrap {
	border-radius: 0 !important;
}

#sidebar {
	position: fixed;
	top: 50%;
	right: 0px;
	z-index: 2000;
	width: auto;
	margin-top: 63px;
}

#sidebar>div.aside {
	float: right;
	clear: both;
	margin-bottom: 1px;
}

#sidebar>div>p {
	color: #333;
	width: 50px;
	margin-right: 0;
	position: relative;
	z-index: 100;
	height: 50px;
	padding: 10px;
	font-size: 14px;
	letter-spacing: 1px;
	line-height: 15px;
	background: #eee;
	cursor: pointer;
	margin-bottom: 0;
}

#sidebar>div>a, #sidebar>div>span {
	color: #596b75;
	display: inline-block;
	width: 172px;
	height: 50px;
	padding: 10px;
	font-size: 14px;
	letter-spacing: 1px;
	line-height: 30px;
	cursor: pointer;
	position: absolute;
	padding-right: 0;
	padding-left: 54px;
	background: url(images/tel2.png) 12px 13px no-repeat;
	background-color: #eeeeee;
	left: 60px;
	opacity: 0;
}

#sidebar>div>span:hover {
	background-color: #eeeeee;
	color: #fca947;
}

#sidebar>div>a.qq {
	background: url(images/qq2.png) 12px 13px no-repeat;
	background-color: #eeeeee;
}

#sidebar>div>a.vip {
	background: url(images/vip-side2.png) 12px 13px no-repeat;
	background-color: #eeeeee;
}

#sidebar>div>a.qq:hover {
	background-color: #eeeeee;
	text-decoration: none;
	color: #fca947;
}

#sidebar>div>a.vip:hover {
	background-color: #eeeeee;
	text-decoration: none;
	color: #fca947;
}

#sidebar>div>p.TOtop {
	background: url(images/top_a.png) 12px no-repeat;
	background-color: #eee;
}

#sidebar>div>p.TOtop:hover {
	height: 50px;
	background: url(images/top_b.png) 12px no-repeat;
	background-color: #f60;
}

#sidebar>div>p.vip {
	background: url(images/vip_service.png) no-repeat;
	background-size: 100% 100%;
}

#sidebar>div>p.vip:hover {
	background: url(images/vip-b.png) 12px no-repeat;
	background-color: #f60;
	color: #f60;
	text-indent: 100%;
	white-space: nowrap;
	overflow: hidden;
}

#sidebar>div>p.aside {
	background: url(images/fwrexian.png) no-repeat;
	background-size: 100% 100%;
}

#sidebar>div>p.aside:hover {
	background: url(images/tell-b.png) 12px no-repeat;
	background-color: #f60;
	color: #f60;
	text-indent: 100%;
	white-space: nowrap;
	overflow: hidden;
}
</style>


	<script src="method/hhn/my.js"></script>
	<script src="method/hhn/loadUser.js"></script>
	<script type="text/javascript" src="js/common.js"></script>

	<!--<script src="js/index.js?t=201709201011"></script>-->
	<script>
        var html = new page();
        html.menu();
        html.bannerHeigth();
        html.owl();
        html.scrollEnetr();
        </script>
        <script>
        	 $(function (){
        		carousel();
                  //使用bind的绑定事件,跟上上面的效果是一样的
                  $(".search-down").bind("mouseover", function (){
                      var flag = $(".navbar-down").is(":none");
                      if(flag){
                          $(".navbar-down").show();
                          
                      }else{
                          $(".navbar-down").hide();
                      }
                  });

          });
        	
        </script>
        <script>
        	$(document).ready(function(){
        		$.getScript({
        			url:"http://localhost:8080/../MyStudent/js",
        			success: function(){
   						alert("sssssssssss");   			
        			}
        		})
        	})
        	
        	function carousel() {
        		$.ajax({
        			url:Server + "/fon/carousel",
        			type:"POST",
        			success: function(data) {
        				var carousel = '';
        				var navigation = '';
        				for (var i = 0; i < data.length; i++) {
        					var sign = "";
	        				/* carousel += '<a href='+data[i].ADURL+' style="width: 100%; display: block;"><div class="item"\
								style="background: url('+data[i].ADIMG+') center; text-align: center; background-size:cover;">\
								</a>\
							</div>'; */
							carousel += '<div class="owl-item" style="width: 790px;">\
							<div class="item" style="background: url('+data[i].ADIMG+') center center / cover; text-align: center; height: 350px;">\
							<a href='+data[i].ADURL+' style="width: 100%; display: block; height: 350px;"></a>\
						</div></div>';
							if(i == 0) {
								sign = "active";
							}
							navigation += '<div class="owl-page '+sign+'"><span class=""></span></div>';
        				}
						$(".owl-wrapper").html(carousel);
						$(".owl-pagination").html(navigation);
        			}
        		});
        	}
        </script>


        <script type="text/javascript">

$(document).ready(function(e) {
    $("#link").click(function(e) {
        $("#thediv").toggle();
    });
			$.ajax({
					url :Server + "/visitRecord/getRecord",
					//url :"http://localhost/visitRecord/getRecord",
					data : {
					},
					type : "POST",
					dataType: "JSON",
					success : function(data) {
						$("#total").text(data.allRecord);
						$("#today").text(data.todayRecord);
						//$(".foot").append("<p>总访问量："+data.allRecord+"  今日访问量："+data.todayRecord+"</p>");
					},
			});	
});


</script>

</body>

</html>