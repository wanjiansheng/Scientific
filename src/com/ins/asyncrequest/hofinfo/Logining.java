package com.ins.asyncrequest.hofinfo;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Logining extends HttpServlet {

	private String user = "F";
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		response.setContentType("text/html");
		ServletContext sc = getServletContext();
		RequestDispatcher rd = null;
		String m = (String) request.getParameter("innumber");
		String inid =(String) request.getParameter("inid");
		if(m == null && inid == null){
			if(request.getSession().getAttribute("userid").toString() != null){
				HttpSession session=request.getSession();
				session.setAttribute("userid", request.getSession().getAttribute("userid"));
				session.setAttribute("usertype", request.getSession().getAttribute("usertype"));//0：个人用户
				session.setAttribute("user", user);//用户sesscion授权
				request.getRequestDispatcher("/index.jsp").forward(request,response);
				return;
			}
		}
		// 企事业
		if (m.equals("1")) {
			HttpSession session=request.getSession();
			session.setAttribute("userid", inid);
			session.setAttribute("usertype", "0");//0：个人用户
			session.setAttribute("user", user);//用户sesscion授权
			request.getRequestDispatcher("/index.jsp").forward(request,response);
			Writer writer = response.getWriter();
			return;
		}
		// 个人
		if (m.equals("2")) {
			HttpSession session=request.getSession();
			session.setAttribute("userid", inid);
			session.setAttribute("usertype", "1");//1：企事业用户
			session.setAttribute("user", user);//用户sesscion授权
			request.getRequestDispatcher("/index.jsp").forward(request,response);
			Writer writer = response.getWriter();
			return;
		}
		// 专家
		if (m.equals("3")) {
			HttpSession session=request.getSession();
			session.setAttribute("userid", inid);
			session.setAttribute("usertype", "2");//2：专家
			session.setAttribute("user", user);//用户sesscion授权
			request.getRequestDispatcher("/index.jsp").forward(request,response);
			Writer writer = response.getWriter();
			return;
		}
		request.getRequestDispatcher("/login.jsp").forward(request, response);
		Writer writer = response.getWriter();
	}

}
