/**
 * Copyright (C), 2018-2018,
 * FileName: publishResults34
 * Author:   Administrator
 * Date:     2018/6/26 10:48
 * Description: 发布成果
 * History:
 * <author>          <time>          <version>          <desc>
 * xhh         2018/6/26 10:48           1.0.0              描述
 */
package com.ins.asyncrequest;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

public class publishResults34 extends HttpServlet {

    /**
     * Constructor of the object.
     */
    public publishResults34() {
        super();
    }

    /**
     * Destruction of the servlet. <br>
     */
    public void destroy() {
        super.destroy(); // Just puts "destroy" string in log
        // Put your code here
    }

    /**
     * The doGet method of the servlet. <br>
     *
     * This method is called when a form has its tag value method equals to get.
     *
     * @param request the request send by the client to the server
     * @param response the response send by the server to the client
     * @throws ServletException if an error occurred
     * @throws IOException if an error occurred
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        ServletContext sc = getServletContext();
        RequestDispatcher rd = null;

        rd = sc.getRequestDispatcher("/WEB-INF/pages/publishResults34.jsp");

        rd.forward(request, response);

        Writer writer = response.getWriter();
    }

    /**
     * The doPost method of the servlet. <br>
     *
     * This method is called when a form has its tag value method equals to post.
     *
     * @param request the request send by the client to the server
     * @param response the response send by the server to the client
     * @throws ServletException if an error occurred
     * @throws IOException if an error occurred
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doGet(request, response);
    }

    /**
     * Initialization of the servlet. <br>
     *
     * @throws ServletException if an error occurs
     */
    public void init() throws ServletException {
        // Put your code here
    }

}