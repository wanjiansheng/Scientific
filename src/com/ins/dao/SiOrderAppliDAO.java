package com.ins.dao;

import com.ins.domain.SiOrderAppli;
import com.ins.hibernate.BaseHibernateDAO;
import java.math.BigDecimal;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 	* A data access object (DAO) providing persistence and search support for SiOrderAppli entities.
 			* Transaction control of the save(), update() and delete() operations 
		can directly support Spring container-managed transactions or they can be augmented	to handle user-managed Spring transactions. 
		Each of these methods provides additional information for how to configure it for the desired type of transaction control. 	
	 * @see com.ins.domain.SiOrderAppli
  * @author MyEclipse Persistence Tools 
 */
public class SiOrderAppliDAO extends BaseHibernateDAO  {
	     private static final Logger log = LoggerFactory.getLogger(SiOrderAppliDAO.class);
		//property constants
	public static final String USE = "use";
	public static final String WAY = "way";
	public static final String FUNDING_SOURCE = "fundingSource";
	public static final String PRINPICAL = "prinpical";
	public static final String PIRNPICAL_TEL = "pirnpicalTel";
	public static final String TIME = "time";
	public static final String PROJECT_NAME = "projectName";



    
    public void save(SiOrderAppli transientInstance) {
        log.debug("saving SiOrderAppli instance");
        try {
            getSession().save(transientInstance);
            log.debug("save successful");
        } catch (RuntimeException re) {
            log.error("save failed", re);
            throw re;
        }
    }
    
	public void delete(SiOrderAppli persistentInstance) {
        log.debug("deleting SiOrderAppli instance");
        try {
            getSession().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }
    
    public SiOrderAppli findById( java.lang.Long id) {
        log.debug("getting SiOrderAppli instance with id: " + id);
        try {
            SiOrderAppli instance = (SiOrderAppli) getSession()
                    .get("com.ins.test.SiOrderAppli", id);
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    
    public List findByExample(SiOrderAppli instance) {
        log.debug("finding SiOrderAppli instance by example");
        try {
            List results = getSession()
                    .createCriteria("com.ins.test.SiOrderAppli")
                    .add(Example.create(instance))
            .list();
            log.debug("find by example successful, result size: " + results.size());
            return results;
        } catch (RuntimeException re) {
            log.error("find by example failed", re);
            throw re;
        }
    }    
    
    public List findByProperty(String propertyName, Object value) {
      log.debug("finding SiOrderAppli instance with property: " + propertyName
            + ", value: " + value);
      try {
         String queryString = "from SiOrderAppli as model where model." 
         						+ propertyName + "= ?";
         Query queryObject = getSession().createQuery(queryString);
		 queryObject.setParameter(0, value);
		 return queryObject.list();
      } catch (RuntimeException re) {
         log.error("find by property name failed", re);
         throw re;
      }
	}

	public List findByUse(Object use
	) {
		return findByProperty(USE, use
		);
	}
	
	public List findByWay(Object way
	) {
		return findByProperty(WAY, way
		);
	}
	
	public List findByFundingSource(Object fundingSource
	) {
		return findByProperty(FUNDING_SOURCE, fundingSource
		);
	}
	
	public List findByPrinpical(Object prinpical
	) {
		return findByProperty(PRINPICAL, prinpical
		);
	}
	
	public List findByPirnpicalTel(Object pirnpicalTel
	) {
		return findByProperty(PIRNPICAL_TEL, pirnpicalTel
		);
	}
	
	public List findByTime(Object time
	) {
		return findByProperty(TIME, time
		);
	}
	
	public List findByProjectName(Object projectName
	) {
		return findByProperty(PROJECT_NAME, projectName
		);
	}
	

	public List findAll() {
		log.debug("finding all SiOrderAppli instances");
		try {
			String queryString = "from SiOrderAppli";
	         Query queryObject = getSession().createQuery(queryString);
			 return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}
	
    public SiOrderAppli merge(SiOrderAppli detachedInstance) {
        log.debug("merging SiOrderAppli instance");
        try {
            SiOrderAppli result = (SiOrderAppli) getSession()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public void attachDirty(SiOrderAppli instance) {
        log.debug("attaching dirty SiOrderAppli instance");
        try {
            getSession().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
    
    public void attachClean(SiOrderAppli instance) {
        log.debug("attaching clean SiOrderAppli instance");
        try {
                      	getSession().buildLockRequest(LockOptions.NONE).lock(instance);
          	            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
}