package com.ins.dao;

import com.ins.domain.SiOrder;
import com.ins.hibernate.BaseHibernateDAO;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 	* A data access object (DAO) providing persistence and search support for SiOrder entities.
 			* Transaction control of the save(), update() and delete() operations 
		can directly support Spring container-managed transactions or they can be augmented	to handle user-managed Spring transactions. 
		Each of these methods provides additional information for how to configure it for the desired type of transaction control. 	
	 * @see com.ins.domain.SiOrder
  * @author MyEclipse Persistence Tools 
 */
public class SiOrderDAO extends BaseHibernateDAO  {
	     private static final Logger log = LoggerFactory.getLogger(SiOrderDAO.class);
		//property constants
	public static final String NUMBER = "number";
	public static final String PAYMENT = "payment";
	public static final String STATUS = "status";
	public static final String PAYMENT_TYPE = "paymentType";
	public static final String CU_ID = "cuId";
	public static final String MESSAGE = "message";
	public static final String RATE = "rate";
	public static final String IN_NAME = "inName";
	public static final String ORDER_TYPE = "orderType";



    
    public void save(SiOrder transientInstance) {
        log.debug("saving SiOrder instance");
        try {
            getSession().save(transientInstance);
            log.debug("save successful");
        } catch (RuntimeException re) {
            log.error("save failed", re);
            throw re;
        }
    }
    
	public void delete(SiOrder persistentInstance) {
        log.debug("deleting SiOrder instance");
        try {
            getSession().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }
    
    public SiOrder findById( java.lang.Long id) {
        log.debug("getting SiOrder instance with id: " + id);
        try {
            SiOrder instance = (SiOrder) getSession()
                    .get("com.ins.test.SiOrder", id);
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    
    public List findByExample(SiOrder instance) {
        log.debug("finding SiOrder instance by example");
        try {
            List results = getSession()
                    .createCriteria("com.ins.test.SiOrder")
                    .add(Example.create(instance))
            .list();
            log.debug("find by example successful, result size: " + results.size());
            return results;
        } catch (RuntimeException re) {
            log.error("find by example failed", re);
            throw re;
        }
    }    
    
    public List findByProperty(String propertyName, Object value) {
      log.debug("finding SiOrder instance with property: " + propertyName
            + ", value: " + value);
      try {
         String queryString = "from SiOrder as model where model." 
         						+ propertyName + "= ?";
         Query queryObject = getSession().createQuery(queryString);
		 queryObject.setParameter(0, value);
		 return queryObject.list();
      } catch (RuntimeException re) {
         log.error("find by property name failed", re);
         throw re;
      }
	}

	public List findByNumber(Object number
	) {
		return findByProperty(NUMBER, number
		);
	}
	
	public List findByPayment(Object payment
	) {
		return findByProperty(PAYMENT, payment
		);
	}
	
	public List findByStatus(Object status
	) {
		return findByProperty(STATUS, status
		);
	}
	
	public List findByPaymentType(Object paymentType
	) {
		return findByProperty(PAYMENT_TYPE, paymentType
		);
	}
	
	public List findByCuId(Object cuId
	) {
		return findByProperty(CU_ID, cuId
		);
	}
	
	public List findByMessage(Object message
	) {
		return findByProperty(MESSAGE, message
		);
	}
	
	public List findByRate(Object rate
	) {
		return findByProperty(RATE, rate
		);
	}
	
	public List findByInName(Object inName
	) {
		return findByProperty(IN_NAME, inName
		);
	}
	
	public List findByOrderType(Object orderType
	) {
		return findByProperty(ORDER_TYPE, orderType
		);
	}
	

	public List findAll() {
		log.debug("finding all SiOrder instances");
		try {
			String queryString = "from SiOrder";
	         Query queryObject = getSession().createQuery(queryString);
			 return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}
	
    public SiOrder merge(SiOrder detachedInstance) {
        log.debug("merging SiOrder instance");
        try {
            SiOrder result = (SiOrder) getSession()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public void attachDirty(SiOrder instance) {
        log.debug("attaching dirty SiOrder instance");
        try {
            getSession().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
    
    public void attachClean(SiOrder instance) {
        log.debug("attaching clean SiOrder instance");
        try {
                      	getSession().buildLockRequest(LockOptions.NONE).lock(instance);
          	            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
}