package com.ins.dao;

import com.ins.domain.SiControlUser;
import com.ins.hibernate.BaseHibernateDAO;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * SiControlUser entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.ins.domain.SiControlUser
 * @author MyEclipse Persistence Tools
 */
public class SiControlUserDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(SiControlUserDAO.class);
	// property constants
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String REALNAME = "realname";
	public static final String ID_CARD = "idCard";
	public static final String UNIT_NAME = "unitName";
	public static final String POST = "post";
	public static final String PHONE = "phone";
	public static final String EMAIL = "email";
	public static final String LOGIN_COUNT = "loginCount";
	public static final String CHECK_RESULT = "checkResult";
	public static final String OU_ID = "ouId";
	public static final String ROLE_ID = "roleId";

	public void save(SiControlUser transientInstance) {
		log.debug("saving SiControlUser instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(SiControlUser persistentInstance) {
		log.debug("deleting SiControlUser instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public SiControlUser findById(java.lang.Long id) {
		log.debug("getting SiControlUser instance with id: " + id);
		try {
			SiControlUser instance = (SiControlUser) getSession().get("com.ins.test.SiControlUser", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(SiControlUser instance) {
		log.debug("finding SiControlUser instance by example");
		try {
			List results = getSession().createCriteria("com.ins.test.SiControlUser").add(Example.create(instance))
					.list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding SiControlUser instance with property: " + propertyName + ", value: " + value);
		try {
			String queryString = "from SiControlUser as model where model." + propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUsername(Object username) {
		return findByProperty(USERNAME, username);
	}

	public List findByPassword(Object password) {
		return findByProperty(PASSWORD, password);
	}

	public List findByRealname(Object realname) {
		return findByProperty(REALNAME, realname);
	}

	public List findByIdCard(Object idCard) {
		return findByProperty(ID_CARD, idCard);
	}

	public List findByUnitName(Object unitName) {
		return findByProperty(UNIT_NAME, unitName);
	}

	public List findByPost(Object post) {
		return findByProperty(POST, post);
	}

	public List findByPhone(Object phone) {
		return findByProperty(PHONE, phone);
	}

	public List findByEmail(Object email) {
		return findByProperty(EMAIL, email);
	}

	public List findByLoginCount(Object loginCount) {
		return findByProperty(LOGIN_COUNT, loginCount);
	}

	public List findByCheckResult(Object checkResult) {
		return findByProperty(CHECK_RESULT, checkResult);
	}

	public List findByOuId(Object ouId) {
		return findByProperty(OU_ID, ouId);
	}

	public List findByRoleId(Object roleId) {
		return findByProperty(ROLE_ID, roleId);
	}

	public List findAll() {
		log.debug("finding all SiControlUser instances");
		try {
			String queryString = "from SiControlUser";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public SiControlUser merge(SiControlUser detachedInstance) {
		log.debug("merging SiControlUser instance");
		try {
			SiControlUser result = (SiControlUser) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(SiControlUser instance) {
		log.debug("attaching dirty SiControlUser instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(SiControlUser instance) {
		log.debug("attaching clean SiControlUser instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}