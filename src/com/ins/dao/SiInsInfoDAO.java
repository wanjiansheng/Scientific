package com.ins.dao;

import com.ins.domain.SiInsInfo;
import com.ins.hibernate.BaseHibernateDAO;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * SiInsInfo entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.ins.domain.SiInsInfo
 * @author MyEclipse Persistence Tools
 */
public class SiInsInfoDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(SiInsInfoDAO.class);
	// property constants
	public static final String SERIAL = "serial";
	public static final String ASSET = "asset";
	public static final String PRICE = "price";
	public static final String IT_ID = "itId";
	public static final String IU_ID = "iuId";
	public static final String CHINESE_NAME = "chineseName";
	public static final String ENGLISH_NAME = "englishName";
	public static final String ORGINAL = "orginal";
	public static final String TOTAL = "total";
	public static final String COUNT = "count";
	public static final String MAKE = "make";
	public static final String CU_ID = "cuId";
	public static final String SOURCE = "source";
	public static final String ADDRESS = "address";
	public static final String _ISTATUS = "IStatus";
	public static final String INFO_SRC = "infoSrc";
	public static final String ISCOM = "iscom";
	public static final String MODEL = "model";
	public static final String INS_PARAM = "insParam";
	public static final String INS_DESC = "insDesc";
	public static final String SAMPLE_REQUIRED = "sampleRequired";
	public static final String CAUTIONS = "cautions";
	public static final String VALID_STARTTIME = "validStarttime";
	public static final String VALID_ENDTIME = "validEndtime";
	public static final String BOOT_UP_TIME = "bootUpTime";
	public static final String RUN_STATUS = "runStatus";

	public void save(SiInsInfo transientInstance) {
		log.debug("saving SiInsInfo instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(SiInsInfo persistentInstance) {
		log.debug("deleting SiInsInfo instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public SiInsInfo findById(java.lang.Long id) {
		log.debug("getting SiInsInfo instance with id: " + id);
		try {
			SiInsInfo instance = (SiInsInfo) getSession().get("com.ins.test.SiInsInfo", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(SiInsInfo instance) {
		log.debug("finding SiInsInfo instance by example");
		try {
			List results = getSession().createCriteria("com.ins.test.SiInsInfo").add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding SiInsInfo instance with property: " + propertyName + ", value: " + value);
		try {
			String queryString = "from SiInsInfo as model where model." + propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findBySerial(Object serial) {
		return findByProperty(SERIAL, serial);
	}

	public List findByAsset(Object asset) {
		return findByProperty(ASSET, asset);
	}

	public List findByPrice(Object price) {
		return findByProperty(PRICE, price);
	}

	public List findByItId(Object itId) {
		return findByProperty(IT_ID, itId);
	}

	public List findByIuId(Object iuId) {
		return findByProperty(IU_ID, iuId);
	}

	public List findByChineseName(Object chineseName) {
		return findByProperty(CHINESE_NAME, chineseName);
	}

	public List findByEnglishName(Object englishName) {
		return findByProperty(ENGLISH_NAME, englishName);
	}

	public List findByOrginal(Object orginal) {
		return findByProperty(ORGINAL, orginal);
	}

	public List findByTotal(Object total) {
		return findByProperty(TOTAL, total);
	}

	public List findByCount(Object count) {
		return findByProperty(COUNT, count);
	}

	public List findByMake(Object make) {
		return findByProperty(MAKE, make);
	}

	public List findByCuId(Object cuId) {
		return findByProperty(CU_ID, cuId);
	}

	public List findBySource(Object source) {
		return findByProperty(SOURCE, source);
	}

	public List findByAddress(Object address) {
		return findByProperty(ADDRESS, address);
	}

	public List findByIStatus(Object IStatus) {
		return findByProperty(_ISTATUS, IStatus);
	}

	public List findByInfoSrc(Object infoSrc) {
		return findByProperty(INFO_SRC, infoSrc);
	}

	public List findByIscom(Object iscom) {
		return findByProperty(ISCOM, iscom);
	}

	public List findByModel(Object model) {
		return findByProperty(MODEL, model);
	}

	public List findByInsParam(Object insParam) {
		return findByProperty(INS_PARAM, insParam);
	}

	public List findByInsDesc(Object insDesc) {
		return findByProperty(INS_DESC, insDesc);
	}

	public List findBySampleRequired(Object sampleRequired) {
		return findByProperty(SAMPLE_REQUIRED, sampleRequired);
	}

	public List findByCautions(Object cautions) {
		return findByProperty(CAUTIONS, cautions);
	}

	public List findByValidStarttime(Object validStarttime) {
		return findByProperty(VALID_STARTTIME, validStarttime);
	}

	public List findByValidEndtime(Object validEndtime) {
		return findByProperty(VALID_ENDTIME, validEndtime);
	}

	public List findByBootUpTime(Object bootUpTime) {
		return findByProperty(BOOT_UP_TIME, bootUpTime);
	}

	public List findByRunStatus(Object runStatus) {
		return findByProperty(RUN_STATUS, runStatus);
	}

	public List findAll() {
		log.debug("finding all SiInsInfo instances");
		try {
			String queryString = "from SiInsInfo";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public SiInsInfo merge(SiInsInfo detachedInstance) {
		log.debug("merging SiInsInfo instance");
		try {
			SiInsInfo result = (SiInsInfo) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(SiInsInfo instance) {
		log.debug("attaching dirty SiInsInfo instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(SiInsInfo instance) {
		log.debug("attaching clean SiInsInfo instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}