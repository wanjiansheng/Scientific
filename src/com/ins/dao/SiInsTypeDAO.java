package com.ins.dao;

import com.ins.domain.SiInsType;
import com.ins.hibernate.BaseHibernateDAO;
import java.math.BigDecimal;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * SiInsType entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.ins.domain.SiInsType
 * @author MyEclipse Persistence Tools
 */
public class SiInsTypeDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(SiInsTypeDAO.class);
	// property constants
	public static final String NAME = "name";
	public static final String _PID = "PId";
	public static final String DESC = "desc";
	public static final String IS_DISPLAY = "isDisplay";

	public void save(SiInsType transientInstance) {
		log.debug("saving SiInsType instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(SiInsType persistentInstance) {
		log.debug("deleting SiInsType instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public SiInsType findById(java.lang.Long id) {
		log.debug("getting SiInsType instance with id: " + id);
		try {
			SiInsType instance = (SiInsType) getSession().get("com.ins.test.SiInsType", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(SiInsType instance) {
		log.debug("finding SiInsType instance by example");
		try {
			List results = getSession().createCriteria("com.ins.test.SiInsType").add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding SiInsType instance with property: " + propertyName + ", value: " + value);
		try {
			String queryString = "from SiInsType as model where model." + propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List findByPId(Object PId) {
		return findByProperty(_PID, PId);
	}

	public List findByDesc(Object desc) {
		return findByProperty(DESC, desc);
	}

	public List findByIsDisplay(Object isDisplay) {
		return findByProperty(IS_DISPLAY, isDisplay);
	}

	public List findAll() {
		log.debug("finding all SiInsType instances");
		try {
			String queryString = "from SiInsType";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public SiInsType merge(SiInsType detachedInstance) {
		log.debug("merging SiInsType instance");
		try {
			SiInsType result = (SiInsType) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(SiInsType instance) {
		log.debug("attaching dirty SiInsType instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(SiInsType instance) {
		log.debug("attaching clean SiInsType instance");
		try {
			getSession().buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}