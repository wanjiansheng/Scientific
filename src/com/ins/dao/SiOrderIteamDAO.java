package com.ins.dao;

import com.ins.domain.SiOrderIteam;
import com.ins.hibernate.BaseHibernateDAO;
import java.util.List;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 	* A data access object (DAO) providing persistence and search support for SiOrderIteam entities.
 			* Transaction control of the save(), update() and delete() operations 
		can directly support Spring container-managed transactions or they can be augmented	to handle user-managed Spring transactions. 
		Each of these methods provides additional information for how to configure it for the desired type of transaction control. 	
	 * @see com.ins.domain.SiOrderIteam
  * @author MyEclipse Persistence Tools 
 */
public class SiOrderIteamDAO extends BaseHibernateDAO  {
	     private static final Logger log = LoggerFactory.getLogger(SiOrderIteamDAO.class);
		//property constants
	public static final String NUMBER = "number";
	public static final String PRICE = "price";
	public static final String SERVICE_ID = "serviceId";



    
    public void save(SiOrderIteam transientInstance) {
        log.debug("saving SiOrderIteam instance");
        try {
            getSession().save(transientInstance);
            log.debug("save successful");
        } catch (RuntimeException re) {
            log.error("save failed", re);
            throw re;
        }
    }
    
	public void delete(SiOrderIteam persistentInstance) {
        log.debug("deleting SiOrderIteam instance");
        try {
            getSession().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }
    
    public SiOrderIteam findById( java.lang.Long id) {
        log.debug("getting SiOrderIteam instance with id: " + id);
        try {
            SiOrderIteam instance = (SiOrderIteam) getSession()
                    .get("com.ins.test.SiOrderIteam", id);
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    
    public List findByExample(SiOrderIteam instance) {
        log.debug("finding SiOrderIteam instance by example");
        try {
            List results = getSession()
                    .createCriteria("com.ins.test.SiOrderIteam")
                    .add(Example.create(instance))
            .list();
            log.debug("find by example successful, result size: " + results.size());
            return results;
        } catch (RuntimeException re) {
            log.error("find by example failed", re);
            throw re;
        }
    }    
    
    public List findByProperty(String propertyName, Object value) {
      log.debug("finding SiOrderIteam instance with property: " + propertyName
            + ", value: " + value);
      try {
         String queryString = "from SiOrderIteam as model where model." 
         						+ propertyName + "= ?";
         Query queryObject = getSession().createQuery(queryString);
		 queryObject.setParameter(0, value);
		 return queryObject.list();
      } catch (RuntimeException re) {
         log.error("find by property name failed", re);
         throw re;
      }
	}

	public List findByNumber(Object number
	) {
		return findByProperty(NUMBER, number
		);
	}
	
	public List findByPrice(Object price
	) {
		return findByProperty(PRICE, price
		);
	}
	
	public List findByServiceId(Object serviceId
	) {
		return findByProperty(SERVICE_ID, serviceId
		);
	}
	

	public List findAll() {
		log.debug("finding all SiOrderIteam instances");
		try {
			String queryString = "from SiOrderIteam";
	         Query queryObject = getSession().createQuery(queryString);
			 return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}
	
    public SiOrderIteam merge(SiOrderIteam detachedInstance) {
        log.debug("merging SiOrderIteam instance");
        try {
            SiOrderIteam result = (SiOrderIteam) getSession()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public void attachDirty(SiOrderIteam instance) {
        log.debug("attaching dirty SiOrderIteam instance");
        try {
            getSession().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
    
    public void attachClean(SiOrderIteam instance) {
        log.debug("attaching clean SiOrderIteam instance");
        try {
                      	getSession().buildLockRequest(LockOptions.NONE).lock(instance);
          	            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
}