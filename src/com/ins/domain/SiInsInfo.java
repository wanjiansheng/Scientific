package com.ins.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * SiInsInfo entity. @author MyEclipse Persistence Tools
 */

public class SiInsInfo implements java.io.Serializable {

	// Fields

	private Long id;
	private BigDecimal number;
	private String serial;
	private String asset;
	private Double price;
	private Long itId;
	private Long iuId;
	private String chineseName;
	private String englishName;
	private String orginal;
	private Double total;
	private Long count;
	private Date manufactureTime;
	private String make;
	private Date buyTime;
	private Long cuId;
	private Date createTime;
	private String source;
	private String address;
	private Date enable;
	private String IStatus;
	private String infoSrc;
	private Long iscom;
	private String model;
	private BigDecimal serviceTime;
	private String insParam;
	private String insDesc;
	private String sampleRequired;
	private String cautions;
	private String validStarttime;
	private String validEndtime;
	private String bootUpTime;
	private Long runStatus;
	private BigDecimal insStatus;

	// Constructors

	/** default constructor */
	public SiInsInfo() {
	}

	/** full constructor */
	public SiInsInfo(BigDecimal number, String serial, String asset, Double price, Long itId, Long iuId,
			String chineseName, String englishName, String orginal, Double total, Long count, Date manufactureTime,
			String make, Date buyTime, Long cuId, Date createTime, String source, String address, Date enable,
			String IStatus, String infoSrc, Long iscom, String model, BigDecimal serviceTime, String insParam,
			String insDesc, String sampleRequired, String cautions, String validStarttime, String validEndtime,
			String bootUpTime, Long runStatus, BigDecimal insStatus) {
		this.number = number;
		this.serial = serial;
		this.asset = asset;
		this.price = price;
		this.itId = itId;
		this.iuId = iuId;
		this.chineseName = chineseName;
		this.englishName = englishName;
		this.orginal = orginal;
		this.total = total;
		this.count = count;
		this.manufactureTime = manufactureTime;
		this.make = make;
		this.buyTime = buyTime;
		this.cuId = cuId;
		this.createTime = createTime;
		this.source = source;
		this.address = address;
		this.enable = enable;
		this.IStatus = IStatus;
		this.infoSrc = infoSrc;
		this.iscom = iscom;
		this.model = model;
		this.serviceTime = serviceTime;
		this.insParam = insParam;
		this.insDesc = insDesc;
		this.sampleRequired = sampleRequired;
		this.cautions = cautions;
		this.validStarttime = validStarttime;
		this.validEndtime = validEndtime;
		this.bootUpTime = bootUpTime;
		this.runStatus = runStatus;
		this.insStatus = insStatus;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getNumber() {
		return this.number;
	}

	public void setNumber(BigDecimal number) {
		this.number = number;
	}

	public String getSerial() {
		return this.serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getAsset() {
		return this.asset;
	}

	public void setAsset(String asset) {
		this.asset = asset;
	}

	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Long getItId() {
		return this.itId;
	}

	public void setItId(Long itId) {
		this.itId = itId;
	}

	public Long getIuId() {
		return this.iuId;
	}

	public void setIuId(Long iuId) {
		this.iuId = iuId;
	}

	public String getChineseName() {
		return this.chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public String getEnglishName() {
		return this.englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getOrginal() {
		return this.orginal;
	}

	public void setOrginal(String orginal) {
		this.orginal = orginal;
	}

	public Double getTotal() {
		return this.total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Long getCount() {
		return this.count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Date getManufactureTime() {
		return this.manufactureTime;
	}

	public void setManufactureTime(Date manufactureTime) {
		this.manufactureTime = manufactureTime;
	}

	public String getMake() {
		return this.make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public Date getBuyTime() {
		return this.buyTime;
	}

	public void setBuyTime(Date buyTime) {
		this.buyTime = buyTime;
	}

	public Long getCuId() {
		return this.cuId;
	}

	public void setCuId(Long cuId) {
		this.cuId = cuId;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getEnable() {
		return this.enable;
	}

	public void setEnable(Date enable) {
		this.enable = enable;
	}

	public String getIStatus() {
		return this.IStatus;
	}

	public void setIStatus(String IStatus) {
		this.IStatus = IStatus;
	}

	public String getInfoSrc() {
		return this.infoSrc;
	}

	public void setInfoSrc(String infoSrc) {
		this.infoSrc = infoSrc;
	}

	public Long getIscom() {
		return this.iscom;
	}

	public void setIscom(Long iscom) {
		this.iscom = iscom;
	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public BigDecimal getServiceTime() {
		return this.serviceTime;
	}

	public void setServiceTime(BigDecimal serviceTime) {
		this.serviceTime = serviceTime;
	}

	public String getInsParam() {
		return this.insParam;
	}

	public void setInsParam(String insParam) {
		this.insParam = insParam;
	}

	public String getInsDesc() {
		return this.insDesc;
	}

	public void setInsDesc(String insDesc) {
		this.insDesc = insDesc;
	}

	public String getSampleRequired() {
		return this.sampleRequired;
	}

	public void setSampleRequired(String sampleRequired) {
		this.sampleRequired = sampleRequired;
	}

	public String getCautions() {
		return this.cautions;
	}

	public void setCautions(String cautions) {
		this.cautions = cautions;
	}

	public String getValidStarttime() {
		return this.validStarttime;
	}

	public void setValidStarttime(String validStarttime) {
		this.validStarttime = validStarttime;
	}

	public String getValidEndtime() {
		return this.validEndtime;
	}

	public void setValidEndtime(String validEndtime) {
		this.validEndtime = validEndtime;
	}

	public String getBootUpTime() {
		return this.bootUpTime;
	}

	public void setBootUpTime(String bootUpTime) {
		this.bootUpTime = bootUpTime;
	}

	public Long getRunStatus() {
		return this.runStatus;
	}

	public void setRunStatus(Long runStatus) {
		this.runStatus = runStatus;
	}

	public BigDecimal getInsStatus() {
		return this.insStatus;
	}

	public void setInsStatus(BigDecimal insStatus) {
		this.insStatus = insStatus;
	}
}