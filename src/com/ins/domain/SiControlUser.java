package com.ins.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * SiControlUser entity. @author MyEclipse Persistence Tools
 */

public class SiControlUser implements java.io.Serializable {

	// Fields

	private Long id;
	private String username;
	private String password;
	private String realname;
	private String idCard;
	private String unitName;
	private String post;
	private String phone;
	private String email;
	private Date createTime;
	private Date loginTime;
	private Long loginCount;
	private Short checkResult;
	private Date checkTime;
	private Long ouId;
	private Long roleId;
	private Set siInsNotices = new HashSet(0);

	// Constructors

	/** default constructor */
	public SiControlUser() {
	}

	/** full constructor */
	public SiControlUser(String username, String password, String realname, String idCard, String unitName, String post,
			String phone, String email, Date createTime, Date loginTime, Long loginCount, Short checkResult,
			Date checkTime, Long ouId, Long roleId, Set siInsNotices) {
		this.username = username;
		this.password = password;
		this.realname = realname;
		this.idCard = idCard;
		this.unitName = unitName;
		this.post = post;
		this.phone = phone;
		this.email = email;
		this.createTime = createTime;
		this.loginTime = loginTime;
		this.loginCount = loginCount;
		this.checkResult = checkResult;
		this.checkTime = checkTime;
		this.ouId = ouId;
		this.roleId = roleId;
		this.siInsNotices = siInsNotices;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRealname() {
		return this.realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getIdCard() {
		return this.idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getUnitName() {
		return this.unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getPost() {
		return this.post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getLoginTime() {
		return this.loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Long getLoginCount() {
		return this.loginCount;
	}

	public void setLoginCount(Long loginCount) {
		this.loginCount = loginCount;
	}

	public Short getCheckResult() {
		return this.checkResult;
	}

	public void setCheckResult(Short checkResult) {
		this.checkResult = checkResult;
	}

	public Date getCheckTime() {
		return this.checkTime;
	}

	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}

	public Long getOuId() {
		return this.ouId;
	}

	public void setOuId(Long ouId) {
		this.ouId = ouId;
	}

	public Long getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Set getSiInsNotices() {
		return this.siInsNotices;
	}

	public void setSiInsNotices(Set siInsNotices) {
		this.siInsNotices = siInsNotices;
	}

}