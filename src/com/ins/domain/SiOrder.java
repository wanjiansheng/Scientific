package com.ins.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * SiOrder entity. @author MyEclipse Persistence Tools
 */

public class SiOrder  implements java.io.Serializable {


    // Fields    

     private Long id;
     private String number;
     private Double payment;
     private Short status;
     private Short paymentType;
     private Date createTime;
     private Date updateTime;
     private Date paymentTime;
     private Date endTime;
     private Date closeTime;
     private Long cuId;
     private String message;
     private Short rate;
     private String inName;
     private Long orderType;

    // Constructors

    /** default constructor */
    public SiOrder() {
    }

    
    /** full constructor */
    public SiOrder(String number, Double payment, Short status, Short paymentType, Date createTime, Date updateTime, Date paymentTime, Date endTime, Date closeTime, Long cuId, String message, Short rate, String inName, Long orderType) {
        this.number = number;
        this.payment = payment;
        this.status = status;
        this.paymentType = paymentType;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.paymentTime = paymentTime;
        this.endTime = endTime;
        this.closeTime = closeTime;
        this.cuId = cuId;
        this.message = message;
        this.rate = rate;
        this.inName = inName;
        this.orderType = orderType;
    }

   
    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return this.number;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }

    public Double getPayment() {
        return this.payment;
    }
    
    public void setPayment(Double payment) {
        this.payment = payment;
    }

    public Short getStatus() {
        return this.status;
    }
    
    public void setStatus(Short status) {
        this.status = status;
    }

    public Short getPaymentType() {
        return this.paymentType;
    }
    
    public void setPaymentType(Short paymentType) {
        this.paymentType = paymentType;
    }

    public Date getCreateTime() {
        return this.createTime;
    }
    
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }
    
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getPaymentTime() {
        return this.paymentTime;
    }
    
    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    public Date getEndTime() {
        return this.endTime;
    }
    
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCloseTime() {
        return this.closeTime;
    }
    
    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public Long getCuId() {
        return this.cuId;
    }
    
    public void setCuId(Long cuId) {
        this.cuId = cuId;
    }

    public String getMessage() {
        return this.message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }

    public Short getRate() {
        return this.rate;
    }
    
    public void setRate(Short rate) {
        this.rate = rate;
    }

    public String getInName() {
        return this.inName;
    }
    
    public void setInName(String inName) {
        this.inName = inName;
    }

    public Long getOrderType() {
        return this.orderType;
    }
    
    public void setOrderType(Long orderType) {
        this.orderType = orderType;
    }


	@Override
	public String toString() {
		return "SiOrder [id=" + id + ", number=" + number + ", payment=" + payment + ", status=" + status
				+ ", paymentType=" + paymentType + ", createTime=" + createTime + ", updateTime=" + updateTime
				+ ", paymentTime=" + paymentTime + ", endTime=" + endTime + ", closeTime=" + closeTime + ", cuId="
				+ cuId + ", message=" + message + ", rate=" + rate + ", inName=" + inName + ", orderType=" + orderType
				+ "]";
	}

}