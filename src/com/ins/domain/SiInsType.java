package com.ins.domain;

import java.math.BigDecimal;

/**
 * SiInsType entity. @author MyEclipse Persistence Tools
 */

public class SiInsType implements java.io.Serializable {

	// Fields

	private Long id;
	private String name;
	private Long PId;
	private String desc;
	private BigDecimal code;
	private Short isDisplay;

	// Constructors

	/** default constructor */
	public SiInsType() {
	}

	/** full constructor */
	public SiInsType(String name, Long PId, String desc, BigDecimal code, Short isDisplay) {
		this.name = name;
		this.PId = PId;
		this.desc = desc;
		this.code = code;
		this.isDisplay = isDisplay;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPId() {
		return this.PId;
	}

	public void setPId(Long PId) {
		this.PId = PId;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public BigDecimal getCode() {
		return this.code;
	}

	public void setCode(BigDecimal code) {
		this.code = code;
	}

	public Short getIsDisplay() {
		return this.isDisplay;
	}

	public void setIsDisplay(Short isDisplay) {
		this.isDisplay = isDisplay;
	}

}