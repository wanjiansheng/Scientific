package com.ins.domain;

import java.math.BigDecimal;


/**
 * SiOrderAppli entity. @author MyEclipse Persistence Tools
 */

public class SiOrderAppli  implements java.io.Serializable {


    // Fields    

     private Long id;
     private SiOrder siOrder;
     private String use;
     private String way;
     private String fundingSource;
     private String prinpical;
     private String pirnpicalTel;
     private BigDecimal fundingCode;
     private String time;
     private String projectName;


    // Constructors

    /** default constructor */
    public SiOrderAppli() {
    }

    
    /** full constructor */
    public SiOrderAppli(SiOrder siOrder, String use, String way, String fundingSource, String prinpical, String pirnpicalTel, BigDecimal fundingCode, String time, String projectName) {
        this.siOrder = siOrder;
        this.use = use;
        this.way = way;
        this.fundingSource = fundingSource;
        this.prinpical = prinpical;
        this.pirnpicalTel = pirnpicalTel;
        this.fundingCode = fundingCode;
        this.time = time;
        this.projectName = projectName;
    }

   
    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public SiOrder getSiOrder() {
        return this.siOrder;
    }
    
    public void setSiOrder(SiOrder siOrder) {
        this.siOrder = siOrder;
    }

    public String getUse() {
        return this.use;
    }
    
    public void setUse(String use) {
        this.use = use;
    }

    public String getWay() {
        return this.way;
    }
    
    public void setWay(String way) {
        this.way = way;
    }

    public String getFundingSource() {
        return this.fundingSource;
    }
    
    public void setFundingSource(String fundingSource) {
        this.fundingSource = fundingSource;
    }

    public String getPrinpical() {
        return this.prinpical;
    }
    
    public void setPrinpical(String prinpical) {
        this.prinpical = prinpical;
    }

    public String getPirnpicalTel() {
        return this.pirnpicalTel;
    }
    
    public void setPirnpicalTel(String pirnpicalTel) {
        this.pirnpicalTel = pirnpicalTel;
    }

    public BigDecimal getFundingCode() {
        return this.fundingCode;
    }
    
    public void setFundingCode(BigDecimal fundingCode) {
        this.fundingCode = fundingCode;
    }

    public String getTime() {
        return this.time;
    }
    
    public void setTime(String time) {
        this.time = time;
    }

    public String getProjectName() {
        return this.projectName;
    }
    
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
   








}