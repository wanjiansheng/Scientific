package com.ins.domain;

/**
 * SiOrderIteam entity. @author MyEclipse Persistence Tools
 */

public class SiOrderIteam  implements java.io.Serializable {


    // Fields    

     private Long id;
     private SiOrder siOrder;
     private SiInsInfo siInsInfo;
     private Long number;
     private Double price;
     private Long serviceId;


    // Constructors

    /** default constructor */
    public SiOrderIteam() {
    }

    
    /** full constructor */
    public SiOrderIteam(SiOrder siOrder, SiInsInfo siInsInfo, Long number, Double price, Long serviceId) {
        this.siOrder = siOrder;
        this.siInsInfo = siInsInfo;
        this.number = number;
        this.price = price;
        this.serviceId = serviceId;
    }

   
    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public SiOrder getSiOrder() {
        return this.siOrder;
    }
    
    public void setSiOrder(SiOrder siOrder) {
        this.siOrder = siOrder;
    }

    public SiInsInfo getSiInsInfo() {
        return this.siInsInfo;
    }
    
    public void setSiInsInfo(SiInsInfo siInsInfo) {
        this.siInsInfo = siInsInfo;
    }

    public Long getNumber() {
        return this.number;
    }
    
    public void setNumber(Long number) {
        this.number = number;
    }

    public Double getPrice() {
        return this.price;
    }
    
    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getServiceId() {
        return this.serviceId;
    }
    
    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }
   








}