package action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PayBackpost extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String responseMsg = request.getParameter("response");
		String customerId = request.getParameter("customerIdentification");
		Map<String, String> result = YeepayService.callback(responseMsg);
		Map map = new HashMap();
		map.put("parentMerchantNo", result.get("parentMerchantNo"));
		map.put("merchantNo", result.get("merchantNo"));
		map.put("orderId", result.get("orderId"));
		map.put("uniqueOrderNo", result.get("uniqueOrderNo"));
		map.put("status", result.get("status"));
		map.put("orderAmount", result.get("orderAmount"));
		map.put("payAmount", result.get("payAmount"));
		map.put("requestDate", result.get("requestDate"));
		map.put("paySuccessDate", result.get("paySuccessDate"));
		map.put("platformType", result.get("platformType"));
		//StudentHttpClient.send("http://localhost/fon/payportosecond", map, "utf-8");
		StudentHttpClient.send("http://222.76.204.85:20501/SrUnit/fon/payportosecond", map, "utf-8");
	}

}
