package action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TakeTokenServlet
 */
public class TakeTokenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public TakeTokenServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		//获取父编号-配置文件
		String parentMerchantNo = YeepayService.getParentMerchantNo();
		//获取子编号-配置文件
		String merchantNo = YeepayService.getMerchantNo();
		//获取订单号-页面动态获取
		String orderId = request.getParameter("orderId");
		//获取订单金额-页面动态获取
		String orderAmount = request.getParameter("orderAmount");
		String timeoutExpress = request.getParameter("timeoutExpress");
		//获取请求时间-页面动态获取
		String requestDate = request.getParameter("requestDate");
		//获取页面回调地址 - 页面写死
		String redirectUrl = request.getParameter("redirectUrl");
		//获取服务器回调地址 - 页面写死
		String notifyUrl = request.getParameter("notifyUrl");
		//获取商品名称 - 页面动态获取
		String goodsName = request.getParameter("goodsName");
		String goodsDesc = request.getParameter("goodsDesc");
		String paymentParamExt = request.getParameter("paymentParamExt");
		String industryParamExt = request.getParameter("industryParamExt");
		String memo = request.getParameter("memo");
		String riskParamExt = request.getParameter("riskParamExt");
		String csUrl = request.getParameter("csUrl");
		//获取订单类型- 页面动态获取-两种数据-如下
		//<option value="DELAY_SETTLE">延迟结算</option>
		//<option value="REAL_TIME">实时订单</option>
		String fundProcessType=request.getParameter("fundProcessType");
		String goodsParamExt = "{\"goodsName\":\""+goodsName+"\",\"goodsDesc\":\""+goodsDesc+"\"}";
		
		Map<String, String> params = new HashMap<>();
		params.put("parentMerchantNo", parentMerchantNo);
		params.put("merchantNo", merchantNo);
		params.put("orderId", orderId);
		params.put("orderAmount", orderAmount);
		params.put("timeoutExpress", timeoutExpress);
		params.put("requestDate", requestDate);
		params.put("redirectUrl", redirectUrl);
		params.put("notifyUrl", notifyUrl);
		params.put("goodsParamExt", goodsParamExt);
		params.put("paymentParamExt", paymentParamExt);
		params.put("industryParamExt", industryParamExt);
		params.put("memo", memo);
		params.put("riskParamExt", riskParamExt);
		params.put("csUrl", csUrl);
		params.put("fundProcessType", fundProcessType);
		
		//从配置文件获取【yop接口应用URI地址】链接地址-传递参数：tradeOrderURI
		//获取的配置数据：/rest/v1.0/sys/trade/order
		
		String uri = YeepayService.getUrl(YeepayService.TRADEORDER_URL);
		
		//获取结果集，访问：
		//请求YOP接口---其他接口使用
		//params 		请求参数,parentMerchantNo除外
		//uri 			请求yop的应用URI地址
		//YeepayService.TRADEORDER 			接口参数
		//YeepayService.TRADEORDER_HMAC 	验签顺序
		  
		Map<String, String> result = YeepayService.requestYOP(params, uri, YeepayService.TRADEORDER, YeepayService.TRADEORDER_HMAC);
		request.setAttribute("result", result);
		RequestDispatcher view	= request.getRequestDispatcher("jsp/3.1takeTokenResponse.jsp");
		view.forward(request, response);
	}

}
