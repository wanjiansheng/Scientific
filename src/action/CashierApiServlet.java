package action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CashierApiServlet
 */
public class CashierApiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CashierApiServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		//获取父编号-配置文件
		String parentMerchantNo = YeepayService.getParentMerchantNo();
		//获取子编号-配置文件
		String merchantNo = YeepayService.getMerchantNo();
		//获取token-页面动态获取
		String token = request.getParameter("token");
		//获取订单时间戳 - 页面动态获取
		String timestamp = request.getParameter("timestamp");
		
		String directPayType = request.getParameter("directPayType");
		String cardType = request.getParameter("cardType");
		
		//用户标识 - 页面动态获取
		String userNo = request.getParameter("userNo");
		//用户标识类型 - 页面动态获取
		String userType = request.getParameter("userType");
		
		String appId = request.getParameter("appId");
		String openId = request.getParameter("openId");
		String clientId = request.getParameter("clientId");
		//拼接 公众号ID + 用户ID + 对公银行客户号
		String ext = "{\"appId\":\""+appId+"\",\"openId\":\""+openId+"\",\"clientId\":\""+clientId+"\"}";

		//封装参数
		Map<String,String> params = new HashMap<String,String>();
		params.put("parentMerchantNo", parentMerchantNo);
		params.put("merchantNo", merchantNo);
		params.put("token", token);
		params.put("timestamp", timestamp);
		params.put("directPayType", directPayType);
		params.put("cardType", cardType);
		params.put("userNo", userNo);
		params.put("userType", userType);
		params.put("ext", ext);
		
		//拼接支付链接
		String url = YeepayService.getUrl(params);
		System.out.println(url);
		request.setAttribute("url", url);
		RequestDispatcher view	= request.getRequestDispatcher("jsp/3.1sendURL.jsp");
		view.forward(request, response);
	}

}
